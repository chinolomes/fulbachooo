SOURCES: 
- https://www.namecheap.com/support/knowledgebase/article.aspx/794/67/how-do-i-activate-an-ssl-certificate?utm_source=triggered-email&utm_medium=mail&utm_campaign=renewal_3&utm_content=renewal_activate
- https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&id=SO6506&actp=search&viewlocale=en_US&searchid=1270237704682
- https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&actp=CROSSLINK&id=SO17540
- 

in case you need, use 'sudo su' to use super privileges.

------------------------
A) Buy a certificate on name cheap

------------------------
B) Log in into the server, 
ssh -p 31125 federodes@188.166.117.135

Then, cd to /etc/nginx and mkdir ssl folder to store the certificates. (USE A NEW ssl[i] folder for each ssl renewal)

Generate a server.csr and server.key as follows in order to certify the domain name:
This document provides instructions for generating a Certificate Signing Request (CSR) for Nginx Server. If you are unable to use these instructions for your server, RapidSSL recommends that you contact a compnay that supports Nginx.

NOTE: To generate a CSR, you will need to create a key pair for your server. These two items are a digital certificate key pair and cannot be separated. If you lose your public/private key file or your password and generate a new one, your SSL Certificate will no longer match.

NOTE: All certificates that will expire after October 2013 must have a 2048 bit key size.

To generate a CSR on Nginx, please do the following:

    1.    Login to your server via your terminal client (ssh). At the prompt, type:

            openssl req -new -newkey rsa:2048 -nodes -keyout server.key -out server.csr
    2.    This will begin the process of generating two files: the Private-Key file for the decryption
           of your SSL Certificate, 
           and a certificate signing request (CSR) file used to apply for your SSL Certificate. 
    3.    The command from Step 1 will prompt for the following X.509 attributes of the certificate

    Country Name (C): Use the two-letter code without punctuation for country, for example: US or CA.
    State or Province (S): Spell out the state completely; do not abbreviate the state or province name, for example: California.
    Locality or City (L): The Locality field is the city or town name, for example: Berkeley.
    Organization (O): If your company or department has an &, @, or any other symbol using the
    shift key in its name, you must spell out the symbol or omit it to enroll, for example:
    XY & Z Corporation would be XYZ Corportation or XY and Z Corportation.
    Organizational Unit (OU): This field is the name of the department or
    organization unit making the request.
    Common Name (CN): The Common Name is the Host + Domain Name.
    NOTE: RapidSSL certificates can only be used on Web servers using the Common Name
    specified during enrollment. For example, a certificate for the domain "domain.com" will
    receive a warning if accessing a site named www.domain.com or "secure.domain.com",
    because "www.domain.com" and "secure.domain.com" are different from "domain.com."
     

    4.    When promted, please do not enter your email address, challenge password or an optional
           company name when generating the CSR.Leave out this fields blank.
    5.    Your .csr file will then be created.
    6.    Proceed to Enrollment and paste the CSR in the enrollment form when required.

------------------------
c) Activate the certificate:
go to https://www.namecheap.com/support/knowledgebase/article.aspx/794/67/how-do-i-activate-an-ssl-certificate?utm_source=triggered-email&utm_medium=mail&utm_campaign=renewal_3&utm_content=renewal_activate, click on the 'activate button' and enter the recently generated certificate

Once you have a CSR code generated, login to your Namecheap account and start the activation:

    Hover your mouse over your account username in the upper left corner, then select Dashboard.

    dashboard
    Next, select Product List > SSL Certificates.

    sslactivation2.png
    Click the “Activate” button next to the certificate you wish to activate.

    sslactivation3.png
    Enter (or cut-and-paste) your CSR code and choose the web server type from the drop-down menu. Once done, click “Submit”. NOTE: In this step, if you are activating a multi-domain certificate, you need to specify the additional domains you wish to include in the issued certificate.

    sslactivation4.png
    Be sure that the information submitted in the CSR code is correct before proceeding.
    On the next page, select Email validation, HTTP-based validation or DNS-based validation as the domain control validation method from the dropdown list. NOTE: The domain control validation method selection is omitted during activation of OV and EV certificates from Symantec, GeoTrust and Thawte.

    sslactivation5.png

    For email validation, select an email address to which the approval email will be sent. For security reasons, the approval email can only be sent to a generic email associated with the domain name (e.g., webmaster@, postmaster@, hostmaster@, administrator@, admin@, etc.) or to the registered Whois email address.
    Please note that if you do not select an email address, certificate activation will fail with a “Purchase Error” status.

    For HTTP-based validation, you will need to upload a certain text file into the root directory of your website to have the certificate issued. You'll then be able to download the certificate file in your account after the order is submitted to the Certificate Authority for activation.

    For DNS-based validation, you must create a special CNAME record in the DNS records for your domain. This record will be also provided after the activation.

    You can find more details about validation methods here.
    Enter the administrative contact information. Depending on the certificate type or brand, you may be asked for different types of information. Certificates that require business validation, for example, will require the business' or company's information. Non-mandatory fields are shown with an “Optional” tag. Administrator's contact information must be submitted using latin characters (Aa-Zz) and digits (0-9) only.
    Once you have entered this information correctly, click "Next".

    sslactivation6.png
    Click "Confirm" to confirm the domain control validation method and submit the order.

    sslactivation7.png

Once done, you will be taken to the Certificate Management page where you can view the Order ID, Certificate Authority's order ID, and other details of the certificate.

sslactivation8.png

By selecting "See Details," you can review certificate-related information, re-send the approval email, download the necessary file for HTTP-based validation, select a different validation method (Comodo certificates only), and check the CNAME record.

sslactivation9.png

sslactivation10.png

sslactivation11.png

Confirmation emails are delivered to the approval address within 10 minutes of activation. You must confirm the issuance by clicking the link included in the approval email. The validated certificate will then be sent to the administrative email address selected during activation.

HTTP-based and CNAME-based validation may take longer, as the Certificate Authority will need to locate the file or record. Please make sure that the file is publicly accessible and that there is no firewall blocking the requests behind your server.

The validation systems used by Certificate Authorities perform automatic checks of the validation file/record over a certain period of time. If validation fails, double-check that the file/record data is correct and accessible and make any necessary edits. Once the information is correct and accessible, the file/record should be validated when the CA validation system runs its next check.

If you order an OV/EV certificate*, Certificate Authority will send you a list of documents required to verify your business, depending on the type of the certificate. To expedite the certificate issuance, also make sure that the Whois for your domain contains the correct contact information. Order processing by Certification Authority may take up to an hour. If you do not receive an approval email within the specified time frame or the certificate has not been issued after one hour, and you have confirmed that the file or record is publicly accessible, please contact our SSL Support Team via live chat or ticket system for further assistance.

------------------------
ACA

SOURCE: https://knowledge.rapidssl.com/support/ssl-certificate-support/index?page=content&actp=CROSSLINK&id=SO17664

D) Once you get the email from the certificate provider, log in into the server, go to /etc/nginx/ssl and use 'nano' to create/store the recently created certificates: SSL.crt intermediate.crt

This document provides installation instructions for Nginx server. If you are unable to use these instructions for your server, RapidSSL recommends that you contact the server vendor or the organization, which supports Nginx.

Step 1: Obtain the RapidSSL Certificate

    The RapidSSL certificate will be sent by email.
    Copy the certificate imbedded in the body of the email and paste it into a text file using Vi or Notepad.

    The text file should look like:

    -----BEGIN CERTIFICATE-----

    [encoded data]

    ------END CERTIFICATE-----

    Make sure there are 5 dashes to either side of the BEGIN CERTIFICATE and END CERTIFICATE and that no white spaces, extra line breaks or additional characters have been inadvertently added.

    NOTE: The certificate can be also downloaded from the RapidSSL User Portal by following the steps from this link.

    Please select X.509 as a certificate format and copy only the End Entity Certificate.
     
******
    To follow the naming convention for Nginx, rename the certificate filename with the .crt extension.
    For example: SSL.crt        
******

Step 2: Download the RapidSSL Intermediate CA Certificate

    Download the Intermediate CA certificate that corresponds to your SSL product.
    Copy and paste the file to a Notepad document.
    Save the file as intermediate.crt


Step 3: Concatenate the SSL and Intermediate CA Certificate

    You need to combine the SSL.crt file and the intermediate.crt into a single concatenated file
    To get a single concatenated file out of the Intermediate CA and the SSL Certificate run the following command:

    // cat intermediate.crt >> SSL.crt
    >> sudo su
    cat SSL.crt intermediate.crt > bundle.crt


/*
Step 4: Edit the Nginx virtual hosts file (---> modify etc/nginx/sites-avalable/fulbacho)

    Open your Nginx virtual host file for the website you are securing.

    NOTE:  If you need your site to be accessible through both secure (https) and non-secure (http) connections, you will need a server module for each type of connection.
     
    Make a copy of the existing non-secure server module and paste it below the original.

    Then add the lines in bold below:

    server {
    listen 443;

    ssl on;
    ssl_certificate /etc/ssl/your_SSL.crt; <-- ATTENTION with ;
    ssl_certificate_key /etc/ssl/your_domain_name.key;

    server_name your.domain.com;
    access_log /var/log/nginx/nginx.vhost.access.log;
    error_log /var/log/nginx/nginx.vhost.error.log;
    location / {
    root /home/www/public_html/your.domain.com/public/;
    index index.html;
    }
    }
     
    Adjust the file names to match your certificate files:

    ssl_certificate should be your concatenated file created in Step 3
    ssl_certificate_key should be the key file generated when you created the CSR.
     
    Restart Nginx. Run the following command to restart Nginx:

    sudo /etc/init.d/nginx restart
     
    You can verify the certificate installation using the RapidSSL Installation Checker 

(/etc/nginx/ssl)
(/usr/share/nginx/html$)
------------------------
E) reset server:

sudo nginx -t

sudo nginx -s reload

exit
*/

========================
Step 4:

Comodo, possitive SSL:
https://www.namecheap.com/support/knowledgebase/article.aspx/9419/0/nginx

---> modify etc/nginx/sites-avalable/fulbacho
========================
https://support.comodo.com/index.php?/Knowledgebase/Article/View/1091/19/certificate-installation--nginx


Prerequisites:

Concatenate the CAbundle and the certificate file which we sent you using the following command.                                                                  

> cat domain_com.crt domain_com.ca-bundle > ssl-bundle.crt


If you are Using GUI Text Editor (Ex: Notepad):

(i) To concatenate the certificate files into single bundle file, first open domainname.crt and domainname.ca-bundle files using any text editor.

(ii) Now copy all the content of domainname.crt and paste it on the top of domainname.ca-bundle file.

(iii) Now save the file name as ‘ssl-bundle.crt’.

 

Note: If you have not the received the 'ca-bundle' file in the ZIP that we sent you, you can download it from this article's attachments. (End of this page)

For our CCM "Comodo Certificate Manager" customers: since you receive multiple downloadable links you must make sure that you download the x.509 base64 encoded "Certificate Only" as well as the Root/Intermediate "Certificate only" files. You will be presented with .cer formatted files which you can change the file extension to .crt to complete the process above.

 
/* 
Installation:


1. Store the bundle in the appropriate nginx ssl folder

EX :

> mkdir -p /etc/nginx/ssl/example_com/

> mv ssl-bundle.crt /etc/nginx/ssl/example_com/


2. Store your private key in the appropriate nginx ssl folder,

EX :

> mv example_com.key /etc/nginx/ssl/example_com/
*/
 

3. Make sure your nginx config points to the right cert file and to the private key you generated earlier:

    server {
    listen 443;
    server_name domainname.com;
    ssl on;
    ssl_certificate /etc/ssl/certs/ssl-bundle.crt;
    ssl_certificate_key /etc/ssl/private/domainname.key;
    ssl_prefer_server_ciphers on;
    }

 

5. After making changes to your config file check the file for syntax errors before attempting to use it. The following command will check for errors:

> sudo nginx -t -c /etc/nginx/nginx.conf

 

6. Restart your server. Run the following command to do it:

> sudo /etc/init.d/nginx restart

7. Check fulbacho site, certificate should be working by now
 
/* 
7. To verify if your certificate is installed correctly, use our SSL Analyzer.

 

Example Virtual Host Configuration:

server {
    listen 80 default_server;
    listen [::]:80 default_server;

    # Redirect all HTTP requests to HTTPS with a 301 Moved Permanently response.
    return 301 https://$host$request_uri;
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    # certs sent to the client in SERVER HELLO are concatenated in ssl_certificate
    ssl_certificate /path/to/signed_cert_plus_root_plus_intermediates;
    ssl_certificate_key /path/to/private_key;
    ssl_session_timeout 1d;
    ssl_session_cache shared:SSL:50m;
    ssl_session_tickets off;

    # Diffie-Hellman parameter for DHE ciphersuites, recommended 2048 bits
    ssl_dhparam /path/to/dhparam.pem;

    # intermediate configuration.
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA:ECDHE-RSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-RSA-AES256-SHA:ECDHE-ECDSA-DES-CBC3-SHA:ECDHE-RSA-DES-CBC3-SHA:EDH-RSA-DES-CBC3-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:AES128-SHA256:AES256-SHA256:AES128-SHA:AES256-SHA:DES-CBC3-SHA:!DSS';
    ssl_prefer_server_ciphers on;

    # HSTS (ngx_http_headers_module is required) (15768000 seconds = 6 months)
    add_header Strict-Transport-Security max-age=15768000;

    # OCSP Stapling ---
    # fetch OCSP records from URL in ssl_certificate and cache them
    ssl_stapling on;
    ssl_stapling_verify on;

    ## verify chain of trust of OCSP response using Root CA and Intermediate certs
    ssl_trusted_certificate /path/to/root_CA_cert_plus_root_plus_intermediates;

    resolver <IP DNS resolver>;

    ....
}

 

* If you want a more pre-defined, secure configuration, please check Mozilla SSL Configuration Generator

 

Reference : http://nginx.org/en/docs/http/configuring_https_servers.html

 
Related Articles:

    CSR Generation: Using OpenSSL (Apache w/mod_ssl, NGINX, OS X)
    Enable OCSP Stapling on NGINX
*/
