1- create a new droplet (call it fulbachoSANDBOX) from ssh-ufw-ntp-swap-nginx-fail2ban-visudo-set snapshot
2- point droplet to psa-ilp.com DNS: remove the current A record by setting a new one. DON't update the existing A record!
3- set server to redirect 3000 traffic as follows: 

ssh -p 31125 user@ip_fulbachoSANDBOX  188.166.15.134
federodes/cd ..
home/cd ..

--------
create SSL self signed cert:
SOURCE: https://serversforhackers.com/self-signed-ssl-certificates

1) make sure you have OpenSSL installed
sudo apt-get install openssl

2) we need a place to put our certificates
sudo mkdir /etc/nginx/ssl

3) we can begin creating our certificate. First, we need a private key:
# Create a 2048 bit private key
sudo openssl genrsa -out "/etc/nginx/ssl/example.key" 2048

4) Then we can generate the Certificate Signing Request. This uses the Private Key generated before:

sudo openssl req -new -key "/etc/nginx/ssl/example.key" \
                 -out "/etc/nginx/ssl/example.csr"

This will ask you a series of question. Here are the questions along with what I happen to fill in:
Common Name (e.g. server FQDN or YOUR name) []:www.psa-ilp.com

5) So, we now have example.key and example.csr files created. Let's finish this up by creating the self-signed certificate.

sudo openssl x509 -req -days 365 -in "/etc/nginx/ssl/example.csr" \
                  -signkey "/etc/nginx/ssl/example.key"  \
                  -out "/etc/nginx/ssl/example.crt"


--------

sudo nano etc/nginx/sites-available/fulbacho

/*
server{
        listen 80;
        
        # this could be your IP address, a subdomain or a full domain
        server_name www.psa-ilp.com;
        access_log /var/log/nginx/app.dev.access.log;
        error_log /var/log/nginx/app.dev.error.log;
        location / {
                proxy_pass http://127.0.0.1:3000;
                proxy_http_version 1.1;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection 'upgrade';
                proxy_set_header X-Forwarded-For $remote_addr;
        }

}
*/
-------------
server_tokens off; # for security-by-obscurity: stop displaying nginx version

# this section is needed to proxy web-socket connections
map $http_upgrade $connection_upgrade {
    default upgrade;
    ''      close;
}

# redirect to www.
#server {
#    listen 443;
#    server_name psa-ilp.com;
#    return 301 $scheme://www.psa-ilp.com$request_uri;
#}

# HTTP
server {
    listen 80 default_server; # if this is not a default server, remove "default_server"
    listen [::]:80 default_server ipv6only=on;

    root /usr/share/nginx/html; # root is irrelevant
    index index.html index.htm; # this is also irrelevant

    server_name www.psa-ilp.com; # the domain on which we want to host the application. Since we set "default_server" previously, nginx will answer all hosts anyway.

    # redirect non-SSL to SSL
    location / {
        rewrite     ^ https://$server_name$request_uri? permanent;
    }
}

# redirect to www.
server {
    listen 443 ssl;
    server_name psa-ilp.com;
    ssl_certificate /etc/nginx/ssl/example.crt; # full path to SSL certificate $
    ssl_certificate_key /etc/nginx/ssl/example.key; # full path to SSL key
    return 301 https://www.psa-ilp.com$request_uri;
}

# HTTPS server
server {
    listen 443 ssl spdy; # we enable SPDY here
    server_name www.psa-ilp.com; # this domain must match Common Name (CN) in the SSL certificate

    root html; # irrelevant
    index index.html; # irrelevant

    ssl_certificate /etc/nginx/ssl/example.crt; # full path to SSL certificate and CA certificate concatenated together
    ssl_certificate_key /etc/nginx/ssl/example.key; # full path to SSL key

    # performance enhancement for SSL
    ssl_stapling on;
    ssl_session_cache shared:SSL:10m;
    ssl_session_timeout 5m;

    # safety enhancement to SSL: make sure we actually use a safe cipher
    ssl_prefer_server_ciphers on;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers 'ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-GCM-SHA384:kEDH+AESGCM:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA:ECDHE-ECDSA-AES128-SHA:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:ECDHE-ECDSA-AES256-SHA:DHE-RSA-AES128-SHA256:DHE-RSA-AES128-SHA:DHE-RSA-AES256-SHA256:DHE-DSS-AES256-SHA:AES128-GCM-SHA256:AES256-GCM-SHA384:ECDHE-RSA-RC4-SHA:ECDHE-ECDSA-RC4-SHA:RC4-SHA:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!3DES:!MD5:!PSK';

    # config to enable HSTS(HTTP Strict Transport Security) https://developer.mozilla.org/en-US/docs/Security/HTTP_Strict_Transport_Security
    # to avoid ssl stripping https://en.wikipedia.org/wiki/SSL_stripping#SSL_stripping
    add_header Strict-Transport-Security "max-age=31536000;";

    # If your application is not compatible with IE <= 10, this will redirect visitors to a page advising a browser update
    # This works because IE 11 does not present itself as MSIE anymore
    if ($http_user_agent ~ "MSIE" ) {
        return 303 https://browser-update.org/update.html;
    }

    # pass all requests to Meteor
    location / {
        proxy_pass http://127.0.0.1:3000;
        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade; # allow websockets
        proxy_set_header Connection $connection_upgrade;
        proxy_set_header X-Forwarded-For $remote_addr; # preserve client IP

        # this setting allows the browser to cache the application in a way compatible with Meteor
        # on every applicaiton update the name of CSS and JS file is different, so they can be cache infinitely (here: 30 days)
        # the root path (/) MUST NOT be cached
        if ($uri != '/') {
            expires 30d;
        }
    }
}
-------------

cd etc/nginx/sites-enabled

sudo rm default

sudo ln -s /etc/nginx/sites-available/fulbacho /etc/nginx/sites-enabled/fulbacho

sudo nginx -t

sudo nginx -s reload

exit

check out ip_fulbachoSANDBOX on browser (should show nginx. Not app deployed yet).

4- create sandbox DB and make a fresh copy of the current fulbacho DB
5- set DB oplogger user
6- consider the last version of the app stored in the SANDBOX folder
7-BIS- remove 'dump' folder if any
7- set mup.json: server host-ip + DNS + DB
8- set settings.json --> kaddira SANDBOX, SIKKA, facebbok, and mailgun
9- set server/lib/config.js --> to setup facebook login and mailgun for psa-ilp.com (must match with settings.json)
10- from command line: go to meteor/SANDBOX/last-version and mupx setup + mupx deploy the previous version to the new droplet  

/*in case of '502 Bad Gateway' message:
	- go to sandbox server
	- restart nginx: sudo service nginx restart*/

/**/

11- copy the last version of the app (the one that will be deployed) from the PRODUCTION folder to the SANDBOX folder
12- set mup.json:
	a- replace server ip
	b- replace domain name
	c- set DB sandbox 
13- set settings.json --> kaddira SANDBOX, SIKKA, facebbok, and mailgun
14- set server/lib/config.js --> to setup facebook login and mailgun for psa-ilp.com (must match with settings.json)
15- from ../meteor/SANDBOX/NEW-VERSION mupx deploy
16- check that everything works properly. If yes, go to ./meteor/PRODUCTION/README.txt
17- check browser policy (remove this point after check)


===========================
When I restart the nginx service in command line on an ubuntu server, the service crashes when a nginx config file has errors. On a multi-site server this puts down all the sites, even the ones without config errors.

To prevent this, I run the nginx config test first:

nginx -t

after the test ran successful, I can restart the service

/etc/init.d/nginx restart

or only reload the nignx site configs without a restart

nginx -s reload

Is there a way to combine those two commands where the restart command is conditional to the config test's result? 
===========================
 
