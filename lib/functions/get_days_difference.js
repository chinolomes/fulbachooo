// Expected firstDate > secondDate. If not, the result will be negative!
getDaysDifference = function(firstDate, secondDate) {
   var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
   return Math.ceil((firstDate.getTime() - secondDate.getTime())/(oneDay));
}
