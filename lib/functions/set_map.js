setMap = function(elemId, centerLat, centerLng, zoom) {

	var mapCenter = new google.maps.LatLng(parseFloat(centerLat, 10), parseFloat(centerLng, 10));
	var mapOptions = {
			zoom: zoom,
			center: mapCenter,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			disableDefaultUI: true,
			mapTypeControl: true,
		   mapTypeControlOptions: {
		      style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
		      position: google.maps.ControlPosition.RIGHT_TOP
		   },
		   zoomControl: true,
		   zoomControlOptions: {
		      style: google.maps.ZoomControlStyle.SMALL,
		      position: google.maps.ControlPosition.LEFT_TOP
		   }
	};

	return new google.maps.Map(document.getElementById(elemId), mapOptions);
}
