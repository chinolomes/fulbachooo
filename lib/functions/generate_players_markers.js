generatePlayersMarkers = function(users) {

   // users[i] = {user._id, user.geo.loc.coordinates}
   var markers = [];
   var userOk = false;
   var lat = 0.0;
   var lng = 0.0;
   var fields = null;
   var marker = null;

   _.each(users, function (user) {
      userOk = !_.isUndefined(user) && !_.isUndefined(user.geo) && !_.isUndefined(user.geo.loc) && !_.isUndefined(user.geo.loc.coordinates);
      if (userOk) {
         lat = parseFloat(user.geo.loc.coordinates[1], 10);
         lng = parseFloat(user.geo.loc.coordinates[0], 10);
         fields = {
            position: new google.maps.LatLng(lat, lng),
            icon: '/markers/marker50red.png'
         };
         marker = new google.maps.Marker(fields);
         markers.push(marker);
      }
	});

	return markers;
}
