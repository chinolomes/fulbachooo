nextWeek = function(date) {

   console.log('original date: ', date);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Next Week';
   var nextWeek = date; // original date

   ////////////
   // CHECKS //
   ////////////

   if (!date || !_.isDate(date)) {
      throwError(context, 'date is not set');
      return;
   }

   ///////////
   // LOGIC //
   ///////////

   // Original date + 7 days
   nextWeek.setDate(nextWeek.getDate() + 7);
   console.log('nextWeek: ', nextWeek);
   return nextWeek;

}
