getCityName = function(lat, lng) {

   var activityCenter = {latitude: lat, longitude: lng};
   var enschede = {latitude: 52.22129386642608, longitude: 6.905364962294698};
   var corrientes = {latitude: -27.48567839947519, longitude: -58.83112192153931};
   var bsas = {latitude: -34.735267, longitude: -58.5321441};

   // Return true if the distance to Enschede is less than 50 km
   var city = 'other';
   if (geolib.getDistance(activityCenter, enschede) <= 50000) { // in meters
      city = 'enschede';
   } else if (geolib.getDistance(activityCenter, corrientes) <= 50000) { // in meters
      city = 'corrientes';
   } else if (geolib.getDistance(activityCenter, bsas) <= 50000) { // in meters
      city = 'bsas';
   }
   return city;
}
