placePlayersMarkers = function(map, markers) {

   _.each(markers, function (marker) {
		marker.setMap(map);
	});

	return map;
}
