setMomentLang = function(lang) {
	/*moment.locale('es', {
		months : "Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre".split("_"),
		weekdays : "Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sábado".split("_"),
		weekdaysShort : "Dom_Lun_Mar_Mie_Jue_Vie_Sab".split("_"),
		weekdaysMin : "Do_Lu_Ma_Mi_Ju_Vi_Sa".split("_")
	});

	moment.locale('de', {
		months : "Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
		weekdays : "Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),
		weekdaysShort : "Son_Mon_Die_Mit_Don_Fre_Sam".split("_"),
		weekdaysMin : "So_Mo_Di_Mi_Do_Fr_Sa".split("_")
	});

	moment.locale('nl', {
		months : "Januari_Februari_Maart_April_Mei_Juni_Juli_Augustus_September_Oktober_November_December".split("_"),
		weekdays : "Zondag_Maandag_Dinsdag_Woensdag_Donderdag_Vrijdag_Zaterdag".split("_"),
		weekdaysShort : "Zon_Maa_Din_Woe_Don_Vri_Zat".split("_"),
		weekdaysMin : "Zo_Ma_Di_Wo_Do_Vr_Za".split("_")
	});*/

	switch (lang) {
      case 'es':
		   moment.locale('es'); // change the global locale to Spanish
         break;
	   case 'en':
		   moment.locale('en'); // change the global locale to English
         break;
	   case 'nl':
		   moment.locale('nl'); // change the global locale to Dutch
         break;
	   case 'de':
		   moment.locale('de'); // change the global locale to German
         break;
	   case 'it':
		   moment.locale('it'); // change the global locale to Italian
         break;
	}
}
