getPeopleMissing = function(match, team) {

   if (!_.isUndefined(team) && (team === 'A' || 'B')) {
      var peopleInTeamA = [];
      var peopleInTeamB = [];
      var friendsOfTeamA = [];
      var friendsOfTeamB = [];
      var teamA = [];
      var teamB = [];

      _.each(match.peopleIn, function(player) {
         if (player.team === 'A') {
            peopleInTeamA.push(player);
            teamA.push(player);
         } else {
            peopleInTeamB.push(player);
            teamB.push(player);
         }
      });

      _.each(match.friendsOf, function(player) {
         if (player.team === 'A') {
            friendsOfTeamA.push(player);
            teamA.push(player);
         } else {
            friendsOfTeamB.push(player);
            teamB.push(player);
         }
      });

      var res = match.fieldSize - (team === 'A' ? teamA.length : teamB.length);
      if (!_.isNumber(res) || _.isNaN(res)) {
         throw new Meteor.Error('getPeopleMissing: result is not a number!');
         return;
      }
      return res;
   }

   var res = 2 * match.fieldSize - match.peopleIn.length - match.friendsOf.length;
   if (!_.isNumber(res) || _.isNaN(res)) {
      throw new Meteor.Error('getPeopleMissing: result is not a number!');
      return;
   }
   return res;
}
