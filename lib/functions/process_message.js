// msgArray => message: {status: "success/error/warning", text: "String", title: "String"} or message-array [message, message, ...]
processMessage = function(msgArray) {

	if (_.isArray(msgArray)) {
		_.each(msgArray, function(msg) {
			if (!_.isUndefined(msg.text) && _.isString(msg.text)) {
				if (!_.isUndefined(msg.title) && _.isString(msg.title)) {
					toastr[msg.status](TAPi18n.__(msg.text), TAPi18n.__(msg.title));
				} else {
					toastr[msg.status](TAPi18n.__(msg.text));
				}
			}
		});
	} else {
		if (!_.isNull(msgArray) && !_.isUndefined(msgArray)) {
			if (!_.isUndefined(msgArray.text) && _.isString(msgArray.text)) {
				if (!_.isUndefined(msgArray.title) && _.isString(msgArray.title)) {
					toastr[msgArray.status](TAPi18n.__(msgArray.text), TAPi18n.__(msgArray.title));
				} else {
					toastr[msgArray.status](TAPi18n.__(msgArray.text));
				}
			}
		}
	}
}


/*
processMessage = function(msgArray) {

	if (_.isArray(msgArray)) {
		_.each(msgArray, function(msg) {
			if (!_.isUndefined(msg.text) && _.isString(msg.text)) {
				toastr[msg.status](TAPi18n.__(msg.text));
			}
		});
	} else {
		if (!_.isNull(msgArray) && !_.isUndefined(msgArray) && !_.isUndefined(msgArray.text) && _.isString(msgArray.text)) {
			toastr[msgArray.status](TAPi18n.__(msgArray.text));
		}
	}
}
*/
