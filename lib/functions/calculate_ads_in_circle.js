calculateAdsInCircle = function(ads, loc) {

   // Internal vars
   //console.log('CALCULATE ADS IN CIRCLE: ', ads.length);
   var lat = parseFloat(loc.lat, 10);
   var lng = parseFloat(loc.lng, 10);
   var radius = 100000; // in meters!
   var counter = 0; // total number of users in circle
   var center = new google.maps.LatLng(lat, lng);
   var distance;
	var selected = [];
	var markerPosition;
   var userOk = false;

	_.each(ads, function(ad) {
      lat = parseFloat(ad.loc.coordinates[1], 10);
      lng = parseFloat(ad.loc.coordinates[0], 10);
      markerPosition = new google.maps.LatLng(lat, lng);
      // Calculate distance using google maps API
      distance = google.maps.geometry.spherical.computeDistanceBetween(center, markerPosition); // in meters!
      if (distance - radius < 0.0001) {
         counter++;
         selected.push(ad);
      }
   });
   //console.log('CALCULATE ADS IN CIRCLE AFTER FILTER: ', counter);

   return {selected: selected, total: counter};
}
