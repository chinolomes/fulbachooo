setGPSButton = function(map) {

	/**
	 * The GPSControl adds a control to the map that recenters the map on GPS user location.
	 * This constructor takes the control DIV as an argument.
	 * @constructor
	 */
	function GPSControl(controlDiv, map) {

	   // Set CSS for the control border
	   var controlUI = document.createElement('div');
	   controlUI.style.backgroundColor = 'white'; //'#3399FF'; //'#FFCC00';
	   controlUI.style.border = '1px solid #fff';
	   controlUI.style.borderRadius = '2px';
	   controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	   controlUI.style.cursor = 'pointer';
	   controlUI.style.marginTop = '6px';
		controlUI.style.marginBottom = '10px';
		controlUI.style.marginLeft = '7px';
		controlUI.style.paddingRight = '3px';
      controlUI.style.paddingLeft = '3px';
		controlUI.style.paddingTop = '0px';
		controlUI.style.paddingBottom = '0px';
	   controlUI.style.textAlign = 'center';
	   controlUI.title = TAPi18n.__('Locate_Me_Using_GPS');
	   controlDiv.appendChild(controlUI);

	   // Set CSS for the control interior
	   var controlText = document.createElement('div');
	   controlText.style.color = 'rgb(25,25,25)';
		controlText.style.fontWeight = '700';
	   controlText.innerHTML = '<span><i class="ion-android-locate"></i> GPS</span>';
	   controlUI.appendChild(controlText);

	   // Add GPS-button click event
	   google.maps.event.addDomListener(controlUI, 'click', function() {
		   function success(position) {
				// Reset map center and marker/circle location
				Session.set('mapCenterLatGPS', position.coords.latitude);
				Session.set('mapCenterLngGPS', position.coords.longitude);
				Session.set('resetMapGPS', true); // triggers a deps funtion in google_maps.js
			}

			function error() {
				alert(TAPi18n.__('Unable_To_Retrive_Your_Location'));
			}

			navigator.geolocation.getCurrentPosition(success, error);
		});

	}

   // Create the DIV to hold the control and call the CenterControl() constructor
   // passing in this DIV.
   var GPSControlDiv = document.createElement('div');
   var GPSControl = new GPSControl(GPSControlDiv, map);
   GPSControlDiv.index = 1;
   map.controls[google.maps.ControlPosition.TOP_LEFT].push(GPSControlDiv);

   return map;
}
