curUserIsOwnerOrAdmin = function(match) {

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Current User Is Owner or Admin';
   var curUserId = Meteor.userId();
   var isOwner = false;
   var isAdmin = false;

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      return false;
   }
   // Activity existance
   if (!match) {
      throwError(context, 'activity does not exist');
      return false; // msg
   }
   // Activity owner is defined
   if (!match.createdBy) {
      throwError(context, 'activity.owner is not defined or null');
		return false;
	}

   //////////////
   // RESPONSE //
   //////////////

   /*console.log("curUserId === match.createdBy: ", curUserId === match.createdBy);
   console.log("!_.isUndefined(match.admin):", !_.isUndefined(match.admin));
   console.log("match.admin.length > 0: ", match.admin.length > 0);
   console.log("curUserId === match.admin[0]: ", curUserId === match.admin[0]);*/
   //console.log("curUserId: ", curUserId);
   isOwner = curUserId === match.createdBy;
   isAdmin = !_.isUndefined(match.admin) && match.admin.length > 0 && curUserId === match.admin[0];
   return isOwner || isAdmin;

}
