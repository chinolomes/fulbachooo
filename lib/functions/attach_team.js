attachTeam = function(ids, team) {

   var player = null;
   var res = [];
   _.each(ids, function(userId) {
      player = {userId: userId, team: team};
      res.push(player);
   });
   return res;
}
