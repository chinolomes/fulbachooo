checkActivityExistanceAndStatus = function(activity, label) {
   if (!activity) {
	   throwError(label, 'activity does not exist');
      var msg = {status: 'error', text: 'The_Activity_Does_Not_Exist', title: 'Operation_Unsuccessful'};
      return {status: 'failed', msg: msg};

   } else if (_.isUndefined(activity.status)) {
      throwError(label, 'status is not defined');
      var msg = {status: 'error', text: 'Unexpected_Error', title: 'Operation_Unsuccessful'};
      return {status: 'failed', msg: msg};

   } else if (activity.status !== 'active') {
	   throwError(label, 'event is not active');
      var msg = {status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled', title: 'Operation_Unsuccessful'};
      return {status: 'failed', msg: msg};
   }
   return {status: 'success'};
}
