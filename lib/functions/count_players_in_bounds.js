countPlayersInBounds = function(users, bounds) {

   ///////////////
   // VARIABLES //
   ///////////////

   //console.log('COUNT USERS IN BOUNDS: ', users.lengh);
   var bottomLeft = bounds.bottomLeft; // [lng, lat]
   var topRight = bounds.topRight; // [lng, lat]
   var lat, lng;
   var counter = 0; // total number of users in region

   ///////////
   // LOGIC //
   ///////////

	_.each(users, function(user) {
      if (user && user.geo && user.geo.loc && user.geo.loc.coordinates) {
         lat = parseFloat(user.geo.loc.coordinates[1], 10);
         lng = parseFloat(user.geo.loc.coordinates[0], 10);
         if (bottomLeft[0] <= lng && topRight[0] >= lng && bottomLeft[1] <= lat && topRight[1] >= lat) {
            counter++;
         }
      }
   });
   return counter;
}
