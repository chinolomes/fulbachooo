setSaveRegionButton = function(map) {

	/**
	* The SaveChangesControl adds a control to the map for saving the new marker position.
	* This constructor takes the control DIV as an argument.
	* @constructor
	*/
	function SaveChangesControl(controlDiv, map, text, title) {

		// Set CSS for the control border
		var controlUI = document.createElement('div');
	   controlUI.style.backgroundColor = '#F00'; //'#3399FF'; //'#FFCC00';
	   controlUI.style.border = '1px solid #F36';
	   controlUI.style.borderRadius = '2px';
	   controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	   controlUI.style.cursor = 'pointer';
		controlUI.style.marginBottom = '16px';
		controlUI.style.marginLeft = '7px';
		controlUI.style.paddingRight = '10px';
      controlUI.style.paddingLeft = '10px';
		controlUI.style.paddingTop = '0px';
		controlUI.style.paddingBottom = '0px';
	   controlUI.style.textAlign = 'center';
		controlUI.title = title;
		controlDiv.appendChild(controlUI);

		// Set CSS for the control interior
	   var controlText = document.createElement('div');
	   controlText.style.color = 'white';
		controlText.style.fontWeight = '700';
		controlText.innerHTML = '<span style="font-size: 18px;"><i class="ion-earth"></i> ' + text + '</span>';
		controlUI.appendChild(controlText);

		// Add click event
		google.maps.event.addDomListener(controlUI, 'click', _.throttle(function() {
			if (Meteor.userId()) {
				console.log('set_save_region_button clicked!');
				Session.set('saveRegionBTN', true); // triggers autorun function at save_map_button.js
			} else {
            var msg = {status: 'warning', text: 'Login_To_Use_This_Feature'};
            processMessage(msg);
			}
		}, 1000));

	}

	// Create the DIV to hold the control and call the CenterControl() constructor passing in this DIV.
	var SaveChangesControlDiv = document.createElement('div');
	var btnLabel = TAPi18n.__('Save_Region');
	var btnTitle = TAPi18n.__('Make_It_My_Default_Region');
	var SaveChangesControl = new SaveChangesControl(SaveChangesControlDiv, map, btnLabel, btnTitle);
	SaveChangesControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(SaveChangesControlDiv);

   return map;
}
