getMapCenterBounds = function(bottomLeft, topRight) {

   if (_.isUndefined(bottomLeft) || _.isUndefined(topRight)) {
      throw new Meteor.Error('getMapCenterBounds, undefined input value(s)');
      return; // undefined
   }

   // Calculate map bounds center
   var lat = bottomLeft[1] + (topRight[1] - bottomLeft[1]) / 2;
   var lng = bottomLeft[0] + (topRight[0] - bottomLeft[0]) / 2;
   return {lat: lat, lng: lng}; // center
}
