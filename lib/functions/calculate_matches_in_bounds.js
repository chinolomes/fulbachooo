calculateMatchesInBounds = function(matches, bounds, limit) {

   // Internal vars
   //console.log('CALCULATE MATCHES IN CIRCLE: ', matches.length);
   var sorted = _.sortBy(matches, function(obj){return obj.date;});
   var bottomLeft = bounds.bottomLeft; // [lng, lat]
   var topRight = bounds.topRight; // [lng, lat]
   var lat, lng;
   var counter = 0; // total number of matches in circle
	var selectedMatches = [];
   var matchOk = false;

	_.each(sorted, function(match) {
      matchOk = !_.isUndefined(match) && !_.isUndefined(match.date) && !_.isUndefined(match.loc) && !_.isUndefined(match.loc.coordinates);
      if (matchOk) {
         lat = parseFloat(match.loc.coordinates[1], 10);
         lng = parseFloat(match.loc.coordinates[0], 10);

         if (bottomLeft[0] <= lng && topRight[0] >= lng && bottomLeft[1] <= lat && topRight[1] >= lat) {
            counter++;
            if (selectedMatches.length < limit) {
               selectedMatches.push(match);
            }
         }
      }
	});
   return {selected: selectedMatches, total: counter};
}
