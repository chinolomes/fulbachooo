saveCircleInSession = function(circle) {
   /*console.log('LAT: ' + circle.getCenter().lat());
   console.log('LNG: ' + circle.getCenter().lng());
   console.log('RAD: ' + circle.getRadius() / 1000);*/
	Session.set('circleCenterLatMATCHES', circle.getCenter().lat());
	Session.set('circleCenterLngMATCHES', circle.getCenter().lng());
	Session.set('circleRadiusMATCHES', circle.getRadius() / 1000); // from meters to kilometers
	console.log('SAVE CIRCLE, radius: ', Session.get('circleRadiusMATCHES'));
}
