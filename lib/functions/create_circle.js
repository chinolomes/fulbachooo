createCircle = function(map, radius, draggable) {

	var mapCenter = map.getCenter(); // is not wrapped
	var circleCenter = new google.maps.LatLng(mapCenter.lat(), mapCenter.lng());
	var circleOptions = {
		strokeColor: '#009999',
		strokeOpacity: 0.8,
		strokeWeight: 2,
		fillColor: '#009999',
		fillOpacity: 0.35,
		map: map,
		draggable: draggable,
		center: circleCenter,
		radius: radius * 1000  // from kilometers to meters
	};

	return new google.maps.Circle(circleOptions);
}
