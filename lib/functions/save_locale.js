saveLocale = function(lang) {
   if (Meteor.userId()) {
      Meteor.call('changeUserLang', lang, function(err) {if (err) {console.log(err);}});
   }
   Cookie.set('langFULBACHO', lang, {months: 1});
}
