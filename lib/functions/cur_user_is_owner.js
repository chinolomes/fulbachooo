curUserIsOwner = function(match) {

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Current User Is Owner';
   var curUserId = Meteor.userId();

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      return false;
   }
   // Activity existance
   if (!match) {
      throwError(context, 'activity does not exist');
      return false; // msg
   }
   // Activity owner is defined
   if (!match.createdBy) {
      throwError(context, 'activity.owner is not defined or null');
		return false;
	}

   //////////////
   // RESPONSE //
   //////////////

   // Current user is owner
   return curUserId === match.createdBy;

}
