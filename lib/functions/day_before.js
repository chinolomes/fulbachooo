dayBefore = function(date) {

   console.log('original date: ', date);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Day Before';
   var dayBefore = date; // original date

   ////////////
   // CHECKS //
   ////////////

   if (!date || !_.isDate(date)) {
      throwError(context, 'date is not set');
      return;
   }

   ///////////
   // LOGIC //
   ///////////

   // Original date - 1 day
   dayBefore.setDate(dayBefore.getDate() - 1);
   console.log('dayBefore: ', dayBefore);
   return dayBefore;

}
