saveBoundsInSession = function(newBounds) {

	// newBounds = {bottomLeft: [lng, lat], topRight: [lng, lat]}
	Session.set('bottomLeftMapBounds', newBounds.bottomLeft);
	Session.set('topRightMapBounds', newBounds.topRight);
	/*console.log('SAVE BOUNDS, bottomLeft: ', Session.get('bottomLeftMapBounds'));
	console.log('SAVE BOUNDS, topRight: ', Session.get('topRightMapBounds'));*/
}
