/*
DESCRIPTION: checks user.category: 'regular/pro'. In case the user is 'regular',
restrict the number of upcoming-created activities to 5.

REQUIRES: user.category + user.activities.upcoming
*/
checkUserCategory = function(user, label) {

   ///////////////
   // VARIABLES //
   ///////////////

   var msg = {};
   var upcoming = {};
   var count = 0;

   ////////////
   // CHECKS //
   ////////////

   if (!user) {
	   throwError(label, 'user does not exist');
      msg = {status: 'error', text: 'Unexpected_Error'};
      return {status: 'failed', msg: msg};
   }
   if (!user.category) {
      throwError(label, 'category is not defined');
      msg = {status: 'error', text: 'Unexpected_Error'};
      return {status: 'failed', msg: msg};
   }
   if (!label) {
      throwError(label, 'label is not set');
      msg = {status: 'error', text: 'Unexpected_Error'};
      return {status: 'failed', msg: msg};
   }

   ///////////
   // LOGIC //
   ///////////

   if (user.category === 'regular') {
      // Restrict the number of upcoming-created activities to 5
      upcoming = user.activities.upcoming;
		count = _.union(upcoming.private.created, upcoming.public.created).length;
      if (count >= 5) {
         msg = {status: 'error', text: 'Need_To_Be_PRO_To_Create_More_Than_5_Activities', title: 'Operation_Unsuccessful'};
         return {status: 'failed', msg: msg};
      }
   }
   return {status: 'success'};
}
