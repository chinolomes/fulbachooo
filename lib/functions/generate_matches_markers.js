generateMatchesMarkers = function(matches) {

   // matches[i] = {match._id, match.sport, match.loc.coordinates}
   var markers = [];
	var marker = null;

   _.each(matches, function (match) {
		marker = new google.maps.Marker({
                           position: new google.maps.LatLng(parseFloat(match.loc.coordinates[1], 10), parseFloat(match.loc.coordinates[0], 10)),
									icon: '/markers/marker50red.png'
									});

		// Set icon based on sport and color
		marker.setIcon(getMarkerIcon(match.sport, 'red'));

      // Attach click event listener to highlight marker
      marker.addListener('click', function() {
         window.location.hash = '#' + match._id; // scroll to current activity
         window.location.hash = '';
         Session.set('curMatch', match._id); // highlight current activity
      });

		// Set custom fields
		marker.sport = match.sport;
		marker.color = 'red';
      marker.matchId = match._id;

		markers.push(marker);
	});

	return markers;
}
