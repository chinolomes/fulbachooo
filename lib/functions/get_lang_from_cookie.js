getLangFromCookie = function() {

   /*console.log('_.isUndefined(Cookie): ', _.isUndefined(Cookie));
   console.log('_.isNull(Cookie): ', _.isNull(Cookie));
   console.log('_.isUndefined(Cookie.get(langFULBACHO)): ', _.isUndefined(Cookie.get('langFULBACHO')));
   console.log('_.isNull(Cookie.get(langFULBACHO)): ', _.isNull(Cookie.get('langFULBACHO')));*/

   if (_.isUndefined(Cookie) || _.isNull(Cookie) || _.isUndefined(Cookie.get('langFULBACHO')) || _.isNull(Cookie.get('langFULBACHO'))) {
      return 'en';
   }
   var langs = ['en', 'es', 'it', 'fr', 'ch', 'de', 'du', 'ar', 'nl'];
   var lang = Cookie.get('langFULBACHO');
   /*console.log('lang: ', lang);
   console.log('typeof(lang): ', typeof(lang));
   console.log('_.indexOf(langs, lang): ', _.indexOf(langs, lang));*/
   return _.indexOf(langs, lang) !== -1 ? lang : 'en';
}
