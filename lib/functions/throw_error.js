throwError = function(context, text) {

   if (!context || !text) {
      throw new Meteor.Error('throwError no context or text');
      return;
   }
   throw new Meteor.Error(context + ': ' + text);
}
