calculatePlayersInBounds = function(users, bounds, limit) {

   // Internal vars
   //console.log('CALCULATE USERS IN BOUNDS: ', users.length);
   //console.log('limit: ', limit);
   var sorted = _.sortBy(users, function(obj){return -obj.createdAt;}); // newest first
   var bottomLeft = bounds.bottomLeft; // [lng, lat]
   var topRight = bounds.topRight; // [lng, lat]
   var lat, lng;
   var counter = 0; // total number of users in circle
	var selectedUsers = [];
   var userOk = false;

	_.each(sorted, function(user) {
      userOk = !_.isUndefined(user) && !_.isUndefined(user.createdAt) && !_.isUndefined(user.geo) && !_.isUndefined(user.geo.loc) && !_.isUndefined(user.geo.loc.coordinates);
      if (userOk) {
         lat = parseFloat(user.geo.loc.coordinates[1], 10);
         lng = parseFloat(user.geo.loc.coordinates[0], 10);

         if (bottomLeft[0] <= lng && topRight[0] >= lng && bottomLeft[1] <= lat && topRight[1] >= lat) {
            counter++;
            if (selectedUsers.length < limit) {
               selectedUsers.push(user);
            }
         }
      }
   })
   return {selected: selectedUsers, total: counter};
}
