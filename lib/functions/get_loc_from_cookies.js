getLocFromCookies = function() {

   var defaultLoc = {lat: DEFAULT_LAT, lng: DEFAULT_LNG};
   if (_.isUndefined(Cookie) || _.isNull(Cookie) || _.isUndefined(Cookie.get('latFULBACHO')) || _.isNull(Cookie.get('latFULBACHO')) || _.isUndefined(Cookie.get('lngFULBACHO')) || _.isNull(Cookie.get('lngFULBACHO'))) {
      return defaultLoc;
   }
   var lat = parseFloat(Cookie.get('latFULBACHO'), 10);
   var lng = parseFloat(Cookie.get('lngFULBACHO'), 10);
   if (lat < -180 || lat > 180 || lng < -90 || lng > 90) {
      return defaultLoc;
   }
   return {lat: lat, lng: lng};
}
