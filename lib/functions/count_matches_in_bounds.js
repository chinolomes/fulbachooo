countMatchesInBounds = function(matches, bounds, limit) {

   ///////////////
   // VARIABLES //
   ///////////////

   //console.log('CALCULATE MATCHES IN CIRCLE: ', matches.length);
   var bottomLeft = bounds.bottomLeft; // [lng, lat]
   var topRight = bounds.topRight; // [lng, lat]
   var lat, lng;
   var counter = 0; // total number of activities in region

   ///////////
   // LOGIC //
   ///////////

	_.each(matches, function(match) {
      if (match && match.loc && match.loc.coordinates) {
         lat = parseFloat(match.loc.coordinates[1], 10);
         lng = parseFloat(match.loc.coordinates[0], 10);
         if (bottomLeft[0] <= lng && topRight[0] >= lng && bottomLeft[1] <= lat && topRight[1] >= lat) {
            counter++;
         }
      }
	});
   return counter;
}
