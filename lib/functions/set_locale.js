// TODO: pass onSuccess function as second arg
setLocale = function(lang) {
   TAPi18n.setLanguage(lang)
   .done(function() {moment().locale(lang);})
   .fail(function (err) {console.log(err);});
}
