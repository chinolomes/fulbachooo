createMarker = function(lat, lng, map, sport, draggable) {

   var position = new google.maps.LatLng(parseFloat(lat, 10), parseFloat(lng, 10));
	var marker = new google.maps.Marker({
					position: position,
					map: map,
					draggable: draggable,
               icon: '/markers/' + DEFAULT_SPORT + '50red.png'
				});

   // Set sport icon
   var icon = getMarkerIcon(sport, 'red');
   marker.setIcon(icon);

	return marker;
}
