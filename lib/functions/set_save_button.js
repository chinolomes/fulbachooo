setSaveButton = function(map) {

	/**
	* The SaveChangesControl adds a control to the map for saving the new marker position.
	* This constructor takes the control DIV as an argument.
	* @constructor
	*/
	function SaveChangesControl(controlDiv, map, text, title) {

		// Set CSS for the control border
		var controlUI = document.createElement('div');
	   controlUI.style.backgroundColor = '#F00'; //'#3399FF'; //'#FFCC00';
	   controlUI.style.border = '1px solid #F36';
	   controlUI.style.borderRadius = '2px';
	   controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
	   controlUI.style.cursor = 'pointer';
		controlUI.style.marginBottom = '16px';
		controlUI.style.marginLeft = '7px';
		controlUI.style.paddingRight = '10px';
      controlUI.style.paddingLeft = '10px';
		controlUI.style.paddingTop = '0px';
		controlUI.style.paddingBottom = '0px';
	   controlUI.style.textAlign = 'center';
		controlUI.title = title;
		controlDiv.appendChild(controlUI);

		// Set CSS for the control interior
	   var controlText = document.createElement('div');
	   controlText.style.color = 'white';
		controlText.style.fontWeight = '700';
		controlText.innerHTML = '<span style="font-size: 18px;"><i class="ion-checkmark"></i> ' + text + '</span>';
		controlUI.appendChild(controlText);

		// Add GPS-button click event
		google.maps.event.addDomListener(controlUI, 'click', function() {
			Session.set('resetMapSAVE', true); // triggers autorun function
		});

	}

	// Create the DIV to hold the control and call the CenterControl() constructor passing in this DIV.
	var SaveChangesControlDiv = document.createElement('div');
	var btnLabel = TAPi18n.__('Save');
	var btnTitle = TAPi18n.__('Save_Changes');
	var SaveChangesControl = new SaveChangesControl(SaveChangesControlDiv, map, btnLabel, btnTitle);
	SaveChangesControlDiv.index = 1;
	map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(SaveChangesControlDiv);

   return map;
}
