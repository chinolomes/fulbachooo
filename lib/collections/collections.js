// TODO: create an individual file for each collection. Add schema + deny rules
Matches = new Mongo.Collection("matches");
Emails = new Mongo.Collection("emails"); // emailNotifications
WelcomeEmails = new Mongo.Collection("welcomeEmails");
Notifications = new Mongo.Collection("notifications");
Posts = new Mongo.Collection("posts");
UnreadNotifications = new Mongo.Collection("unreadNotifications");
CollectionsCounter = new Mongo.Collection("collectionsCounter");
SpidersIPs = new Mongo.Collection("spidersIPs");
Ads = new Mongo.Collection("ads");

/*
SOURCE: https://themeteorchef.com/recipes/building-a-user-admin/
To save face, we can “lock down” all of our rules when we define our collection
to prevent any client-side database operations from taking place. This means
that when we interact with the database, we’re required to do it from the server
(a trusted environment) via methods.
*/
/*
SOURCE: http://docs.meteor.com/#/full/deny
When a client tries to write to a collection, the Meteor server first checks the
collection's deny rules. If none of them return true then it checks the
collection's allow rules. Meteor allows the write only if no deny rules return
true and at least one allow rule returns true.
*/
/* USERS */
Meteor.users.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* ACTIVITIES */
Matches.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* EMAIL NOTIFICATIONS */
Emails.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* WELCOME EMAILS */
WelcomeEmails.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* INTERNAL NOTIFICATIONS */
Notifications.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* POSTS */
Posts.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* UNSEEN NOTIFICATIONS */
UnreadNotifications.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* COLLECTIONS COUNTER */
CollectionsCounter.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* SPIDERS IPS */
SpidersIPs.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});

/* ADVERTISEMENTS */
Ads.deny({
   insert: function() {return true},
   update: function() {return true},
   remove: function() {return true}
});
