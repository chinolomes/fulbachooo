// TODO: rename --> {settings: {emailNotifications: {deliveryAddresses: {primary, ...}, options/values(?): []}, ...}
// Also options should be an array with allowed values
// TODO: add rating limit
//======================================================================
// SAVE EMAIL NOTIFICATIONS SETTINGS:
//======================================================================
Meteor.methods({'saveEmailNotifSettings': function(settings) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(settings, [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Save Email Notifications Settings';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var update = {
      $set: {
         'emailNotifSettings.gameNewGamePlayingZone': _.indexOf(settings, 'gameNewGamePlayingZone') !== -1,
         'emailNotifSettings.gameNewComment': _.indexOf(settings, 'gameNewComment') !== -1,
         'emailNotifSettings.gameWasCanceled': _.indexOf(settings, 'gameWasCanceled') !== -1,
         'emailNotifSettings.gameAddRemovePlayer': _.indexOf(settings, 'gameAddRemovePlayer') !== -1,
         'emailNotifSettings.gameAddedMe': _.indexOf(settings, 'gameAddedMe') !== -1,
         'emailNotifSettings.gamePassFromWaitingToPlaying': _.indexOf(settings, 'gamePassFromWaitingToPlaying') !== -1,
         'emailNotifSettings.gameNewGameRepeat': _.indexOf(settings, 'gameNewGameRepeat') !== -1
      }
   };

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   Meteor.users.update({_id: curUserId}, update);
   return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg

}});
