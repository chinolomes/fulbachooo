/*
DESCRIPTION: changes activity location to the given lat-lng coordinates

REQUIREMENTS: current user must be owner or admin
*/
// CHECKED
// TODO: add rating limit
//======================================================================
// EDIT ACTIVITY MAP:
//======================================================================
Meteor.methods({'editActivityMap': function(matchId, lat, lng) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);
   check([lat, lng], [Number]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Add Current User To Participants List';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var match = null;
   var result = {};

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "admin": 1, "status": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Current user is owner or admin
   if (!curUserIsOwnerOrAdmin(match)) {
   	throwError(context, 'the user is not the owner nor the admin of the activity');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   Matches.update({_id: matchId}, {$set: {'loc.coordinates': [lng, lat]}});
   return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg

}});
