
// TODO: return error in case some error occurs on DB-operations
// TODO: add rating limit
//======================================================================
// SAVE NEARBY REGION:
//======================================================================
Meteor.methods({'saveNearbyRegion': function(lat, lng, rad, zoom) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([lat, lng, rad, zoom], [Number]);
   /*console.log('lat: ', lat);
   console.log('lng: ', lng);*/

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Save Nearby Region';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var update = {
      $set: {
         'geo.loc.coordinates': [lng, lat],
         'geo.radius': rad,
         'geo.zoom': zoom
      }
   }

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   Meteor.users.update({_id: curUserId}, update);
   return {status: 'success', text: 'Region_Saved_Successfully_Explanation', title: 'Changes_Were_Made_Successfully'}; // msg

}});
