// CHECKED!
// TODO: add rating limit
//======================================================================
// EDIT POST:
//======================================================================
Meteor.methods({'editPost': function(postId, text) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([postId, text], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Edit Post';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var post = null;

   /////////////
   // QUERIES //
   /////////////

   post = Posts.findOne({_id: postId});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Post existence
   if (!post) {
      throwError(context, 'post does not exist');
      return unexpError; // msg
   }
	// Post author
	if (post.createdBy !== curUserId) {
		throwError(context, 'the user is not the owner');
		return unexpError;
	}
	// Post content
	if (text.length === 0 || text.length > 2000) {
		throwError(context, 'text length = 0 or > 2000');
		return;
	}

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	Posts.update({_id: postId}, {$set: {text: text}});

}});
