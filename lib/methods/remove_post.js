// CHECKED!
// TODO: add rating limit
//======================================================================
// REMOVE POST:
//======================================================================
Meteor.methods({'removePost': function(postId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

	check(postId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Remove Post';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var post = null;

   /////////////
   // QUERIES //
   /////////////

   post = Posts.findOne({_id: postId}, {fields: {"createdBy": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
	// Post existence
   if (!post) {
      throwError(context, 'post does not exist');
      return unexpError; // msg
   }
	// Post author
	if (curUserId !== post.createdBy) {
		throwError(context, 'the user is not the owner');
		return;
	}

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	Posts.remove(postId);

}});
