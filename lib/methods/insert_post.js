// CHECKED!
/*
TODO:
SOURCE: http://guide.meteor.com/ui-ux.html
Indicating when a write is in progress

Sometimes the user may be interested in knowing when the update has hit the
server. For instance, in a chat application, it’s a typical pattern to
optimistically display the message in the chat log, but indicate that it is
“pending” until the server has acknowledged the write. We can do this easily in
Meteor by simply modifying the Method to act differently on the client:

Messages.methods.insert = new Method({
  name: 'Messages.methods.insert',
  schema: new SimpleSchema({
    text: {type: String}
  }),
  run(message) {
    // In the simulation (on the client), we add an extra pending field.
    // It will be removed when the server comes back with the "true" data.
    if (this.isSimulation) {
      message.pending = true;
    }

    Messages.insert(message);
  }
})
*/
// TODO: add rating limit
//======================================================================
// INSERT POST:
//======================================================================
Meteor.methods({'insertPost': function(text, matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([matchId, text], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Insert Post';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var curUser = null;
   var match = null;
   var postId = '';
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameNewComment';
   var textNotif = 'User_Posted_Comment_In_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_User_Posted_Comment_In_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#comments';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {"profile.name": 1}});
   match = Matches.findOne({_id: matchId}, {fields: {"followers": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity existence (doesn't need to be active)
   if (!match) {
      throwError(context, 'activity does not exist');
      return unexpError; // msg
   }
	// Post content
	if (text.length === 0 || text.length > 2000) {
		throwError(context, 'text length = 0 or > 2000');
		return;
	}

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   postId = Posts.insert({createdBy: curUserId, postedOn: matchId, text: text});

   // Add current user to activity's list of followers, and add activityId to the list of activities being followed by the user
   if (_.indexOf(match.followers, curUserId) === -1) {
      Meteor.call('followUnfollowActivity', matchId);
   }

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Run server side only
   if (!this.isSimulation) {

      // Don't wait for the following task to be done before giving the client the green light to move ahead
      Meteor.defer(function () {

         // Set variables
         senderName = curUser.profile.name;
         userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

         // Deliver internal notifications
         _.each(userIdsToNotify, function(userId) {
            Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
         });

         // Notify users via email
         Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
      });
	}

	return postId;

}});
