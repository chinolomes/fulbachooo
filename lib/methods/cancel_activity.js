/*
Set activity.status to 'cancelled'
*/
// CHECKED: (owner cancelled) --> public and private with owner, admin, particpants, friends and waiting list
// (admin cancelled) --> public
// TODO: add rating limit
//======================================================================
// CANCEL ACTIVITY:
//======================================================================
Meteor.methods({'cancelActivity': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

	check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Cancel Activity';
   var curUserId = Meteor.userId(); // client and server side
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var match = null;
   var curUser = null;
	var result = {};
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameWasCanceled';
   var textNotif = 'User_Canceled_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_User_Canceled_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#data';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {'activities': 1, 'profile.name': 1}});
   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "admin": 1, "status": 1, "followers": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Current user is owner/admin
   if (!curUserIsOwnerOrAdmin(match)) {
   	throwError('Cancel Match: the user is not the owner nor the admin of the activity');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	// Update activity status
	Matches.update({_id: matchId}, {$set: {status: 'canceled'}});

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Run server side only
   if (!this.isSimulation) {

		// Don't wait for the following task to be done before giving the client the green light to move ahead
		Meteor.defer(function () {

			// Set variables
	      senderName = curUser.profile.name;
	      userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

	      // Internal notifications
	      _.each(userIdsToNotify, function(userId) {
	         Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
	      });

	      // Notify users via email
	      Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
		});
   }

   return {status: 'success', text: 'The_Activity_Was_Canceled', title: 'Operation_Successful'}; // msg

}});
