/*
DESCRIPTION: remove activity from DB and update user's record

REQUERIMENTS: activityId to be removed + current user must be owner
*/
// CHECKED: public and private with owner, admin, particpants, friends and waiting list
// TODO: add rating limit
//======================================================================
// DELETE ACTIVITY:
//======================================================================
Meteor.methods({'deleteActivity': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Delete Activity';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var curUser = null;
   var match = null;
   var admin = ''; // userId
   var type = ''; // upcoming / past
   var privacy = ''; // public / private
   var key = '';
   var pull = {
      created: {},
      admin: {},
      enrolled: {},
      waiting: {},
      friendIsEnrolled: {},
      following: {}
   };

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"status": 1, "createdBy": 1, "admin": 1, "peopleIn": 1, "friendsOf": 1, "waitingList": 1, "followers": 1, "privacy": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Current user is logged in
   if (!curUserId) {
		throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Checks match existence (status could be 'cancelled' or 'finished')
   if (!match) {
      throwError(context, 'match does not exist');
      return {status: 'error', text: 'The_Activity_Does_Not_Exist', title: 'Operation_Unsuccessful'}; // msg
   }
   // Current user is owner
   if (!curUserIsOwner(match)) {
   	throwError(context, 'the user is not the owner of the activity');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   admin = match.admin && match.admin[0]; // could be undefined or null
   type = match.status === 'finished' ? 'past' : 'upcoming';
   privacy = match.privacy;
   // initial pull = {created: {}, admin: {}, enrolled: {}, waiting: {}, friendIsEnrolled: {}, following: {}};
   for (key in pull) {
      pull[key]['activities.' + type + '.' + privacy + '.' + key] = matchId;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	// Remove activity
	Matches.remove({_id: matchId});

   // Remove activityId from owner doc
   Meteor.users.update({_id: match.createdBy}, {$pull: pull.created});

   // Remove activityId from admin doc
   if (admin) {
      Meteor.users.update({_id: admin}, {$pull: pull.admin});
   }
   // Remove activityId from participants doc
   Meteor.users.update({_id: {$in: getUserIds(match.peopleIn)}}, {$pull: pull.enrolled}, {multi: true});
   Meteor.users.update({_id: {$in: getUserIds(match.waitingList)}}, {$pull: pull.waiting}, {multi: true});
   Meteor.users.update({_id: {$in: getUserIds(match.friendsOf)}}, {$pull: pull.friendIsEnrolled}, {multi: true});
   Meteor.users.update({_id: {$in: match.followers}}, {$pull: pull.following}, {multi: true});

   return {status: 'success', text: 'The_Activity_Was_Deleted', title: 'Operation_Successful'};

}});
