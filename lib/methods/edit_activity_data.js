// CHECKED!
// TODO: add rating limit
//======================================================================
// EDIT ACTIVITY DATA:
//======================================================================
Meteor.methods({'editActivityData': function(matchId, date, time, duration, cost, address, fieldType, fieldPhone, fieldWebsite, title, description) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([matchId, time, duration, cost, address, fieldType, fieldPhone, fieldWebsite, title, description], [String]);
   check(date, Date);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Edit Activity Data';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var match = null;
   var result = {};
   var update = {
      $set: {
         date: date,
         time: time,
         duration: duration,
         cost: cost,
         address: address,
         fieldType: fieldType,
         fieldPhone: fieldPhone,
         fieldWebsite: fieldWebsite,
         title: title,
         description: description
      }
   };

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "status": 1, "admin": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Current user is owner or admin
   if (!curUserIsOwnerOrAdmin(match)) {
		throwError('current user is not the owner nor the admin of the activity');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	Matches.update({_id: matchId}, update);
	return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg

}});
