// CHECKED!!
// TODO: add rating limit
//======================================================================
// SAVE PROFILE:
//======================================================================
Meteor.methods({'saveProfile': function(sports, name, gender, leg, hand, text) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([name, gender, leg, hand, text], [String]);
   check(sports, [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Save Profile';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var update = {
      $set: {
         'profile.sports': sports,
         'profile.name': name,
         'profile.gender': gender,
         'profile.dominantLeg': leg,
         'profile.dominantHand': hand,
         'profile.selfDescription': text
      }
   };

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	Meteor.users.update({_id: curUserId}, update);
	return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg

}});
