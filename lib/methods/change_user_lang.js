
// TODO: add rating limit
//======================================================================
// CHANGE USER LANGUAGE:
//======================================================================
Meteor.methods({'changeUserLang': function(lang) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(lang, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Change User Language';
   var curUserId = Meteor.userId(); // client and server side
   var unexpError = {status: 'error', text: 'Unexpected_Error'};

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   Meteor.users.update({_id: curUserId}, {$set: {'profile.lang.value': lang}});

}});
