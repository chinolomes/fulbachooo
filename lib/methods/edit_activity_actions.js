/*
DESCRIPTION: reset activity repetition and admin

REQUERIMENTS: current user must be the activity's owner
*/
// TODO: re-write methods in a more general way for a general number of admins instead of one:
// determine new and previous admins in a more general way (see manage participants)
// use 'admins' array instead of 'admin' (array upto 1 element).
// CHECKED!
// TODO: add rating limit
//======================================================================
// EDIT ACTIVITY ACTIONS:
//======================================================================
Meteor.methods({'editActivityActions': function(matchId, admin, repeat) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);
   check(admin, [String]);
   check(repeat, Boolean);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Edit Activity Actions';
   var curUserId = Meteor.userId(); // client and server side
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var match = null;
   var result = {};
   var prevAdmin = ''; // userId
   var newAdmin =''; // userId
   var modifier = {}; // DB modifier
   //var senderName = '';
   //var userIdsToNotify = [];
   //var notifType = '';
   var textNotif = 'User_Named_You_Admin';
   //var textEmail = '';
   var anchor = '/activities/activity/' + matchId + '#data';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "status": 1, "admin": 1, "privacy": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Current user is owner
   if (!curUserIsOwner(match)) {
   	throwError(context, 'the user is not the owner of the activity');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   prevAdmin = match.admin && match.admin.length > 0 ? match.admin[0] : '';
   newAdmin = admin && admin.length > 0 ? admin[0] : '';
   modifier['activities.upcoming.' + match.privacy + '.admin'] = matchId;

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Set new admin and new repeat value
	Matches.update({_id: matchId}, {$set: {admin: [newAdmin], repeat: repeat}});

	// Remove activityId from previous admin (if any).
   if (prevAdmin !== '' && prevAdmin !== newAdmin) {
      Meteor.users.update({_id: prevAdmin}, {$pull: modifier});
   }

   // Attach activityId to new admin (if any).
   if (newAdmin !== '' && newAdmin !== prevAdmin) {
      Meteor.users.update({_id: newAdmin}, {$addToSet: modifier});
   }

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Run server side only
   if (!this.isSimulation) {

      // Don't wait for the following task to be done before giving the client the green light to move ahead
      Meteor.defer(function () {

         // Send notification to new admin (if applies)
         if (newAdmin !== '' && newAdmin !== prevAdmin) {
            Notifications.insert({createdBy: curUserId, recipient: newAdmin, text: textNotif, anchor: anchor});
         }
      });
   }

	return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg

}});
