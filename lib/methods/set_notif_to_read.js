// CHECKED!!
// TODO: add rating limit
//======================================================================
// SET NOTIFICATIONS TO READ:
//======================================================================
Meteor.methods({'setNotifToRead': function() {

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Set Notifications to Read';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var unreadNotif = null;

   /////////////
   // QUERIES //
   /////////////

   unreadNotif = UnreadNotifications.findOne({recipientId: curUserId});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Well defined
   if (!unreadNotif || !unreadNotif.counter) {
      throwError(context, 'unreadNotif is not defined, userId: ' + curUserId);
      return unexpError; // msg
   }
   // Any notifications?
   if (unreadNotif.counter === 0) {
      return;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Set all notifications to 'read'
   Notifications.update({recipient: curUserId, didRead: 'no'}, {$set: {didRead: 'yes'}}, {multi: true});
   UnreadNotifications.update({recipientId: curUserId}, {$set: {counter: 0}});

}});
