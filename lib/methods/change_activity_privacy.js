/*
DESCRIPTION: changes the activity.privacy from 'private' to 'public'.

REQUIREMENTS: current user must be owner or admin.
*/
//CHECKED!
// TODO: add rating limit
//======================================================================
// CHANGE ACTIVITY PRIVACY:
//======================================================================
Meteor.methods({'changeActivityPrivacy': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

	check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Change Activity Privacy';
   var curUserId = Meteor.userId();
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var match = null;
   var admin = ''; // userId
	var result = {};
	var key = '';
	var pull = { // DB modifier
	   created: {},
	   admin: {},
	   enrolled: {},
	   waiting: {},
	   friendIsEnrolled: {},
	   following: {}
	};
	var addToSet = { // DB modifier
	   created: {},
	   admin: {},
	   enrolled: {},
	   waiting: {},
	   friendIsEnrolled: {},
	   following: {}
	};

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "admin": 1, "status": 1, "peopleIn": 1, "friendsOf": 1, "waitingList": 1, "followers": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Current user is owner/admin
   if (!curUserIsOwnerOrAdmin(match)) {
      throwError(context, 'the user is not the owner nor the admin');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   admin = match.admin && match.admin[0]; // could be null or undefined
	// initial pull = {created: {}, admin: {}, enrolled: {}, waiting: {}, friendIsEnrolled: {}, following: {}};
	for (key in pull) {
	   pull[key]['activities.upcoming.private.' + key] = matchId;
	}
	// initial addToSet = {created: {}, admin: {}, enrolled: {}, waiting: {}, friendIsEnrolled: {}, following: {}};
	for (key in addToSet) {
	   addToSet[key]['activities.upcoming.public.' + key] = matchId;
	}

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Update activity privacy
   Matches.update({_id: matchId}, {$set: {privacy: 'public'}});

   // Update owner.activities
   Meteor.users.update({_id: match.createdBy}, {$pull: pull.created, $addToSet: addToSet.created});

   // Update admin.activities
   if (admin) {
      Meteor.users.update({_id: admin}, {$pull: pull.admin, $addToSet: addToSet.admin});
   }

   // Update participants docs
   Meteor.users.update({_id: {$in: getUserIds(match.peopleIn)}}, {$pull: pull.enrolled, $addToSet: addToSet.enrolled}, {multi: true});
   Meteor.users.update({_id: {$in: getUserIds(match.waitingList)}}, {$pull: pull.waiting, $addToSet: addToSet.waiting}, {multi: true});
   Meteor.users.update({_id: {$in: getUserIds(match.friendsOf)}}, {$pull: pull.friendIsEnrolled, $addToSet: addToSet.friendIsEnrolled}, {multi: true});
   Meteor.users.update({_id: {$in: match.followers}}, {$pull: pull.following, $addToSet: addToSet.following}, {multi: true});

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the following task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

   	Meteor.call('notifyUsersInNearbyRegion', matchId);
   });

   return {status: 'success', text: 'The_Activity_Is_Now_Public', title: 'Operation_Successful'}; // msg

}});
