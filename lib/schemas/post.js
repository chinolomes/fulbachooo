//======================================================================
// POST SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
PostSchema = new SimpleSchema({

   createdBy: {
      type: String,
      label: "AuthorId",
      //regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true
   },

   createdAt: {
      type: Date,
      label: "Date of creation",
      denyUpdate: true,
      autoValue: function() {
         if (this.isInsert) {
            return new Date;
         } else {
            this.unset();
         }
      }
   },

   postedOn: {
      type: String,
      label: "Activity id where the comment was posted on",
      //regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true
   },

   text: {
      type: String,
      label: "Post content",
      max: 2000
   }

});

// Attach schema to collection
Posts.attachSchema(PostSchema);
