//======================================================================
// NOTIFICATIONS COUNTER SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
NotifCounterSchema = new SimpleSchema({

   recipientId: {
      type: String,
      label: "userId of the user who receives the notifications",
      //regEx: SimpleSchema.RegEx.Id,
      index: true,
      unique: true,
      denyUpdate: true
   },

   counter: {
      type: Number,
      label: 'Number of unread notifications',
      autoValue: function() {
         if (this.isInsert) {
            return 0;
         }
      }
   }

});

// Attach schema to collection
UnreadNotifications.attachSchema(NotifCounterSchema);
