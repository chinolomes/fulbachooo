//======================================================================
// PLAYER SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
PlayerSchema = new SimpleSchema({

   userId: {
      type: String,
      label: "userId",
      //regEx: SimpleSchema.RegEx.Id
      //denyUpdate: true
   },

   addedAt: {
      type: Date,
      label: "Date in which the player gets enrolled into the match",
      autoValue: function() {
         if (this.isInsert) {
            return new Date;
         } else if (this.isUpdate && this.operator === "$addToSet") {
            return new Date;
         } /*else {
            this.unset();
         }*/
      }
   },

   team: {
      type: String,
      label: "Team in which the player has being enrolled",
      allowedValues: ['A', 'B']
   }

});
