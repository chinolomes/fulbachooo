//======================================================================
// USER ACTIVITIES SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
UserActivitiesSchema = new SimpleSchema({

   "created": {
      type: [String],
      label: "Array of activities ids created by the user",
      /*autoValue: function() {
         if (this.isInsert) {
            return [];
         }
      }*/
   },

   "enrolled": {
      type: [String],
      label: "Array of activities ids for which the user is enrolled"
   },

   "waiting": {
      type: [String],
      label: "Array of activities ids for which the user is in the waiting list"
   },

   "admin": {
      type: [String],
      label: "Array of activities ids for which the user is admin"
   },

   "following": {
      type: [String],
      label: "Array of activities ids that the user is following"
   },

   "friendIsEnrolled": {
      type: [String],
      label: "Array of activities ids for which the user's friend is enrolled"
   }

});
