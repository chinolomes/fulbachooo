//======================================================================
// ADS SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
var adsSchema = new SimpleSchema({

   companyName: {
      type: String,
      denyUpdate: true
   },

   mobileImg: {
      type: String,
      label: "Where to find mobile img"
   },

   desktopImg: {
      type: String,
      label: "Where to find desktop img"
   },

   anchor: {
      type: String,
      label: "Where to redirect the user on ad-click"
   },

   /* GEO LOCATION */
   loc: {
      type: Object,
      label: "Coordinates in geoJSON format",
      index: '2dsphere'
   },

   "loc.type": {
      type: String,
      label: 'geoJSON type',
      allowedValues: ['Point'],
      autoValue: function() {
         if (this.isInsert) {
            return 'Point';
         }
      },
      denyUpdate: true
   },

   "loc.coordinates": {
      type: [Number],
      label: '[lng, lat]',
      decimal: true,
      minCount: 2,
      maxCount: 2,
      custom: function () {
         if (this.isSet) { // TODO: don't throw error, set it to bonuds value
            if (this.value[0] < -180 || this.value[0] > 180) {
               return "lngOutOfRange";
            } else if (this.value[1] < -85 || this.value[1] > 85) {
               return "latOutOfRange";
            }
         }
      }
   },

   active: {
      type: Boolean,
      label: "Ad status"
   },

   clicks: {
      type: Number,
      label: "Clicks counter",
      defaultValue: 0
   }

   // TODO: animation?

});

// Attach schema to collection
Ads.attachSchema(adsSchema);
