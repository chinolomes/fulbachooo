//======================================================================
// FRIEND OF SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
FriendOfSchema = new SimpleSchema({

   name: {
      type: String,
      label: "Friend name"
   },

   addedAt: {
      type: Date,
      label: "Date in which the substitute is added to the waiting list",
      autoValue: function() {
         if (this.isInsert) {
            return new Date;
         } else if (this.isUpdate && this.operator === "$addToSet") {
            return new Date;
         } else {
            this.unset();
         }
      }
   },

   team: {
      type: String,
      label: "Team in which the player has being enrolled",
      allowedValues: ['A','B']
   },

   userId: {
      type: String,
      label: "Id of the user who enrolled this player",
      //regEx: SimpleSchema.RegEx.Id,
   }

});
