//======================================================================
// SPIDER SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
SpiderSchema = new SimpleSchema({

   ip: {
      type: String,
      label: "first 6 digits of the spider-ip"
      //regEx: SimpleSchema.RegEx.IP
   }

});

// Attach schema to collection
SpidersIPs.attachSchema(SpiderSchema);
