//======================================================================
// COLLECTIONS COUNTER SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
CounterSchema = new SimpleSchema({

   collectionName: {
      type: String
   },

   counter: {
      type: Number,
      label: "Keep track of the number of active docs in the collectionName collection"
   }

});

// Attach schema to collection
CollectionsCounter.attachSchema(CounterSchema);
