//======================================================================
// MATCH SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
MatchSchema = new SimpleSchema({

   createdBy: {
      type: String,
      label: "Author",
      //regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true
   },

   createdAt: {
      type: Date,
      label: "Date of creation",
      denyUpdate: true,
      autoValue: function() {
         if (this.isInsert) {
            return new Date;
         }
      }
   },

   sport: {
      type: String,
      label: "Sport type",
      allowedValues: SPORTS_ARRAY // see client(server)/lib/constants.js
   },

   date: {
      type: Date,
      label: "Date of the match"
   },

   time: {
      type: String,
      label: "Time of the match",
      regEx: /^[0-9:]{5}$/
   },

   duration: {
      type: String,
      label: "Duration of the activity",
      max: 40,
      optional: true
   },

   cost: {
      type: String,
      label: "Cost per person",
      max: 40
   },

   address: {
      type: String,
      label: "Match address",
      max: 70
   },

   /* GEO LOCATION */
   loc: {
      type: Object,
      label: "Coordinates in geoJSON format",
      index: '2dsphere'
   },

   "loc.type": {
      type: String,
      label: 'geoJSON type',
      allowedValues: ['Point'],
      autoValue: function() {
         if (this.isInsert) {
            return 'Point';
         }
      },
      denyUpdate: true
   },

   "loc.coordinates": {
      type: [Number],
      label: '[lng, lat]',
      decimal: true,
      minCount: 2,
      maxCount: 2,
      custom: function () {
         if (this.isSet) { // TODO: don't throw error, set it to bonuds value
            if (this.value[0] < -180 || this.value[0] > 180) {
               return "lngOutOfRange";
            } else if (this.value[1] < -85 || this.value[1] > 85) {
               return "latOutOfRange";
            }
         }
      }
   },

   /* FIELD CHARACTERISTICS */
   fieldSize: {
      type: Number,
      label: "Number of players vs number of players",
      min: 1,
      max: 20
   },

   fieldType: {
      type: String,
      label: "Brief description of the field",
      max: 70,
      optional: true
   },

   fieldPhone: {
      type: String,
      label: "Phone of the sport field",
      max: 40,
      optional: true
   },

   fieldWebsite: {
      type: String,
      //regEx: SimpleSchema.RegEx.Url,
      max: 300,
      optional: true
   },

   title: {
      type: String,
      label: "Brief description of the match",
      max: 200
   },

   description: {
      type: String,
      label: "Full description of the match",
      max: 5000,
      optional: true
   },

   privacy: {
      type: String,
      allowedValues: ['public', 'private']
   },

   privacyOnCreation: {
      type: String,
      label: "keeps the value of privacy at the moment of the match creation (used to auto-populate new matches)",
      allowedValues: ['public', 'private'],
      denyUpdate: true
   },

   status: {
      type: String,
      allowedValues: ['active', 'canceled', 'finished'],
      defaultValue: 'active'
   },

   repeat: {
      type: Boolean,
      label: "Repeat the event evey week",
      optional: true
   },

   /* PEOPLE IN */
   peopleIn: {
      type: [PlayerSchema],
      label: "list of registered users enrolled in the match: [player, player, ...]"
   },

   /* PEOPLE IN ON CREATION */
   peopleInOnCreation: {
      type: [PlayerSchema],
      label: "keeps the value of peopleIn at the moment of the match creation (used to auto-populate new matches)",
      denyUpdate: true
   },

   friendsOf: {
      type: [FriendOfSchema],
      label: "list of unregistered users enrolled in the match: [friendOf, friendOf, ...]",
      defaultValue: []
   },

   waitingList: {
      type: [PlayerSchema],
      label: "list of registered users waiting for a place in the match: [user, user, ...]",
      defaultValue: []
   },

   admin: {
      type: [String],
      label: "id of the user allowed to admin this activity",
      //regEx: SimpleSchema.RegEx.Id,
      minCount: 0,
      maxCount: 1,
      optional: true
   },

   followers: {
      type: [String],
      label: "list of users following this match: [userId, userId, ...]"
      //regEx: SimpleSchema.RegEx.Id,
   },

   /*numberOfComments: {
      type: Number,
      label: "number of posts in the match",
      min: 0,
      defaultValue: 0
   }*/

});

// Attach schema to collection
Matches.attachSchema(MatchSchema);
