//======================================================================
// EMAIL SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
// TODO: change name to emailNotifSchema
//----------------------------------------------------------------------
EmailSchema = new SimpleSchema({

   recipient: {
      type: String,
      label: "Single email address",
      //regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true
   },

   sender: {
      type: [String],
      label: "[senderName, senderName, ...]"/*,
      regEx: SimpleSchema.RegEx.Id,
      max: 150,
      denyUpdate: true*/
   },

   text: {
      type: [String],
      label: "[text, text, ...]",
      //denyUpdate: true
   },

   url: {
      type: [String],
      label: "Address associated with each notification",
      regEx: SimpleSchema.RegEx.Url,
      //denyUpdate: true
   },

   lang: {
      type: String,
      label: "Language to deliver the email",
      allowedValues: ['en', 'es', 'it', 'fr', 'ch', 'de', 'du', 'ar', 'nl']
      //denyUpdate: true
   },

   sent: {
      type: Boolean,
      label: "Email status",
      defaultValue: false
   },

   length: {
      type: Number,
      label: "Number of chunks (text.length)",
      min: 0
   }

});

// Attach schema to collection
Emails.attachSchema(EmailSchema);
