//======================================================================
// USER SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
UserSchema = new SimpleSchema({

   /* PROFILE */
   profile: {
      type: Object
   },

   "profile.name": {
      type: String,
      max: 150
   },

   "profile.avatar": {
      type: String,
      regEx: SimpleSchema.RegEx.Url,
      optional: true
   },

   "profile.avatarColor": {
      type: String,
      regEx: /^[0-9#]{3,6}$/,
      optional: true
   },

   "profile.birthday": {
      type: Date,
      optional: true
   },

   "profile.gender": {
      type: String,
      allowedValues: ['male', 'female', 'other', 'undefined'],
      optional: true
   },

   /*"profile.language": {
      type: String,
      allowedValues: ['en', 'es', 'it', 'fr', 'ch', 'de', 'du', 'ar'],
      autoValue: function() {
         if (this.isInsert) {
            return 'en';
         }
      }
   },*/

   "profile.lang": {
      type: Object,
   },

   "profile.lang.value": {
      type: String,
      allowedValues: ['en', 'es', 'it', 'fr', 'ch', 'de', 'du', 'ar', 'nl'],
      autoValue: function() {
         if (this.isInsert) {
            return 'en';
         }
      }
   },

   "profile.lang.flag": {
      type: Boolean,
      label: "true if the language was set after user creation. false otherwise",
      autoValue: function() {
         if (this.isInsert) {
            return false;
         }
      }
   },

   "profile.dominantLeg": {
      type: String,
      allowedValues: ['left', 'right', 'both'],
      optional: true
   },

   "profile.dominantHand": {
      type: String,
      allowedValues: ['left', 'right', 'both'],
      optional: true
   },

   "profile.selfDescription": {
      type: String,
      max: 600,
      optional: true
   },

   "profile.sports": {
      type: [String],
      label: "List of sports played by the user [sport, sport, ...]"/*,
      optional: true*/
   },

   "profile.sports.$": {
      type: String,
      label: "Sport type",
      allowedValues: SPORTS_ARRAY // see client(server)/lib/constants.js
   },

   welcome: {
      type: Boolean,
      label: "Welcome message has been sent",
      autoValue: function() {
         if (this.isInsert) {
            return false;
         }
      }
   },

  gdpr: {
      type: Boolean,
      label: "GDPR message has been sent",
      autoValue: function() {
         if (this.isInsert) {
            return false;
         }
      }
   },

   category: {
      type: String,
      allowedValues: ['regular', 'pro'],
      autoValue: function() {
         if (this.isInsert) {
            return 'regular';
         }
      }
   },

   createdAt: {
      type: Date
   },

   ip: {
      type: String,
      regEx: SimpleSchema.RegEx.IP
   },

   lastLogin: {
      type: Date
   },

   /* EMAILS */
   emails: {
      type: [Object],
      // this must be optional if you also use other login services like facebook,
      // but if you use only accounts-password, then it can be required
      optional: true
   },

   "emails.$.address": {
      type: String,
      regEx: SimpleSchema.RegEx.Email
   },

   "emails.$.verified": {
      type: Boolean
   },

   /* NOTIFICATIONS EMAIL */
   notifEmails: {
      type: Object//,
      // this must be optional if you also use other login services like facebook,
      // but if you use only accounts-password, then it can be required
      //optional: true
   },

   "notifEmails.verified": {
      type: [String],
      label: "array of verified email addresses",
      regEx: SimpleSchema.RegEx.Email,
      //denyUpdate: true,
   },

   "notifEmails.sent": {
      type: [String],
      label: "array of addresses that were sent for verification",
      regEx: SimpleSchema.RegEx.Email,
      //denyUpdate: true,
   },

   "notifEmails.primary": {
      type: String,
      label: "primary email address",
      optional: true
   },

   "notifEmails.verificationToken": {
      type: Object
   },

   "notifEmails.verificationToken.token": {
      type: String,
      optional: true
   },

   // TODO: what's this?? I can get userId, from the doc :/
   "notifEmails.verificationToken.userId": {
      type: String,
      optional: true
   },

   "notifEmails.verificationToken.address": {
      type: String,
      regEx: SimpleSchema.RegEx.Email,
      optional: true
   },

   "notifEmails.verificationToken.expiresAt": {
      type: Date,
      optional: true
   },

   /* SERVICES */
   services: {
      type: Object,
      optional: true,
      blackbox: true
   },

   //////////// TO BE ERASED
   /* MATCHES */
   matches: {
      type: Object
   },

   "matches.public": {
      type: Object,
      label: "List of public matches related to the user"
   },

   "matches.private": {
      type: Object,
      label: "List of public matches related to the user"
   },

   "matches.public.created": {
      type: [String],
      label: "Array of activities ids created by the user"
   },

   "matches.public.played": {
      type: [String],
      label: "Array of activities ids in which the user in signed in"
   },

   "matches.public.waiting": {
      type: [String],
      label: "Array of activities ids for which the user is in the waiting list"
   },

   "matches.public.admin": {
      type: [String],
      label: "Array of activities ids for which the user is admin"
   },

   "matches.private.created": {
      type: [String],
      label: "Array of activities ids created by the user"
   },

   "matches.private.played": {
      type: [String],
      label: "Array of activities ids in which the user in signed in"
   },

   "matches.private.waiting": {
      type: [String],
      label: "Array of activities ids for which the user is in the waiting list"
   },

   "matches.private.admin": {
      type: [String],
      label: "Array of activities ids for which the user is admin"
   },
   //////////// TO BE ERASED

   /* ACTIVITIES */
   activities: {
      type: Object
   },

   "activities.upcoming": {
      type: Object,
      label: "List of upcoming activities related to the user"
   },

   "activities.past": {
      type: Object,
      label: "List of past activities related to the user"
   },

   "activities.upcoming.public": {
      type: UserActivitiesSchema,
      label: "List of public upcoming activities related to the user"
   },

   "activities.upcoming.private": {
      type: UserActivitiesSchema,
      label: "List of private upcoming activities related to the user"
   },

   "activities.past.public": {
      type: UserActivitiesSchema,
      label: "List of public past activities related to the user"
   },

   "activities.past.private": {
      type: UserActivitiesSchema,
      label: "List of private past activities related to the user"
   },

   // TO BE REMOVED //
   // IN THE SECOND DEPLOYMENT COMMENT THIS
   /* SETTINGS */
   /*settings: {
      type: Object,
      optional: true // ADDED
   },


   "settings.language": { // CHANGED TO profile.language !!
      type: String,
      allowedValues: ['en', 'es', 'it', 'fr', 'ch', 'de', 'du', 'ar'],
      optional: true // ADDED
   },

   "settings.circleCenterLat": {
      type: Number,
      label: "Latitude",
      min: -180.0,
      max: 180.0,
      decimal:true,
optional: true // ADDED
   },

   "settings.circleCenterLng": {
      type: Number,
      label: "Longitude",
      min: -90.0,
      max: 90.0,
      decimal: true,
optional: true // ADDED
   },

   "settings.circleRadius": {
      type: Number,
      min: 0,
      max: 20,
optional: true // ADDED
   },

   "settings.mapZoom": {
      type: Number,
      min: 0,
      max: 21,
optional: true // ADDED
   },

   "settings.loc": {
      type: Object,
      label: "Coordinates in geoJSON format",
      index: '2dsphere',
optional: true // ADDED
   },

   "settings.loc.type": {
      type: String,
      label: 'geoJSON type',
      allowedValues: ['Point'],
      autoValue: function() {
         if (this.isInsert) {
            return 'Point';
         }
      },
      optional: true // ADDED
      //denyUpdate: true // TODO, remove comment once deployed
   },

   "settings.loc.coordinates": {
      type: [Number],
      label: '[lng, lat]',
      decimal: true,
      minCount: 2,
      maxCount: 2,
      custom: function () {
         if (this.isSet) {
            if (this.value[0] < -90 || this.value[0] > 90) {
               return "lngOutOfRange";
            } else if (this.value[1] < -180 || this.value[1] > 180) {
               return "latOutOfRange";
            }
         }
      },
   optional: true // ADDED
   },*/
   // TO BE REMOVED //

   /* GEO */
   geo: {
      type: Object
   },

   "geo.flag": {
      type: Boolean,
      label: "true if the geo location was set after user creation. false otherwise",
      autoValue: function() {
         if (this.isInsert) {
            return false;
         }
      }
   },

   "geo.radius": { // CHANGED FROM circleRadius to radius
      type: Number,
      min: 0,
      max: 20
   },

   "geo.zoom": { // CHANGED FROM mapZoom to zoom
      type: Number,
      min: 0,
      max: 21
   },

   "geo.loc": {
      type: Object,
      label: "Coordinates in geoJSON format",
      index: '2dsphere'
   },

   "geo.loc.type": {
      type: String,
      label: 'geoJSON type',
      allowedValues: ['Point'],
      autoValue: function() {
         if (this.isInsert) {
            return 'Point';
         }
      },
      denyUpdate: true // TODO: remove comment after update
   },

   "geo.loc.coordinates": {
      type: [Number],
      label: '[lng, lat]',
      decimal: true,
      minCount: 2,
      maxCount: 2,
      custom: function () {
         if (this.isSet) {
            if (this.value[0] < -180 || this.value[0] > 180) {
               return "lngOutOfRange";
            } else if (this.value[1] < -85 || this.value[1] > 85) {
               return "latOutOfRange";
            }
         }
      }
   },

   /* EMAIL NOTIF. SETTINGS */
   emailNotifSettings: {
      type: Object
   },

   "emailNotifSettings.gameNewGamePlayingZone": {
      type: Boolean,
      label: "New match in my playing zone"
   },

   "emailNotifSettings.gameNewComment": {
      type: Boolean,
      label: "New comment in one of the matches I'm enrolled"
   },

   "emailNotifSettings.gameWasCanceled": {
      type: Boolean,
      label: "One of my games has been canceled"
   },

   "emailNotifSettings.gameAddRemovePlayer": {
      type: Boolean,
      label: "One of my games has been canceled"
   },

   "emailNotifSettings.gameAddedMe": {
      type: Boolean,
      label: "I was added to one team"
   },

   "emailNotifSettings.gamePassFromWaitingToPlaying": {
      type: Boolean,
      label: "Passing from the waiting list to the list of participants of the activity"
   },

   "emailNotifSettings.gameNewGameRepeat": {
      type: Boolean,
      label: "Notify the user every time one of his events is repeated automatically"
   },

  // In order to avoid an 'Exception in setInterval callback' from Meteor
  heartbeat: {
    type: Date,
    optional: true,
  },
   /*,
   // Add `roles` to your schema if you use the meteor-roles package.
   // Option 1: Object type
   // If you specify that type as Object, you must also specify the
   // `Roles.GLOBAL_GROUP` group whenever you add a user to a role.
   // Example:
   // Roles.addUsersToRoles(userId, ["admin"], Roles.GLOBAL_GROUP);
   // You can't mix and match adding with and without a group since
   // you will fail validation in some cases.
   roles: {
      type: Object,
      optional: true,
      blackbox: true
   },
   // Option 2: [String] type
   // If you are sure you will never need to use role groups, then
   // you can specify [String] as the type
   roles: {
      type: [String],
      optional: true
   }*/
});

// Attach schema to collection
Meteor.users.attachSchema(UserSchema);
