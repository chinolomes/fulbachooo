//======================================================================
// NOTIFICATION SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
// TODO: change name to 'internalNotifSchema'
//----------------------------------------------------------------------
NotificationSchema = new SimpleSchema({

   createdBy: {
      type: String,
      label: "AuthorId",
      //regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true
   },

   // Force value to be current date (on server) upon insert
   // and prevent updates thereafter.
   createdAt: {
      type: Date,
      label: "Date of creation",
      denyUpdate: true,
      autoValue: function() {
         if (this.isInsert) {
            return new Date;
         } /*else {
            this.unset();
         }*/
      }
   },

   recipient: {
      type: String,
      label: "userId who receives the notification",
      //regEx: SimpleSchema.RegEx.Id,
      index: 1,
      denyUpdate: true
   },

   text: {
      type: String,
      label: "Notification content",
      max: 300,
      //denyUpdate: true
   },

   anchor: {
      type: String,
      label: "Address associated with the notification",
      denyUpdate: true
   },

   didRead: {
      type: String,
      label: "Notification status",
      allowedValues: ['yes', 'no'],
      defaultValue: 'no'
   }

});

// Attach schema to collection
Notifications.attachSchema(NotificationSchema);
