//======================================================================
// EMAIL SCHEMA:
//======================================================================
// SEE: http://themeteorchef.com/snippets/using-the-collection2-package/
//----------------------------------------------------------------------
WelcomeEmailSchema = new SimpleSchema({

   recipientId: {
      type: String,
      label: "Single email address",
      //regEx: SimpleSchema.RegEx.Id,
      denyUpdate: true
   },

   sent: {
      type: Boolean,
      label: "Email status",
      defaultValue: false
   }

});

// Attach schema to collection
WelcomeEmails.attachSchema(WelcomeEmailSchema);
