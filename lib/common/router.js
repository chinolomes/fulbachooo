//======================================================================
// CONFIGURE:
//======================================================================
Router.configure({
	layoutTemplate: 'mainLayout',
	// trackPageView: true,
	loadingTemplate: 'spinner'
});

//======================================================================
// DEFAULT ROUTE:
//======================================================================
Router.route('/', function() {
	Router.go('home'); // Redirect to matches template
});

//======================================================================
// REDIRECT OLD MATCHES/MATCH ROUTE TO NEW ACTIVITIES/ACTIVITY ROUTE:
//======================================================================
Router.route('match', {
	path: '/matches/match/:_id',
	onBeforeAction: function() {
		Router.go('/activities/activity/' + this.params._id); // Redirect to activity template
	}
});
Router.route('matches', {
	path: '/matches',
	onBeforeAction: function() {
		Router.go('/activities'); // Redirect to activities template
	}
});
Router.route('players', {
	path: '/players',
	onBeforeAction: function() {
		Router.go('/users'); // Redirect to activities template
	}
});

//======================================================================
// ROUTES DEFINITION:
//======================================================================
// TODO:
// add the dataNotFound plugin, which is responsible for
// rendering the dataNotFound template if your RouteController
// data function returns a falsy value
Router.plugin('dataNotFound',{
   notFoundTemplate: 'dataNotFound'
});

Router.route('/activityNotFound');

Router.route('home', {
	path: '/home',
	layoutTemplate: 'homeLayout'
});


Router.route('languages', {
	path: '/languages',
	layoutTemplate: 'wideLayout' // overwrite layout
});


Router.route('notifications', {
		path: '/notifications',
		layoutTemplate: 'wideLayout', // overwrite layout
		// Set data context
		data: function() {
			return {type: 'mobile'};
		}
});

Router.route('verifyEmail', {
      path: '/verify-email/:userId/:token',
		layoutTemplate: 'wideLayout', // overwrite layout
		data: function() {
			return {userId: this.params.userId, token: this.params.token};
		}
});


Router.route('emailNotifMOBILE', {
	path: '/settings',
	layoutTemplate: 'profileLayout', // overwrite layout
	onBeforeAction: function() {
		if (!Meteor.userId()) {
   		return this.redirect('users');
   	} else {
   		this.next();
		}
	}
});

Router.route('more', {
	path: '/more',
	layoutTemplate: 'profileLayout', // overwrite layout
	// Set data context
	data: function() {
		return {type: 'mobile'};
	}
});

Router.route('FAQ', {
	path: '/faq',
	layoutTemplate: 'profileLayout', // overwrite layout
	// Set data context
	data: function() {
		return {type: 'mobile'};
	}
});

Router.route('whoWeAre', {
	path: '/whoweare',
	layoutTemplate: 'profileLayout', // overwrite layout
	// Set data context
	data: function() {
		return {type: 'mobile'};
	}
});

Router.route('termsAndConditions', {
	path: '/terms',
	layoutTemplate: 'profileLayout', // overwrite layout
	// Set data context
	data: function() {
		return {type: 'mobile'};
	}
});

Router.route('privacyPolicy', {
	path: '/privacy',
	layoutTemplate: 'profileLayout', // overwrite layout
	// Set data context
	data: function() {
		return {type: 'mobile'};
	}
});

Router.route('contactUs', {
	path: '/contact',
	layoutTemplate: 'profileLayout', // overwrite layout
	// Set data context
	data: function() {
		return {type: 'mobile'};
	}
});


Router.route('users', {
	path: '/users',
	layoutTemplate: 'wideLayout', // overwrite layout
	waitOn: function() { // TODO: must be a global subs
		return [
			Meteor.subscribe('totalUsersCounter')
		]
	}
});


Router.route('activities', {
	path: '/activities',
	layoutTemplate: 'wideLayout', // overwrite layout
	waitOn: function() { // TODO: must be a global subs
		return [
			Meteor.subscribe('totalUpcomingActivitiesCounter')
		]
	}
});


Router.route('activity', {
	path: '/activities/activity/:_id',
	layoutTemplate: 'activityLayout', // overwrite layout
	notFoundTemplate: 'activityNotFound',
	waitOn: function() {
		return [
			Meteor.subscribe('match', this.params._id)
		]
	},
	// Set data context
	data: function () {
		var match = Matches.findOne({_id: this.params._id});
		return !match ? null : {matchId: match._id};
	}
});

// TODO: don't wait for the data. Use template-level subscriptions
// otherwise if a random user goes to editActivity route we are subs but it's not needed
Router.route('editActivity', {
	path: '/activity/edit/:_id',
	layoutTemplate: 'wideLayout', // overwrite layout
	notFoundTemplate: 'activityNotFound',
	/*onBeforeAction: function() {
		var owner = Matches.findOne({_id: this.params._id}).createdBy;
		if (Meteor.userId() !== owner) {
   		return this.redirect('matches');
   		} else {
   		this.next();
		}
	},*/
	waitOn: function() {
		return [
			Meteor.subscribe('match', this.params._id)
		]
	},
	// Set data context
	data: function () {
		var match = Matches.findOne({_id: this.params._id});
		return !match ? null : {match: match};
	}
});

Router.route('profile', {
	path: '/profile/:_id',
	layoutTemplate: 'profileLayout', // overwrite layout
	notFoundTemplate: 'profileNotFound',
	waitOn: function() {
		return [
			Meteor.subscribe('userProfile', this.params._id),
			Meteor.subscribe('userServices', this.params._id),
			Meteor.subscribe('userActivities', this.params._id)
		]
	},
	// Set data context
	data: function () {
		var user = Meteor.users.findOne({_id: this.params._id});
		return !user ? null : {userId: user._id};
	}
});

Router.route('editProfileMOBILE', {
	path: '/profile/edit/:_id',
	layoutTemplate: 'wideLayout', // overwrite layout
	notFoundTemplate: 'profileNotFound',
	onBeforeAction: function() {
		if (this.params._id !== Meteor.userId()) {
   		return this.redirect('users');
   	} else {
   		this.next();
		}
	}
});

// TODO: load user map settings and check if the user is logged in (use hooks onBeforeAction)
Router.route('newActivity', {
	path: '/activities/new_activity',
	layoutTemplate: 'wideLayout' // overwrite layout
});


//======================================================================
// ON BEFORE ACTION:
//======================================================================

Router.onBeforeAction(function(pause) {
	// Manage players vars
	Session.set('resetAll_FORM_COMPONENT', true);
	//console.log('router.js session vars cleaned!');
	this.next();
	}, {
	only: ["editActivity", "newActivity"]
});

/*Router.onBeforeAction(function() {
	// New match session vars cleaning
	Session.set('resetAll_FORM_COMPONENT', true);
	this.next();
}, {only: ["newActivity"]});*/


//======================================================================
// ON STOP ACTION:
//======================================================================
// onStop hook is executed whenever we LEAVE a route
Router.onStop(function() {
	// Close popups if any
	bootbox.hideAll();
});
