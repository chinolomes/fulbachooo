/* CONSTRUCTOR */
MatchClass = function(admin, sport, date, time, duration, cost, address, lat, lng, userIdsTeamA, userIdsTeamB, friendsOf, waitingList, fieldSize, fieldType, fieldPhone, fieldWebsite, title, privacy, description, repeat) {
	this.admin = admin; // array of ids max length = 1
	this.sport = sport;
	this.date = date;
	this.time = time;
	this.duration = duration;
	this.cost = cost;
	this.address = address;
	this.lat = lat;
	this.lng = lng;
	this.fieldSize = fieldSize;
	this.fieldType = fieldType;
	this.fieldPhone = fieldPhone;
	this.fieldWebsite = fieldWebsite;
	this.title = title;
	this.privacy = privacy;
	//this.peopleIn = peopleIn; // array of objects
	this.userIdsTeamA = userIdsTeamA; // array of ids
	this.userIdsTeamB = userIdsTeamB; // array of ids
	this.friendsOf = friendsOf; // array of objects
	this.waitingList = waitingList; // array of abjects
	this.description = description;
	this.repeat = repeat;
}

/* GETTERS */
MatchClass.prototype.getPeopleIn = function() {
	//console.log('this.userIdsTeamA', this.userIdsTeamA);
	//console.log('this.userIdsTeamB', this.userIdsTeamB);
	var peopleIn = _.union(attachTeam(this.userIdsTeamA, 'A'), attachTeam(this.userIdsTeamB, 'B'));
	//console.log('peopleIn', peopleIn);
	return peopleIn.length > 0 ? peopleIn : [];
};

/* OTHER METHODS */
MatchClass.prototype.check = function() {

	if (this.sport === '' || this.title === '' || this.lat === '' || this.lng === '' || this.privacy === '' || this.address === '' || this.date === '' || this.time === '' || this.cost === '' || this.fieldSize === '') {
		//console.log('sport: ' + match.sport + ', title: ' + match.title + ', lat: ' + match.lat + ', lng: ' + match.lng + ', privacy: ' + match.privacy + ', address: ' + match.address + ', date: ' + match.date + ', time: ' + match.time + ', cost: ' + match.cost + ', fieldSize: ' + match.fieldSize);
		return {status: 'error', text: 'Fill_All_Mandatory_Fields', title: 'Operation_Unsuccessful'}; // msg
	}
	if (this.title.length > 200) {
		return {status: 'error', text: 'The_Title_Exceeds_200_Chars', title: 'Operation_Unsuccessful'};	// msg
	}
	if (this.address.length > 70) {
		return {status: 'error', text: 'The_Address_Exceeds_70_Chars', title: 'Operation_Unsuccessful'}; // msg
	}
	if (this.userIdsTeamA.length > this.fieldSize || this.userIdsTeamB.length > this.fieldSize) {
		return {status: 'error', text: 'The_Number_Of_Participants_Exceeds_The_Maximum', title: 'Operation_Unsuccessful'}; // msg
	}
	if (_.intersection(this.userIdsTeamA, this.userIdsTeamB).length > 0) {
		//console.log('intersection: ', _.intersection(this.userIdsTeamA, this.userIdsTeamB));
		return {status: 'error', text: 'Participants_Are_Duplicate', title: 'Operation_Unsuccessful'}; // msg
	}
	if (this.fieldWebsite.length > 0 && this.fieldWebsite.substr(0,7) !== 'http://' && this.fieldWebsite.substr(0,8) !== 'https://') {
		return {status: 'error', text: 'Incorrect_Website_Explanation', title: 'Operation_Unsuccessful'}; // msg
	}
	if (this.description.length > 5000) {
		return {status: 'error', text: 'The_Description_Exceeds_5000_Chars', title: 'Operation_Unsuccessful'}; // msg
	}
	if (_.isArray(this.admin) && this.admin.length > 1) {
		return {status: 'error', text: 'Only_One_Admin', title: 'Operation_Unsuccessful'}; // msg
	}
	if (_.isArray(this.admin) && this.admin.length === 1 && this.admin[0] === Meteor.userId()) {
		return {status: 'error', text: 'Admin_Cant_Be_Yourself', title: 'Operation_Unsuccessful'}; // msg
	}
	if (!_.isDate(this.date)) {
		return {status: 'error', text: 'Incorrect_Date', title: 'Operation_Unsuccessful'}; // msg
	}
	if (this.lat < -85 || this.lat > 85) {
		return {status: 'error', text: 'Forbidden_Area', title: 'Operation_Unsuccessful'}; // msg
	}
	if (this.lng < -180 || this.lng > 180) {
		return {status: 'error', text: 'Forbidden_Area', title: 'Operation_Unsuccessful'}; // msg
	}
	return {status: 'success', text: 'ok'};
};

/*MatchClass.prototype.getAdmin = function() {
	return _.isArray(this.admin) && this.admin.length === 1 ? this.admin[0] : [];
};*/

MatchClass.prototype.display = function() {
	console.log('admin: ' + this.admin);
	console.log('sport: ' + this.sport);
 	console.log('title: ' + this.title);
	console.log('duration: ' + this.duration);
 	console.log('lat: ' + this.lat);
 	console.log('lng: ' + this.lng);
 	console.log('privacy: ' + this.privacy);
 	console.log('address: ' + this.address);
 	console.log('date: ' + this.date);
 	console.log('time: ' + this.time);
 	console.log('cost: ' + this.cost);
 	console.log('fieldSize: ' + this.fieldSize);
	console.log('fieldPhone: ' + this.fieldPhone);
	console.log('fieldType: ' + this.fieldType);
	console.log('description: ' + this.description);
	console.log('repeat: ' + this.repeat);
};
