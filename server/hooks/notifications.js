//======================================================================
// NOTIFICATIONS COLLECTION HOOKS:
//======================================================================
// Increase unread notifications counter every new notification
Notifications.after.insert(function(userId, doc) {
   // doc = recently inserted document, ie, notification
   var recipientId = doc.recipient;
   UnreadNotifications.update({recipientId: recipientId}, {$inc: {'counter': 1}});
});
