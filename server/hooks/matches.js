//======================================================================
// MATCHES COLLECTION HOOKS:
//======================================================================
// Every time a new match is created send notification to enrolled players
// TODO: create method and call it from insertNewActivity and cronJo OR hooks leaving a comment in the prev methods
/*Matches.after.insert(function(userId, doc) {

   ///////////////
   // VARIABLES //
   ///////////////

   var matchId = doc._id;
   var sport = doc.sport;
   var peopleIn = doc.peopleIn;
   var ownerId = doc.createdBy;
   var owner = Meteor.users.findOne(ownerId, {fields: {"profile.name": 1}});
   var ownerName = owner.profile.name;
   var notifType = 'gameAddedMe';
   var textNotif = 'User_Created_New_Activity_And_Added_You_To_List_Of_Participants';
   var anchor = '/activities/activity/' + matchId + '#data';
   var textEmail = 'Email_Notif_User_Created_New_Activity_And_Added_You_To_List_Of_Participants';
   var url = 'https://www.fulbacho.net' + anchor;
   var userIdsToNotify = _.without(_.pluck(peopleIn, 'userId'), ownerId); // don't notify activity organizer

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Send notification to enrolled players
   _.each(userIdsToNotify, function(userId) {
   	Notifications.insert({createdBy: ownerId, recipient: userId, text: textNotif, anchor: anchor});
   });

   // Notify users via email
   Meteor.call('notifyUsersViaEmail', ownerName, textEmail, url, userIdsToNotify, notifType);

});*/
//----------------------------------------------------------------------
// Every time a new match is created send notification to admin
// TODO: create method and call it from insertNewActivity and cronJo OR hooks leaving a comment in the prev methods
/*Matches.after.insert(function(userId, doc) {
   console.log('NEW MATCH NOTIF ADMIN');
   var matchId = doc._id;
   var adminId = doc.admin[0]; // might be undefined
   //console.log('adminId: ', adminId);
   //console.log('doc.admin: ', doc.admin);
   if (_.isUndefined(adminId) || _.isNull(adminId)) {
      console.log('NO ADMIN TO BE NOTIFIED');
      return; // no admin has being set
   }

   var ownerId = doc.createdBy;

   // Set notification text
   var text = 'User_Created_New_Activity_And_Named_You_Admin';
   var anchor = '/activities/activity/' + matchId + '#data';

   // Send notification to enrolled players
   Notifications.insert({
   	createdBy: ownerId,
   	recipient: adminId,
   	text: text,
   	anchor: anchor
   });

   console.log('NEW MATCH NOTIF ADMIN DONE');
});*/

//======================================================================
// UPDATE UPCOMING ACTIVITIES COLLECTION COUNTER:
//======================================================================
// Every time a new public activity is created, increase the upcoming activities counter
Matches.after.insert(function(userId, doc) {
   var privacy = doc.privacy;
   if (privacy === 'private') return;
   CollectionsCounter.update({collectionName: 'upcomingActivities'}, {$inc: {counter: 1}});
});
//----------------------------------------------------------------------
// Every time a public activity is removed, decrease the upcoming activities counter
Matches.after.remove(function(userId, doc) {
   var privacy = doc.privacy;
   var status = doc.status;
   if (privacy === 'private' || status === 'finished') return;
   CollectionsCounter.update({collectionName: 'upcomingActivities'}, {$inc: {counter: -1}});
});
//----------------------------------------------------------------------
// Every time a public activity is set to 'finished', decrease the upcoming activities counter
Matches.after.update(function (userId, doc, fieldNames, modifier) {
   var prevPrivacy = this.previous.privacy;
   var prevStatus = this.previous.status;
   var newPrivacy = doc.privacy;
   var newStatus = doc.status;

   if (prevPrivacy === 'private' && newPrivacy === 'public' && newStatus !== 'finished') {
      CollectionsCounter.update({collectionName: 'upcomingActivities'}, {$inc: {counter: 1}});
   } else if (prevPrivacy === 'public' && prevStatus !== 'finished' && newPrivacy === 'private') {
      CollectionsCounter.update({collectionName: 'upcomingActivities'}, {$inc: {counter: -1}});
   } else if (prevPrivacy === 'public' && prevStatus !== 'finished' && newPrivacy === 'public' && newStatus === 'finished') {
      CollectionsCounter.update({collectionName: 'upcomingActivities'}, {$inc: {counter: -1}});
   }
});
