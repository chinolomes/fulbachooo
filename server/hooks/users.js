//======================================================================
// METEOR USERS COLLECTION HOOKS:
//======================================================================
// Set unreadNotifications document right after user creation
Meteor.users.after.insert(function(userId, doc) {
   // this = recently created user
   UnreadNotifications.insert({recipientId: this._id});
});
//----------------------------------------------------------------------
// Increase users counter
Meteor.users.after.insert(function(userId, doc) {
   CollectionsCounter.update({collectionName: 'users'}, {$inc: {counter: 1}});
});
//----------------------------------------------------------------------
// Add user id to the welcome emails list
Meteor.users.after.insert(function(userId, doc) {
   // this = recently created user
   if (!_.isUndefined(doc.notifEmails) && !_.isNull(doc.notifEmails) && !_.isUndefined(doc.notifEmails.primary) && !_.isNull(doc.notifEmails.primary) && doc.welcome === false) {
      console.log('Adding welcome email to stack...');
      WelcomeEmails.insert({recipientId: this._id});
   }
});
