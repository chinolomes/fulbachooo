// INPUT: {senderName, text, url, address, lang}
// TODO: rewrite using new emailNotif schema
// CHECKED
/*
Note that you should only use a Method in the case where some code needs to be
callable from the client; if you just want to modularize code that is only going
to be called from the server, use a regular JavaScript function, not a Method.
*/
addEmailToStack = function(params) {

   ///////////////
   // VARIABLES //
   ///////////////

   var senderName = params.senderName;
   var text = params.text;
   var url = params.url;
   var address = params.address;
   var lang = params.lang;
   var stack = null; // collection of email notifications associated to the given email address to be delivered by cron job
   var selector = {
      recipient: address,
      sent: false
   };
   var options = {
      $push: {
         sender: senderName,
         text: text,
         url: url
      },
      $set: {
         lang: lang
      },
      $inc: {
         length: 1
      }
   };
   var insert = {
      recipient: address,
      sender: [senderName],
      text: [text],
      url: [url],
      lang: lang,
      length: 1
   };

   /////////////
   // QUERIES //
   /////////////

   stack = Emails.findOne({recipient: address, sent: false});

   ///////////
   // LOGIC //
   ///////////

   // Check if the recipient has a stack already initialized
   if (stack) {
      Emails.update(selector, options);
   } else {
      Emails.insert(insert);
   }
}
