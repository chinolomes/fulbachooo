/*
Note that you should only use a Method in the case where some code needs to be
callable from the client; if you just want to modularize code that is only going
to be called from the server, use a regular JavaScript function, not a Method.
*/
recreateActivityForNextWeek = function(activity) {

   var admin = activity.admin && activity.admin[0] ? activity.admin : [];
   //console.log("admin: ", admin);

   return {
      createdBy:          activity.createdBy,
      admin:              admin,
      sport:              activity.sport,
      date:               nextWeek(activity.date),
      time:               activity.time,
      duration:           activity.duration || '',
      cost:               activity.cost,
      address:            activity.address,
      loc:                {coordinates: [activity.loc.coordinates[0], activity.loc.coordinates[1]]},
      fieldSize:          activity.fieldSize,
      fieldType:          activity.fieldType,
      fieldPhone:         activity.fieldPhone,
      fieldWebsite:       activity.fieldWebsite,
      title:              activity.title,
      privacy:            activity.privacyOnCreation,
      privacyOnCreation:  activity.privacyOnCreation,
      peopleIn:           activity.peopleInOnCreation, //peopleIn,
      peopleInOnCreation: activity.peopleInOnCreation, //peopleIn,
      followers:          _.union(_.pluck(activity.peopleInOnCreation, "userId"), admin, [activity.createdBy]), // skips duplicate values
      description:        activity.description,
      repeat:             activity.repeat
   }
}

/*
recreateActivityForNextWeek = function(activity) {

   return {
      createdBy:          activity.createdBy,
      admin:              activity.admin || [],
      sport:              activity.sport,
      date:               nextWeek(activity.date),
      time:               activity.time,
      duration:           activity.duration || '',
      cost:               activity.cost,
      address:            activity.address,
      loc:                {coordinates: [activity.loc.coordinates[0], activity.loc.coordinates[1]]},
      fieldSize:          activity.fieldSize,
      fieldType:          activity.fieldType,
      fieldPhone:         activity.fieldPhone,
      fieldWebsite:       activity.fieldWebsite,
      title:              activity.title,
      privacy:            activity.privacyOnCreation,
      privacyOnCreation:  activity.privacyOnCreation,
      peopleIn:           activity.peopleInOnCreation, //peopleIn,
      peopleInOnCreation: activity.peopleInOnCreation, //peopleIn,
      followers:          _.union(_.pluck(activity.peopleInOnCreation, "userId"), activity.admin, [activity.createdBy]), // skips duplicate values
      description:        activity.description,
      repeat:             activity.repeat
   }
}*/
