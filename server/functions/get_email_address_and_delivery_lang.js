/*
DESCRIPTION: given the userId and notifType return email address and language
OUTPUT: {address, lang};
*/
/*
Note that you should only use a Method in the case where some code needs to be
callable from the client; if you just want to modularize code that is only going
to be called from the server, use a regular JavaScript function, not a Method.
*/
getEmailAddressAndDeliveryLang = function(userId, notifType) {

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Get Email Address And Delivery Language';
   var user = null;
   var notifTypeIsChecked = false;
   var primaryEmailIsSet = false;

   /////////////
   // QUERIES //
   /////////////

   user = Meteor.users.findOne({_id: userId}, {fields: {"profile.lang": 1, "emailNotifSettings": 1, "notifEmails": 1}});

   ////////////
   // CHECKS //
   ////////////

   if (!user) {
      throwError(context, 'user not registered');
      return undefined;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   primaryEmailIsSet = user.notifEmails && user.notifEmails.primary;
   notifTypeIsChecked = user.emailNotifSettings && user.emailNotifSettings[notifType] === true;

   ///////////
   // LOGIC //
   ///////////

   if (primaryEmailIsSet && notifTypeIsChecked) {
      return {
         address: user.notifEmails.primary,
         lang: user.profile.lang.value || 'en'
      }
   }
   return undefined;

}

/*
getEmailAddressAndDeliveryLang = function(userIds, notifType) {

   ///////////////
   // VARIABLES //
   ///////////////

   var langs = ['es', 'en', 'nl', 'de', 'it']; // TODO: introduce global languages array.
   var user = null;
   var notifTypeChecked = false;
   var primaryEmailSet = false;
   var lang = 'en';
   var output = {};
   // Setup output object
   _.each(langs, function(lang) {
      output[lang] = [];
   });
   var display = '';

   ///////////
   // LOGIC //
   ///////////

   _.each(userIds, function(userId) {
      // Query user email and language
      user = Meteor.users.findOne({_id: userId}, {fields: {"profile.lang": 1, "emailNotifSettings": 1, "notifEmails": 1}});
      // In case the primary email is set and the user have chosen to receive the given type of notifications...
      notifTypeChecked = user.emailNotifSettings[notifType] === true;
      primaryEmailSet = !_.isUndefined(user.notifEmails) && !_.isUndefined(user.notifEmails.primary);
      // ...add the address to the list of emails to be delivered
      if (notifTypeChecked && primaryEmailSet) {
         lang = user.profile.lang.value || 'en';
         output[lang].push(user.notifEmails.primary);
      }
   });

   // Display
   _.each(langs, function(lang) {
      display += 'to_' + lang + ': ' + output[lang] + ', ';
   });
   console.log(display);

   return output;
}
*/



/*
getLangAddressesArray = function(usersIds, notifType) { // TODO: remove exception

   var toES = [], toEN = [], toNL = [], toDE = [], toIT = [];
   var user = null;
   var addUserEmail = false
   var output = [];

   _.each(usersIds, function(userId) {
      //if (_.isUndefined(exception) || userId !== exception) { // TODO: remove exception

         // Query user emails and language
         user = Meteor.users.findOne(userId, {fields: {"profile.lang": 1, "emailNotifSettings": 1, "notifEmails": 1});

         // In case the primary email is set and the user have chosen to receive the given type of notifications...
         addUserEmail = user.emailNotifSettings[notifType] === true && !_.isUndefined(user.notifEmails) && !_.isUndefined(user.notifEmails.primary);

         // ...add his address to the list of emails
         if (addUserEmail) {
            switch (user.profile.lang.value) {
               case 'es':
                  toES.push(user.notifEmails.primary);
                  break;
               case 'nl':
                  toNL.push(user.notifEmails.primary);
                  break;
               case 'de':
                  toDE.push(user.notifEmails.primary);
                  break;
               case 'it':
                  toIT.push(user.notifEmails.primary);
                  break;
               default:
                  toEN.push(user.notifEmails.primary);
                  break;
            }
         }
      //}
   });

   // TODO: only return the group of email/lang that is not empty
   console.log('toES: ' + toES + ' ' + 'toEN: '+ toEN + ' ' + 'toDE: '+ toDE + ' ' + 'toNL: '+ toNL + ' ' + 'toIT: '+ toIT);

   output
   //return [{lang: 'es', addresses: toES}, {lang: 'en', addresses: toEN}, {lang: 'nl', addresses: toNL}, {lang: 'de', addresses: toDE}, {lang: 'it', addresses: toIT}];
}
*/


/*
getEmailsTo = function(playersIds, condition, exception) {

   var toES = [];
   var toEN = [];
   var toNL = [];
   var toDE = [];
   var user;
   var options = {
      fields: {
         "profile.lang": 1,
         emailNotifSettings: 1,
         notifEmails: 1
      }
   };

   _.each(playersIds, function(recipientId) {
      if (_.isUndefined(exception) || recipientId !== exception) {
         user = Meteor.users.findOne(recipientId, options);
         if (user.emailNotifSettings[condition] === true && !_.isUndefined(user.notifEmails) && !_.isUndefined(user.notifEmails.primary)) {
            switch (user.profile.lang.value) {
               case 'es':
                  toES.push(user.notifEmails.primary);
                  break;
               case 'nl':
                  toNL.push(user.notifEmails.primary);
                  break;
               case 'de':
                  toDE.push(user.notifEmails.primary);
                  break;
               default:
                  toEN.push(user.notifEmails.primary);
                  break;
            }
         }
      }
   });

   console.log('toES: ' + toES + ' ' + 'toEN: '+ toEN + ' ' + 'toDE: '+ toDE + ' ' + 'toNL: '+ toNL);
   return [{lang: 'es', addresses: toES}, {lang: 'en', addresses: toEN}, {lang: 'nl', addresses: toNL}, {lang: 'de', addresses: toDE}];
}
*/
