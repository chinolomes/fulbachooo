// SERVER SIDE:
/*
 * Users are by default allowed to specify their own profile field with
 * Accounts.createUser and modify it with Meteor.users.update. To allow
 * users to edit additional fields, use Meteor.users.allow. To forbid
 * users from making any modifications to their user document:
 * Meteor.users.deny({update: function () { return true; }});
 *
 * Source: http://docs.meteor.com/#/basic/Meteor-users
 */

// TODO: remove collection hooks --> set userId inside the onCreated fucntion
// and then insert the new user into the other collections
//======================================================================
// ON ACCOUNTS CREATION:
//======================================================================
// TODO: add email account
//----------------------------------------------------------------------
Accounts.onCreateUser(function (options, user) {

	console.log('sign up time: '+ new Date());

	// Twitter
	if (user.services.twitter) {
		console.log('twitter sign up');
		console.log('name: '+ user.services.twitter.screenName);
		console.log('avatar: '+ user.services.twitter.profile_image_url);
		user.profile = {
			name: user.services.twitter.screenName,
			avatar: user.services.twitter.profile_image_url,
			//birthday: '?',
			//gender: '?',
			//fieldPosition: '?',
			//dominantLeg: '?',
			//selfDescription: ''
			sports: []
		};
		user.notifEmails = {
			verified: [],
			sent: [],
			primary: '',
			verificationToken: {
				token: '',
				userId: '',
				address: '',
				expiresAt: new Date()
			}
		};
		/*user.private = {
			//password: null,
			//email: null
		};*/
	}

	// Facebook
	else if (user.services.facebook) {
		console.log('facebook sign up');
		console.log('name: '+ user.services.facebook.name);
		console.log('avatar: '+ user.services.facebook.id);
		console.log('gender: '+ user.services.facebook.gender);
		console.log('email: '+ user.services.facebook.email);
		user.profile = {
			name: user.services.facebook.name,
			avatar: 'http://graph.facebook.com/' + user.services.facebook.id + '/picture/?type=large',
			//birthday: '?',
			gender: user.services.facebook.gender,
		  	//fieldPosition: '?',
		  	//dominantLeg: '?',
		  	//selfDescription: ''
			sports: []
		};
		user.notifEmails = {
			verified: [user.services.facebook.email],
			sent: [],
			primary: user.services.facebook.email,
			verificationToken: {
				token: '',
				userId: '',
				address: '',
				expiresAt: new Date()
			}
		};
		/*user.private = {
			//password: null,
			email: user.services.facebook.email
		};*/
		/*user.notifEmails.push({
			address: user.services.facebook.email,
			verified: true,
 			primary: true
		});*/
	}

	// Google+
	else if (user.services.google) {
		console.log('google+ sign up');
		console.log('name: '+ user.services.google.name);
		console.log('avatar: '+ user.services.google.picture);
		console.log('gender: '+ user.services.google.gender);
		console.log('email: '+ user.services.google.email);
		user.profile = {
			name: user.services.google.name,
			avatar: user.services.google.picture,
			//birthday: '?',
			gender: !_.isUndefined(user.services.google.gender) ? user.services.google.gender : 'undefined',
		  	//fieldPosition: '?',
		  	//dominantLeg: '?',
		  	//selfDescription: ''
			sports: []
		};
		user.notifEmails = {
			verified: [user.services.google.email],
			sent: [],
			primary: user.services.google.email,
			verificationToken: {
				token: '',
				userId: '',
				address: '',
				expiresAt: new Date()
			}
		};
		/*user.private = {
			//password: null,
			email: user.services.google.email
		};*/
		/*user.notifEmails.push({
			address: user.services.google.email,
			verified: true,
 			primary: true
		});*/
	}

	// Meteor
	else if (user.services['meteor-developer']) {
		console.log('meteor sign up');
		console.log('name: '+ user.services['meteor-developer'].username);
		console.log('email: '+ user.services['meteor-developer'].emails[0].address);
		user.profile = {
			name: user.services['meteor-developer'].username,
			avatar: 'https://www.meteor.com/meteor-logo.png',
			//birthday: '?',
			//gender: 'male',
		  	//fieldPosition: '?',
		  	//dominantLeg: '?',
		  	//selfDescription: ''
			sports: []
		};
		user.notifEmails = {
			verified: [user.services['meteor-developer'].emails[0].address],
			sent: [],
			primary: user.services['meteor-developer'].emails[0].address,
			verificationToken: {
				token: '',
				userId: '',
				address: '',
				expiresAt: new Date()
			}
		};
		/*user.private = {
			//password: null,
			email: user.services['meteor-developer'].emails[0].address
		};*/
		/*user.notifEmails.push({
			address: user.services['meteor-developer'].emails[0].address,
			verified: true,
 			primary: true
		});*/
	}


	// Password
	else {
		throw new Meteor.Error('Server Accounts Creation: email account registration is not implemented yet! Please, use Facebook Login :)');
		return;
		/*
		var email = user.emails[0].address;
		var index = _.indexOf(email, '@');
		var name = email.substr(0, index);
		user.profile = {
			name: name,
			avatar: '/default_avatar2.jpg',
			birthday: '?',
			gender: '?',
		  	fieldPosition: '?',
		  	dominantLeg: '?',
		  	selfDescription: ''
		};
		user.private = {
			password: null,
			email: email
		};
		// Send confirmation email
		*/
	}

	// Common to all services
	/*var data = {'latitude': DEFAULT_LAT, 'longitude': DEFAULT_LNG}; // Enschede
	user.geo = {
		//language: 'en',
		//circleCenterLat: data.latitude,
		//circleCenterLng: data.longitude,
		radius: 2,
		zoom: 10,
		loc: {
			coordinates: [data.longitude, data.latitude]
		}
	};*/
	user.geo = {
		radius: DEFAULT_RADIUS,
		zoom: DEFAULT_ZOOM,
		loc: {
			coordinates: [DEFAULT_LNG, DEFAULT_LAT]
		}
	};
	user.lastLogin = new Date();
	user.ip = Meteor.call('getCurUserIP');
	user.matches = {
		public: {
			created: [],
			played: [],
			waiting: [],
			admin: []
		},
		private: {
			created: [],
			played: [],
			waiting: [],
			admin: []
		}
	};
	var userActivities = {
		created: [],
		enrolled: [],
		waiting: [],
		admin: [],
		following: [],
		friendIsEnrolled: []
	}
	user.activities = {
		upcoming: {
			public: userActivities,
			private: userActivities
		},
		past: {
			public: userActivities,
			private: userActivities
		}
	};
	user.emailNotifSettings = {
		gameNewGamePlayingZone: true,
		gameNewComment: false,
		gameWasCanceled: false,
		gameAddRemovePlayer: false,
		gameAddedMe: false,
		gamePassFromWaitingToPlaying: false,
		gameNewGameRepeat: false, /*,
		organizerAddRemovePlayer: false,
		organizerNewComment: false*/
	}

	return user;
});
