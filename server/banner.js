// SOURCE:
// http://docs.meteor.com/#/full/accounts_validateloginattempt
// http://stackoverflow.com/questions/22702305/banning-system-with-meteor-accounts
// In order to ban a user, just add the 'isBanned' field to its profile with value equals 'true' (boolean)
//=================================================//
// USER BANNER (BLACKLIST)
//=================================================//
Accounts.validateLoginAttempt(function(info) {

   ///////////////
   // VARIABLES //
   ///////////////

   var user = info.user;
   var isBanned = !_.isUndefined(user) && !_.isUndefined(user.isBanned) && user.isBanned === true;

   ///////////
   // LOGIC //
   ///////////

   if (!isBanned) {
      return true;
   }
   console.log('BANNED USER!!!! userId: ', user._id);
   return false;

});

//=================================================//
// IP BANNER (BLACKLIST)
//=================================================//
// SOURCE:
// https://forums.meteor.com/t/how-do-you-restrict-access-to-a-meteor-site-based-on-ip/6513
// http://docs.meteor.com/#/full/meteor_onconnection
/*Meteor.onConnection(function (connection) {
   console.log('CONNECTION: ', connection.clientAddress);
   // Check if connected client has their IP banned
   if (BannedIPs.findOne({ip: connection.clientAddress})) {
      console.log('BANNED IP CONNECTION: ', connection.clientAddress);
      // Close/deny connection
      //connection.close();
      //Meteor.disconnect();
   }
})*/

/*var Fiber = Npm.require('fibers');
var self = this;
Meteor.onConnection(function (connection) {
   console.log('CONNECTION: ', connection.clientAddress);
   // Check if connected client has their IP banned
   if (BannedIPs.findOne({ip: connection.clientAddress})) {
      console.log('BANNED IP CONNECTION: ', connection.clientAddress);
      // Close/deny connection
      //connection.close();

      // From Sikka
      Fiber(function() {
         // ask to reload the page or cordova app
         self.send({
           msg: "added",
           collection: "sikka-commands",
           id: "reload",
           fields: {}
         });

         // Don't close the socket.
         // Just ignore the load.
         // If we try to close the socket, it'll try to reconnect again.
         // That leads to a lot of requests and make the DOS attempt worst
         self.socket.removeAllListeners('data');
      }).run();
   }
})*/
