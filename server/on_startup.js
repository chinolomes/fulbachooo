/*
'Meteor.startup' allows you to register a callback that will get executed each time the server is (re)started.
*/
//======================================================================
// reCAPTCHA:
//======================================================================
Meteor.startup(function() {
   reCAPTCHA.config({
      privatekey: '6LcmtAcTAAAAAHi0wI_c4R39TuIWpLJYNuCggK94'
   });
});

//======================================================================
// CRON JOBS:
//======================================================================
Meteor.startup(function() {
	SyncedCron.start();
});

//======================================================================
// SET COLLECTIONS COUNTER IF NOT SET YET:
//======================================================================
// CollectionsCounter keeps track of the number of documents for the given collection name
Meteor.startup(function() {

   ///////////////
   // VARIABLES //
   ///////////////

	var collName = 'users';
	var doc = CollectionsCounter.findOne({collectionName: collName});
	var nUsers = Meteor.users.find({}, {fields: {"_id": 1}}).count();

   ///////////
   // LOGIC //
   ///////////

   // In case there is no document in the CollectionsCounter associated to the users
   // collection, add it. Otherwise, update it.
	if (!doc) {
		console.log('INSERTING COLL COUNTER USERS');
		CollectionsCounter.insert({collectionName: collName, counter: nUsers});
	} else {
		console.log('UPDATING COLL COUNTER USERS');
		CollectionsCounter.update({collectionName: collName}, {$set: {counter: nUsers}});
	}

});
//----------------------------------------------------------------------
Meteor.startup(function() {

   ///////////////
   // VARIABLES //
   ///////////////

	var collName = 'upcomingActivities';
	var doc = CollectionsCounter.findOne({collectionName: collName});
	var nUpcomingActivities = Matches.find({status: {$ne: 'finished'}, privacy: 'public'}, {fields: {"_id": 1}}).count();

   ///////////
   // LOGIC //
   ///////////

   // In case there is no document in the CollectionsCounter associated to the (upcoming) activities
   // collection, add it. Otherwise, update it.
	if (!doc) {
		console.log('INSERTING COLL COUNTER ACTIVITIES');
		CollectionsCounter.insert({collectionName: collName, counter: nUpcomingActivities});
	} else {
		console.log('UPDATING COLL COUNTER ACTIVITIES');
		CollectionsCounter.update({collectionName: collName}, {$set: {counter: nUpcomingActivities}});
	}

});

//======================================================================
// IP GEO-CODER:
//======================================================================
// somewhere on the server
/* Meteor.startup(function() {
   IPGeocoder.load();
}); */
