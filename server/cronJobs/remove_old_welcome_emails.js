// CHECKED
//======================================================================
// REMOVE ALREADY SENT WELCOME EMAILS:
//======================================================================
SyncedCron.add({
	name: 'Remove old welcome emails',
	schedule: function(parser) {
		return parser.text('at 7:00 am');
		//return parser.text('every 1 minutes');
	},
	job: function() {
		WelcomeEmails.remove({sent: true});
	}
});
