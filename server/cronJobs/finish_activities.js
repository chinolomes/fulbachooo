// CHECKED
//======================================================================
// SET OLD ACTIVITIES TO FINISHED:
//======================================================================
SyncedCron.add({
	name: 'Set old activities to finished',
	schedule: function(parser) {
		return parser.text('at 6:00 am');
		//return parser.text('every 1 minutes');
	},
	job: function() {

		///////////////
		// VARIABLES //
		///////////////

		var now = new Date();
		var yesterday = dayBefore(now);
		var selector = {
			date: {
				$lt: yesterday
			},
			status: {
				$ne: 'finished'
			}
		};
		var newActivity = {};
		var newActivityId = '';

		///////////
		// LOGIC //
		///////////

		Matches.find(selector).forEach(function(oldActivity) {

		   // Set current activity to finished
		   Matches.update({_id: oldActivity._id}, {$set: {status: 'finished'}});

		   // Update owner, admin, paticipants and followers docs
		   Meteor.call('updateUserDocMoveActivityIdFromUpcomingToPast', oldActivity._id);

		   // If the 'repeat' field isn't set to 'true', skip to the next iteration
		   if (!oldActivity.repeat || oldActivity.repeat !== true) {
		      return; // continues to the next iteration
		   }

		   // Create a new activity for next week using the old activity values
		   newActivity = recreateActivityForNextWeek(oldActivity);
		   newActivityId = Matches.insert(newActivity);
		   Meteor.call('attachActivityIdToOwnerAdminAndPartcipants', newActivityId);
		   Meteor.call('notifyParticipantsEnrolledInNewActivity', newActivityId);
			Meteor.call('notifyOwnerActivityRecreatedAutomatically', newActivityId);

		   // In case the activity has admin, notify him
		   if (oldActivity.admin[0]) { // might be undefined or null
		      Meteor.call('notifyAdminNamedInNewActivity', newActivityId);
		   }

		   // In case the activity is public, notify users in nearby region (except owner, admin and participants)
		   if (oldActivity.privacyOnCreation === 'public') {
		      Meteor.call('notifyUsersInNearbyRegion', newActivityId);
		   }

		}); // end forEach
	}
});
