/*
DESCRIPTION: queries the list of internal notifications for each user, removing
the oldest notifications in case the number of notifications is grater than 30
*/
// CHECKED
//======================================================================
// REMOVE OLD NOTIFICATIONS:
//======================================================================
SyncedCron.add({
	name: 'Remove old notifications',
	schedule: function(parser) {
		return parser.text('at 5:25 am');
		//return parser.text('every 1 minutes');
	},
	job: function() {
	   Meteor.users.find({}, {fields: {"_id": 1}}).forEach(function(doc) {
			// doc = {_id: 'userId'}
			Meteor.call('removeOldNotifications', doc._id);
		});
	}
});
