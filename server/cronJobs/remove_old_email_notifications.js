// CHECKED
//======================================================================
// REMOVE OLD EMAIL NOTIFICATIONS:
//======================================================================
SyncedCron.add({
	name: 'Remove old email notifications',
	schedule: function(parser) {
		return parser.text('at 5:00 am');
		//return parser.text('every 1 minutes');
	},
	job: function() {
		Emails.remove({sent: true});
	}
});
