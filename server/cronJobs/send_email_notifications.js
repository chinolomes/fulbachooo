/*
DESCRIPTION: queries the list of emial notifications, process that data to get individual 'chunks'
and finally, delivers each of the individual emails
*/
/* TODO: modify email structure:
obj = {
   _id: String,
   recipient: String,
   lang: 'en/es/...',
   sent: Boolean,
   data: [{
      sender: String,
      text: String,
      url: String
   }, {}, ...]
}
*/
// CHECKED
//======================================================================
// SEND EMAIL NOTIFICATIONS:
//======================================================================
/* SyncedCron.add({
	name: 'Send email notifications',
	schedule: function(parser) {
		return parser.text('every 30 minutes');
		//return parser.text('every 1 minutes');
	},
	job: function() {

      ///////////////////
		// AUX FUNCTIONS //
		///////////////////

		// obj = {_id: String, recipient: String, sender: [String], text: [String], url: [String], lang: 'en/es/...', sent: Boolean, length: Number}
		var getChunks = function(obj) {
		   // VARIABLES
		   var lang = obj.lang;
		   var length = obj.length;
		   var textArray = obj.text;
		   var senderArray = obj.sender;
		   var urlArray = obj.url;
		   var chunks = [];
		   var i = 0;

		   // LOGIC
		   for (i = 0; i < length; i++) {
		   	chunks.push({
		         text: TAPi18n.__(textArray[i], {createdBy: senderArray[i]}, lang_tag = lang),
		         link: urlArray[i]
		      });
		   }
		   return chunks;
		}

		///////////////
		// VARIABLES //
		///////////////

		var conext = 'Notification Emails Dispatcher';
		var notificationEmails = [];
		var params = {};

		/////////////
		// QUERIES //
		/////////////

		notificationEmails = Emails.find({sent: false}).fetch();

		///////////
		// LOGIC //
		///////////

		_.each(notificationEmails, function(obj) { // obj = {_id: String, recipient: String, sender: [String], text: [String], url: [String], lang: 'en/es/...', sent: Boolean, length: Number}
		   // TODO: the following lines, can be all put together within a single method?
		   // Set data to send email
			params = {
				to: obj.recipient,
				lang: obj.lang,
				chunks: getChunks(obj)
			}
			// Send all emails associated to the same recipient
			Meteor.call('sendNotifEmail', params, function(err) {
				if (err) {
					console.log(err);
				} else {
		         // TODO: can't be inside the method obove?
					Emails.update({_id: obj._id, sent: false}, {$set: {sent: true}});
				}
			});
		});
	}
}); */
