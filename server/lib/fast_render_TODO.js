// TODO: https://meteorhacks.com/fast-render/api/
// fast-render can't be user component-level:
// https://forums.meteor.com/t/seo-best-practices-for-react-flowrouter-project/14365
/*
FastRender.route('/post/:_id', function(params) {
  this.subscribe('blogPost', params._id);
})

FastRender.onAllRoutes(function(path) {
  this.subscribe('currentUser');
})

this.userId is available inside FastRender.route, FastRender.onAllRoutes and the
Meteor publications(with this.subscribe).
*/
