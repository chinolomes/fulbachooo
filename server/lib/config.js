//======================================================================
// SET ACCOUNTS CONFIGURATION:
//======================================================================
// SOURCE: https://atmospherejs.com/meteor/service-configuration
// SOURCE: http://guide.meteor.com/security.html#served-files
// TODO: get all services via Meteor.settings.services (modify settings.json for
// this) using 'for key in services'... and then use _.each to loop and set every
// service (handy when deploying to sandbox / production) BE CAREFUL WITH Meteor
// MUST ADD '-developer' to service name
// See settings.json
//----------------------------------------------------------------------
ServiceConfiguration.configurations.upsert(
   {service: "facebook"},
   {$set: {
      appId: Meteor.settings.facebook.clientId,
      loginStyle: "redirect",
      secret: Meteor.settings.facebook.secret}
   }
);

ServiceConfiguration.configurations.upsert(
   {service: "twitter"},
   {$set: {
      consumerKey: Meteor.settings.twitter.clientId,
      loginStyle: "redirect",
      secret: Meteor.settings.twitter.secret}
   }
);

ServiceConfiguration.configurations.upsert(
   {service: "google"},
   {$set: {
      clientId: Meteor.settings.google.clientId,
      loginStyle: "redirect",
      secret: Meteor.settings.google.secret}
   }
);

ServiceConfiguration.configurations.upsert(
   {service: "meteor-developer"},
   {$set: {
      clientId: Meteor.settings.meteor.clientId,
      loginStyle: "redirect",
      secret: Meteor.settings.meteor.secret}
   }
);

//======================================================================
// MAILGUN CONFIGURATION:
//======================================================================
// See settings.json
//----------------------------------------------------------------------
Meteor.startup(function() {

   Meteor.Mailgun.config({
      username: Meteor.settings.mailgun.username,
      password: Meteor.settings.mailgun.password
   });

});

//======================================================================
// BROWSER POLICY CONFIGURATION:
//======================================================================
Meteor.startup(function() {

   BrowserPolicy.framing.disallow();
   BrowserPolicy.content.disallowInlineScripts();
   BrowserPolicy.content.disallowEval();


   // console.log('BrowserPolicy', BrowserPolicy);
   // BrowserPolicy.content.allowFrameOrigin("https://syndication.twitter.com/");

   //BrowserPolicy.content.allowInlineStyles();
   BrowserPolicy.content.allowOriginForAll("*.googleapis.com");
   BrowserPolicy.content.allowOriginForAll("*.google.com");
   BrowserPolicy.content.allowOriginForAll("www.google-analytics.com");
   BrowserPolicy.content.allowOriginForAll("*.gstatic.com");
   BrowserPolicy.content.allowOriginForAll("platform.twitter.com");
   BrowserPolicy.content.allowOriginForAll("connect.facebook.net");
   BrowserPolicy.content.allowOriginForAll("graph.facebook.com");
   BrowserPolicy.content.allowOriginForAll("https://fbcdn-profile-a.akamaihd.net");
   BrowserPolicy.content.allowOriginForAll("https://*.googleusercontent.com");
   BrowserPolicy.content.allowOriginForAll("*.twimg.com");
   BrowserPolicy.content.allowOriginForAll("https://secure.gravatar.com");
   BrowserPolicy.content.allowOriginForAll("http://i0.wp.com");
   BrowserPolicy.content.allowOriginForAll("https://www.meteor.com");
   BrowserPolicy.content.allowOriginForAll("https://apis.google.com");
   BrowserPolicy.content.allowOriginForAll("http://www.meteovista.co.uk");
   BrowserPolicy.content.allowOriginForAll("https://query.yahooapis.com");
   BrowserPolicy.content.allowOriginForAll("google-maps-utility-library-v3.googlecode.com");
   BrowserPolicy.content.allowOriginForAll("https://scontent.xx.fbcdn.net");
   BrowserPolicy.content.allowOriginForAll("https://enginex.kadira.io");
   BrowserPolicy.content.allowOriginForAll("https://fbstatic-a.akamaihd.net");
   BrowserPolicy.content.allowOriginForAll('http://staticxx.facebook.com');
   //BrowserPolicy.content.allowOriginForAll("cdn.jsdelivr.net/jquery.scrollto/2.1.2/jquery.scrollTo.min.js");
   //BrowserPolicy.content.allowOriginForAll("cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js");

   BrowserPolicy.content.allowEval("http://csi.gstatic.com");
   BrowserPolicy.content.allowEval("www.google-analytics.com");
   BrowserPolicy.content.allowEval("http://fonts.googleapis.com");
   BrowserPolicy.content.allowEval("http://maps.google.com");
   BrowserPolicy.content.allowEval("http://maps.gstatic.com");
   BrowserPolicy.content.allowEval("http://csi.gstatic.com");

   //BrowserPolicy.content.allowInlineScripts("*.facebook.com");
   //BrowserPolicy.content.allowEval("cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js");

   //BrowserPolicy.content.allowInlineScripts("platform.twitter.com/widgets.js");
   //BrowserPolicy.content.allowInlineScripts("cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.0/jquery.scrollTo.min.js");

   BrowserPolicy.content.allowImageOrigin("https://fbstatic-a.akamaihd.net");
   BrowserPolicy.content.allowImageOrigin("https://stats.g.doubleclick.net");
   BrowserPolicy.content.allowImageOrigin("https://static.xx.fbcdn.net");
   BrowserPolicy.content.allowImageOrigin("https://*.fbcdn.net");
   BrowserPolicy.content.allowImageOrigin("https://*.twitter.com");
   BrowserPolicy.content.allowImageOrigin("*.wp.com");
   BrowserPolicy.content.allowImageOrigin("google-maps-utility-library-v3.googlecode.com");
   BrowserPolicy.content.allowImageOrigin('*.facebook.com');
   BrowserPolicy.content.allowImageOrigin('https://*.fbsbx.com');

   BrowserPolicy.content.disallowConnect();

   // Allow Meteor DDP Connections
   var rootUrl = __meteor_runtime_config__.ROOT_URL;
   console.log('ROOT_URL: ' + rootUrl);

   BrowserPolicy.content.allowConnectOrigin(rootUrl);
   BrowserPolicy.content.allowConnectOrigin(rootUrl.replace(/http(s?)/, 'ws$1'));
   BrowserPolicy.content.allowConnectOrigin("https://enginex.kadira.io");
   BrowserPolicy.content.allowConnectOrigin("https://connect.facebook.net");

   //BrowserPolicy.content.allowScriptOrigin("https://connect.facebook.net");

  BrowserPolicy.content.allowInlineScripts("https://connect.facebook.net/en_US/fbevents.js");
  //BrowserPolicy.content.allowInlineScripts(rootUrl + 'libs/marker-clusterer/src/markerclusterer.js');

});

//======================================================================
// VERIFICATION EMAIL CONFIGURATION:
//======================================================================
Meteor.startup(function() {
  // By default, the email is sent from no-reply@meteor.com. If you wish to receive email from users asking for help with their account, be sure to set this to an email address that you can receive email at.
  Accounts.emailTemplates.from = 'fulbacho.net <no-reply@fulbacho.net>';

  // The public name of your application. Defaults to the DNS name of the application (eg: awesome.meteor.com).
  Accounts.emailTemplates.siteName = 'fulbacho.net';

  // A Function that takes a user object and returns a String for the subject line of the email.
  Accounts.emailTemplates.verifyEmail.subject = function(user) {
    return 'Confirm Your Email Address';
  };

  // A Function that takes a user object and a url, and returns the body text for the email.
  // Note: if you need to return HTML instead, use Accounts.emailTemplates.verifyEmail.html
  Accounts.emailTemplates.verifyEmail.text = function(user, url) {
    return 'click on the following link to verify your email address: ' + url;
  };

  Accounts.emailTemplates.verifyEmail.html = function(user, url) {
    return 'click on the following link to verify your email address: ' + url;
  };
});
