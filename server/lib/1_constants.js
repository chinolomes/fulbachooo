//=============================================
// CONSTANTS
//=============================================
// IMPORTANT: KEEP IT IN SYNC WITH client/lib/1_constants.js and lib/schemas/0_constants.js

/* PAGINATION RESULTS */
LIMIT = 10;
LOAD_MORE = 15;

/* GEO */
DEFAULT_LAT = 52.23587518623051; // Enschede
DEFAULT_LNG = 6.868286104872823; // Enschede
DEFAULT_RADIUS = 7;
DEFAULT_ZOOM = 12;

/* SPORTS */
// README: to add a new sport, add the name of the sport in the SPORTS_ARRAY,
// add the new icon in the public folder, and set the sport in i18n
SPORTS_ARRAY = [
  'archery',
  'badminton',
  'baseball',
  'basketball',
  'bbq',
  'b-fit',
  'bike-polo',
  'bootcamp',
  'bowling',
  'climbing',
  'cricket',
  'cycling',
  'football',
  'frisbee',
  'hockey',
  'horse-riding',
  'icehockey',
  'iceskating',
  'karting',
  'lasertag',
  'paddle',
  'paintball',
  'pingpong',
  'running',
  'shotokan-karate',
  'squash',
  'tennis',
  'trekking',
  'volley',
];
DEFAULT_SPORT = 'football';

/* LANGUAGES */
LANGS = ['es', 'en', 'nl', 'de', 'it'];

/* NOTIFICATIONS */
//NOTIF_LIMIT = 20;
