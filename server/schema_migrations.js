//======================================================================
// PRE-POPULATE PLAYERS LIST:
//======================================================================
// v21
/*Meteor.startup(function() {

   var docs = [
      {
         companyName: 'drienerlo',
         mobileImg: 'drienerloMobile.gif',
         desktopImg: 'drienerloDesktop.gif',
         anchor: '/activities/activity/#',
         loc: {coordinates: [6.8750212, 52.2067013]}, // lng, lat
         active: false
      }
   ];

   console.log('typeof(Ads): ', typeof(Ads));
	var ok = !!Ads && Ads instanceof Meteor.Collection; // && typeof(Ads) === ''
	if (ok) {
      docs.forEach(function(doc) {
         //console.log('_.keys(doc): ', _.keys(doc));
         if (!Ads.findOne({companyName: doc.companyName})) { // avoid inserting duplicate docs
            console.log('ADDING AD: ', doc.companyName);
            Ads.insert(doc);
         }
      });
	}

});*/


// For each user get the list of matches in which he is involved and update user.activities
// Also keep track of each operation (sign in/out for/from a game, create game, etc) in user.activities
// V19
// Remove Feedbacks collection
/*Meteor.startup(function(){
   var globalObject = Meteor.isClient ? window : global;
   for (var property in globalObject) {
      var object = globalObject[property];
      if (object instanceof Meteor.Collection && object._name === 'feedback') {
			//console.log('_.keys(object): ', _.keys(object));
			console.log('object._name: ', object._name);
			object.remove({});
      }
   }
});*/

// What happens if we run this operation multiple times (regarding addToSet)
/*Meteor.startup(function() {

   var options = {
      "privacy": 1,
      "status": 1
   };
   // Loop over all users
	Meteor.users.find().forEach(function(user) {
		// find games for which the user is the owner
		var userId = user._id;
		console.log('SETTING UP ACTIVITIES FOR USER: ', userId);

		var owner = Matches.find({createdBy: userId}, options).fetch();
		var ownerPublicUpcoming = _.pluck(_.filter(owner, function(obj) {return obj.privacy === 'public' && obj.status !== 'finished'}), '_id');
		var ownerPrivateUpcoming = _.pluck(_.filter(owner, function(obj) {return obj.privacy === 'private' && obj.status !== 'finished'}), '_id');
		var ownerPublicPast = _.pluck(_.filter(owner, function(obj) {return obj.privacy === 'public' && obj.status === 'finished'}), '_id');
		var ownerPrivatePast = _.pluck(_.filter(owner, function(obj) {return obj.privacy === 'private' && obj.status === 'finished'}), '_id');

		var played = Matches.find({'peopleIn.userId': {$in: [userId]}}, options).fetch();
		var playedPublicUpcoming = _.pluck(_.filter(played, function(obj) {return obj.privacy === 'public' && obj.status !== 'finished'}), '_id');
		var playedPrivateUpcoming = _.pluck(_.filter(played, function(obj) {return obj.privacy === 'private' && obj.status !== 'finished'}), '_id');
		var playedPublicPast = _.pluck(_.filter(played, function(obj) {return obj.privacy === 'public' && obj.status === 'finished'}), '_id');
		var playedPrivatePast = _.pluck(_.filter(played, function(obj) {return obj.privacy === 'private' && obj.status === 'finished'}), '_id');

		var waiting = Matches.find({'waitingList.userId': {$in: [userId]}}, options).fetch();
		var waitingPublicUpcoming = _.pluck(_.filter(waiting, function(obj) {return obj.privacy === 'public' && obj.status !== 'finished'}), '_id');
		var waitingPrivateUpcoming = _.pluck(_.filter(waiting, function(obj) {return obj.privacy === 'private' && obj.status !== 'finished'}), '_id');
		var waitingPublicPast = _.pluck(_.filter(waiting, function(obj) {return obj.privacy === 'public' && obj.status === 'finished'}), '_id');
		var waitingPrivatePast = _.pluck(_.filter(waiting, function(obj) {return obj.privacy === 'private' && obj.status === 'finished'}), '_id');

		var admin = Matches.find({'admin': {$in: [userId]}}, options).fetch();
		var adminPublicUpcoming = _.pluck(_.filter(admin, function(obj) {return obj.privacy === 'public' && obj.status !== 'finished'}), '_id');
		var adminPrivateUpcoming = _.pluck(_.filter(admin, function(obj) {return obj.privacy === 'private' && obj.status !== 'finished'}), '_id');
		var adminPublicPast = _.pluck(_.filter(admin, function(obj) {return obj.privacy === 'public' && obj.status === 'finished'}), '_id');
		var adminPrivatePast = _.pluck(_.filter(admin, function(obj) {return obj.privacy === 'private' && obj.status === 'finished'}), '_id');

		var following = Matches.find({'followers': {$in: [userId]}}, options).fetch();
		var followingPublicUpcoming = _.pluck(_.filter(following, function(obj) {return obj.privacy === 'public' && obj.status !== 'finished'}), '_id');
		var followingPrivateUpcoming = _.pluck(_.filter(following, function(obj) {return obj.privacy === 'private' && obj.status !== 'finished'}), '_id');
		var followingPublicPast = _.pluck(_.filter(following, function(obj) {return obj.privacy === 'public' && obj.status === 'finished'}), '_id');
		var followingPrivatePast = _.pluck(_.filter(following, function(obj) {return obj.privacy === 'private' && obj.status === 'finished'}), '_id');

		var friendIsEnrolled = Matches.find({'friendsOf.userId': {$in: [userId]}}, options).fetch();
		var friendIsEnrolledPublicUpcoming = _.pluck(_.filter(friendIsEnrolled, function(obj) {return obj.privacy === 'public' && obj.status !== 'finished'}), '_id');
		var friendIsEnrolledPrivateUpcoming = _.pluck(_.filter(friendIsEnrolled, function(obj) {return obj.privacy === 'private' && obj.status !== 'finished'}), '_id');
		var friendIsEnrolledPublicPast = _.pluck(_.filter(friendIsEnrolled, function(obj) {return obj.privacy === 'public' && obj.status === 'finished'}), '_id');
		var friendIsEnrolledPrivatePast = _.pluck(_.filter(friendIsEnrolled, function(obj) {return obj.privacy === 'private' && obj.status === 'finished'}), '_id');

		var addToSet = {
			$set: {
				'activities.upcoming.public.created': ownerPublicUpcoming,
				'activities.upcoming.public.enrolled': playedPublicUpcoming,
				'activities.upcoming.public.waiting': waitingPublicUpcoming,
				'activities.upcoming.public.admin': adminPublicUpcoming,
				'activities.upcoming.public.following': followingPublicUpcoming,
				'activities.upcoming.public.friendIsEnrolled': friendIsEnrolledPublicUpcoming,
				'activities.upcoming.private.created': ownerPrivateUpcoming,
				'activities.upcoming.private.enrolled': playedPrivateUpcoming,
				'activities.upcoming.private.waiting': waitingPrivateUpcoming,
				'activities.upcoming.private.admin': adminPrivateUpcoming,
				'activities.upcoming.private.following': followingPrivateUpcoming,
				'activities.upcoming.private.friendIsEnrolled': friendIsEnrolledPrivateUpcoming,
				'activities.past.public.created': ownerPublicPast,
				'activities.past.public.enrolled': playedPublicPast,
				'activities.past.public.waiting': waitingPublicPast,
				'activities.past.public.admin': adminPublicPast,
				'activities.past.public.following': followingPublicPast,
				'activities.past.public.friendIsEnrolled': friendIsEnrolledPublicPast,
				'activities.past.private.created': ownerPrivatePast,
				'activities.past.private.enrolled': playedPrivatePast,
				'activities.past.private.waiting': waitingPrivatePast,
				'activities.past.private.admin': adminPrivatePast,
				'activities.past.private.following': followingPrivatePast,
				'activities.past.private.friendIsEnrolled': friendIsEnrolledPrivatePast
			}
		};
		console.log('ADD TO SET DONE!');
		Meteor.users.update({_id: userId}, addToSet);
	});

});*/



// PRODUCTION V15
//======================================================================
// DELETE OLD NOTIFICATIONS:
//======================================================================
/*Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.welcome)) {
			console.log('SETTING WELCOME FIELD ON USER, userId: ', user._id);
			Meteor.users.update({_id: user._id}, {$set: {welcome: false}});
			// TODO: add userId to welcomeEmails
		}
	});

});

Meteor.startup(function() {

	Notifications.update({text: 'NOTIFICATIONS_Insert_player_in_match'}, {$set: {text: 'User_Signed_In_For_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 1!');
	Notifications.update({text: 'NOTIFICATIONS_Insert_friend_of_in_match'}, {$set: {text: 'Friend_Signed_In_For_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 2!');
	Notifications.update({text: 'NOTIFICATIONS_Remove_player_from_match'}, {$set: {text: 'User_Signed_Out_Of_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 3!');
	Notifications.update({text: 'NOTIFICATIONS_Remove_friend_of_from_match'}, {$set: {text: 'Friend_Signed_Out_Of_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 4!');
	Notifications.update({text: 'NOTIFICATIONS_Confirm_your_presence_in_the_match'}, {$set: {text: 'You_Passed_From_Waiting_List_To_The_List_Of_Participants_Confirm_Presence'}}, {multi: true});
	console.log('NOTIF 5!');
	Notifications.update({text: 'NOTIFICATIONS_Insert_player_in_waiting_list'}, {$set: {text: 'User_Added_To_The_Waiting_List_Of_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 6!');
	Notifications.update({text: 'NOTIFICATIONS_Remove_player_from_waiting_list'}, {$set: {text: 'User_Removed_From_The_Waiting_List_Of_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 7!');
	Notifications.update({text: 'NOTIFICATIONS_Player_in_from_waiting_list'}, {$set: {text: 'User_Passed_From_Waiting_List_To_The_List_Of_Participants_In_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 8!');
	Notifications.update({text: 'NOTIFICATIONS_New_post'}, {$set: {text: 'User_Posted_Comment_In_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 9!');
	Notifications.update({text: 'NOTIFICATIONS_Match_was_canceled'}, {$set: {text: 'User_Canceled_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 10!');
	Notifications.update({text: 'NOTIFICATIONS_New_match_you_are_in'}, {$set: {text: 'User_Created_New_Activity_And_Added_You_To_List_Of_Participants'}}, {multi: true});
	console.log('NOTIF 11!');
	Notifications.update({text: 'NOTIFICATIONS_New_match_you_are_admin'}, {$set: {text: 'User_Created_New_Activity_And_Named_You_Admin'}}, {multi: true});
	console.log('NOTIF 12!');
	Notifications.update({text: 'NOTIFICATIONS_You_are_admin'}, {$set: {text: 'User_Named_You_Admin'}}, {multi: true});
	console.log('NOTIF 13!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_players'}, {$set: {text: 'User_Modified_List_Of_Particpants_In_One_Of_Your_Activities'}}, {multi: true});
	console.log('NOTIF 14!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_added_you_to_people_in'}, {$set: {text: 'User_Signed_You_In_For_One_Of_His_Activities'}}, {multi: true});
	console.log('NOTIF 15!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_removed_you_from_people_in'}, {$set: {text: 'User_Signed_You_Out_From_One_Of_His_Activities'}}, {multi: true});
	console.log('NOTIF 16!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_added_your_friend_to_match'}, {$set: {text: 'User_Added_Your_Friend_To_One_Of_His_Activities'}}, {multi: true});
	console.log('NOTIF 17!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_removed_your_friend_from_match'}, {$set: {text: 'User_Removed_Your_Friend_From_One_Of_His_Activities'}}, {multi: true});
	console.log('NOTIF 18!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_added_you_to_waiting_list'}, {$set: {text: 'User_Added_You_To_The_Waiting_List_Of_One_Of_His_Activities'}}, {multi: true});
	console.log('NOTIF 19!');
	Notifications.update({text: 'NOTIFICATIONS_Match_admin_removed_you_from_waiting_list'}, {$set: {text: 'User_Removed_You_From_The_Waiting_List_Of_One_Of_His_Activities'}}, {multi: true});
	console.log('NOTIF 20!');
	Notifications.update({text: 'NOTIFICATIONS_New_match_in_your_playing_zone'}, {$set: {text: 'User_Is_Organizing_New_Activity_In_Your_Region'}}, {multi: true});
	console.log('NOTIF 21!');
	Notifications.update({text: 'NOTIFICATIONS_New_match_repeat'}, {$set: {text: 'Activity_Recreated_Automatically'}}, {multi: true});
	console.log('UPDATE NOTIFICATIONS DONE!')

});*/

/*
Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users
	var welEmails = WelcomeEmails.find().fetch(); // get all welcomeEmails
	var recipients = _.pluck(welEmails, 'recipientId');

	users.forEach(function(user) {
		var ok = !_.isUndefined(user.notifEmails) && !_.isNull(user.notifEmails) && !_.isUndefined(user.notifEmails.primary) && !_.isNull(user.notifEmails.primary) && !_.isUndefined(user.welcome) && user.welcome === false && user._id !== '8jh6SDpYEeWEpNrZh'; // Cro Nos
		if (ok && _.indexOf(recipients, user._id) === -1) {
			console.log('ADDING WELCOME DOC FOR USER, userId: ', user._id);
			var doc = {
				recipientId: user._id
			};
			WelcomeEmails.insert(doc);
		}
	});

});*/

// PRODUCTION V15
/*Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.category)) {
			console.log('SETTING CATEGORY FIELD ON USER, userId: ', user._id);
			Meteor.users.update({_id: user._id}, {$set: {category: 'regular'}});
		}
	});

});


// PRODUCTION V14
Meteor.startup(function() {

	var oldSpidersDB = SpidersIPs.find().fetch(); // get all spiders
	var oldSpiders = _.pluck(oldSpidersDB, 'ip') || [];
	var newSpiders = ['66.249', '66.102'];

	_.each(newSpiders, function(newSpider) {
		if (_.indexOf(oldSpiders, newSpider) === -1) {
			console.log('INSERTING NEW SPIDER: ', newSpider);
			SpidersIPs.insert({'ip': newSpider});
		}
	});

});*/

// PRODUCTION V12
/*Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.profile.language)) {
			console.log('ERROR, PROFILE.LANGUAGE IS UNDEFINED');
		}
		if (_.isUndefined(user.profile.lang)) {
			console.log('SETTING LANG FIELD ON USER, userId: ', user._id);
			var lang = user.profile.language;
			var toSet = {"profile.lang.value": lang, "profile.lang.flag": true};
			Meteor.users.update({_id: user._id}, {$set: toSet});
		}
	});

});

Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.geo.flag)) {
			console.log('SETTING GEO FLAG FIELD ON USER, userId: ', user._id);
			var toSet = {"geo.flag": true};
			Meteor.users.update({_id: user._id}, {$set: toSet});
		}
	});
});*/

// PRODUCTION V11
/*Meteor.startup(function() {

	var collectionName = 'users';
	var usersCollectionCounter = CollectionsCounter.findOne({collectionName: collectionName});
	var users = Meteor.users.find();
	var counter = users.count();
	if (!usersCollectionCounter) {
		console.log('INSERTING COLL COUNTER USERS');
		CollectionsCounter.insert({collectionName: collectionName, counter: counter});
	} else {
		console.log('UPDATING COLL COUNTER USERS');
		CollectionsCounter.update({collectionName: collectionName}, {$set: {counter: counter}});
	}

});

Meteor.startup(function() {

	var collectionName = 'upcomingActivities';
	var activitiesCollectionCounter = CollectionsCounter.findOne({collectionName: collectionName});
	var upcomingActivities = Matches.find({status: {$ne: 'finished'}, privacy: 'public'});
	var counter = upcomingActivities.count();
	console.log('counter: ', counter);
	if (!activitiesCollectionCounter) {
		console.log('INSERTING COLL COUNTER ACTIVITIES');
		CollectionsCounter.insert({collectionName: collectionName, counter: counter});
	} else {
		console.log('UPDATING COLL COUNTER ACTIVITIES');
		CollectionsCounter.update({collectionName: collectionName}, {$set: {counter: counter}});
	}

});

Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.matches.public.admin)) {
			console.log('MATCHES PUBLIC ADMIN... userId: ', user._id);
			var toSet = {"matches.public.admin": [], "matches.private.admin": []};
			Meteor.users.update({_id: user._id}, {$set: toSet});
		}
	});
});*/

// PRODUCTION V10
/*Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.profile.language)) {
			console.log('PROFILE LANG... userId: ', user._id);
			// In case notifEmals is not set...
			if (_.isUndefined(user.settings)) {
				console.log("no settings")
			} else {
				var lang = user.settings.language;
				var toSet = {"profile.language": lang};
				console.log('SETTING...');
				Meteor.users.update({_id: user._id}, {$set: toSet});
				//console.log('REMOVING...');
				//Meteor.users.update({_id: user._id}, {$unset: {"settings.language": ""}});
			}
		}
	});
});

//Meteor.startup(function() {
//	console.log('REMOVING FEEDBACK...');
//	Meteor.Feedback.drop();
//});

Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
		if (_.isUndefined(user.geo)) {
			console.log('USER GEO...');
			// In case notifEmals is not set...
			if (_.isUndefined(user.settings)) {
				throw new Meteor.Error('no settings!');
			} else if (_.isUndefined(user.settings.circleRadius)) {
				throw new Meteor.Error('no radius!');
			} else if (_.isUndefined(user.settings.mapZoom)) {
				throw new Meteor.Error('no zoom!');
			} else if (_.isUndefined(user.settings.loc)) {
				throw new Meteor.Error('no loc!');
			} else if (_.isUndefined(user.settings.loc.coordinates)) {
				throw new Meteor.Error('no coordinates!');
			} else {
				var radius =  user.settings.circleRadius;
				var zoom =  user.settings.mapZoom;
				var lng = user.settings.loc.coordinates[0];
				var lat = user.settings.loc.coordinates[1];
				var toSet = {geo:
									{radius: radius,
									 zoom: zoom,
									 loc: {type: "Point", coordinates: [lng, lat]}
								}};
				Meteor.users.update({_id: user._id}, {$set: toSet});
				//Meteor.users.update({_id: user._id}, {$unset: {settings: ""}});
			}
		}
	});
});*/

// PRODUCTION V7
/*Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
	//if (user) {
		if (_.isUndefined(user.emailNotifSettings.gameNewGameRepeat)) {
				console.log('SETTINGS emailNotifSettings...');
				// In case notifEmals is not set...
				var toSet = {"emailNotifSettings.gameNewGameRepeat": false};
				Meteor.users.update({_id: user._id}, {$set: toSet});
		}
	//}
	});
});*/


//PRODUCTION V3
// Add loc field to matches collection
/*Meteor.startup(function() {

	var matches = Matches.find(); // get all matches
	matches.forEach(function(match) {
		// 1- Set fields if not set yet
		if (_.isUndefined(match.loc) && !_.isUndefined(match.lat) && !_.isUndefined(match.lng)) {
			console.log('MATCH.LOC...');
			var toSet = {loc: {
				type: 'Point',
				coordinates: [match.lng, match.lat]
			}};
			Matches.update(match._id, {$set: toSet});
		}
	});
});

// Add privacy field to matches collection
Meteor.startup(function() {

	var matches = Matches.find(); // get all matches
	matches.forEach(function(match) {
		// 1- Set fields if not set yet
		if (_.isUndefined(match.privacy) && _.isUndefined(match.privacyOnCreation) && !_.isUndefined(match.label) && !_.isUndefined(match.labelOnCreation)) {
			console.log('MATCH.PRIVACY...');
			var toSet = {privacy: match.label, privacyOnCreation: match.labelOnCreation};
			Matches.update(match._id, {$set: toSet});
		}
	});
});

// Setup unreadNotifications collection
Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users
	users.forEach(function(user) {
		var doc = UnreadNotifications.findOne({recipientId: user._id});
		if (!doc) {
			console.log('SETTING UNREAD NOTIF...');
			UnreadNotifications.insert({recipientId: user._id});
			var counter = Notifications.find({recipient: user._id, didRead: 'no'}).count();
			UnreadNotifications.update({recipientId: user._id}, {$set: {counter: counter}});
		}
	});
});


// Add loc field to users collection
Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users
	users.forEach(function(user) {
		// 1- Set fields if not set yet
		if (_.isUndefined(user.settings.loc) && !_.isUndefined(user.settings.circleCenterLat) && !_.isUndefined(user.settings.circleCenterLng)) {
			console.log('USER.LOC...');
			var toSet = {"settings.loc": {
				type: 'Point',
				coordinates: [user.settings.circleCenterLng, user.settings.circleCenterLat]
			}};
			Meteor.users.update(user._id, {$set: toSet});
		}
	});
});*/

//======================================================================
/*
PRODUCTION V2
Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
	//if (user) {
		if (_.isUndefined(user.notifEmails)) {
			console.log('SETTINGS notifEmails...');
			// In case notifEmals is not set...
			var toSet = {notifEmails: {
				verified: [],
				sent: [],
				primary: '',
				verificationToken: {
					token: '',
					userId: '',
					address: '',
					expiresAt: new Date()}
			}};
			Meteor.users.update({_id: user._id}, {$set: toSet});
		}
	//}

	});
});

Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
	//if (user) {
		if (_.isUndefined(user.emailNotifSettings)) {
				console.log('SETTINGS emailNotifSettings...');
				// In case notifEmals is not set...
				var toSet = {emailNotifSettings: {
					gameNewGamePlayingZone: false,
					gameNewComment: false,
					gameWasCanceled: false,
					gameAddRemovePlayer: false,
					gameAddedMe: false,
					gamePassFromWaitingToPlaying: false}
				};
				Meteor.users.update({_id: user._id}, {$set: toSet});
		}
	//}
	});
});


Meteor.startup(function() {

	var users = Meteor.users.find(); // get all users

	users.forEach(function(user) {
	//if (user) {
		if (!_.isUndefined(user.notifEmails)) {
			console.log('SETTINGS primary email...');
			var primary = '';
			//users.forEach(function(user) {
				if (!_.isUndefined(user.private) && !_.isUndefined(user.private.email) && user.private.email !== '' && user.private.email !== null) {
					primary =  user.private.email;
					var toSet = {'notifEmails.primary': primary};
					var toAddToSet = {'notifEmails.verified': primary};
					Meteor.users.update({_id: user._id}, {$set: toSet, $addToSet: toAddToSet});
				}
			//});
		}
	//}
	});




});



Meteor.startup(function() {

	var matches = Matches.find(); // get all users

	matches.forEach(function(match) {
	//if (match) {
		if (_.isUndefined(match.followers)) {
			console.log('SETTINGS match.followers...');
			var followers = _.union(getUserIds(match.peopleIn), getUserIds(match.friendsOf), match.createdBy, getUserIds(match.waitingList));

			console.log('followers: ' + followers);
			console.log('followers.length: ' + followers.length);
			console.log('typeof(followers): ' + typeof(followers));
			console.log('followers[0]: ' + followers[0]);
			Matches.update({_id: match._id}, {$set: {followers: []}});
			if (!_.isUndefined(followers[0])) {
				Matches.update({_id: match._id}, {$addToSet: {followers: {$each: followers}}});
			}
		}
	});
	//}


});*/
