//======================================================================
// MATCHES IN CIRCLE:
//======================================================================
/*Meteor.publishComposite('matchesInCircle', function(circle, limit) {

   // circle = {lat, lng, radius}
   // radius must be in km
   console.log('SUBS MATCHES IN CIRCLE');
   check(circle, {
      lat: Number,
      lng: Number,
      radius: Number
   });
   check(limit, Number);

   var query = {
      privacy: 'public',
      status: {
         $ne: 'finished'
      },
      'loc': {
         $geoWithin: {
            $centerSphere: [[circle.lng, circle.lat], circle.radius / 6378.1]
         }
      }
   };
   var matchOptions = {
      fields: {
         "createdAt": 0,
         "loc.type": 0,
         "fieldPhone": 0,
         "fieldWebsite": 0,
         "privacyOnCreation": 0,
         "peopleInOnCreation": 0,
         "followers": 0
      },
      sort: {
         date: 1
      },
      limit: limit
   };
   var userOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };

	return {
      find: function() {
         return Matches.find(query, matchOptions);
      },
      children: [{
         find: function(match) { // loop over each doc in the parent record
            var usersIds = _.union(match.createdBy, _.pluck(match.peopleIn, "userId"), _.pluck(match.friendsOf, "userId") , _.pluck(match.waitingList, "userId"));
            return Meteor.users.find({_id: {$in: usersIds}}, userOptions);
         }
      }]
   }

});*/
