//======================================================================
// MATCH POSTS:
//======================================================================
Meteor.publishComposite('matchPosts', function(matchId) {

	console.log('PUBLISH MATCH POSTS');
	check(matchId, String);
   var options = {
		fields: {
	   	"services.twitter.profile_image_url_https": 1,
	   	"services.facebook.id": 1,
	   	"services.google.picture": 1,
	      "profile.name": 1
   	},
		limit: 1
	};

	return {
      find: function() {
         return Posts.find({postedOn: matchId});
      },
      children: [{
         find: function(post) { // loop over each doc in the parent record
            return Meteor.users.find(post.createdBy, options);
         }
      }]
   }
});
