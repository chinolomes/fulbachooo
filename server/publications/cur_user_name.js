//======================================================================
// CURRENT USER NAME:
//======================================================================
Meteor.publish('curUserName', function() {

   console.log('PUBLISH CUR USER NAME');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var options = {
         fields: {
            'profile.name': 1
         },
         limit: 1
      };
   	return Meteor.users.find(this.userId, options);
	} else {
      this.ready();
      return;
   }

});
