//======================================================================
// TOTAL UPCOMING ACTIVITIES COUNTER:
//======================================================================
Meteor.publish('totalUpcomingActivitiesCounter', function() {

   console.log('PUBLISH TOTAL UPCOMING ACTIVITIES COUNTER');
   var query = {
      collectionName: 'upcomingActivities'
   };
   var options = {
      limit: 1
   };
   var upcomingActivitiesCounter = CollectionsCounter.find(query, options);
   if (upcomingActivitiesCounter) {
      return upcomingActivitiesCounter;
   } else {
      this.ready();
      return;
   }

});
