//======================================================================
// CURRENT USER PREVIOUS MATCHES:
//======================================================================
Meteor.publish('previousMatches', function() {

   console.log('PUBLISH PREV GAMES');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var query = {
         createdBy: this.userId
      };
      var options = {
         sort: {
            date: -1
         },
         limit: 15
      };
      return Matches.find(query, options);
   } else {
      this.ready();
      return;
   }

});
