//======================================================================
// CURRENT USER NOTIFICATIONS:
//======================================================================
Meteor.publishComposite('curUserNotif', {

   find: function() {
      console.log('PUBLISH CUR USER NOTIF');
      // Many times this.userId === null, so check that to avoid re-subscribe if not needed
      if (this.userId) {
         var query = {
            recipient: this.userId
         };
         var options = {
            sort: {
               createdAt: -1
            },
            limit: 30
         };
         return Notifications.find(query, options);
      }
      return;
   },
   children: [{
      find: function(notification) { // loop over each doc in the parent record
         var options = {
            fields: {
            	"services.twitter.profile_image_url_https": 1,
            	"services.facebook.id": 1,
            	"services.google.picture": 1,
               "profile.name": 1
            },
            limit: 1
         };
         return Meteor.users.find(notification.createdBy, options);
      }
   }]

});
