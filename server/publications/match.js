//======================================================================
// MATCH:
//======================================================================
Meteor.publishComposite('match', function(matchId) {

	console.log('PUBLISH MATCH');
	check(matchId, String);
   var options = {
		fields: {
	   	"services.twitter.profile_image_url_https": 1,
	   	"services.facebook.id": 1,
	   	"services.google.picture": 1,
	      "profile.name": 1
   	}
	};

	return {
      find: function() {
         return Matches.find(matchId, {limit: 1});
      },
      children: [{
         find: function(match) { // loop over each doc in the parent record
				var usersIds = _.union(match.createdBy, _.pluck(match.peopleIn, "userId"), _.pluck(match.friendsOf, "userId") , _.pluck(match.waitingList, "userId"));
				if (!_.isUndefined(match.admin) && !_.isNull(match.admin[0])) {
					usersIds = _.union(usersIds, match.admin);
				}
            return Meteor.users.find({_id: {$in: usersIds}}, options);
         }
      }]
   }
});
