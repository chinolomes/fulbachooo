//======================================================================
// USER SETTINGS:
//======================================================================
Meteor.publish('curUserEmailNotifSettings', function() {

   console.log('PUBLISH CUR USER EMAIL NOTIF SETTINGS');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var options = {
         fields: {
            emailNotifSettings: 1
         },
         limit: 1
      };
		return Meteor.users.find(this.userId, options);
	} else {
	   this.ready();
	   return;
	}

});
