//======================================================================
// CURRENT USER PROFILE:
//======================================================================
Meteor.publish('curUserProfile', function() {

   console.log('PUBLISH CUR USER PROFILE');
   // Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var options = {
         fields: {
            'profile.name': 1,
            'profile.gender': 1,
            'profile.sports': 1,
            'profile.dominantLeg': 1,
            'profile.dominantHand': 1,
            'profile.selfDescription': 1,
            'profile.lang': 1 // ADDED
         },
         limit: 1
      };
      return Meteor.users.find(this.userId, options);
   } else {
      this.ready();
      return;
   }

});
