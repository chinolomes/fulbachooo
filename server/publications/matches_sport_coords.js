// bounds = {bottomLeft: [lng, lat], topRight: [lng, lat]}
// NOTICE: longitude goes first for each pair of coordinates!
//======================================================================
// MATCHES COORDINATES AND SPORT WITHIN BOUNDS:
//======================================================================
Meteor.publish('matchesSportCoords', function(bounds) {


	console.log('PUBLISH MATCHES COORDS SPORT IN BOUNDS');
	check(bounds, {
		bottomLeft: [Number],
		topRight: [Number]
	});
	//console.log('bounds: [' + bounds.bottomLeft + '], [' + bounds.topRight + ']');

	var selector = {
		'loc': {
			$geoWithin: {
				$box: [bounds.bottomLeft, bounds.topRight]
			}
		},
		privacy: 'public',
		status: {
			$ne: 'finished'
		}
	};
	var options = {
		fields: {
			"privacy": 1,
			"status": 1,
			"loc.coordinates": 1,
			"sport": 1
		}
	};
	var matches = Matches.find(selector, options);
   if (matches) {
      return matches;
   } else {
		this.ready();
		return;
	}

});
