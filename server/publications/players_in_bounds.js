// TODO: add max limit
//======================================================================
// PLAYERS IN CIRCLE:
//======================================================================
Meteor.publish('playersInBounds', function(bounds, limit) {

   // bounds = {bottomLeft: [lng, lat], topRight: [lng, lat]}
   console.log('PUBLISH PLAYERS IN BOUNDS');
   check(bounds, {
      bottomLeft: [Number],
      topRight: [Number]
   });
   check(limit, Number);
   //console.log('bounds.bottomLeft: ' + bounds.bottomLeft + 'bounds.topRight: ' + bounds.topRight + 'limit: ' + limit);

   var query = {
      'geo.loc': {
         $geoWithin: {
            $box: [bounds.bottomLeft, bounds.topRight]
         }
      }
   };
   var options = {
      fields: {
         "services.twitter.profile_image_url_https": 1,
         "services.facebook.id": 1,
         "services.google.picture": 1,
         "createdAt": 1,
         "profile": 1,
         "geo": 1
      },
      sort: {
         createdAt: -1
      },
      limit: limit
   };
   var users = Meteor.users.find(query, options);
   if (users) {
      return users;
   } else {
      this.ready();
      return;
   }

});
