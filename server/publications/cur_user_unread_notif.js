//======================================================================
// CURRENT USER UNREAD NOTIFICATIONS:
//======================================================================
Meteor.publish('curUserUnreadNotif', function() {

   console.log('PUBLISH CUR USER UNREAD NOTIF');
   // Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var query = {
         recipientId: this.userId
      };
      var options = {
         limit: 1
      };
   	return UnreadNotifications.find(query, options);
	} else {
      this.ready();
      return;
   }

});
