// bounds = {bottomLeft: [lng, lat], topRight: [lng, lat]}
// NOTICE: longitude goes first for each pair of coordinates!
//======================================================================
// PLAYERS COORDINATES WITHIN SPECIFIC BOUNDS:
//======================================================================
Meteor.publish('playersCoords', function(bounds) {

	console.log('PUBLISH PLAYERS COORDS IN BOUNDS');
	check(bounds, {
		bottomLeft: [Number],
		topRight: [Number]
	});
	//console.log('bounds: [' + bounds.bottomLeft + '], [' + bounds.topRight + ']');

	var selector = {
		'geo.loc': {
			$geoWithin: {
				$box: [bounds.bottomLeft, bounds.topRight]
			}
		}
	};
	var options = {
		fields: {
			"geo.loc.coordinates": 1
		}
	};
	var players = Meteor.users.find(selector, options);
	if (players) {
		return players;
	} else {
		this.ready();
		return;
	}

});
