//======================================================================
// PLAYERS COORDINATES:
//======================================================================
Meteor.publish('allPlayersCoords', function() {

	console.log('SUBS ALL PLAYERS COORDS');
	// Internal var(s)
	var query = {};
	var options = {
		fields: {
			'geo.loc.coordinates': 1
		}
	};

	var players = Meteor.users.find(query, options);
	if (players) {
		return players;
	} else {
		this.ready();
		return;
	}

});
