//======================================================================
// TOTAL USERS COUNTER:
//======================================================================
Meteor.publish('totalUsersCounter', function() {

   console.log('PUBLISH TOTAL USERS COUNTER');
   var query = {
      collectionName: 'users'
   };
   var options = {
      limit: 1
   };
   var usersCounter = CollectionsCounter.find(query, options);
   if (usersCounter) {
      return usersCounter;
   } else {
      this.ready();
      return;
   }

});
