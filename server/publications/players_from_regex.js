//======================================================================
// GET LIST OF PLAYERS MATCHING REGULAR EXPRESSION:
//======================================================================
Meteor.publish('playersFromRegex', function(partialName) {

		// bounds = {bottomLeft, topRight}
		// longitudE goes first for each pair of coordinates
		//console.log('partialName: ', partialName);
		console.log('PUBLISH PLAYERS AVATAR REGEX');
		check(partialName, String);
		//console.log('bounds: [' + bounds.bottomLeft + '], [' + bounds.topRight + ']');
	   // Internal var(s)
		var query = {
	      'profile.name': {
	         '$regex': '^' + partialName,
	         '$options': 'i' // upper/lower case insensitive
	      }
	   }
		var options = {
			fields: {
				'profile.name': 1,
				'profile.avatar': 1
			},
	      limit: 10
		};

		var users = Meteor.users.find(query, options);
		//console.log('users: ', Meteor.users.find(query, options).fetch());
		if (users) {
			return users;
		}
		this.ready();
		return;

});
