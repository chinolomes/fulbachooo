//======================================================================
// USER MATCHES:
//======================================================================
// category = past/upcoming
Meteor.publishComposite('userMatches', function(userId, category, limit) {

   // radius must be in km
   console.log('PUBLISH USER MATCHES');
	check([userId, category], [String]);
	check(limit, Number);
   //console.log('userId: ' + userId + ', category: ' + category + ', limit: ' + limit);

   // Query userId and get activities ids.
   var user = Meteor.users.findOne(userId, {fields: {activities: 1}});
   var activities = user.activities;
   /* 4 viewer-category combinations:
            |upcoming|past|
   owner    |   x    |  x |
   not-owner|   x    |  x |
   */
   var actUpcPub = activities.upcoming.public;
   var actUpcPri = activities.upcoming.private;
   var actPasPub = activities.past.public;
   var actPasPri = activities.past.private;

   // Get activities ids
   var actIdsOwnerUpcoming = _.uniq(_.union(_.flatten(_.values(actUpcPub)), _.flatten(_.values(actUpcPri))));
   var actIdsOwnerPast = _.uniq(_.union(_.flatten(_.values(actPasPub)), _.flatten(_.values(actPasPri))));
   var actIdsNotOwnerUpcoming = _.union(actUpcPub.created, actUpcPub.enrolled, actUpcPub.waiting, actUpcPub.friendIsEnrolled);
   var actIdsNotOwnerPast = _.union(actPasPub.created, actPasPub.enrolled, actPasPub.waiting, actPasPub.friendIsEnrolled);
   //var actIdsNotOwnerUpcoming = _.uniq(_.flatten(_.values(actUpcPub)));
   //var actIdsNotOwnerPast = _.uniq(_.flatten(_.values(actPasPub)));
   /*console.log('actIdsOwnerUpcoming: ', actIdsOwnerUpcoming);
   console.log('actIdsOwnerPast: ', actIdsOwnerPast);
   console.log('actIdsNotOwnerUpcoming: ', actIdsNotOwnerUpcoming);
   console.log('actIdsNotOwnerPast: ', actIdsNotOwnerPast);*/

	// Initialize internal vars
   var userOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };
   // Activity fields when the viewer is owner
   var fieldsPrivPubActs = {
      "createdAt": 0,
   	"loc.type": 0,
   	"fieldPhone": 0,
   	"fieldWebsite": 0,
   	"privacyOnCreation": 0,
   	"peopleInOnCreation": 0
   };
   // Activity fields when the viewer is NOT owner
   var fieldsOnlyPubActs = _.extend({"followers": 0}, fieldsPrivPubActs);

	return {
      find: function() {
         // If the viewer is the profile's owner, publish both public and private activities
         var matches = null;
         if (this.userId === userId) {
            switch(category) {
      			case 'upcoming':
      				matches = Matches.find({_id: {$in: actIdsOwnerUpcoming}}, {sort: {date: 1, time: 1}, limit: limit, fields: fieldsPrivPubActs});
      				break;
      			case 'past':
      				matches = Matches.find({_id: {$in: actIdsOwnerPast}}, {sort: {date: -1, time: -1}, limit: limit, fields: fieldsPrivPubActs});
      				break;
      		}
      	} else { // Else, only display the public activities
      		switch(category) {
      			case 'upcoming':
      				matches = Matches.find({_id: {$in: actIdsNotOwnerUpcoming}}, {sort: {date: 1, time: 1}, limit: limit, fields: fieldsOnlyPubActs});
      				break;
      			case 'past':
      				matches = Matches.find({_id: {$in: actIdsNotOwnerPast}}, {sort: {date: -1, time: -1}, limit: limit, fields: fieldsOnlyPubActs});
      				break;
      		}
         }
			return matches;
      },
      children: [{
         find: function(match) { // loop over each doc in the parent record
            var usersIds = _.union(match.createdBy, _.pluck(match.peopleIn, "userId"), _.pluck(match.friendsOf, "userId") , _.pluck(match.waitingList, "userId"));
            return Meteor.users.find({_id: {$in: usersIds}}, userOptions);
         }
      }]
   }

});

/*
//======================================================================
// USER MATCHES:
//======================================================================
// category = past/upcoming
Meteor.publishComposite('userMatches', function(userId, category, limit) {

   // circle = {lat, lng, radius}
   // radius must be in km
   console.log('SUBS USER MATCHES');
	check([userId, category], [String]);
	check(limit, Number);

	// Initialize internal vars
   var userOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };
   // Owner queries and options
   var fieldsOwner = {
      "createdAt": 0,
   	"loc.type": 0,
   	"fieldPhone": 0,
   	"fieldWebsite": 0,
   	"privacyOnCreation": 0,
   	"peopleInOnCreation": 0
   };
   var orOwner = [
      {'peopleIn.userId': {$in: [userId]}},
      {'friendsOf.userId': {$in: [userId]}},
      {createdBy: userId},
      {'waitingList.userId': {$in: [userId]}},
      {followers: {$in: [userId]}},
      {admin: {$in: [userId]}}
   ];
   var queryUpcomingOwner = {
      status: {
         $ne: 'finished'
      },
      $or: orOwner
   };
   var queryPastOwner = {
      status: 'finished',
      $or: orOwner
   };
   var optionsUpcomingOwner = {
      sort: {
         date: 1
      },
      limit: limit,
      fields: fieldsOwner
   }
   var optionsPastOwner = {
      sort: {
         date: -1
      },
      limit: limit,
      fields: fieldsOwner
   }

   // Not owner queries and options
   var fields = {
      "createdAt": 0,
   	"loc.type": 0,
   	"fieldPhone": 0,
   	"fieldWebsite": 0,
   	"privacyOnCreation": 0,
   	"peopleInOnCreation": 0,
      "followers": 0
   };
   var or = [
      {'peopleIn.userId': {$in: [userId]}},
      {'friendsOf.userId': {$in: [userId]}},
      {createdBy: userId},
      {'waitingList.userId': {$in: [userId]}}
   ];
   var queryUpcoming = {
      privacy: 'public',
      status: {
         $ne: 'finished'
      },
      $or: or
   };
   var queryPast = {
      privacy: 'public',
      status: 'finished',
      $or: or
   };
   var optionsUpcoming = {
      sort: {
         date: 1
      },
      limit: limit,
      fields: fields
   };
   var optionsPast = {
      sort: {
         date: -1
      },
      limit: limit,
      fields: fields
   }

	return {
      find: function() {
         // If the viewer is the profile's owner, publish both public and private games
         var matches = null;
         if (this.userId === userId) {
            switch(category) {
      			case 'upcoming':
      				matches = Matches.find(queryUpcomingOwner, optionsUpcomingOwner);
      				break;
      			case 'past':
      				matches = Matches.find(queryPastOwner, optionsPastOwner);
      				break;
      		}
      	} else { // Else, only display the public games
      		switch(category) {
      			case 'upcoming':
      				matches = Matches.find(queryUpcoming, optionsUpcoming);
      				break;
      			case 'past':
      				matches = Matches.find(queryPast, optionsPast);
      				break;
      		}
         }
			return matches;
      },
      children: [{
         find: function(match) { // loop over each doc in the parent record
            var usersIds = _.union(match.createdBy, _.pluck(match.peopleIn, "userId"), _.pluck(match.friendsOf, "userId") , _.pluck(match.waitingList, "userId"));
            return Meteor.users.find({_id: {$in: usersIds}}, userOptions);
         }
      }]
   }

});
*/
