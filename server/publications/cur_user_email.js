//======================================================================
// CURRENT USER EMAIL:
//======================================================================
Meteor.publish('curUserEmail', function() {

   console.log('PUBLISH CUR USER EMAIL');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var options = {
         fields: {
            'notifEmails.primary': 1
         },
         limit: 1
      };
   	return Meteor.users.find(this.userId, options);
	} else {
      this.ready();
      return;
   }

});
