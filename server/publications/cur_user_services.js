//======================================================================
// CURRENT USER SERVICES:
//======================================================================
Meteor.publish('curUserServices', function() {

   console.log('PUBLISH CUR USER SERVICES');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
		var options = {
         fields: {
            "services.twitter.profile_image_url_https": 1,
				"services.facebook.id": 1,
				"services.google.picture": 1
         },
         limit: 1
      };
   	return Meteor.users.find(this.userId, options);
	} else {
      this.ready();
      return;
	}

});
