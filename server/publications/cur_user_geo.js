//======================================================================
// CURRENT USER GEO:
//======================================================================
Meteor.publish('curUserGeo', function() {

   console.log('PUBLISH CUR USER GEO');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var options = {
         fields: {
            geo: 1
         },
         limit: 1
      };
 		return Meteor.users.find(this.userId, options);
	} else {
      this.ready();
      return;
	}

});
