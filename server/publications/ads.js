// loc = {lat, lng}
//======================================================================
// ADVERTISEMENTS:
//======================================================================
Meteor.publish('ads', function(loc) {

   // radius must be in km
   console.log('PUBLISH ADS');
   check(loc, {
      lat: Number,
      lng: Number
   });

   var query = {
      active: true,
      'loc': {
         $geoWithin: {
            $centerSphere: [[loc.lng, loc.lat], 100 / 6378.1] // 100km radius
         }
      }
   };

   return Ads.find(query);

});
