/*
The 'this' context, which has information about the current DDP connection.
For example, you can access the current user’s _id with 'this.userId'.
*/
//======================================================================
// CURRENT USER AVATAR (PROFILE + SERVICES):
//======================================================================
Meteor.publish('curUserAvatar', function() {

   console.log('PUBLISH CUR USER AVATAR');
	// Many times this.userId === null, so check that to avoid re-subscribe if not needed
   if (this.userId) {
      var options = {
         fields: {
            "profile.name": 1,
				"services.twitter.profile_image_url_https": 1,
				"services.facebook.id": 1,
				"services.google.picture": 1
         },
         limit: 1
      };
 		return Meteor.users.find(this.userId, options);
	} else {
      this.ready();
      return;
	}

});
