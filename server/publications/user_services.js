//======================================================================
// USER SERVICES:
//======================================================================
Meteor.publish('userServices', function(userId) {

	console.log("PUBLISH USER SERVICES ", userId);
	check(userId, String);

   var options = {
		fields: {
	      "services.twitter.profile_image_url_https": 1,
	      "services.facebook.id": 1,
	      "services.google.picture": 1
   	}
	};

	var user = Meteor.users.find(userId, options);
   if (user) {
		return user;
   } else {
      this.ready();
      return;
	}

});
