//======================================================================
// USER PROFILE:
//======================================================================
Meteor.publish('userProfile', function(userId) {

	console.log("PUBLISH USER PROFILE ", userId);
	check(userId, String);

	var options = {
		fields: {
			profile: 1,
			createdAt: 1
		},
		limit: 1
	};

   var user = Meteor.users.find(userId, options);
   if (user) {
		return user;
   } else {
      this.ready();
      return;
	}

});
