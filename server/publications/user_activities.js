//======================================================================
// USER ACTVITIES:
//======================================================================
Meteor.publish('userActivities', function(userId) {

   console.log('PUBLISH USER ACTVITIES');
	check(userId, String);

   var options = {
		fields: {
	      "activities": 1
   	}
	};

	var user = Meteor.users.find(userId, options);
   if (user) {
		return user;
   } else {
      this.ready();
      return;
	}

});
