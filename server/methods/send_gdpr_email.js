/*
DESCRIPTION: send welcome email to recipientId
INPUT: doc = {_id: 'id', recipientId: 'userId', [sent: 'boolean]}
*/
// SOURCES: // https://themeteorchef.com/snippets/using-the-email-package/
// https://meteorhacks.com/server-side-rendering
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// SEND WELCOME EMAIL:
//======================================================================
Meteor.methods({'sendGDPREmail': function (doc) {

   console.log("about to send email...");
   check(doc, {
      _id: String,
      // gdpr: Match.Maybe(Boolean)
      //sent: Boolean
   });

   ///////////////////
	// AUX FUNCTIONS //
	///////////////////

	// Returns 'true' if the given user has all the requiered data properly set, and 'false' otherwise.
	var allFieldsAreSet = function(user) {
	   return user.notifEmails && user.notifEmails.primary && user.notifEmails.primary.length > 0 && (!user.gdpr || user.gdpr === false)
             && user.profile.lang.flag === true && _.indexOf(LANGS, user.profile.lang.value) !== -1;
	}

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Send GDPR Email';
   var recipientId = doc._id;
   var recipient = null;
   var email = '';
   var lang = '';
   var recipientName = '';

	/////////////
	// QUERIES //
	/////////////

   recipient = Meteor.users.findOne({_id: recipientId}, {fields: {"gdpr": 1, "profile.name": 1, "profile.lang": 1, "notifEmails.primary": 1}});

	////////////
	// CHECKS //
	////////////

	if (!recipient) {
      throwError(context, 'user is not registered');
	   return;
	}
	if (!allFieldsAreSet(recipient)) {
      console.log(context + ': fields are not properly set!');
	   return;
	}

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   email = recipient.notifEmails.primary;
   console.log('email', email);
   // email = 'federodes@gmail.com';
   lang = recipient.profile.lang.value;
   // get first name, chop off last name
   recipientName = (
     recipient &&
     recipient.profile &&
     recipient.profile.name &&
     recipient.profile.name.split(' ')[0]
   ) || '';

   ///////////////
   // SET EMAIL //
   ///////////////


    SSR.compileTemplate('htmlEmail', Assets.getText('email-gdpr-template.html'));
    Template.htmlEmail.helpers({
       'title': function() {
          return 'Hello,';
       },
       /* 'lineOne': function() {
          return 'As part of our GDPR compliance process, we updated our privacy policy and our Terms of Services. Please read them carefully.';
       }, */
       'lineTwo': function() {
          return 'Here\'s what it means for you:';
       },
       'lineThree': function() {
          return 'From your profile page, you\'re now able to download every piece of data you shared with us;';
       },
       'lineFour': function() {
          return 'We have updated our internal security practices;';
       },
       'lineFive': function() {
          return 'We\'ll regularly erase data from users that haven\'t been active in a while;';
       },
       'lineSix': function() {
          return 'Feel free to send us a message if you have any question regarding your personal data.';
       },
       'lineSeven': function() {
          return 'Thanks for your trust.';
       },
       'thanks': function() {
          return 'Have a great day,';
       },
       'facebook': function() {
          return TAPi18n.__('Support_Us_On_Facebook', {}, lang_tag = lang);
       },
       'youtube': function() {
          return TAPi18n.__('Subscribe_To_Our_Youtube_Channel', {}, lang_tag = lang);
       }
    });

   ////////////////
   // SEND EMAIL //
   ////////////////

   // Let other method calls from the same client start running,
   // without waiting for the email sending to complete.
   this.unblock();
   Meteor.Mailgun.send({
      to: email,
      from: 'fulbacho.net <no-reply@fulbacho.net>',
      subject: 'GDPR - We updated our Terms of Services and Privacy Policy',
      html: SSR.render('htmlEmail')
   });

   Meteor.users.update({_id: recipientId}, {$set: {gdpr: true}});
   console.log("email sent!");

}});
