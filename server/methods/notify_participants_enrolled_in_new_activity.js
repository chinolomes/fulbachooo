// TODO: must be a JS function instead of a Meteor method
//======================================================================
// NOTIFY PARTICIPANTS ENROLLED IN NEW ACTIVITY:
//======================================================================
Meteor.methods({'notifyParticipantsEnrolledInNewActivity': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('NOTIFY PARTICIPANTS ENROLLED IN NEW ACTIVITY');
   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Notify Participants Enrolled In New Activity';
   var match = null;
   var ownerId = '';
   var owner = null;
   var result = {};
   var ownerName = '';
   var notifType = 'gameAddedMe';
   var textNotif = 'User_Created_New_Activity_And_Added_You_To_List_Of_Participants';
   var anchor = '/activities/activity/' + matchId + '#data';
   var textEmail = 'Email_Notif_User_Created_New_Activity_And_Added_You_To_List_Of_Participants';
   var url = 'https://www.fulbacho.net' + anchor;
   var userIdsToNotify = [];

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "status": 1, "peopleIn": 1}});
   owner = Meteor.users.findOne({_id: match.createdBy}, {fields: {"profile.name": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      throwError(context, 'activity does not exist / is not active. activityId: ' + matchId);
      return;
   }
   // Owner existance
   if (!owner) {
      throwError(context, 'owner does not exist');
      return;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   ownerId = owner._id;
   ownerName = owner.profile.name;
   userIdsToNotify = _.without(_.pluck(match.peopleIn, 'userId'), ownerId); // don't notify activity organizer

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Send notification to participants
   _.each(userIdsToNotify, function(userId) {
   	Notifications.insert({createdBy: ownerId, recipient: userId, text: textNotif, anchor: anchor});
   });

   // Notify users via email
   Meteor.call('notifyUsersViaEmail', ownerName, textEmail, url, userIdsToNotify, notifType);

}});
