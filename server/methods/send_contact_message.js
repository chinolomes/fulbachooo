// verifyCaptchaResponse.data returns a json {
// 'success': true|false,
// 'error-codes': an-error-code
//  };
// check at https://developers.google.com/recaptcha/docs/verify
// CHECKED
// TODO: add rating limit
//======================================================================
// SEND CONTACT MESSAGE:
//======================================================================
Meteor.methods({'sendContactMessage': function(name, email, message, captcha) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log("about to send email...");
   check([name, email, message, captcha], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Send Contact Message';
   var verifyCaptchaResponse = reCAPTCHA.verifyCaptcha(this.connection.clientAddress, captcha);
   console.log('reCAPTCHA response: ', verifyCaptchaResponse.data);

   ////////////
   // CHECKS //
   ////////////

   // reCAPTCHA
   if (verifyCaptchaResponse.data.success === false) {
      console.log(context, 'didnt pass captcha: ' + this.userId);
   	return {status: 'error', text: 'Did_not_pass_captcha'}; // msg
   }
	// Data
	if (name.length === 0 || name.length > 50) {
		throwError(context, 'name length = 0 or > 50');
		return;
	} else if (email.length === 0 || email.length > 50) {
   	throwError(context, 'email length = 0 or > 50');
   	return;
   } else if (message.length === 0 || message.length > 2000) {
	   throwError(context, 'message length = 0 or > 2000');
   	return;
   }

   //////////
   // SEND //
   //////////

   // Let other method calls from the same client start running,
   // without waiting for the email sending to complete.
   this.unblock();
   Meteor.Mailgun.send({
      to: 'federodes@gmail.com',
      from: 'fulbacho.net <no-reply@fulbacho.net>',
      subject: 'Fulbacho user contact!',
      text: "User name: " + name + "\r\n\r\n" + "User email: " + email + "\r\n\r\n" + "Message: " + message,
      html: 'User name: ' + name + '<br><br>' + 'User email: ' + email + '<br><br>' + 'Message: ' + message
   });
   console.log("email sent!");

}});
