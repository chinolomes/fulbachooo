/*
DESCRIPTION: returns array of users matching the given regular expression.
INPUT: String
OUTPUT: Array
*/
// CHECKED
// TODO: add rating limit
//======================================================================
// GET LIST OF USERS MATCHING REGULAR EXPRESSION:
//======================================================================
Meteor.methods({'getUsersMatchingRegExp': function(partialName) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

	console.log('GET USER MATCHING REGEX: ', partialName);
	check(partialName, String);

   ///////////////
   // VARIABLES //
   ///////////////

	var selector = {
      'profile.name': {
         '$regex': '^' + partialName,
         '$options': 'i' // upper/lower case insensitive
      }
   }
	var options = {
		fields: {
			'profile.name': 1,
			'profile.avatar': 1
		},
      limit: 10
	};

   /////////////
   // QUERIES //
   /////////////

	return Meteor.users.find(selector, options).fetch();

}});
