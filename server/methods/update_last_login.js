// CHECKED
// TODO: add rating limit
//======================================================================
// UPDATE USER LAST LOGIN:
//======================================================================
Meteor.methods({'updateLastLogin': function() {

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Update Last Login';
   var curUserId = this.userId; // server side only
   var curUser = null;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {'profile.name': 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	console.log('USER JUST LOGGED IN: ', curUser.profile.name);
	Meteor.users.update({_id: curUserId}, {$set: {'lastLogin': new Date()}});

}});
