/*
DESCRIPTION: adds activityId to owner, admin and participants docs
*/
// CHECKED
// TODO: this must be a JS function instead of a Meteor method
//======================================================================
// ATTACH ACTIVITY ID TO OWNER, ADMIN AND PARTICPANTS:
//======================================================================
Meteor.methods({'attachActivityIdToOwnerAdminAndPartcipants': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Attach Activity Id to Owner Admin and Partcipants';
   var match = null;
   var result = {};
   var privacy = '';
   var key = '';
   var addToSet = { // DB modifier
      created: {},
      admin: {},
      enrolled: {}
   };

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"privacy": 1, "status": 1, "createdBy": 1, "peopleIn": 1, "admin": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   privacy = match.privacy;
   // initial addToSet = {created: {}, admin: {}, enrolled: {}};
   for (key in addToSet) {
      addToSet[key]['activities.upcoming.' + privacy + '.' + key] = matchId;
      addToSet[key]['activities.upcoming.' + privacy + '.following'] = matchId;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Attach activityId to organizer
	Meteor.users.update({_id: match.createdBy}, {$addToSet: addToSet.created});

   // Attach activityId to admin
   if (!_.isUndefined(match.admin) && match.admin.length > 0) {
      Meteor.users.update({_id: match.admin[0]}, {$addToSet: addToSet.admin});
   }
   // Attach activityId to participants
   Meteor.users.update({_id: {$in: getUserIds(match.peopleIn)}}, {$addToSet: addToSet.enrolled}, {multi: true});

}});
