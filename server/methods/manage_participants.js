/*
DESCRIPTION: given the new list of participants (which includes registered users,
friends and substitutes), determine if the new composition is feasible, add the
new memebers to the activity doc as well ass the activityId into the users doc,
and remove the those particpants that left the activity
REQUIRED: cuerrent user must be owner or admin
INPUT: newPeopleInTeamA, newFriendsOfTeamA, newPeopleInTeamB, newFriendsOfTeamB, newWaitingListTeamA and newWaitingListTeamB are all id-arrays
*/
// TODO: don't send notifications in case it's just a re-order
// CHECKED
// TODO: add rating limit
//======================================================================
// MANAGE PARTICIPANTS:
//======================================================================
Meteor.methods({'manageParticipants': function(matchId, newPeopleInTeamA, newFriendsOfTeamA, newPeopleInTeamB, newFriendsOfTeamB, newWaitingListTeamA, newWaitingListTeamB) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('MANAGE PARTICIPANTS');
   check(matchId, String);
   check(newPeopleInTeamA, [String]);
   check(newFriendsOfTeamA, [String]);
   check(newPeopleInTeamB, [String]);
   check(newFriendsOfTeamB, [String]);
   check(newWaitingListTeamA, [String]);
   check(newWaitingListTeamB, [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Manage Participants';
   var curUserId = this.userId; // server side only
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var curUser = null;
   var match = null;
   var result = {};
   var prevPeopleInTeamA = []; // array of user ids
   var prevPeopleInTeamB = []; // array of user ids
   var prevFriendsOfTeamA = []; // array of user ids
   var prevFriendsOfTeamB = []; // array of user ids
   var prevWaitingListTeamA = []; // array of user ids
   var prevWaitingListTeamB = []; // array of user ids
   var addedPeopleInTeamA = []; // array of user ids
   var addedPeopleInTeamB = []; // array of user ids
   var addedFriendsOfTeamA = []; // array of user ids
   var addedFriendsOfTeamB = []; // array of user ids
   var addedWaitingListTeamA = []; // array of user ids
   var addedWaitingListTeamB = []; // array of user ids
   var removedPeopleInTeamA = []; // array of user ids
   var removedPeopleInTeamB = []; // array of user ids
   var removedFriendsOfTeamA = []; // array of user ids
   var removedFriendsOfTeamB = []; // array of user ids
   var removedWaitingListTeamA = []; // array of user ids
   var removedWaitingListTeamB = []; // array of user ids
   var added = []; // array of user ids
   var addedLength = 0;
   var changesTeamA = 0; // number of changes made
   var changesTeamB = 0; // number of changes made
   var peopleInToAdd = []; // array of objects
   var peopleInToRemove = []; // array of ids
   var friendsOfToAdd = []; // array of objects
   var friendsOfToRemove = []; // array of ids
   var waitingListToAdd = []; // array of objects
   var waitingListToRemove = []; // array of ids
   var key = '';
   var addToSet = { // DB modifier
      enrolled: {},
      friendIsEnrolled: {},
      waiting: {}
   };
   var pull = { // DB modifier
      enrolled: {},
      friendIsEnrolled: {},
      waiting: {}
   };
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameAddRemovePlayer';
   var textEmail = 'Email_Notif_User_Modified_List_Of_Particpants_In_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {"profile.name": 1}});
   match = Matches.findOne({_id: matchId});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
	// Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // If the activity-column is NOT full, the waiting list shouldn't be displayed
   if (getPeopleMissing(match, 'A') > 0 && (filterByTeam(match.waitingList, 'A').length > 0 || newWaitingListTeamA.length > 0)) {
      throwError(context, 'activity is NOT full');
      return unexpError; // msg

   } else if (getPeopleMissing(match, 'B') > 0 && (filterByTeam(match.waitingList, 'B').length > 0 || newWaitingListTeamB.length > 0)) {
      throwError(context, 'activity is NOT full');
      return unexpError; // msg
   }
   // Current user is owner/admin
   if (!curUserIsOwnerOrAdmin(match)) {
      throwError(context, 'current user is not owner nor admin');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   // Previous participants
   prevPeopleInTeamA = getUserIds(filterByTeam(match.peopleIn, 'A'));
   prevPeopleInTeamB = getUserIds(filterByTeam(match.peopleIn, 'B'));
   prevFriendsOfTeamA = getUserIds(filterByTeam(match.friendsOf, 'A'));
   prevFriendsOfTeamB = getUserIds(filterByTeam(match.friendsOf, 'B'));
   prevWaitingListTeamA = getUserIds(filterByTeam(match.waitingList, 'A'));
   prevWaitingListTeamB = getUserIds(filterByTeam(match.waitingList, 'B'));

   // Participants added to the teams
   addedPeopleInTeamA = _.difference(newPeopleInTeamA, prevPeopleInTeamA);
   addedPeopleInTeamB = _.difference(newPeopleInTeamB, prevPeopleInTeamB);
   addedFriendsOfTeamA = _.difference(newFriendsOfTeamA, prevFriendsOfTeamA);
   addedFriendsOfTeamB = _.difference(newFriendsOfTeamB, prevFriendsOfTeamB);
   addedWaitingListTeamA = _.difference(newWaitingListTeamA, prevWaitingListTeamA);
   addedWaitingListTeamB = _.difference(newWaitingListTeamB, prevWaitingListTeamB);

   // Participants removed from the teams
   removedPeopleInTeamA = _.difference(prevPeopleInTeamA, newPeopleInTeamA);
   removedPeopleInTeamB = _.difference(prevPeopleInTeamB, newPeopleInTeamB);
   removedFriendsOfTeamA = _.difference(prevFriendsOfTeamA, newFriendsOfTeamA);
   removedFriendsOfTeamB = _.difference(prevFriendsOfTeamB, newFriendsOfTeamB);
   removedWaitingListTeamA = _.difference(prevWaitingListTeamA, newWaitingListTeamA);
   removedWaitingListTeamB = _.difference(prevWaitingListTeamB, newWaitingListTeamB);

   // Ids related to all added participants (needed for checking user existance)
   added = _.union(addedPeopleInTeamA, addedPeopleInTeamB, addedFriendsOfTeamA, addedFriendsOfTeamB, addedWaitingListTeamA, addedWaitingListTeamB);
   addedLength = added.length;

   // Calculate the number of changes made
   changesTeamA = addedPeopleInTeamA.length + addedFriendsOfTeamA.length + addedWaitingListTeamA.length + removedPeopleInTeamA.length + removedFriendsOfTeamA.length + removedWaitingListTeamA.length;
   changesTeamB = addedPeopleInTeamB.length + addedFriendsOfTeamB.length + addedWaitingListTeamB.length + removedPeopleInTeamB.length + removedFriendsOfTeamB.length + removedWaitingListTeamB.length;

   // PeopleIn to add (array of objects)
   _.each(addedPeopleInTeamA, function(userId) {
      peopleInToAdd.push({'userId': userId, 'team': 'A'});
   });
   _.each(addedPeopleInTeamB, function(userId) {
      peopleInToAdd.push({'userId': userId, 'team': 'B'});
   });
   // FriendsOf to add (array of objects)
   _.each(addedFriendsOfTeamA, function(userId) {
      friendsOfToAdd.push({'name': 'Friend_Of', 'team': 'A', 'userId': userId});
   });
   _.each(addedFriendsOfTeamB, function(userId) {
      friendsOfToAdd.push({'name': 'Friend_Of', 'team': 'B', 'userId': userId});
   });
   // WaitingList to add (array of objects)
   _.each(addedWaitingListTeamA, function(userId) {
      waitingListToAdd.push({'userId': userId, 'team': 'A'});
   });
   _.each(addedWaitingListTeamB, function(userId) {
      waitingListToAdd.push({'userId': userId, 'team': 'B'});
   });
   // Ids to be removed (array of ids)
   peopleInToRemove = _.union(removedPeopleInTeamA, removedPeopleInTeamB);
   friendsOfToRemove = _.union(removedFriendsOfTeamA, removedFriendsOfTeamB);
   waitingListToRemove = _.union(removedWaitingListTeamA, removedWaitingListTeamB);

   // Needed for notifications
   senderName = curUser.profile.name;
   userIdsToNotify = _.without(_.union(match.followers, getUserIds(peopleInToAdd), peopleInToRemove, getUserIds(friendsOfToAdd), friendsOfToRemove, getUserIds(waitingListToAdd), waitingListToRemove), curUserId);
   /*
   console.log('prevPeopleInTeamA: '+ prevPeopleInTeamA);
   console.log('newPeopleInTeamA: '+ newPeopleInTeamA);
   console.log('addedPeopleInTeamA: '+ addedPeopleInTeamA);
   console.log('removedPeopleInTeamA: '+ removedPeopleInTeamA);
   */

   ////////////
   // CHECKS //
   ////////////

   // Any changes?
   if (changesTeamA + changesTeamB === 0) {
      return {status: 'warning', text: 'No_Changes_Were_Made'}; // msg
   }
   // Participants fit in team/column
   if (newPeopleInTeamA.length + newFriendsOfTeamA.length > match.fieldSize) {
      return {status: 'error', text: 'The_Number_Of_Participants_Exceeds_The_Maximum', title: 'Operation_Unsuccessful'}; // msg

   } else if (newPeopleInTeamB.length + newFriendsOfTeamB.length > match.fieldSize) {
      return {status: 'error', text: 'The_Number_Of_Participants_Exceeds_The_Maximum', title: 'Operation_Unsuccessful'}; // msg
   }
   // Repetitions among participants
   if (_.intersection(newPeopleInTeamA, newPeopleInTeamB).length > 0) {
   	return {status: 'error', text: 'Participants_Are_Duplicate', title: 'Operation_Unsuccessful'}; // msg

   } else if (_.intersection(newFriendsOfTeamA, newFriendsOfTeamB).length > 0) {
   	return {status: 'error', text: 'Friends_Are_Duplicate', title: 'Operation_Unsuccessful'}; // msg
   }
   // Repetitions among participants and waiting list
   if (_.intersection(_.union(newPeopleInTeamA, newPeopleInTeamB), _.union(newWaitingListTeamA, newWaitingListTeamB)).length > 0) {
   	return {status: 'error', text: 'Participants_Are_Duplicate', title: 'Operation_Unsuccessful'}; // msg
   }
   // All added users should be registered
   if (Meteor.users.find({_id: {$in: added}}, {fields: {"_id": 1}}).count() !== addedLength) {
      throwError(context, 'at least one of the participants is not registered.');
      return {status: 'error', text: 'User_Not_Registered', title: 'Operation_Unsuccessful'}; // msg
   }
   // ActivityId is in users (to be removed) doc
   /*if (Meteor.users.find({_id: {$in: peopleInToRemove}, 'activities.upcoming.public.enrolled': matchId}).count() !== peopleInToRemove.length) {
      throwError(context, 'activityId is not in user doc: peopleInToRemove');
      return unexpError;
   }
   if (Meteor.users.find({_id: {$in: friendsOfToRemove}, 'activities.upcoming.public.friendIsEnrolled': matchId}).count() !== friendsOfToRemove.length) {
      throwError(context, 'activityId is not in user doc: friendsOfToRemove');
      return unexpError;
   }
   if (Meteor.users.find({_id: {$in: waitingListToRemove}, 'activities.upcoming.public.waiting': matchId}).count() !== waitingListToRemove.length) {
      throwError(context, 'activityId is not in user doc: waitingListToRemove');
      return unexpError;
   }*/

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   privacy = match.privacy;
   // initial pull = {enrolled: {}, friendIsEnrolled: {}, waiting: {}};
   for (key in pull) {
      pull[key]['activities.upcoming.' + privacy + '.' + key] = matchId;
   }
   // initial addToSet = {enrolled: {}, friendIsEnrolled: {}, waiting: {}};
   for (key in addToSet) {
      addToSet[key]['activities.upcoming.' + privacy + '.' + key] = matchId;
      addToSet[key]['activities.upcoming.' + privacy + '.following'] = matchId;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Update activity.peopleIn
   Matches.update({_id: matchId}, {$pull: {peopleIn: {'userId': {$in: peopleInToRemove}}}});
   Matches.update({_id: matchId}, {$addToSet: {peopleIn: {$each: peopleInToAdd}, followers: {$each: getUserIds(peopleInToAdd)}}});
   // Update activity.friendsOf
   Matches.update({_id: matchId}, {$pull: {friendsOf: {'userId': {$in: friendsOfToRemove}}}});
   Matches.update({_id: matchId}, {$addToSet: {friendsOf: {$each: friendsOfToAdd}, followers: {$each: getUserIds(friendsOfToAdd)}}});
   // Update activity.waitingList
   Matches.update({_id: matchId}, {$pull: {waitingList: {'userId': {$in: waitingListToRemove}}}});
   Matches.update({_id: matchId}, {$addToSet: {waitingList: {$each: waitingListToAdd}, followers: {$each: getUserIds(waitingListToAdd)}}});

   // Update user doc. First remove activityId from the leaving participants, then attach it to the new participants
	// Remove activityId from those participants that were removed
	Meteor.users.update({_id: {$in: _.union(removedPeopleInTeamA, removedPeopleInTeamB)}}, {$pull: pull.enrolled}, {multi: true});
	// Remove activityId from users whose friend was removed
	Meteor.users.update({_id: {$in: _.union(removedFriendsOfTeamA, removedFriendsOfTeamB)}}, {$pull: pull.friendIsEnrolled}, {multi: true});
   // Remove activityId from removed participants in waitingList
   Meteor.users.update({_id: {$in: _.union(removedWaitingListTeamA, removedWaitingListTeamB)}}, {$pull: pull.waiting}, {multi: true});
   // Attach activityId to the new users in the participants list
   Meteor.users.update({_id: {$in: _.union(addedPeopleInTeamA, addedPeopleInTeamB)}}, {$addToSet: addToSet.enrolled}, {multi: true});
   // Attach activityId to the users whose friends were enrolled
   Meteor.users.update({_id: {$in: _.union(addedFriendsOfTeamA, addedFriendsOfTeamB)}}, {$addToSet: addToSet.friendIsEnrolled}, {multi: true});
   // Attach activityId to the new participants in the waitingList
   Meteor.users.update({_id: {$in: _.union(addedWaitingListTeamA, addedWaitingListTeamB)}}, {$addToSet: addToSet.waiting}, {multi: true});

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the follwoing task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

   	// Send notification to peopleInAdded
   	_.each(peopleInToAdd, function(recipient) {
   		if (recipient.userId !== curUserId) {
   			Notifications.insert({
   				createdBy: curUserId,
   				recipient: recipient.userId,
   				text: 'User_Signed_You_In_For_One_Of_His_Activities',
   				anchor: anchor
   			});
   		}
   	});

   	// Send notification to peopleInRemoved
   	_.each(peopleInToRemove, function(recipient) {
   		if (recipient !== curUserId) {
   			Notifications.insert({
   				createdBy: curUserId,
   				recipient: recipient,
   				text: 'User_Signed_You_Out_From_One_Of_His_Activities',
   				anchor: anchor
   			});
   		}
   	});

   	// Send notification to friendsOfAdded
   	_.each(friendsOfToAdd, function(recipient) {
   		if (recipient.userId !== curUserId) {
   			Notifications.insert({
   				createdBy: curUserId,
   				recipient: recipient.userId,
   				text: 'User_Added_Your_Friend_To_One_Of_His_Activities',
   				anchor: anchor
   			});
   		}
   	});

   	// Send notification to friendsOfRemoved
   	_.each(friendsOfToRemove, function(recipient) {
   		if (recipient !== curUserId) {
   			Notifications.insert({
   				createdBy: curUserId,
   				recipient: recipient,
   				text: 'User_Removed_Your_Friend_From_One_Of_His_Activities',
   				anchor: anchor
   			});
   		}
   	});

   	// Send notification to waitingListIdsAdded
   	_.each(_.without(_.uniq(getUserIds(waitingListToAdd)), curUserId), function(recipientId) {
   		Notifications.insert({
   			createdBy: curUserId,
   			recipient: recipientId,
   			text: 'User_Added_You_To_The_Waiting_List_Of_One_Of_His_Activities',
   			anchor: anchor
   		});
   	});

   	// Send notification to peopleInRemoved
   	_.each(waitingListToRemove, function(recipient) {
   		if (recipient !== curUserId) {
   			Notifications.insert({
   				createdBy: curUserId,
   				recipient: recipient,
   				text: 'User_Removed_You_From_The_Waiting_List_Of_One_Of_His_Activities',
   				anchor: anchor
   			});
   		}
   	});

      // Notify users via email
      Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
   });

	// Re-order participants if needed
   Meteor.call('checkWaitingList', matchId);

   return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg

}});
