/*
DESCRIPTION: get activity sport and location, and add that info to the current
user doc in case the user.sports does not contain said sport and in case the
distance between the user location and the activity location is grater than 50 km
*/
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// SET CURRENT SPORT AND LOCATION:
//======================================================================
Meteor.methods({'setSportAndLocationForCurUser': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([matchId], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Set Sport And Location';
   var curUserId = this.userId;
   var match = null;
   var curUser = null;
   var userSports = [];
   var userLocation = {};
   var activitySport = '';
   var activityLocation = {};

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {"profile.sports": 1, "geo.loc.coordinates": 1}});
   match = Matches.findOne({_id: matchId}, {fields: {"sport": 1, "loc.coordinates": 1}});

   ////////////
   // CHECKS //
   ////////////

   if (!curUser || !match) {
      throwError(context, '!user || !activity');
      return;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   userSports = curUser.profile.sports;
   userLocation = {
      latitude: curUser.geo.loc.coordinates[1],
      longitude: curUser.geo.loc.coordinates[0]
   },
   activitySport = match.sport;
   activityLocation = {
      latitude: match.loc.coordinates[1],
      longitude: match.loc.coordinates[0]
   };

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Add activitySport to the userSports in case it's not set yet
   if (_.indexOf(userSports, activitySport) === -1) {
      Meteor.users.update({_id: curUserId}, {$addToSet: {"profile.sports": activitySport}});
   }

   // In case the distance between the current userLocation and the activityLocation is grater than 50km...
   if (geolib.getDistance(activityLocation, userLocation) >= 50000) { // Geolib returns distance in meters
      // ...reset userLocation to activityLocation
      Meteor.users.update({_id: curUserId}, {$set: {"geo.loc.coordinates": [activityLocation.longitude, activityLocation.latitude]}});
      // See client/autorun/listen_to_cur_user_location_changes.js for session vars update
   }

}});
