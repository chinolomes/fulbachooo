// CHECKED: public and private activities
// TODO: add rating limit
//======================================================================
// FOLLOW/UNFOLLOW ACTIVITY:
//======================================================================
Meteor.methods({'followUnfollowActivity': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Follow / Unfollow Activity';
   var curUserId = this.userId; // server side only
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var match = null;
   var result = {};
   var modifier = {}; // DB modifier

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"status": 1, "followers": 1, "privacy": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   modifier['activities.upcoming.' + match.privacy + '.following'] = matchId;

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Add/remove current user from the list of followers + update user's doc
   if (_.indexOf(match.followers, curUserId) !== -1) {
      Matches.update({_id: matchId}, {$pull: {followers: curUserId}});
      Meteor.users.update({_id: curUserId}, {$pull: modifier});
      console.log('UNFOLLOW ACTIVITY: ', matchId);
   } else {
      Matches.update({_id: matchId}, {$addToSet: {followers: curUserId}});
      Meteor.users.update({_id: curUserId}, {$addToSet: modifier});
      console.log('FOLLOW ACTIVITY: ', matchId);
   }

}});
