// CHECKED
// TODO: add rating limit
//======================================================================
// REMOVE CURRENT USER FROM PARTICIPANTS LIST:
//======================================================================
Meteor.methods({'removeCurUserFromParticipantsList': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

	check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Remove Current User From Participants List';
   var curUserId = this.userId; // server side only
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var curUser = null;
   var match = null;
	var result = {};
   var pull = {}; // DB modifier
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameAddRemovePlayer';
   var textNotif = 'User_Signed_Out_Of_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_User_Signed_Out_Of_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {'activities': 1, 'profile.name': 1}});
   match = Matches.findOne({_id: matchId}, {fields: {"status": 1, "peopleIn": 1, "followers": 1, "privacy": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
	// Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
	// User is enrolled
   if (_.indexOf(getUserIds(match.peopleIn), curUserId) === -1) {
      throwError(context, 'user not enrolled');
      return unexpError; // msg
   }
   // Check that the activity is in the user record already
   if (_.indexOf(curUser.activities.upcoming[match.privacy].enrolled, matchId) === -1) {
      throwError(context, 'activity NOT in user.enrolled');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

	pull['activities.upcoming.' + match.privacy + '.enrolled'] = matchId;
   senderName = curUser.profile.name;
   userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Remove user from participants list
	Matches.update({_id: matchId}, {$pull: {peopleIn: {'userId': curUserId}}});

   // Remove activityId from current user
	Meteor.users.update({_id: curUserId}, {$pull: pull});

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the following task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

		// Deliver internal notifications
	   _.each(userIdsToNotify, function(userId) {
	      Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
	   });

	   // Notify users via email
	   Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
	});

	// Re-order participants if needed
   Meteor.call('checkWaitingList', matchId);

}});
