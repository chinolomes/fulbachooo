/*
DESCRIPTION: send internal and email notifications to users in the 21km radius of
the activity location for all users who practice the activity sport
*/
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// NOTIFY USERS IN PLAYING ZONE:
//======================================================================
Meteor.methods({'notifyUsersInNearbyRegion': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('NOTIFY USERS IN NEARBY REGION');
   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Notify Users In Nearby Region';
   var match = null;
   var result = {};
   var activityLocation = {};
   var ubRadius = 21; // upper bound radius
   var ownerId = '';
   var owner = null;
   var ownerName = '';
   var admin = [];
   var query = {};
   var usersInRegion = [];
   var userIdsToNotify = [];
   var userLocation = {};
   var userRadius = 0;
   var textNotif = 'User_Is_Organizing_New_Activity_In_Your_Region';
   var anchor = '/activities/activity/' + matchId + '#data';
   var url = 'https://www.fulbacho.net' + anchor;
   var textEmail = 'Email_Notif_User_Is_Organizing_New_Activity_In_Your_Region';
   var notifType = 'gameNewGamePlayingZone';

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"status": 1, "createdBy": 1, "loc.coordinates": 1, "admin": 1, "sport": 1, "peopleIn": 1}});
   owner = Meteor.users.findOne({_id: match.createdBy}, {fields: {'profile.name': 1}});

   ////////////
   // CHECKS //
   ////////////

   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      throwError(context, 'activity does not exist / is not active. activityId: ' + matchId);
      return;
   }
   // Owner existance
   if (!owner) {
      throwError(context, 'owner does not exist');
      return;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   ownerId = match.createdBy;
   activityLocation = {
      latitude: match.loc.coordinates[1], // 'latitude', 'longitude' is the req sintaxis for geolib
      longitude: match.loc.coordinates[0]
   };
   ownerName = owner.profile.name;
   adminId = match.admin[0];
   // Query users with the selcted sport and inside a radius of 21km from the activity
   selector = {
      'geo.loc': {
         $geoWithin: {
            $centerSphere: [
               [activityLocation.longitude, activityLocation.latitude], // center
               ubRadius / 6378.1 // radius
            ]
         }
      },
      'profile.sports': {
         $in: [match.sport]
      }
   };
   usersInRegion = Meteor.users.find(selector, {fields: {'geo': 1}}).fetch();

   ///////////
   // LOGIC //
   ///////////

   _.each(usersInRegion, function(user) {
      userLocation = {latitude: user.geo.loc.coordinates[1], longitude: user.geo.loc.coordinates[0]};
      userRadius = user.geo.radius;
      // In case the distance between the activity and the user location is less that the user radius, add user to the list of users to notify
      if (geolib.getDistance(activityLocation, userLocation) <= userRadius * 1000) { // Geolib returns distance in meters
         userIdsToNotify.push(user._id);
      }
   });

   // Remove owner, admin and participants
   userIdsToNotify = _.difference(userIdsToNotify, _.union([ownerId], [adminId], getUserIds(match.peopleIn)));

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Deliver internal notifications
	_.each(userIdsToNotify, function(userId) {
		Notifications.insert({createdBy: ownerId, recipient: userId, text: textNotif, anchor: anchor});
	});

   // Notify users via email
   Meteor.call('notifyUsersViaEmail', ownerName, textEmail, url, userIdsToNotify, notifType);

}});
