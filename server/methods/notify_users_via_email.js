/*
DESCRIPTION: for each user in the userIdsToNotify array, compose and email and
add it to the emails collection to be delivered later on by the cron job
*/
// TODO: rewrite emailNotifications schema using objects instead of arrays
// --> don't save email and lang! query data in cron job instead, and also use senderId instead of senderName
// remove lang (query before sending email), remove length
// CHECKED
// TODO: must be a JS function instead of a Meteor method
// TODO: addEmailToStack can be an auxiliary function
//======================================================================
// NOTIFY USERS VIA EMAIL:
//======================================================================
Meteor.methods({'notifyUsersViaEmail': function (senderName, text, url, userIdsToNotify, notifType) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([senderName, text, url, notifType], [String]);
   check(userIdsToNotify, [String]);
   if (userIdsToNotify.length === 0) {
      return;
   }

   ///////////////
   // VARIABLES //
   ///////////////

   var result = {};
   var params = {
      senderName: senderName,
      text: text,
      url: url
      // address
      // lang
   };

   ///////////
   // LOGIC //
   ///////////

   _.each(userIdsToNotify, function(userId) {
      // Get user email address and lang
      result = getEmailAddressAndDeliveryLang(userId, notifType); // result = {address, lang} or undefined
      // If result is undefined, skip to the next iteration
      if (!result) {
         return;
      }
      // Add email to stack to be delivered by cron job
      console.log('adding email to stack: ' + result.address);
      params.address = result.address;
      params.lang = result.lang;
      addEmailToStack(params);
   });

}});
