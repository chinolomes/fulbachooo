/*
DESCRIPTION: inserts the given substitute into the given activity-team (column)
*/
// TODO: don't notify on re-order only
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// INSERT SUBSTITUTE
//======================================================================
Meteor.methods({'insertSubstitute': function(matchId, team, substituteId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('INSERT SUBSTITUTE: ', substituteId);
   check([matchId, team, substituteId], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Insert Substitute';
   var match = null;
   var ownerId = '';
   var owner = null;
   var substitute = null;
   var pull = {}; // DB modifier
   var addToSet = {}; // DB modifier
   var senderName = '';
   var userIdsToNotify = [];
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"privacy": 1, "createdBy": 1, "followers": 1}});
   owner = Meteor.users.findOne({_id: match.createdBy}, {fields: {"profile.name": 1}});
   substitute = Meteor.users.findOne({_id: substituteId}, {fields: {"profile.name": 1}});

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   pull['activities.upcoming.' + match.privacy + '.waiting'] = matchId;
   addToSet['activities.upcoming.' + match.privacy + '.enrolled'] = matchId;
   ownerId = match.createdBy;
   senderName = owner.profile.name;
   userIdsToNotify = _.without(match.followers, substituteId); // differenciate between substitute and the rest of the followers

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Update activity: add substitute into peopleIn and remove it from (both) waitingList(s)
   Matches.update({_id: matchId}, {$pull: {waitingList: {userId: substituteId}}, $addToSet: {peopleIn: {'userId': substituteId, 'team': team}}});

   // Update substitute: remove activityId from user.waiting and add it to user.enrolled
   Meteor.users.update({_id: substituteId}, {$pull: pull, $addToSet: addToSet});

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the follwoing task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

      // Send confirmation request to the substitute that just entered the activity
      Notifications.insert({
         createdBy: ownerId,
         recipient: substituteId,
         text: 'You_Passed_From_Waiting_List_To_The_List_Of_Participants_Confirm_Presence',
         anchor: anchor
      });

      // Notify all followers except the substituted that just entered
      _.each(userIdsToNotify, function(userId) {
      	Notifications.insert({
      		createdBy: substituteId,
      		recipient: userId,
      		text: 'User_Passed_From_Waiting_List_To_The_List_Of_Participants_In_One_Of_Your_Activities',
      		anchor: anchor
      	});
      });

      // Notify the substitute that just entered the activity
      Meteor.call('notifyUsersViaEmail',
         senderName,
         'Email_Notif_You_Passed_From_Waiting_List_To_The_List_Of_Participants_Confirm_Presence',
         url,
         [substituteId],
         'gamePassFromWaitingToPlaying'
      );

      // Notify the rest of the followers via email
      Meteor.call('notifyUsersViaEmail',
         senderName,
         'Email_Notif_User_Passed_From_Waiting_List_To_The_List_Of_Participants_In_One_Of_Your_Activities',
         url,
         userIdsToNotify,
         'gameAddRemovePlayer'
      );
   });

}});
