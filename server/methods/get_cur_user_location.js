/* Source: https://github.com/thebakeryio/meteor-ipgeocoder
Data format:
var geoData = {
   city: {
      name: "Neuilly-sur-Seine"
   },
   continent: {
      name: "Europe",
      code: "EU"
   },
   country: {
      name: "France",
      code: "FR"
   },
   location: {
      latitude: 48.8833,
      longitude: 2.2667,
      time_zone: "Europe/Paris"
   }
}
*/
// CHECKED
// TODO: add rating limit
//======================================================================
// GET USER LOCATION:
//======================================================================
Meteor.methods({'getCurUserLocation': function() {

   ///////////////////
   // AUX FUNCTIONS //
   ///////////////////

	var displayCountryName = function (geoData) {
		if (geoData && geoData.country && geoData.country.name) {
			console.log('COUNTRY: ', geoData.country.name);
		} else {
			console.log('CANT DETERMINE COUNTRY!');
		}
	}

   ///////////////
   // VARIABLES //
   ///////////////

	var context = 'Get Current User Location';
	var defaultGeoData = {location: {latitude: DEFAULT_LAT, longitude: DEFAULT_LNG}};
	var geoData = null;
	var ip = ''; // string
	var userAgent = ''; // string
	var isBot = null;


	// return defaultGeoData.location;
   /////////////
   // HEADERS //
   /////////////

	ip = headers.get(this, 'x-forwarded-for');
	userAgent = headers.get(this, 'user-agent');
	// ip = "130.89.230.77"; // (Netherlands) testing only
	// ip = "157.92.29.3"; // (Argentina) testing only

   ////////////
   // CHECKS //
   ////////////

	// Is IP address set?
	if (!ip || ip === '') {
		return defaultGeoData.location;
	}
	console.log('IP: '+ ip);
	// Is user agent set?
	if (!userAgent || userAgent === '') {
		return defaultGeoData.location;
	}
	console.log('USER AGENT: '+ userAgent);

   ///////////////////
   // SET VARIABLES //
   ///////////////////

	// string.match returns null if not match is found
	isBot = userAgent.match(/curl|Bot|B-O-T|Crawler|Spider|Spyder|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|Charlotte|NewsGator|TinEye|Cerberian|SearchSight|Zao|Scrubby|Qseero|PycURL|Pompos|oegp|SBIder|yoogliFetchAgent|yacy|webcollage|VYU2|voyager|updated|truwoGPS|StackRambler|Sqworm|silk|semanticdiscovery|ScoutJet|Nymesis|NetResearchServer|MVAClient|mogimogi|Mnogosearch|Arachmo|Accoona|holmes|htdig|ichiro|webis|LinkWalker|lwp-trivial/i);

   ///////////
   // LOGIC //
   ///////////

	// Is it a Bot?
	if (!_.isNull(isBot)) {
		console.log('BOT!!');
		return defaultGeoData.location;
	}

	// Not a Bot, calculate location...
	/* try {
		this.unblock();
		geoData = IPGeocoder.geocode(ip);
	} catch(e) {
	   throwError(context, "Cannot get location data: " + e);
	} */

	// In case no results were found, set default and return
	if (!geoData || !geoData.location) {
		console.log('No geoData, return default');
		return defaultGeoData.location;
	}

	displayCountryName(geoData);
	// If country is Netherlands, default to Enschede
	if (geoData && geoData.country && geoData.country.name && geoData.country.name === 'Netherlands') {
		return defaultGeoData.location;
	}
	return geoData.location;

}});

/*
// CHECKED
// TODO: add rating limit
//======================================================================
// GET USER LOCATION:
//======================================================================
Meteor.methods({'getCurUserLocation': function() {

   ///////////////////
   // AUX FUNCTIONS //
   ///////////////////

	var displayCountryName = function (geoData) {
		if (geoData && geoData.country && geoData.country.name) {
			console.log('COUNTRY: ', geoData.country.name);
		} else {
			console.log('CANT DETERMINE COUNTRY!');
		}
	}

   ///////////////
   // VARIABLES //
   ///////////////

	var context = 'Get Current User Location';
	var defaultGeoData = {location: {latitude: DEFAULT_LAT, longitude: DEFAULT_LNG}};
	var geoData = null;
	var ip = ''; // string
	var userAgent = ''; // string
	var isBot = null;

   /////////////
   // HEADERS //
   /////////////

	ip = headers.get(this, 'x-forwarded-for');
	userAgent = headers.get(this, 'user-agent');
	// ip = "130.89.230.77"; // (Netherlands) testing only
	// ip = "157.92.29.3"; // (Argentina) testing only

   ////////////
   // CHECKS //
   ////////////

	// Is IP address set?
	if (!ip || ip === '') {
		return defaultGeoData.location;
	}
	console.log('IP: '+ ip);
	// Is user agent set?
	if (!userAgent || userAgent === '') {
		return defaultGeoData.location;
	}
	console.log('USER AGENT: '+ userAgent);

   ///////////////////
   // SET VARIABLES //
   ///////////////////

	// string.match returns null if not match is found
	isBot = userAgent.match(/curl|Bot|B-O-T|Crawler|Spider|Spyder|Yahoo|ia_archiver|Covario-IDS|findlinks|DataparkSearch|larbin|Mediapartners-Google|NG-Search|Snappy|Teoma|Jeeves|Charlotte|NewsGator|TinEye|Cerberian|SearchSight|Zao|Scrubby|Qseero|PycURL|Pompos|oegp|SBIder|yoogliFetchAgent|yacy|webcollage|VYU2|voyager|updated|truwoGPS|StackRambler|Sqworm|silk|semanticdiscovery|ScoutJet|Nymesis|NetResearchServer|MVAClient|mogimogi|Mnogosearch|Arachmo|Accoona|holmes|htdig|ichiro|webis|LinkWalker|lwp-trivial/i);

   ///////////
   // LOGIC //
   ///////////

	// Is it a Bot?
	if (!_.isNull(isBot)) {
		console.log('BOT!!');
		return defaultGeoData.location;
	}

	// Not a Bot, calculate location...
	try {
		this.unblock();
		geoData = IPGeocoder.geocode(ip);
	} catch(e) {
	   throwError(context, "Cannot get location data: " + e);
	}

	// In case no results were found, set default and return
	if (!geoData || !geoData.location) {
		console.log('No geoData, return default');
		return defaultGeoData.location;
	}

	displayCountryName(geoData);
	// If country is Netherlands, default to Enschede
	if (geoData && geoData.country && geoData.country.name && geoData.country.name === 'Netherlands') {
		return defaultGeoData.location;
	}
	return geoData.location;

}});
*/
