/*
DESCRIPTION: checks if any of the substitutes can pass from the waiting list to
the list of participants. This method is called when the list of participants
is modified
*/
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// CHECK WAITING LIST:
//======================================================================
Meteor.methods({'checkWaitingList': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);
   console.log('CHECKING WL... matchId: ', matchId);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Check Waiting List';
   var match = null;
   var result = {};
   var teams = ['A', 'B'];
   var sortedList = [];
   var freeSpots = 0;
   var i = 0;
   var listLength = 0;
   var substituteId = '';

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"status": 1, "peopleIn": 1, "friendsOf": 1, "waitingList": 1, "fieldSize": 1}});

   ////////////
   // CHECKS //
   ////////////

	// Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // In case there are no substitutes in the waiting list or the activity is full, skip
   if (match.waitingList.length === 0 || getPeopleMissing(match) === 0) {
   	return;
   }
   // Repetition
   if (_.intersection(match.peopleIn, match.waitingList).length > 0) {
      throwError(context, 'not empty intersection between peopleIn and waitingList');
      return;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   _.each(teams, function(team) {

      // Re-query activity (waiting list might have changed!).
      match = Matches.findOne({_id: matchId}, {fields: {"peopleIn": 1, "friendsOf": 1, "waitingList": 1, "fieldSize": 1}});

      // Sort waiting list based on added date
      sortedList = _.sortBy(filterByTeam(match.waitingList, team), 'addedAt');
      freeSpots = getPeopleMissing(match, team);
      listLength = sortedList.length;

      // Insert substitutes one by one until the activity is full
      for (i = 0; i < listLength && freeSpots > 0; i++) {
         substituteId = sortedList[i].userId;
         Meteor.call('insertSubstitute', matchId, team, substituteId); // synchronous call
         freeSpots--;
      }

   });

}});
