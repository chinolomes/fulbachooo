/**
DESCRIPTION: adds the current logged in user to the list of participants for the
given 'team' (also called 'column').
*/
// CHECKED!!
// TODO: add rating limit
//======================================================================
// ADD CURRENT USER TO PARTICIPANTS LIST:
//======================================================================
Meteor.methods({'addCurUserToParticipantsList': function(matchId, team) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([matchId, team], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Add Current User To Participants List';
   var curUserId = this.userId; // server side only
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var curUser = null;
   var match = null;
   var result = {};
   var addToSet = {}; // DB modifier
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameAddRemovePlayer';
   var textNotif = 'User_Signed_In_For_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_User_Signed_In_For_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {'activities': 1, 'profile.name': 1}});
   match = Matches.findOne({_id: matchId});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Team / column can't be full
   if (getPeopleMissing(match, team) === 0) {
      throwError(context, 'team is full');
      return unexpError; // msg
   }
   // There can't be people in the waiting list
   if (filterByTeam(match.waitingList, team).length > 0) {
      throwError(context, 'people in waiting list');
      return unexpError; // msg
   }
   // User can't be in the list of participants
   if (_.indexOf(getUserIds(match.peopleIn), curUserId) !== -1) {
      throwError(context, 'user already in the list of participants');
      return unexpError; // msg
   }
   // ActivityId can't be in the user doc
   if (_.indexOf(curUser.activities.upcoming[match.privacy].enrolled, matchId) !== -1) {
      throwError(context, 'activity already in user.enrolled');
      return unexpError; // msg

   } else if (_.indexOf(curUser.activities.upcoming[match.privacy].waiting, matchId) !== -1) {
      throwError(context, 'activity already in user.waiting');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   addToSet['activities.upcoming.' + match.privacy + '.enrolled'] = matchId;
   senderName = curUser.profile.name;
   userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Concatenate the new participant with the previous ones
   Matches.update({_id: matchId}, {$addToSet: {peopleIn: {'userId': curUserId, 'team': team}}});

   // Attach activityId to current user
   Meteor.users.update({_id: curUserId}, {$addToSet: addToSet});

   // Add current user to activity's list of followers, and add activityId to the list of activities being followed by the user
   if (_.indexOf(match.followers, curUserId) === -1) {
      Meteor.call('followUnfollowActivity', matchId);
   }

   // Set user location and sport if needed
   Meteor.call('setSportAndLocationForCurUser', matchId);

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the following task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

      // Deliver internal notifications
      _.each(userIdsToNotify, function(userId) {
         Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
      });

      // Notify users via email
      Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
   });

}});
