// SOURCE: https://themeteorchef.com/snippets/using-the-email-package/
/* TODO: modify email structure:
obj = {
   _id: String,
   recipient: String,
   lang: 'en/es/...',
   sent: Boolean,
   data: [{
      sender: String,
      text: String,
      url: String
   }, {}, ...]
}
*/
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// MAIL GUN SEND EMAIL:
//======================================================================
Meteor.methods({'sendNotifEmail': function (params) {

   console.log("about to send email...");
   check(params, {
      to: String,
      lang: String,
      chunks: [Object] // {text, link}
   });

   var lang = params.lang;

   SSR.compileTemplate('htmlEmail', Assets.getText('email-notif-template.html'));
   Template.htmlEmail.helpers({
      'title': function() {
         return TAPi18n.__('You_Have_Notifications', {}, lang_tag = lang);
      },
      'chunks': function() {
         return params.chunks;
      },
      'thanks': function() {
         return TAPi18n.__('Thanks_For_Choosing_Fulbacho', {}, lang_tag = lang);
      },
      'facebook': function() {
         return TAPi18n.__('Support_Us_On_Facebook', {}, lang_tag = lang);
      },
      'youtube': function() {
         return TAPi18n.__('Subscribe_To_Our_Youtube_Channel', {}, lang_tag = lang);
      }
   })

   // Let other method calls from the same client start running,
   // without waiting for the email sending to complete.
   this.unblock();
   Meteor.Mailgun.send({
      to: params.to,
      from: 'fulbacho.net <no-reply@fulbacho.net>',
      subject: TAPi18n.__('You_Have_Notifications', {}, lang_tag = lang),
      html: SSR.render('htmlEmail')//, emailData)
   });
   console.log("email sent!");

}});
