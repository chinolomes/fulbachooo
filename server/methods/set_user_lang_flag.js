// CHECKED
// TODO: add rating limit
//======================================================================
// SET USER LANGUAGE FLAG TO TRUE:
//======================================================================
Meteor.methods({'setUserLangFlag': function() {

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Set User Language Flag';
   var curUserId = this.userId;
   var unexpError = {status: 'error', text: 'Unexpected_Error'};

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	Meteor.users.update({_id: curUserId}, {$set: {'profile.lang.flag': true}});

}});
