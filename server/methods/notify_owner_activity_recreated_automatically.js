/*
DESCRIPTION: sends internal and email notifications to activity's owner every time
one of his/her activities is re-created automatically
*/
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// NOTIFY OWNER THE ACTIVITY WAS RECREATED AUTOMATICALLY:
//======================================================================
Meteor.methods({'notifyOwnerActivityRecreatedAutomatically': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('NOTIFY OWNER ACTIVITY RECREATED AUTOMATICALLY');
   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Notify Owner Activity Recreated Automatically';
   var match = null;
   var owner = null;
   var result = {};
   var ownerId = '';
   var ownerName = '';
   var textNotif = 'Activity_Recreated_Automatically';
   var anchor = '/activities/activity/' + matchId + '#data';
   var url = 'https://www.fulbacho.net' + anchor;
   var textEmail = 'Email_Notif_Activity_Recreated_Automatically';
   var notifType = 'gameNewGameRepeat';

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "status": 1}});
   owner = Meteor.users.findOne({_id: match.createdBy}, {fields: {"profile.name": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      throwError(context, 'activity does not exist / is not active. activityId: ' + matchId);
      return;
   }
   // Owner existance
   if (!owner) {
      throwError(context, 'owner does not exist');
      return;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   ownerId = match.createdBy;
   ownerName = owner.profile.name;

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Deliver internal notification
   Notifications.insert({createdBy: ownerId, recipient: ownerId, text: textNotif, anchor: anchor});

   // Notify owner via email
   Meteor.call('notifyUsersViaEmail', ownerName, textEmail, url, [ownerId], notifType);

}});
