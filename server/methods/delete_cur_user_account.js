// CHECKED
// TODO: add rating limit
//======================================================================
// DELETE CURRENT USER ACCOUNT:
//======================================================================
Meteor.methods({'deleteCurUserAccount': function() {
  var curUserId = this.userId; // server side only

  var selector = {_id: curUserId};
  var curUser = Meteor.users.findOne(selector);

  if (!curUser) {
    throw new Error(401, 'the user is not logged in');
  }

  var userEmails = _.pluck(curUser.emails, 'address') || [];

  CollectionsCounter.update({ collectionName: 'users' }, { $inc: { counter: -1 } });

  if (userEmails.length > 0) {
    Emails.remove({ recipient: { $in: userEmails } });
  }
  Posts.remove({ createdBy: curUserId });
  Meteor.users.remove(selector);
}});
