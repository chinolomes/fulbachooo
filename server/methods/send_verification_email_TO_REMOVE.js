// SOURCE: https://themeteorchef.com/snippets/using-the-email-package/
//======================================================================
// SEND VERIFICATION EMAIL:
//======================================================================
/*Meteor.methods({'sendVerificationEmail': function (userId, email, token, lang) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('email: ', email);
   console.log("about to send email...");
   check([userId, email, token, lang], [String]);

   ///////////////
   // SET EMAIL //
   ///////////////

   SSR.compileTemplate('htmlEmail', Assets.getText('email-verif-template.html'));
   Template.htmlEmail.helpers({
      'title': function() {
         return TAPi18n.__('Email_Address_Verification', {}, lang_tag = lang);
      },
      'text': function() {
         return TAPi18n.__('To_Verify_Your_Email_Address_Click_On_The_Following_Link', {}, lang_tag = lang);
      },
      'link': function() {
         return 'https://www.fulbacho.net/verify-email/' + userId + '/' + token;
      },
      'button': function() {
         return TAPi18n.__('Verify_Email', {}, lang_tag = lang);
      },
      'greetings': function() {
         return TAPi18n.__('Thanks_For_Choosing_Fulbacho', {}, lang_tag = lang);
      },
      'facebook': function() {
         return TAPi18n.__('Support_Us_On_Facebook', {}, lang_tag = lang);
      },
      'youtube': function() {
         return TAPi18n.__('Subscribe_To_Our_Youtube_Channel', {}, lang_tag = lang);
      }
   })

   ////////////////
   // SEND EMAIL //
   ////////////////

   // Let other method calls from the same client start running,
   // without waiting for the email sending to complete.
   this.unblock();
   Meteor.Mailgun.send({
      to: email,
      from: 'fulbacho.net <no-reply@fulbacho.net>',
      subject: TAPi18n.__('Email_Address_Verification', {}, lang_tag = lang),
      html: SSR.render('htmlEmail')
   });
   console.log("email sent!");

}});*/
