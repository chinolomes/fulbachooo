/*
DESCRIPTION: send welcome email to recipientId
INPUT: doc = {_id: 'id', recipientId: 'userId', [sent: 'boolean]}
*/
// SOURCES: // https://themeteorchef.com/snippets/using-the-email-package/
// https://meteorhacks.com/server-side-rendering
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// SEND WELCOME EMAIL:
//======================================================================
Meteor.methods({'sendWelcomeEmail': function (doc) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log("about to send email...");
   check(doc, {
      _id: String,
      recipientId: String
      //sent: Boolean
   });

   ///////////////////
	// AUX FUNCTIONS //
	///////////////////

	// Returns 'true' if the given user has all the requiered data properly set, and 'false' otherwise.
	var allFieldsAreSet = function(user) {
	   return user.notifEmails && user.notifEmails.primary && user.notifEmails.primary.length > 0 && user.welcome === false
             && user.profile.lang.flag === true && _.indexOf(LANGS, user.profile.lang.value) !== -1;
	}

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Send Welcome Email';
   var welcomeEmailId = doc._id;
   var recipientId = doc.recipientId;
   var recipient = null;
   var email = '';
   var lang = '';
   var recipientName = '';

	/////////////
	// QUERIES //
	/////////////

   recipient = Meteor.users.findOne({_id: recipientId}, {fields: {"welcome": 1, "profile.name": 1, "profile.lang": 1, "notifEmails.primary": 1}});

	////////////
	// CHECKS //
	////////////

	if (!recipient) {
      throwError(context, 'user is not registered');
	   return;
	}
	if (!allFieldsAreSet(recipient)) {
      console.log(context + ': fields are not properly set!');
	   return;
	}

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   email = recipient.notifEmails.primary;
   lang = recipient.profile.lang.value;
   recipientName = recipient.profile.name.split(' ')[0]; // get first name, chop off last name

   ///////////////
   // SET EMAIL //
   ///////////////

   SSR.compileTemplate('htmlEmail', Assets.getText('email-welcome-template.html'));
   Template.htmlEmail.helpers({
      'title': function() {
         return TAPi18n.__('Welcome_To_Fulbacho', {}, lang_tag = lang);
      },
      'lineOne': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_One', {name: recipientName}, lang_tag = lang);
      },
      'lineTwo': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_Two', {}, lang_tag = lang);
      },
      'lineThree': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_Three', {}, lang_tag = lang);
      },
      'lineFour': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_Four', {}, lang_tag = lang);
      },
      'lineFive': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_Five', {}, lang_tag = lang);
      },
      'lineSix': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_Six', {}, lang_tag = lang);
      },
      'lineSeven': function() {
         return TAPi18n.__('Welcome_To_Fulbacho_Email_Line_Seven', {}, lang_tag = lang);
      },
      'thanks': function() {
         return TAPi18n.__('Thanks_For_Choosing_Fulbacho', {}, lang_tag = lang);
      },
      'facebook': function() {
         return TAPi18n.__('Support_Us_On_Facebook', {}, lang_tag = lang);
      },
      'youtube': function() {
         return TAPi18n.__('Subscribe_To_Our_Youtube_Channel', {}, lang_tag = lang);
      }
   });

   ////////////////
   // SEND EMAIL //
   ////////////////

   // Let other method calls from the same client start running,
   // without waiting for the email sending to complete.
   this.unblock();
   Meteor.Mailgun.send({
      to: email,
      from: 'fulbacho.net <no-reply@fulbacho.net>',
      subject: TAPi18n.__('Welcome_To_Fulbacho', {}, lang_tag = lang),
      html: SSR.render('htmlEmail')
   });

   WelcomeEmails.update({_id: welcomeEmailId, sent: false}, {$set: {sent: true}});
   Meteor.users.update({_id: recipientId, welcome: false}, {$set: {welcome: true}});
   console.log("email sent!");

}});
