// CHECKED
// TODO: add rating limit
//======================================================================
// REMOVE FRIEND OF CURRENT USER FROM PARTICIPANTS LIST:
//======================================================================
Meteor.methods({'removeFriendOfCurUserFromParticipantsList': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var context = 'Remove Friend of Current User from Participants List';
   var curUserId = this.userId; // server side only
   var match = null;
   var curUser = null;
   var result = {};
   var pull = {}; // DB modifier
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameAddRemovePlayer';
   var textNotif = 'Friend_Signed_Out_Of_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_Friend_Signed_Out_Of_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {"activities": 1, "profile.name": 1}});
   match = Matches.findOne({_id: matchId}, {fields: {"status": 1, "friendsOf": 1, "privacy": 1, "followers": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Friend should be enrolled
   if (_.indexOf(getUserIds(match.friendsOf), curUserId) === -1) {
      throwError(context, 'friend is not enrolled');
      return {status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled', title: 'Operation_Unsuccessful'}; // msg
   }
   // ActivityId should be in the user record
   if (_.indexOf(curUser.activities.upcoming[match.privacy].friendIsEnrolled, matchId) === -1) {
      throwError(context, 'activity NOT in user.friendIsEnrolled');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   pull['activities.upcoming.' + match.privacy + '.friendIsEnrolled'] = matchId;
   senderName = curUser.profile.name;
   userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	// Remove friend from participants list
	Matches.update({_id: matchId}, {$pull: {friendsOf: {'userId': curUserId}}});

   // Remove activityId from current user
   Meteor.users.update({_id: curUserId}, {$pull: pull});

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the following task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

      // Deliver internal notifications
      _.each(userIdsToNotify, function(userId) {
         Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
      });

      // Notify users via email
      Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
   });

	// Re-order participants if needed
   Meteor.call('checkWaitingList', matchId);

}});
