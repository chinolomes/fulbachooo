/*
DESCRIPTION: adds friend of current user to the list of participants

REQUIREMENTS: current user must be in the list of participants / substitutes
*/
// CHECKED
// TODO: add rating limit
//======================================================================
// ADD FRIEND OF CURRENT USER TO PARTICIPANTS LIST:
//======================================================================
Meteor.methods({'addFriendOfCurUserToParticipantsList': function(matchId, name, team) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([matchId, name, team], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var context = 'Add Friend of Current User to Participants List';
   var curUserId = this.userId; // server side only
   var match = null;
   var curUser = null;
   var result = {};
   var addToSet = {}; // DB modifier
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameAddRemovePlayer';
   var textNotif = 'Friend_Signed_In_For_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_Friend_Signed_In_For_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {'activities': 1, 'profile.name': 1}});
   match = Matches.findOne({_id: matchId});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUserId) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // Team / column can't be full
   if (getPeopleMissing(match, team) === 0) {
      throwError(context, 'team is full');
      return unexpError; // msg
   }
   // There can't be people in the waiting list
   if (filterByTeam(match.waitingList, team).length > 0) {
      throwError(context, 'people in waiting list');
      return unexpError; // msg
   }
   // User can't add more than one friend
   if (_.indexOf(getUserIds(match.friendsOf), curUserId) !== -1) {
      throwError(context, 'friend already in participants list');
      return unexpError; // msg
   }
   // Current user must be enrolled (participants or waiting lists)
   if (_.indexOf(_.union(getUserIds(match.peopleIn), getUserIds(match.waitingList)), curUserId) === -1) {
      throwError(context, 'current user is not in enrolled');
      return unexpError; // msg
   }
   // ActivityId can't be in the user.friendIsEnrolled doc
   if (_.indexOf(curUser.activities.upcoming[match.privacy].friendIsEnrolled, matchId) !== -1) {
      throwError(context, 'activity already in user.friendIsEnrolled');
      return unexpError; // msg
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   addToSet['activities.upcoming.' + match.privacy + '.friendIsEnrolled'] = matchId;
   senderName = curUser.profile.name;
   userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Concatenate the new player with the previous ones
   Matches.update({_id: matchId}, {$addToSet: {friendsOf: {'name': name, 'team': team, 'userId': curUserId}}});

   // Attach activityId to current user
   Meteor.users.update({_id: curUserId}, {$addToSet: addToSet});

   // Add current user to activity's list of followers, and add activityId to the list of activities being followed by the user
   if (_.indexOf(match.followers, curUserId) === -1) {
      Meteor.call('followUnfollowActivity', matchId);
   }

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the follwoing task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

      // Deliver intrnal notifications
      _.each(userIdsToNotify, function(userId) {
         Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
      });

      // Notify users via email
      Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
   });

}});
