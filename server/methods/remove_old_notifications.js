/*
DESCRIPTION: remove old notifications (leave the 30 newest) for the given userId
*/
// CHECKED
// TODO: merge unreadNotif and Notifications (?):
// InternalNotifications = {_id, recipientId, array/list/notifArray?, length, unseen}
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// REMOVE OLD (INTERNAL) NOTIFICATIONS:
//======================================================================
Meteor.methods({'removeOldNotifications': function(userId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(userId, String);

	///////////////
	// VARIABLES //
	///////////////

	var bound = 30;
	var options = {
		fields: {
			createdAt: 1
		},
		sort: {
			createdAt: -1 // sort newest first (descending order)
		}
	};
	var userNotif = [];
	var idsToRemove = [];

	/////////////
	// QUERIES //
	/////////////

	userNotif = Notifications.find({recipient: userId}, options).fetch();

	///////////
	// LOGIC //
	///////////

   // Remove old notifications, keeping only the 30 newest
	if (userNotif.length > bound) {
		idsToRemove = _.rest(_.pluck(userNotif, "_id"), bound); // remove the 30 first records, keeping the rest to be removed
		Notifications.remove({_id: {$in: idsToRemove}});
	}

}});
