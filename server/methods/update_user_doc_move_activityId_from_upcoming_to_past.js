// TODO: use 'admins' array instead of 'admin'
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// EDIT MATCH DATA:
//======================================================================
Meteor.methods({'updateUserDocMoveActivityIdFromUpcomingToPast': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Re-attach ActivityId for Owner Admin Partcipants and Followers';
   var match = null;
   var privacy = '';
   var key = '';
   var pull = { // DB modifier
      created: {},
      admin: {},
      enrolled: {},
      waiting: {},
      friendIsEnrolled: {},
      following: {}
   };
   var addToSet = { // DB modifier
      created: {},
      admin: {},
      enrolled: {},
      waiting: {},
      friendIsEnrolled: {},
      following: {}
   };

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"privacy": 1, "createdBy": 1, "admin": 1, "peopleIn": 1, "friendsOf": 1, "waitingList": 1, "followers": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Activity exists
   if (!match) {
      throwError(context, 'activity does not exist');
      return;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   privacy = match.privacy;
   // initial pull = {created: {}, admin: {}, enrolled: {}, waiting: {}, friendIsEnrolled: {}, following: {}};
   for (key in pull) {
      pull[key]['activities.upcoming.' + privacy + '.' + key] = matchId;
   }
   // initial addToSet = {created: {}, admin: {}, enrolled: {}, waiting: {}, friendIsEnrolled: {}, following: {}};
   for (key in addToSet) {
      addToSet[key]['activities.past.' + privacy + '.' + key] = matchId;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   // Update activityId in organizer
	Meteor.users.update({_id: match.createdBy}, {$pull: pull.created, $addToSet: addToSet.created});

   // Update activityId in admin
   if (!_.isUndefined(match.admin) && match.admin.length > 0) {
      Meteor.users.update({_id: match.admin[0]}, {$pull: pull.admin, $addToSet: addToSet.admin});
   }
   // Update activityId in participants
   Meteor.users.update({_id: {$in: getUserIds(match.peopleIn)}}, {$pull: pull.enrolled, $addToSet: addToSet.enrolled}, {multi: true});

   // Update activityId for substitutes in waiting list
   Meteor.users.update({_id: {$in: getUserIds(match.waitingList)}}, {$pull: pull.waiting, $addToSet: addToSet.waiting}, {multi: true});

   // Update activityId for friends of participants
   Meteor.users.update({_id: {$in: getUserIds(match.friendsOf)}}, {$pull: pull.friendIsEnrolled, $addToSet: addToSet.friendIsEnrolled}, {multi: true});

   // Update activityId in followers
   Meteor.users.update({_id: {$in: match.followers}}, {$pull: pull.following, $addToSet: addToSet.following}, {multi: true});

}});
