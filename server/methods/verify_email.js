
// TODO: do we need to check if user is logged in?
// TODO: re-write using new EmailNotif schema + remove notifEmails.verificationToken.userId field
// CHECKED
//======================================================================
// CUSTOM VERIFY EMAIL:
//======================================================================
Meteor.methods({'customVerifyEmail': function(inputUserId, inputToken) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('verifying token... inputUserId: ' + inputUserId + ', inputToken: ' + inputToken);
   check([inputUserId, inputToken], [String]);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Custom Verify Email';
   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var verifLinkExp = {status: 'failed', error: new Meteor.Error(403, 'Email_Verification_Failed')};
   var user = null;
   var verificationToken = {};
   var token = '';
   var email = '';
   var expiresAt = null;
   var update = {};
   var now = new Date();

   /////////////
   // QUERIES //
   /////////////

   user = Meteor.users.findOne({_id: inputUserId}, {fields: {"notifEmails": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Token input length
   if (inputToken.trim().length !== 16) {
      console.log(context + 'token is not 16 chars --> ' + inputToken);
      return verifLinkExp;
   }
   // User existance
   if (!user || !user.notifEmails || !user.notifEmails.verificationToken) {
      console.log(context + 'user does not exist or is not set properly');
      return verifLinkExp;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   verificationToken = user.notifEmails.verificationToken;
   email = verificationToken.address;
   token = verificationToken.token;
   expiresAt = verificationToken.expiresAt;
   update = {
      $addToSet: {
         'notifEmails.verified': email
      },
      $set: {
         'notifEmails.primary': email,
         'notifEmails.verificationToken.userId': '', // TODO: needed???
         'notifEmails.verificationToken.token': '',
         'notifEmails.verificationToken.address': '',
         'notifEmails.verificationToken.expiresAt': now
      }
   };

   ///////////
   // LOGIC //
   ///////////

   // Token fields are set properly on user doc
   if (!token || !email || !expiresAt) {
      console.log(context + 'verification token fields not set properly');
      return verifLinkExp;
   }
   // Token DB and token input match
   if (token !== inputToken) {
      console.log(context + 'Tokens dont match: ' + 'DB --> ' + token + ', input --> ' + inputToken);
      return verifLinkExp;
   }
   // Token is active
   if (expiresAt <= now) {
      console.log(context + ' --> verification link expired. inputUserId: ' + inputUserId);
      return verifLinkExp;
   }

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

   Meteor.users.update({_id: inputUserId}, update);
   return {status: 'success'};

}});
