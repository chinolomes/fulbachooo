/*
DESCRIPTION: save new email into user doc and send verification email to the new address
*/
 // TODO: re-write function using new emailNotif schema
 // CHECKED
 // TODO: must be a JS function instead of a Meteor method
//======================================================================
// SAVE AND VERIFY EMAIL:
//======================================================================
Meteor.methods({'saveAndSendVerificationEmail': function(email) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check(email, String);
   email = email.trim().toLowerCase();

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Save and Verify New Email';
   var curUserId = this.userId; // server side only
   var curUser = null;
   var lang = '';
   var isVerified = false;
   var isPrimary = false;
   var token = RandToken.generate(16); // TODO hash token
   var now = new Date();
   var update = {
      $addToSet: {
         'notifEmails.sent': email
      },
      $set: {
         'notifEmails.verificationToken': {
            address: email,
            userId: curUserId,
            token: token,
            expiresAt: nextWeek(now)
         }
      }
   };

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {"profile.lang": 1, "notifEmails": 1}});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
	// Is the email empty?
	if (email.length === 0) {
		return {status: 'error', text: 'Enter_Your_Email'}; // msg
	}

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   lang = curUser.profile.lang.value;
   isVerified = _.indexOf(curUser.notifEmails.verified, email) !== -1;
   isPrimary = curUser.notifEmails.primary === email;
   //console.log('isVerified: ' + isVerified + ' isPrimary: ' + isPrimary);

   ///////////
   // LOGIC //
   ///////////

   if (isVerified && isPrimary) {
      console.log('record exists, verified & primary');
      return {status: 'warning', text: 'No_Changes_Were_Made'}; // msg

   } else if (isVerified) {
      console.log('record exists, verified & !primary');
      Meteor.users.update({_id: curUserId}, {$set: {'notifEmails.primary': email}});
      return {status: 'success', text: 'Changes_Were_Made_Successfully'}; // msg
   }

   // !isVerified
   // Add address to user doc
   console.log('record !verified');
   Meteor.users.update({_id: curUserId}, update);
   console.log("about to send email...");

   ///////////////
   // SET EMAIL //
   ///////////////

   SSR.compileTemplate('htmlEmail', Assets.getText('email-verif-template.html'));
   Template.htmlEmail.helpers({
      'title': function() {
         return TAPi18n.__('Email_Address_Verification', {}, lang_tag = lang);
      },
      'text': function() {
         return TAPi18n.__('To_Verify_Your_Email_Address_Click_On_The_Following_Link', {}, lang_tag = lang);
      },
      'link': function() {
         return 'https://www.fulbacho.net/verify-email/' + curUserId + '/' + token;
      },
      'button': function() {
         return TAPi18n.__('Verify_Email', {}, lang_tag = lang);
      },
      'greetings': function() {
         return TAPi18n.__('Thanks_For_Choosing_Fulbacho', {}, lang_tag = lang);
      },
      'facebook': function() {
         return TAPi18n.__('Support_Us_On_Facebook', {}, lang_tag = lang);
      },
      'youtube': function() {
         return TAPi18n.__('Subscribe_To_Our_Youtube_Channel', {}, lang_tag = lang);
      }
   });

   ////////////////
   // SEND EMAIL //
   ////////////////

   // Let other method calls from the same client start running,
   // without waiting for the email sending to complete.
   this.unblock();
   Meteor.Mailgun.send({
      to: email,
      from: 'fulbacho.net <no-reply@fulbacho.net>',
      subject: TAPi18n.__('Email_Address_Verification', {}, lang_tag = lang),
      html: SSR.render('htmlEmail')
   });
   console.log("email sent!");

   return {status: 'success', text: 'An_Email_Has_Been_Sent_To_Your_Inbox'}; // msg

}});
