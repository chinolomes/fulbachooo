// CHECKED
// TODO: add rating limit
//======================================================================
// REMOVE CURRENT USER FROM WAITING LIST:
//======================================================================
Meteor.methods({'removeCurUserFromWaitingList': function(matchId, team) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   check([matchId, team], [String]);

   ///////////////////
   // AUX FUNCTIONS //
   ///////////////////

   var oppositeTeam = function(team) {
      return team === 'A' ? 'B' : 'A';
   }

   ///////////////
   // VARIABLES //
   ///////////////

   var unexpError = {status: 'error', text: 'Unexpected_Error'};
   var context = 'Remove Current User From Waiting List';
   var curUserId = this.userId; // server side only
   var match = null;
   var curUser = null;
   var result = {};
   var pull = {}; // DB modifier
   var senderName = '';
   var userIdsToNotify = [];
   var notifType = 'gameAddRemovePlayer';
   var textNotif = 'User_Removed_From_The_Waiting_List_Of_One_Of_Your_Activities';
   var textEmail = 'Email_Notif_User_Removed_From_The_Waiting_List_Of_One_Of_Your_Activities';
   var anchor = '/activities/activity/' + matchId + '#participants';
   var url = 'https://www.fulbacho.net' + anchor;

   /////////////
   // QUERIES //
   /////////////

   curUser = Meteor.users.findOne({_id: curUserId}, {fields: {"activities": 1, "profile.name": 1}});
   match = Matches.findOne({_id: matchId});

   ////////////
   // CHECKS //
   ////////////

   // User is logged in
   if (!curUser) {
      throwError(context, 'the user is not logged in');
      return unexpError; // msg
   }
   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      return result.msg;
   }
   // UserId is in activity.waitingList
   if (_.indexOf(getUserIds(filterByTeam(match.waitingList, team)), curUserId) === -1) {
      throwError(context, 'user NOT in waiting list');
      return unexpError; // msg
	}
   // ActivityId is in user record
   if (_.indexOf(curUser.activities.upcoming[match.privacy].waiting, matchId) === -1) {
      throwError(context, 'activity NOT in user.waiting');
      return unexpError; // msg
   }
   // Team is NOT full
	if (getPeopleMissing(match, team) > 0) {
      throwError(context, 'team is NOT full');
      return unexpError; // msg
	}

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   pull['activities.upcoming.' + match.privacy + '.waiting'] = matchId;
   senderName = curUser.profile.name;
   userIdsToNotify = _.without(match.followers, curUserId); // don't notify current user

   ///////////////////
   // DB-OPERATIONS //
   ///////////////////

	// Remove current user from waiting list
   Matches.update({_id: matchId}, {$pull: {waitingList: {userId: curUserId, team: team}}});

   // Remove activityId from user record if and only if the user is not in both waiting lists
   if (_.indexOf(getUserIds(filterByTeam(match.waitingList, oppositeTeam(team))), curUserId) === -1) {
      Meteor.users.update({_id: curUserId}, {$pull: pull});
   }

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Don't wait for the following task to be done before giving the client the green light to move ahead
   Meteor.defer(function () {

      // Deliver internal notifications
      _.each(userIdsToNotify, function(userId) {
         Notifications.insert({createdBy: curUserId, recipient: userId, text: textNotif, anchor: anchor});
      });

      // Notify users via email
      Meteor.call('notifyUsersViaEmail', senderName, textEmail, url, userIdsToNotify, notifType);
   });

}});
