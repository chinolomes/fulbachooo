// TODO: add email notifications
// CHECKED
// TODO: must be a JS function instead of a Meteor method
//======================================================================
// NOTIFY ADMIN NAMED IN NEW ACTIVITY:
//======================================================================
Meteor.methods({'notifyAdminNamedInNewActivity': function(matchId) {

   /////////////////////
   // CHECK ARGUMENTS //
   /////////////////////

   console.log('NOTIFY ADMIN NAMED IN NEW ACTIVITY');
   check(matchId, String);

   ///////////////
   // VARIABLES //
   ///////////////

   var context = 'Notify Admin Named In New Activity';
   var match = null;
   var result = {};
   var adminId = '';
   var ownerId = '';
   var textNotif = 'User_Created_New_Activity_And_Named_You_Admin';
   var anchor = '/activities/activity/' + matchId + '#data';

   /////////////
   // QUERIES //
   /////////////

   match = Matches.findOne({_id: matchId}, {fields: {"createdBy": 1, "admin": 1, "status": 1}});

   ////////////
   // CHECKS //
   ////////////

   // Activity exists and status = 'active'
   result = checkActivityExistanceAndStatus(match, context);
   if (result.status === 'failed') {
      throwError(context, 'activity does not exist / is not active. activityId: ' + matchId);
      return;
   }

   ///////////////////
   // SET VARIABLES //
   ///////////////////

   adminId = match.admin[0]; // might be undefined or null
   ownerId = match.createdBy;

   ///////////////////
   // NOTIFICATIONS //
   ///////////////////

   // Deliver internal notification
   if (adminId) {
      Notifications.insert({
         createdBy: ownerId,
         recipient: adminId,
         text: textNotif,
         anchor: anchor
      });
   }

}});
