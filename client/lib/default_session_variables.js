// DEFAULT SESSION VARIABLES

//======================================================================
// INITIAL CONFIG:
//======================================================================
Session.setDefault('initNotifDone', false);
Session.setDefault('initMapDone', false);
Session.setDefault('initLangDone', false);
Session.setDefault('logoutConfigDone', false);
Session.setDefault('updateLastLoginDone', false);

//======================================================================
// DATEPICKER:
//======================================================================
Session.setDefault('selectedDATEPICKER', null);
Session.setDefault('resetDATEPICKER', false);
Session.setDefault('newDateDATEPICKER', null);

//======================================================================
// SELECT2:
//======================================================================
Session.setDefault('people_in_team_A_SELECT2', null);
Session.setDefault('people_in_team_B_SELECT2', null);
Session.setDefault('people_in_no_team_sport_SELECT2', null);
Session.setDefault('friends_of_team_A_SELECT2', null);
Session.setDefault('friends_of_team_B_SELECT2', null);
Session.setDefault('waiting_list_team_A_SELECT2', null);
Session.setDefault('waiting_list_team_B_SELECT2', null);
Session.setDefault('activity_admin_SELECT2', null);

Session.setDefault('people_in_team_A_RESET_SELECT2', false);
Session.setDefault('friends_of_team_A_RESET_SELECT2', false);
Session.setDefault('waiting_list_team_A_RESET_SELECT2', false);
Session.setDefault('people_in_team_B_RESET_SELECT2', false);
Session.setDefault('friends_of_team_B_RESET_SELECT2', false);
Session.setDefault('waiting_list_team_B_RESET_SELECT2', false);

//======================================================================
// FORM COMPONENTS:
//======================================================================
Session.setDefault('resetAll_FORM_COMPONENT', true);
Session.setDefault('sport_FORM_COMPONENT', null);
Session.setDefault('privacy_FORM_COMPONENT', null);
Session.setDefault('address_FORM_COMPONENT', null);
Session.setDefault('time_FORM_COMPONENT', null);
Session.setDefault('duration_FORM_COMPONENT', null);
Session.setDefault('cost_FORM_COMPONENT', null);
Session.setDefault('fieldSize_FORM_COMPONENT', null);
Session.setDefault('maxParticipants_FORM_COMPONENT', null);
Session.setDefault('title_FORM_COMPONENT', null);
Session.setDefault('fieldType_FORM_COMPONENT', null);
Session.setDefault('fieldPhone_FORM_COMPONENT', null);
Session.setDefault('fieldWebsite_FORM_COMPONENT', null);
Session.setDefault('coords_FORM_COMPONENT', null);

//======================================================================
// GOOGLE MAPS:
//======================================================================
Session.setDefault('resetMapMATCHES', false);
Session.setDefault('circleCenterLatMATCHES', DEFAULT_LAT);
Session.setDefault('circleCenterLngMATCHES', DEFAULT_LNG);
Session.setDefault('circleRadiusMATCHES', DEFAULT_RADIUS); // in kilometers
Session.setDefault('mapZoomMATCHES', DEFAULT_ZOOM);

//======================================================================
// SLIDERS:
//======================================================================
Session.setDefault('resetZoomSliderCIRCLE', false);
Session.setDefault('mapZoomCIRCLE', '');
Session.setDefault('zoomSliderValue', 3);

//======================================================================
// EDIT PROFILE:
//======================================================================
Session.setDefault('resetZoomSliderEDITPROF', false);
Session.setDefault('mapZoomEDITPROF', '');

//======================================================================
// MATCHES:
//======================================================================
Session.setDefault('curMatch', null);

//======================================================================
// PLAYERS:
//======================================================================
Session.setDefault('curPlayer', null);

//======================================================================
// NOTIFICATIONS:
//======================================================================
Session.setDefault('curNotif', null);
Session.setDefault('prevNumberOfNotif', null);

//======================================================================
// GPS BUTTON:
//======================================================================
Session.setDefault('resetMapGPS', false);
Session.setDefault('mapCenterLatGPS', '');
Session.setDefault('mapCenterLngGPS', '');

//======================================================================
// PRE-POPULATE BUTTON:
//======================================================================
Session.setDefault('resetMapPREPO', false);
Session.setDefault('mapCenterLatPREPO', '');
Session.setDefault('mapCenterLngPREPO', '');

//======================================================================
// SAVE MAP BUTTON:
//======================================================================
Session.setDefault('resetMapSAVE_REGION_BTN', false);
Session.setDefault('resetMapSAVE_MAP_BTN', false);

//======================================================================
// RESULTS:
//======================================================================
Session.setDefault('seeResults', false);

//======================================================================
// LOADING TEMPLATE:
//======================================================================
Session.setDefault('loading', 'wait');

//======================================================================
// BOTTOM NAVBAR:
//======================================================================
Session.setDefault('curSubSection', null);

console.log('default session variables set');
