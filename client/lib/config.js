//======================================================================
// TOAST MESSAGES:
//======================================================================
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

options1 = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-bottom-left",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

options2 = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": false,
  "progressBar": false,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": 0,
  "extendedTimeOut": 0,
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut",
  "tapToDismiss": false
}


//======================================================================
// SHARE IT:
//======================================================================
ShareIt.init({
   siteOrder: ['facebook', 'twitter'], //, 'googleplus'
   sites: {
      'facebook': {
         'appId': null, //1552049091746436,
         'version': 'v2.3'
      }
   },
   iconOnly: false,
   applyColors: true
});
/*ShareIt.configure({
  	sites: {
   	'facebook': {
      'appId': 1552049091746436
   }
  }
});*/

//======================================================================
// FB SDK:
//======================================================================
/*window.fbAsyncInit = function() {
   FB.init({
      appId      : 1552049091746436,
      status     : true,
      xfbml      : true
   });
};*/
