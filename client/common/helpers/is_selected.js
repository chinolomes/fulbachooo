//======================================================================
// SET SELECTED ATTRIBUTE:
//======================================================================
// USAGE: {{isSelected x y}}
Template.registerHelper('isSelected', function(x, y) {
   if (x == y) {
		return 'selected';
   }
   return;
});
