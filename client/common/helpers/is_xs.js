//======================================================================
// IS XS:
//======================================================================
// USAGE: {{#if isXs}}...{{/if}} or {{#unless isXs}}...{{/unless}}
Template.registerHelper('isXs', function() {
	return ResponsiveHelpers.isXs();
});
