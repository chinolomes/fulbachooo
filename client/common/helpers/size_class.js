//======================================================================
// SIZE CLASS:
//======================================================================
// USAGE: {{sizeClass}}
Template.registerHelper('sizeClass', function() {
	return ResponsiveHelpers.isXs() ? 'small' : 'large';
});
