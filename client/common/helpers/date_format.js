//======================================================================
// DATE FORMAT USING MOMENT.JS PACKAGE:
//======================================================================
// Source: http://momentjs.com/docs/#/displaying/
//----------------------------------------------------------------------
// DEFINE COMMON TEMPLATES:
Template.registerHelper('formatDate', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('DD/MM/YYYY');
	}
});

Template.registerHelper('formatDateTime', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('DD/MM/YYYY hh:mm A');
	}
});

Template.registerHelper('formatDateTimeNotUTC', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment(context).format('DD/MM/YYYY hh:mm A');
	}
});

Template.registerHelper('formatDateName', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('dddd DD/MM/YYYY');
	}
});

Template.registerHelper('formatDateDayNumber', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('DD');
	}
});

Template.registerHelper('formatDateDayName', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('dddd');
	}
});

Template.registerHelper('formatDateMonthName', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('MMMM');
	}
});

Template.registerHelper('formatDateMonthNameUpper', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('MMMM').toUpperCase();
	}
});
Template.registerHelper('formatDateDayNameUpper', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('dddd').toUpperCase();
	}
});
//----------------------------------------------------------------------
Template.registerHelper('formatDateDayShortName', function(context, options) {
	if(context) {
		// SET LANGUAGE:
		var lang = TAPi18n.getLanguage();
		setMomentLang(lang);
		return moment.utc(context).format('ddd');
	}
});
