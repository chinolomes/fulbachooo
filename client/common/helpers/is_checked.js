//======================================================================
// SET CHECKED ATTRIBUTE:
//======================================================================
// USAGE: {{isChecked x y}}
Template.registerHelper('isChecked', function(x, y) {
  	if (_.isArray(x)) {
		return _.indexOf(x, y) !== -1 ? {checked: "checked"} : '';
	} else {
		if (x === y) {
			return {checked: "checked"};
	   }
	}
  	return;
});
