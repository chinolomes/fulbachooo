//======================================================================
// ENGLISH:
//======================================================================
// USAGE: {{english}}
Template.registerHelper('english', function() {
	return TAPi18n.getLanguage() === 'en';
});

//======================================================================
// SPANISH:
//======================================================================
// USAGE: {{english}}
Template.registerHelper('spanish', function() {
	return TAPi18n.getLanguage() === 'es';
});

//======================================================================
// DUTCH:
//======================================================================
// USAGE: {{dutch}}
Template.registerHelper('dutch', function() {
	return TAPi18n.getLanguage() === 'nl';
});

//======================================================================
// GERMAN:
//======================================================================
// USAGE: {{german}}
Template.registerHelper('german', function() {
	return TAPi18n.getLanguage() === 'de';
});

//======================================================================
// ITALIAN:
//======================================================================
// USAGE: {{italian}}
Template.registerHelper('italian', function() {
	return TAPi18n.getLanguage() === 'it';
});
