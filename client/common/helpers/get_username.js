//======================================================================
// GET USER NAME FROM USER_ID:
//======================================================================
// USAGE: {{getUserName userId}}
Template.registerHelper('getUserName', function(userId) {
	check(userId, String);
	var user = Meteor.users.findOne(userId);
	return user && user.profile && user.profile.name ? user.profile.name : '';
});
