//======================================================================
// RETURN MAP BOUNDS FROM SESSION VARS:
//======================================================================
// USAGE: {{mapBounds}}
Template.registerHelper('mapBounds', function() {
	var bottomLeft = Session.get('bottomLeftMapBounds');
	var topRight = Session.get('topRightMapBounds');
	return {bottomLeft: bottomLeft, topRight: topRight};
});
