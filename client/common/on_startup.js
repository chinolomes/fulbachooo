//======================================================================
// AUX FUNCTIONS:
//======================================================================
// MOMENT JS:
// TODO: this function should be inside a mudulo exclusive for languages
var initMomentLang = function() {
	moment.locale('es', {
		months : "Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre".split("_"),
		weekdays : "Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sábado".split("_"),
		weekdaysShort : "Dom_Lun_Mar_Mie_Jue_Vie_Sab".split("_"),
		weekdaysMin : "Do_Lu_Ma_Mi_Ju_Vi_Sa".split("_")
	});

	moment.locale('de', {
		months : "Januar_Februar_März_April_Mai_Juni_Juli_August_September_Oktober_November_Dezember".split("_"),
		weekdays : "Sonntag_Montag_Dienstag_Mittwoch_Donnerstag_Freitag_Samstag".split("_"),
		weekdaysShort : "Son_Mon_Die_Mit_Don_Fre_Sam".split("_"),
		weekdaysMin : "So_Mo_Di_Mi_Do_Fr_Sa".split("_")
	});

	moment.locale('nl', {
		months : "Januari_Februari_Maart_April_Mei_Juni_Juli_Augustus_September_Oktober_November_December".split("_"),
		weekdays : "Zondag_Maandag_Dinsdag_Woensdag_Donderdag_Vrijdag_Zaterdag".split("_"),
		weekdaysShort : "Zon_Maa_Din_Woe_Don_Vri_Zat".split("_"),
		weekdaysMin : "Zo_Ma_Di_Wo_Do_Vr_Za".split("_")
	});

	moment.locale('it', {
		months : "Gennaio_Febbraio_Marzo_Aprile_Maggio_Giugno_Luglio_Agosto_Settembre_Ottobre_Novembre_Dicembre".split("_"),
		weekdays : "Domenica_Lunedi_Martedì_Mercoledì_Giovedi_Venerdì_Sabato".split("_"),
		weekdaysShort : "Dom_Lun_Mar_Mer_Gio_Ven_Sab".split("_"),
		weekdaysMin : "Do_Lu_Ma_Me_Gi_Ve_Sa".split("_")
	});
}
//----------------------------------------------------------------------
// DEFAULT LANGUAGE:
// TODO: this function should be inside a mudulo exclusive for languages
var defaultLang = function() {
	if (!_.isUndefined(Cookie.get('langFULBACHO'))) {
		switch (Cookie.get('langFULBACHO')) {
			case 'es':
				setLocale('es');
				console.log('on startup i18n set: es');
				break;
			case 'nl':
				setLocale('nl');
				console.log('on startup i18n set: nl');
				break;
			case 'de':
				setLocale('de');
				console.log('on startup i18n set: de');
				break;
			case 'it':
				setLocale('it');
				console.log('on startup i18n set: it');
				break;
			default:
				setLocale('en');
				console.log('on startup i18n set: en');
				break;
		}
	} else {
		setLocale('en');
		console.log('on startup i18n set: en');
	}
}

//======================================================================
// SETTINGS ON CLIENT STARTUP:
//======================================================================
// Meteor.startup: run code when a client
// SOURCE: https://atmospherejs.com/differential/event-hooks
Meteor.startup(function () {

	// Initialize moment.js (month and day names) for each of the available languages
	initMomentLang();

	// Set default language for non-logged in users based on cookies
	defaultLang();

	// Initialize hooks to detect login/logout user
	Hooks.init();

	// Set default location based on geo-ip
	Meteor.call('getCurUserLocation', function(err, loc) {
	   if (err) {
			console.log(err);
		} else {
			Cookie.set('latFULBACHO', loc.latitude, {months: 1});
			Cookie.set('lngFULBACHO', loc.longitude, {months: 1});
			// If user is not logged in, set default map based on geo-ip
			if (!Meteor.userId()) {
				Session.set('circleCenterLatMATCHES', loc.latitude);
				Session.set('circleCenterLngMATCHES', loc.longitude);
				Session.set('resetMapMATCHES', true);
			}
			console.log('on startup location set');
		}
	});

});

//======================================================================
// SEO DATA:
//======================================================================
// Meteor.startup: run code when a client
// SOURCE: https://github.com/DerMambo/ms-seo/
Meteor.startup(function() {
   return SEO.config({
      title: 'fulbacho.net',
		rel_author: 'https://www.google.com/+FedericoRodes',
      meta: {
         'description': 'Organize all your sport activities in one place'
      },
      og: {
         'image': 'https://www.fulbacho.net/logos/fulbacho_flyer.png'
      }
   });
});


//======================================================================
// reCAPTCHA:
//======================================================================
// Meteor.startup: run code when a client
Meteor.startup(function() {
   reCAPTCHA.config({
      theme: 'light',  // 'light' default or 'dark'
      publickey: '6LcmtAcTAAAAAFxgUyrvq8CaNUSMDIabKFWjWh_9'
   });
});
