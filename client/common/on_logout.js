// SOURCE: https://github.com/gwendall/meteor-accounts-helpers
// NOTICE: throws an error on user login because of audit-check package
// We can also try to put this code in client/header/buttons/logout.js but it
// will only work if the users logs out using the logout button :/
//======================================================================
// SETTINGS ON USER LOG OUT:
//======================================================================
Hooks.onLoggedOut = function (userId) {

   console.log('log out hook');

	// Clean up notification variables
   Session.set('curNotif', null);
   Session.set('prevNumberOfNotif', null);

	// Reset document title
	document.title = 'Fulbacho';

   // Reset session variables for the next registered user
   Session.set('initNotifDone', false);
   Session.set('initMapDone', false);
   Session.set('initLangDone', false);
   Session.set('updateLastLoginDone', false);

   Session.set('resetMapMATCHES', false);

   console.log('log out user settings');

}
