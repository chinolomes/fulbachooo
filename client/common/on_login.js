// CLIENT SIDE:

// TODO: we should have 3 configuration:
//	1- default: all constant values
//	2- guest: get location from ip and update map when info is ready
//	3- registered user: get settings from DB and update map when info is ready
// In addition, user userId and then query DB to get the data
// + introduce INITIAL_SETUP_READY session var. One this var is set to true, allow further operations

//======================================================================
// SETTINGS FOR LOGGED IN USERS:
//======================================================================
Tracker.autorun(function() {

	//if (!_.isNull(Meteor.userId()) && Session.equals('logoutConfigDone', true)) {
	if (!_.isNull(Meteor.userId())) {

		var notifSubsHandler = Meteor.subscribe('curUserUnreadNotif');
		if (notifSubsHandler.ready() && Session.equals('initNotifDone', false)) {
			initLogInNotif();
		}

		/*var userSettingsSubsHandler = Meteor.subscribe('curUserSettings');
		if (userSettingsSubsHandler.ready()) {
			if (Session.equals('initMapDone', false)) {
				initLogInMap();
			}
			if (Session.equals('initLangDone', false)) {
				initLogInLang();
			}
		}*/
		var userGeoSubsHandler = Meteor.subscribe('curUserGeo');
		if (userGeoSubsHandler.ready()) {
			if (Session.equals('initMapDone', false)) {
				initLogInMap();
			}
		}

		//var userLangSubsHandler = Meteor.subscribe('curUserLang');
		var userLangSubsHandler = Meteor.subscribe('curUserProfile'); // includes user lang
		if (userLangSubsHandler.ready()) {
			if (Session.equals('initLangDone', false)) {
				initLogInLang();
			}
		}

		// Google analytics
		ga('set', '&uid', Meteor.userId());

		// Update last login
		if (Session.equals('updateLastLoginDone', false)) {
			//_.throttle(function() {
				Meteor.call('updateLastLogin', function(err) {
					if (err) {
						throw new Meteor.Error('On Login: unable to update user last login');
						return;
					} else {
						Session.set('updateLastLoginDone', true);
					}
				});
			//}, 1000);
		}
	}
});


//======================================================================
// NOTIFICATIONS:
//======================================================================
function initLogInNotif() {

	var unreadNotif = UnreadNotifications.findOne({recipientId: Meteor.userId()});

	// Keep looping until notifications are ready
	if (_.isUndefined(unreadNotif) || _.isUndefined(unreadNotif.counter)) {
		return;
	}

	var counter = unreadNotif.counter;

	// Set document title
	if (counter > 0) {
		document.title = counter <= NOTIF_LIMIT ? '(' + counter + ') Fulbacho' : '(' + NOTIF_LIMIT + '+) Fulbacho';
	} else {
		document.title = 'Fulbacho';
	}

	Session.set('prevNumberOfNotif', counter);
	Session.set('curNotif', null);
	Session.set('initNotifDone', true);

	console.log('on login notif set: ' + Session.get('prevNumberOfNotif'));
}


//======================================================================
// GOOGLE MAPS:
//======================================================================
function initLogInMap() {

	var curUserId = Meteor.userId();
	var curUser = Meteor.users.findOne(curUserId);

	// If the viewer is not registered, keep looping
	if (!curUser) return;

	// If the record exists but the data is not ready yet, keep looping
	if (_.isUndefined(curUser.geo) || _.isUndefined(curUser.geo.flag) || _.isUndefined(curUser.geo.radius) || _.isUndefined(curUser.geo.zoom) || _.isUndefined(curUser.geo.loc) || _.isUndefined(curUser.geo.loc.coordinates))
	return;

	var geo = curUser.geo; // user location + map and circle settings
	var lat = 0.0;
	var lng = 0.0;
	var radius = geo.radius;
	var zoom = geo.zoom;

	if (curUser.geo.flag === true) {
		lat = geo.loc.coordinates[1];
		lng = geo.loc.coordinates[0];
	} else {
		var loc = getLocFromCookies();
		lat = loc.lat;
		lng = loc.lng;
		Meteor.call('saveNearbyRegion', lat, lng, radius, zoom);
		Meteor.call('setUserGeoFlag');
	}

	Session.set('circleCenterLatMATCHES', lat);
	Session.set('circleCenterLngMATCHES', lng);
	Session.set('circleRadiusMATCHES', radius);
	Session.set('mapZoomMATCHES', zoom);
	Session.set('resetMapMATCHES', true);
	//Session.set('resetCircleMATCHES', true);

	Session.set('initMapDone', true);

	console.log('on login map set');
}

//======================================================================
// GOOGLE MAPS:
//======================================================================
/*function initLogInMap() {

	var curUserId = Meteor.userId();
	var curUser = Meteor.users.findOne(curUserId);

	// If the viewer is not registered, keep looping
	if (!curUser) return;

	// If the record exists but the data is not ready yet, keep looping
	if (!curUser.geo || !curUser.geo.radius || !curUser.geo.zoom || !curUser.geo.loc || !curUser.geo.loc.coordinates)
	return;

	var geo = curUser.geo; // user location + map and circle settings
	var lat = geo.loc.coordinates[1];
	var lng = geo.loc.coordinates[0];
	var radius = geo.radius;
	var zoom = geo.zoom;

	Session.set('circleCenterLatMATCHES', lat);
	Session.set('circleCenterLngMATCHES', lng);
	Session.set('circleRadiusMATCHES', radius);
	Session.set('mapZoomMATCHES', zoom);
	Session.set('resetMapMATCHES', true);
	Session.set('resetCircleMATCHES', true);

	Session.set('initMapDone', true);

	console.log('on login map set');
}*/

//======================================================================
// I18N:
//======================================================================
function initLogInLang() {
	var curUserId = Meteor.userId();
	var curUser = Meteor.users.findOne(curUserId);

	// If the viewer is not registered, keep looping
	if (!curUser) return;

	// If the record exists but the data is not ready yet, keep looping
	if (_.isUndefined(curUser.profile) || _.isUndefined(curUser.profile.lang) || _.isUndefined(curUser.profile.lang.value) || _.isUndefined(curUser.profile.lang.flag)) return;

	// Check language set flag
	var lang = 'en';
	//console.log('curUser.profile.langSet: ', curUser.profile.langSet);
	if (curUser.profile.lang.flag === true) {
		lang = curUser.profile.lang.value;
	} else {
		lang = getLangFromCookie(); // default 'en'
		Meteor.call('changeUserLang', lang);
		Meteor.call('setUserLangFlag');
	}
	// Set user language
	setLocale(lang);
	console.log('on login lang set: ', lang);
	Session.set('initLangDone', true);
}


//======================================================================
// I18N:
//======================================================================
/*function initLogInLang() {
	var curUserId = Meteor.userId();
	var curUser = Meteor.users.findOne(curUserId);

	// If the viewer is not registered, keep looping
	if (!curUser) return;

	// If the record exists but the data is not ready yet, keep looping
	if (!curUser.profile || !curUser.profile.language || !curUser.profile.langSet)	return;

	// Set user language
	var lang = curUser.profile.language;
	setLocale(lang);
	console.log('on login lang set');
	Session.set('initLangDone', true);
}*/
