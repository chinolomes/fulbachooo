// CLIENT SIDE


//======================================================================
// TERMS AND CONDITIONS:
//======================================================================
// CONTEXT:
//----------------------------------------------------------------------
// EVENTS:
Template.footerLegal.events({

   /* 'click .seeTerms': function(event) {
      event.preventDefault();

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.termsAndConditions, {type: 'desktop'}, $("#dialogNode")[0]);
   }, */

   'click .seeWhoWeAre': function(event) {
      event.preventDefault();

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.whoWeAre, {type: 'desktop'}, $("#dialogNode")[0]);
   },

   'click .seeFAQ': function(event) {
      event.preventDefault();

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.FAQ, {type: 'desktop'}, $("#dialogNode")[0]);
   },

   'click .seeContactUs': function(event) {
      event.preventDefault();

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.contactUs, {type: 'desktop'}, $("#dialogNode")[0]);
   }

});
