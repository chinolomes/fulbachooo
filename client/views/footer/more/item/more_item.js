//======================================================================
// MORE ITEM TEMPLATE:
//======================================================================
// REQUIRED: Set Session.setDefault('curSubSection', null) somewhere in your app
// + set Session.set('curSubSection', null) at the parent template.
// CONTEXT: this = {route, text}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.moreItem.helpers({

	'active': function() {
		return Session.equals('curSubSection', this.route) ? 'active' : '';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.moreItem.events({

	'mouseover .moreItem': function() {
		Session.set('curSubSection', this.route);
	},

	'mouseout .moreItem': function() {
		Session.set('curSubSection', null);
	}

});
