//======================================================================
// TERMS AND CONDITIONS TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.termsAndConditions.helpers({

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   }

});
