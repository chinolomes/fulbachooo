//======================================================================
// FAQ TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.FAQ.helpers({

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   }

});
