//======================================================================
// WHO WE ARE TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.whoWeAre.helpers({

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   }

});
