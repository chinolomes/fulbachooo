//======================================================================
// CONTACT US TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.contactUs.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Initialize reactive vars
   instance.name = new ReactiveVar('');
   instance.email = new ReactiveVar('');
   instance.loading = new ReactiveVar('wait'); // loading indicator

   // Internal var(s)
   var options = {
      fields: {
         "profile.name": 1,
         "notifEmails.primary": 1
      }
   };

   // 3. Subscribe to current user email
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub1 = instance.subscribe('curUserName');
         var sub2 = instance.subscribe('curUserEmail');

         // 4. Set reactive vars
         if (sub1.ready() && sub2.ready()) {
            var curUser = Meteor.users.findOne(Meteor.userId(), options);
            instance.name.set(curUser.profile.name);
            instance.email.set(curUser.notifEmails.primary);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.contactUs.helpers({

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   },

   'name': function() {
      return Template.instance().name.get();
   },

   'email': function() {
      return Template.instance().email.get();
   },

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//-----------------------------------------------------------------------
// EVENTS:
Template.contactUs.events({

	'submit form': function(event, instance) {
		event.preventDefault(); // prevents page reload when the form is submitted

		// Initialize internal vars
		var btn = instance.$('button');
      var name = instance.$('#name').val().trim();
      var email = instance.$('#email').val().trim();
      var textarea = instance.$('textarea');
      var message = textarea.val().trim();
      var captcha = instance.$('#g-recaptcha-response').val();

      // Disable send post button
      btn.prop('disabled', true);

      // Show loading indicator
      instance.loading.set('loading');

		// Check name
		if (name.length === 0) {
			btn.prop('disabled', false);
         instance.loading.set('error');
         toastr.error(TAPi18n.__('Enter_Your_Name'));
			return;
		} else if (name.length > 50) {
			btn.prop('disabled', false);
         instance.loading.set('error');
			toastr.error(TAPi18n.__('Name_Cant_Exceed_50_Chars'));
			return;
		}

		// Check email
		if (email.length === 0) {
			btn.prop('disabled', false);
         instance.loading.set('error');
         toastr.error(TAPi18n.__('Enter_Your_Email'));
			return;
		} else if (email.length > 50) {
			btn.prop('disabled', false);
         instance.loading.set('error');
			toastr.error(TAPi18n.__('Email_Cant_Exceed_50_Chars'));
			return;
		}

      // Check message
		if (message.length === 0) {
			btn.prop('disabled', false);
         instance.loading.set('error');
         toastr.error(TAPi18n.__('Enter_Your_Message'));
			return;
		} else if (message.length > 2000) {
			btn.prop('disabled', false);
         instance.loading.set('error');
			toastr.error(TAPi18n.__('The_Message_Exceeds_2000_Chars'));
			return;
		}

		Meteor.call('sendContactMessage', name, email, message, captcha, function(err, msg) {
				if (err) {
               instance.loading.set('error');
               toastr.error(TAPi18n.__('Unexpected_Error'));
               btn.prop('disabled', false);
					console.log(err);
				} else {
               if (!_.isUndefined(msg) && msg.status === 'error') {
			         btn.prop('disabled', false);
                  instance.loading.set('error');
                  processMessage(msg);
               } else {
                  instance.loading.set('success');
                  bootbox.hideAll();
                  textarea.val('');
                  btn.prop('disabled', false);
                  toastr.success(TAPi18n.__('Message_Sent_Successfully'));
               }
				}
			}
		);
	}
});
