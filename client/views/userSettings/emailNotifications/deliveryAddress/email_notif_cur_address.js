//======================================================================
// EMAIL NOTIFICATIONS CURRENT EMAIL ADDRESS TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.emailNotifCurAddress.onCreated(function() {

   // 1. Current template instance
   var instance = this; // current template instance

   // 2. Initialize reactive vars
   instance.primary = new ReactiveVar(''); // primary email address
   instance.loading = new ReactiveVar('wait'); // loading indicator

   // 3. Subscribe to current user email address
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub = instance.subscribe('curUserEmail');

         // 4. Set reactive vars
         if (sub.ready()) {
            var curUser = Meteor.users.findOne(Meteor.userId());
            var primary = curUser && curUser.notifEmails && curUser.notifEmails.primary ? curUser.notifEmails.primary : '';
            instance.primary.set(primary);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.emailNotifCurAddress.helpers({

	'emailIsSet': function() {
      return Template.instance().primary.get() !== '';
	},

	'primary': function() {
      return Template.instance().primary.get();
	},

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.emailNotifCurAddress.events({

	'submit form': function(event, instance) {
		event.preventDefault();

      // Check
      if (!Meteor.userId()) {
         throw new Meteor.Error('emailNotifCurAddress submit form: the user is not logged in');
         return;
      }

      // Get reactive vars
      var primary = instance.primary.get();

      // Variables declaration
      var email = instance.$('input').val().trim();
      var btn = instance.$('button');

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

      // Is the input empty?
      if (email.length === 0) {
			var msg = {status: 'error', text: 'Enter_Your_Email'};
			processMessage(msg);
			btn.prop("disabled", false);
         instance.loading.set('error');
			return;
      }

      // Is there any changes?
      if (primary === email) {
			var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
			processMessage(msg);
			btn.prop("disabled", false);
         instance.loading.set('wait');
			return;
      }

      // Save new address and send email for verification
		Meteor.call('saveAndSendVerificationEmail', email, function(err, msg) {
			if (err) {
            console.log(err);
            processMessage({status: 'error', text: 'Unexpected_Error'});
				btn.prop("disabled", false);
            instance.loading.set('error');
			} else {
				processMessage(msg);
				btn.prop("disabled", false);
            instance.loading.set('success');
			}
		});
	}

});
