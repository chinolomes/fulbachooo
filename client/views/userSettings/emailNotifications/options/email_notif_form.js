//======================================================================
// EMAIL NOTIFICATIONS FORM TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.emailNotifForm.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Initialize reactive vars
   instance.settings = new ReactiveVar([]); // [boolean]
   instance.loading = new ReactiveVar('wait'); // loading indicator

   // Internal var(s)
   var options = {
      fields: {
         emailNotifSettings: 1
      }
   };

   // 3. Subscribe to current user notifications settings
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub = instance.subscribe('curUserEmailNotifSettings');

         // 4. Set reactive vars
         if (sub.ready()) {
            var curUser = Meteor.users.findOne(Meteor.userId(), options);
            instance.settings.set(curUser.emailNotifSettings);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.emailNotifForm.helpers({

   'settings': function() {
      return Template.instance().settings.get();
   },

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.emailNotifForm.events({

   'submit form': function(event, instance) {
		event.preventDefault();

      // Check
      if (!Meteor.userId()) {
         throw new Meteor.Error('emailNotifForm submit form: the user is not logged in');
         return;
      }

      // Get reactive var
      var oldSettings = instance.settings.get();

      // Variables declaration
      var btn = instance.$('button');

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		// Get selected values
      var newSettings = instance.$('input[name=settings]:checked').map(function() {return this.value;}).get(); // [string, string, ...]

      // Is there any changes with respect to current values?
		var sameNewGamePlayingZone = oldSettings.gameNewGamePlayingZone === (_.indexOf(newSettings, 'gameNewGamePlayingZone') !== -1);
		var sameNewComment = oldSettings.gameNewComment === (_.indexOf(newSettings, 'gameNewComment') !== -1);
		var sameWasCanceled = oldSettings.gameWasCanceled === (_.indexOf(newSettings, 'gameWasCanceled') !== -1);
		var sameAddRemovePlayer = oldSettings.gameAddRemovePlayer === (_.indexOf(newSettings, 'gameAddRemovePlayer') !== -1);
		var sameAddedMe = oldSettings.gameAddedMe === (_.indexOf(newSettings, 'gameAddedMe') !== -1);
		var samePassFromWaitingToPlaying = oldSettings.gamePassFromWaitingToPlaying === (_.indexOf(newSettings, 'gamePassFromWaitingToPlaying') !== -1);
      var sameNewGameRepeat = oldSettings.gameNewGameRepeat === (_.indexOf(newSettings, 'gameNewGameRepeat') !== -1);

      if (sameNewGamePlayingZone && sameNewComment && sameWasCanceled && sameAddRemovePlayer && sameAddedMe && samePassFromWaitingToPlaying && sameNewGameRepeat) {
			var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
			processMessage(msg);
			btn.prop("disabled", false);
         instance.loading.set('wait');
			return;
      }

		Meteor.call('saveEmailNotifSettings', newSettings, function(err, msg) {
			if (err) {
				console.log(err);
				btn.prop("disabled", false);
            instance.loading.set('error');
			} else {
				processMessage(msg);
				bootbox.hideAll();
				btn.prop("disabled", false);
            instance.loading.set('success');
			}
		});
	}

});
