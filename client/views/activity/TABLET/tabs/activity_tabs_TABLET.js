//======================================================================
// ACTIVITY TABS TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.activityTabs.onCreated(function() {

   // 1. Initialize reactive variables
	//Session.set('curSubSection', 'data');
	var instance = this;

	// Get -reactive- hash
	instance.autorun(function() {
		var hash = Iron.Location.get().hash;
		if (hash) {
			switch(hash) {
				case '#comments':
					Session.set('curSubSection', 'comments');
					break;
				default:
					Session.set('curSubSection', 'data');
					break;
			}
		} else {
			Session.set('curSubSection', 'data');
		}
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.activityTabs.helpers({

	'data': function() {
		return Session.equals('curSubSection', 'data');
	},

	'comments': function() {
		return Session.equals('curSubSection', 'comments');
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.activityTabs.events({

	'click .data': function(event) {
		event.preventDefault();
		window.location.hash = 'data';
		Session.set('curSubSection', 'data');
	},

	'click .comments': function(event) {
		event.preventDefault();
		window.location.hash = 'comments';
		Session.set('curSubSection', 'comments');
	},

});
