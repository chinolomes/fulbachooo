//======================================================================
// ACTIVITY TABLET TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.activityTABLET.helpers({

	'data': function() {
		return Session.equals('curSubSection', 'data');
	},

	'comments': function() {
		return Session.equals('curSubSection', 'comments');
	}

});
