//======================================================================
// ACTIVITY TITLE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.activityTitle.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.status = new ReactiveVar(null);

   // Internal var(s)
   var matchOptions = {
      fields: {
         status: 1,
         peopleIn: 1,
         friendsOf: 1,
         fieldSize: 1
      }
   };

   // 2. Get data context
   var matchId = Template.currentData().matchId;

   // 3. Set reactive vars
   instance.autorun(function() {
      var match = Matches.findOne(matchId, matchOptions); // reactive source

      if (match && match.status) {
         instance.status.set(null); // re-initialize var
   		if (match.status === 'active') {
            if (getPeopleMissing(match) === 0) { // requires peopleIn, friendsOf and fieldSize
               instance.status.set('full');
            }
   		} else {
            instance.status.set(match.status);
   		}
      }
   });

});
//-----------------------------------------------------------------------
// HELPERS:
Template.activityTitle.helpers({

	'status': function() {
      var status = Template.instance().status.get();
		return !_.isNull(status) ? '-' + TAPi18n.__(status) + '-' : '';
	}

});
