//======================================================================
// ACTIVITY DATA CONTENT:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match' (which includes owner name and services)
//----------------------------------------------------------------------
// ON CREATION:
Template.activityData.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.match = new ReactiveVar(null);
	instance.ownerName = new ReactiveVar(null);

   // Internal var(s)
   var ownerOptions = {
      fields: {
         "profile.name": 1
      }
   };

   // 2. Get data context
   var matchId = Template.currentData().matchId;

   // 3. Set reactive vars
   instance.autorun(function() {
      var match = Matches.findOne(matchId);
      instance.match.set(match);
      if (match && match.createdBy) {
         var owner = Meteor.users.findOne(match.createdBy, ownerOptions);
         var ownerName = owner && owner.profile && owner.profile.name ? owner.profile.name : '';
         instance.ownerName.set(ownerName);
      }
   });

});
//----------------------------------------------------------------------
// ON CREATION:
Template.activityData.onRendered(function() {

   // Current template instance
   var instance = this;

   // Get reactive var(s)
   var match = instance.match.get();
   var lat = match.loc.coordinates[1];
   var lng = match.loc.coordinates[0];
   var city = getCityName(lat, lng);
   var daysDifference = getDaysDifference(match.date, new Date());

   if (daysDifference >= 0 && daysDifference <= 4) {
      var options = {
         location: lat + ',' + lng,
         unit: 'c',
         success: function(weather) {
            html = '<td><i class="sw icon-' + weather.forecast[Math.abs(daysDifference)].code + '" style="font-size: 25px !important; color: inherit !important;"></i></td>';
            if (daysDifference === 0) {
               switch (city) {
                  case 'enschede':
                     html += '<td><a href="http://www.weeronline.nl/Europa/Nederland/Enschede/4057346" target="_blank">' + weather.temp + '&deg;' + weather.units.temp + ' ' + TAPi18n.__(weather.forecast[0].text) + '</a></td>';
                     break;
                  case 'corrientes':
                     html += '<td><a href="https://www.meteoblue.com/es/tiempo/pronostico/semana/ciudad-de-corrientes_argentina_3435217" target="_blank">' + weather.temp + '&deg;' + weather.units.temp + ' ' + TAPi18n.__(weather.forecast[0].text) + '</a></td>';
                     break;
                  case 'bsas':
                     html += '<td><a href="https://www.meteoblue.com/es/tiempo/pronostico/semana/buenos-aires_argentina_3435910" target="_blank">' + weather.temp + '&deg;' + weather.units.temp + ' ' + TAPi18n.__(weather.forecast[0].text) + '</a></td>';
                     break;
                  default:
                     html += '<td>' + weather.temp + '&deg;' + weather.units.temp + ' ' + TAPi18n.__(weather.forecast[0].text) + '</td>';
                     break;
               }
            } else {
               html += '<td>' + TAPi18n.__(weather.forecast[Math.abs(daysDifference)].text) + '</td>';
            }

            $("#weather-row").html(html);
         },
         error: function(error) {
            $("#weather-row").html(error);
         }
      }
   } else {
      html = '<td colspan="2"></td>';
      $("#weather-row").html(html);
   }

   Weather.options = options;
   Weather.load();

});
//----------------------------------------------------------------------
// HELPERS:
Template.activityData.helpers({

   'match': function() {
      return Template.instance().match.get();
   },

	'playersCounter': function() {
      var match = Template.instance().match.get();
		var totalPlayers = 2 * match.fieldSize;
      var enrolledPlayers = match.peopleIn.length + match.friendsOf.length + match.waitingList.length;
		//var enrolledPlayers = totalPlayers - getPeopleMissing(match);
		return enrolledPlayers + ' / ' + totalPlayers;
	},

	'durationIsSet': function() {
      var match = Template.instance().match.get();
		return match.duration && match.duration !== '';
	},

	'fieldTypeIsSet': function() {
      var match = Template.instance().match.get();
		return match.fieldType && match.fieldType !== '';
	},

	'fieldPhoneIsSet': function() {
      var match = Template.instance().match.get();
		return match.fieldPhone && match.fieldPhone !== '';
	},

	'fieldWebsiteIsSet': function() {
      var match = Template.instance().match.get();
		return match.fieldWebsite && match.fieldWebsite !== '';
	},

	'viewerIsOwnerOrAdmin': function() {
      var match = Template.instance().match.get();
      return curUserIsOwnerOrAdmin(match);
	},

	'ownerName': function() {
		return Template.instance().ownerName.get();
	},

	'shareTitle': function() {
      var match = Template.instance().match.get();
		return match.title;
	},

	'public': function() {
      var match = Template.instance().match.get();
		return match.privacy && match.privacy === 'public';
	},

	'statusColor': function() {
      var match = Template.instance().match.get();
		return match.status && match.status === 'active' ? 'green' : 'red';
	},

	'following': function() {
      var match = Template.instance().match.get();
		return match.followers && _.indexOf(match.followers, Meteor.userId()) !== -1 ? 'checked' : '';
	},

	'descriptionIsSet': function() {
      var match = Template.instance().match.get();
		return match.description && match.description.length > 0;
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.activityData.events({

	'click .followingLabel': function(event, instance) {
		event.preventDefault();

      // Get reactive vars
      var match = instance.match.get();

      if (match.status !== 'active') {
         var msg = {status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled'};
         processMessage(msg);
         return;
      }
		Meteor.call('followUnfollowActivity', this.matchId, function(err, msg) {
         if (err) {
            console.log(err);
         } else {
            processMessage(msg);
         }
      });
	},

	'click #manageEditMatchButton': function(event) {
		event.preventDefault();
		Router.go('/activity/edit/' + this.matchId);
	},

	'click .seeMapMOBILE': function(event) {
		event.preventDefault();
		Router.go('/activities/activity/map/' + this.matchId);
	},

	'click .seeParticipantsMOBILE': function(event) {
		event.preventDefault();
		Router.go('/activities/activity/' + this.matchId + '#participants');
	},

	'click .seeMapMOBILE': function(event) {
      event.preventDefault();

		//Session.set('resetMapRESIZE', true); // triggers autorun function on activityMap
      Session.set('curSubSection', 'map')
   },

	'click .seeMapTABLET': function(event) {
      event.preventDefault();

		Session.set('resetMapRESIZE', true); // triggers autorun function on activityMap

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.activityMap, {matchId: this.matchId}, $("#dialogNode")[0]);
   },

	'click .seeMapDESKTOP': function(event) {
      event.preventDefault();

		Session.set('resetMapRESIZE', true); // triggers autorun function on activityMap

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.activityMap, {matchId: this.matchId}, $("#dialogNode")[0]);
   }/*,

	'click .seeWeatherTABLET-DESKTOP': function(event) {
      event.preventDefault();

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.activityForecast, {}, $("#dialogNode")[0]);
   }*/

});
