//======================================================================
// ACTIVITY MOBILE TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.activityMOBILE.helpers({

	'data': function() {
		return Session.equals('curSubSection', 'data');
	},

	'participants': function() {
		return Session.equals('curSubSection', 'participants');
	},

	'comments': function() {
		return Session.equals('curSubSection', 'comments');
	},

	'map': function() {
		return Session.equals('curSubSection', 'map');
	},

	'forecast': function() {
		return Session.equals('curSubSection', 'forecast');
	}

});
