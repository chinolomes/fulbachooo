//======================================================================
// ACTIVITY NAVIGATION BAR TEMPLATE:
//======================================================================
// CONTEXT: this = {activityId}
// REQUIRED SUBS: 'activity'
//----------------------------------------------------------------------
// ON CREATION:
Template.activityNav.onCreated(function() {

   // 1. Initialize reactive variables
	//Session.set('curSubSection', 'data');
	var instance = this;

	// Get -reactive- hash
	instance.autorun(function() {
		var hash = Iron.Location.get().hash;
		if (hash) {
			switch(hash) {
				case '#participants':
					Session.set('curSubSection', 'participants');
					break;
				case '#comments':
					Session.set('curSubSection', 'comments');
					break;
				case '#map':
					Session.set('curSubSection', 'map');
					break;
				default:
					Session.set('curSubSection', 'data');
					break;
			}
		} else {
			Session.set('curSubSection', 'data');
		}
	});

});
//----------------------------------------------------------------------
// ON RENDERED:
/*Template.activityNav.onRendered(function() {

	// Disable carousel auto-scroll
	$('.carousel').carousel({
	   pause: true,
	   interval: false
	});

});*/
//----------------------------------------------------------------------
// HELPERS:
Template.activityNav.helpers({

	/*'enschedeToday': function() {
      var match = Matches.findOne(this.activityId, {fields: {"loc.coordinates": 1, date: 1}});
      var lat = match.loc.coordinates[1];
      var lng = match.loc.coordinates[0];
      return isEnschede(lat, lng) && getDaysDifference(match.date, new Date()) === 0;
	},*/

	'data': function() {
		return Session.equals('curSubSection', 'data') ? 'active' : '';
	},

	'participants': function() {
		return Session.equals('curSubSection', 'participants') ? 'active' : '';
	},

	'comments': function() {
		return Session.equals('curSubSection', 'comments') ? 'active' : '';
	},

	'map': function() {
		return Session.equals('curSubSection', 'map') ? 'active' : '';
	}/*,

	'forecast': function() {
		return Session.equals('curSubSection', 'forecast') ? 'active' : '';
	}*/

});
