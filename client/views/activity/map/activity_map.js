//======================================================================
// ACTIVITY MAP:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.activityMap.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.mapIsSet = new ReactiveVar(false);
	instance.lat = ReactiveVar(null);
	instance.lng = ReactiveVar(null);
   instance.sport = ReactiveVar(DEFAULT_SPORT); // no needs to be reactive

   // Internal var(s)
   var matchOptions = {
      fields: {
         'loc.coordinates': 1,
         sport: 1
      }
   };

	// 2. Get data context
   var matchId = Template.currentData().matchId;

	// Set reactive vars
	instance.autorun(function() {
      var match = Matches.findOne(matchId, matchOptions);
		var coords = match.loc.coordinates;
		instance.lat.set(coords[1]);
		instance.lng.set(coords[0]);
      instance.sport.set(match.sport);
	});

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.activityMap.onRendered(function() {

	// WE NEED TO BE SURE THAT LOGGED-OUT VARS ARE PROPERLY SET!!
   // Current template instance
   var instance = this;

	// Initialize google map
   var elemId = 'googleMapMatch'; // DOM element
   var mapCenterLat = instance.lat.get(); //coords[1];
   var mapCenterLng = instance.lng.get(); // coords[0];
   var zoom = 14;
   var map = setMap(elemId, mapCenterLat, mapCenterLng, zoom);
   instance.mapIsSet.set(true);

	// Set default marker
   var sport = instance.sport.get(); // match.sport;
	var draggable = false;
	var marker = createMarker(mapCenterLat, mapCenterLng, map, sport, draggable);

	// If the map coords are modified while a user is looking at the map, reset the marker location and map center
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true) {
			var newCenter = new google.maps.LatLng(instance.lat.get(), instance.lng.get()); // reactive source
			map.setCenter(newCenter);
			marker.setPosition(newCenter);
		}
	});

	// Resize the map after match is opened to avoid miss behaviors
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMapRESIZE', true)) {
			setTimeout(function() {
 				var center = map.getCenter();
				google.maps.event.trigger(map, 'resize');
				map.setCenter(center);
			}, 200);
			Session.set('resetMapRESIZE', false);
		}
	});

});
