//======================================================================
// FIELD WEBSITE FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [fieldWebsite]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.fieldWebsiteFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.fieldWebsite = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var fieldWebsite = !_.isUndefined(Template.currentData().fieldWebsite) ? Template.currentData().fieldWebsite : ''; // reactive source, could be undefined
		var fieldWebsite = Template.currentData().fieldWebsite || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.fieldWebsite.set(fieldWebsite);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.fieldWebsiteFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var fieldWebsite = '';

	instance.autorun(function(){
		// Get data context
	   fieldWebsite = instance.fieldWebsite.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(fieldWebsite)) {
	      instance.$('#field_website').val(fieldWebsite);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('fieldWebsite_FORM_COMPONENT', null)) {
			instance.$('#field_website').val(Session.get('fieldWebsite_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.fieldWebsiteFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('fieldWebsite_FORM_COMPONENT', instance.$('#field_website').val());

});


/*
//----------------------------------------------------------------------
// RENDERED:
Template.fieldWebsiteFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

   // Get data context
   var fieldWebsite = Template.currentData().fieldWebsite; // could be undefined

   // Initialize input in case some value is provided
   if (!_.isUndefined(fieldWebsite)) {
      instance.$('#field_website').val(fieldWebsite);
   }

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('fieldWebsite_FORM_COMPONENT', null)) {
		instance.$('#field_website').val(Session.get('fieldWebsite_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.fieldWebsiteFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('fieldWebsite_FORM_COMPONENT', instance.$('#field_website').val());

});
*/
