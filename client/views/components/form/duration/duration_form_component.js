//======================================================================
// DURATION FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [duration]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.durationFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.duration = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var duration = !_.isUndefined(Template.currentData().duration) ? Template.currentData().duration : ''; // reactive source, could be undefined
		var duration = Template.currentData().duration || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.duration.set(duration);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.durationFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var duration = '';

	instance.autorun(function(){
		// Get data context
	   duration = instance.duration.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(duration)) {
	      instance.$('#duration').val(duration);
	   }

		// Auto populate component in case the user pre-selects some
	   // value and then changes language or view
		if (!Session.equals('duration_FORM_COMPONENT', null)) {
			instance.$('#duration').val(Session.get('duration_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.durationFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user pre-selects some values and then changes language/view
	Session.set('duration_FORM_COMPONENT', instance.$('#duration').val());

});


/*
//----------------------------------------------------------------------
// RENDERED:
Template.durationFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

   // Get data context
   var duration = Template.currentData().duration; // could be undefined

   // Initialize input in case some value is provided
   if (!_.isUndefined(duration)) {
      instance.$('#duration').val(duration);
   }

	// Auto populate component in case the user pre-selects some
   // value and then changes language or view
	if (!Session.equals('duration_FORM_COMPONENT', null)) {
		instance.$('#duration').val(Session.get('duration_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.durationFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user pre-selects some values and then changes language/view
	Session.set('duration_FORM_COMPONENT', instance.$('#duration').val());

});
*/
