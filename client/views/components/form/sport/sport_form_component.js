//======================================================================
// SPORT FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {[sport], label}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATION:
Template.sportFormComponent.onCreated(function() {

	// Current template instance
	var instance = this;

	// Reactive vars declaration
	instance.sport = new ReactiveVar(DEFAULT_SPORT); // string

});
//----------------------------------------------------------------------
// RENDERED:
Template.sportFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('sport_FORM_COMPONENT', null)) {
		instance.$('#sport').val(Session.get('sport_FORM_COMPONENT'));
		instance.sport.set(Session.get('sport_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.sportFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('sport_FORM_COMPONENT', instance.$('#sport').val());

});
//----------------------------------------------------------------------
// HELPERS:
Template.sportFormComponent.helpers({

	'selectedSport': function() {
		return Template.instance().sport.get() || DEFAULT_SPORT; // see client/lib/constants.js
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.sportFormComponent.events({

	'change select': function(event, instance) {
		event.preventDefault();
      var sport = instance.$('select').val();
		Session.set('markerTypeNEWMATCH', sport);
		Session.set('resetMarkerNEWMATCH', true); // triggers an autorun function on google map
      //instance.sport.set(sport);
	}

});
