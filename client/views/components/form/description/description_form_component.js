//======================================================================
// DESCRIPTION FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [description]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.descriptionFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.description = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var description = !_.isUndefined(Template.currentData().description) ? Template.currentData().description : ''; // reactive source, could be undefined
		var description = Template.currentData().description || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.description.set(description);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.descriptionFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var description = '';

	instance.autorun(function(){
		// Get data context
	   description = instance.description.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(description)) {
	      instance.$('#description').val(description);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('description_FORM_COMPONENT', null)) {
			instance.$('#description').val(Session.get('description_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.descriptionFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('description_FORM_COMPONENT', instance.$('#description').val());

});

/*
//----------------------------------------------------------------------
// RENDERED:
Template.descriptionFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

   // Get data context
   var description = Template.currentData().description; // could be undefined

   // Initialize input in case some value is provided
   if (!_.isUndefined(description)) {
      instance.$('#description').val(description);
   }

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('description_FORM_COMPONENT', null)) {
		instance.$('#description').val(Session.get('description_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.descriptionFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('description_FORM_COMPONENT', instance.$('#description').val());

});
*/
