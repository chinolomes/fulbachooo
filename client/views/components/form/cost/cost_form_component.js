//======================================================================
// COST FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [cost]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.costFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.cost = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var cost = !_.isUndefined(Template.currentData().cost) ? Template.currentData().cost : ''; // reactive source, could be undefined
		var cost = Template.currentData().cost || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.cost.set(cost);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.costFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var cost = '';

	instance.autorun(function(){
		// Get data context
	   cost = instance.cost.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(cost)) {
	      instance.$('#cost').val(cost);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('cost_FORM_COMPONENT', null)) {
			instance.$('#cost').val(Session.get('cost_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.costFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('cost_FORM_COMPONENT', instance.$('#cost').val());

});
