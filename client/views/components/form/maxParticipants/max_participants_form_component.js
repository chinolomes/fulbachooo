//======================================================================
// MAXIMUM NUMBER OF PARTICIPANTS FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// RENDERED:
Template.maxParticipantsFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('maxParticipants_FORM_COMPONENT', null)) {
		instance.$('#max_participants').val(Session.get('maxParticipants_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.maxParticipantsFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('maxParticipants_FORM_COMPONENT', instance.$('#max_participants').val());

});
