//======================================================================
// TIME FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [time]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.timeFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.time = new ReactiveVar('12:00');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var time = !_.isUndefined(Template.currentData().time) ? Template.currentData().time : '12:00'; // reactive source, could be undefined
		var time = Template.currentData().time || '12:00'; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.time.set(time);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.timeFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var time = '';

	instance.autorun(function(){
		// Get data context
	   time = instance.time.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(time)) {
	      instance.$('#time').val(time);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('time_FORM_COMPONENT', null)) {
			instance.$('#time').val(Session.get('time_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.timeFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('time_FORM_COMPONENT', instance.$('#time').val());

});
