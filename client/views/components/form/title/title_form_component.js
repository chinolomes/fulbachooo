//======================================================================
// TITLE FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [title]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.titleFormComponent.onCreated(function() {

	//console.log('Session.get(title_FORM_COMPONENT): ', Session.get('title_FORM_COMPONENT'));
	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.title = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var title = !_.isUndefined(Template.currentData().title) ? Template.currentData().title : ''; // reactive source, could be undefined
		var title = Template.currentData().title || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.title.set(title);
	});

	// TODO: this on created block is not necessary. The data context can be gotten directly in onRendered
});
//----------------------------------------------------------------------
// RENDERED:
Template.titleFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var title = '';

	instance.autorun(function(){
		// Get data context
	   title = instance.title.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(title)) {
	      instance.$('#title').val(title);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('title_FORM_COMPONENT', null)) {
			instance.$('#title').val(Session.get('title_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.titleFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('title_FORM_COMPONENT', instance.$('#title').val());

});
