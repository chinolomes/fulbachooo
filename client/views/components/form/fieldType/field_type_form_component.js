//======================================================================
// FIELD TYPE FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [fieldType]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.fieldTypeFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.fieldType = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var fieldType = !_.isUndefined(Template.currentData().fieldType) ? Template.currentData().fieldType : ''; // reactive source, could be undefined
		var fieldType = Template.currentData().fieldType || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.fieldType.set(fieldType);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.fieldTypeFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var fieldType = '';

	instance.autorun(function(){
		// Get data context
	   fieldType = instance.fieldType.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(fieldType)) {
	      instance.$('#field_type').val(fieldType);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('fieldType_FORM_COMPONENT', null)) {
			instance.$('#field_type').val(Session.get('fieldType_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.fieldTypeFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('fieldType_FORM_COMPONENT', instance.$('#field_type').val());

});


/*
//----------------------------------------------------------------------
// RENDERED:
Template.fieldTypeFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

   // Get data context
   var fieldType = Template.currentData().fieldType; // could be undefined

   // Initialize input in case some value is provided
   if (!_.isUndefined(fieldType)) {
      instance.$('#field_type').val(fieldType);
   }

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('fieldType_FORM_COMPONENT', null)) {
		instance.$('#field_type').val(Session.get('fieldType_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.fieldTypeFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('fieldType_FORM_COMPONENT', instance.$('#field_type').val());

});
*/
