//======================================================================
// REPEAT FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {[repeat]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// RENDERED:
Template.repeatFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

   // Get data context
   var context = Template.currentData(); // could be undefined

   // Initialize input in case some value is provided
   if (context && context.repeat && context.repeat === true) {
      instance.$('#repeat').prop('checked', 'checked');
   }

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('repeat_FORM_COMPONENT', null)) {
		var checked = Session.get('repeat_FORM_COMPONENT') === 'checked'; // boolean
		instance.$('#repeat').prop('checked', checked);
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.repeatFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('repeat_FORM_COMPONENT', instance.$('input[name=repeat]:checked').val());

});
