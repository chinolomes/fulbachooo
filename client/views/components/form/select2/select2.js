//======================================================================
// SELECT2 TEMPLATE:
//======================================================================
// CONTEXT: this = {elemDOM, group, team, preLabel, label, [class], [matchId], [hint]}
// REQUIRED SUBS: template-level + ['match']
// SEE: Router.onBeforeAction
// TODO: clean code and add comments
// TODO: manage the situation when a players is enrolled/leaves the match while
// the match owner is doing admin operations
//----------------------------------------------------------------------
// ON CREATION:
Template.select2.onCreated(function() {

	// Current template instance
	var instance = this;

	// Reactive vars declaration
	instance.match = new ReactiveVar(null); // cursor
	instance.subsReady = new ReactiveVar(false); // boolean,
	instance.selection = new ReactiveVar([]); // array of selected ids

	// Get NOT reactive data context
	var matchId = Template.currentData().matchId; // could be undefined
	var group = Template.currentData().group;
	var elemDOM = Template.currentData().elemDOM;

	// Initialize session var(s)
	Session.set(elemDOM + '_RESET_SELECT2', false);
	Session.set('resetSelect2_PREPO', false);

	// Set reactive vars
	instance.autorun(function() {
		if (matchId) {
			var match = null;
			switch(group) {
				case 'peopleIn':
					match = Matches.findOne(matchId, {fields: {peopleIn: 1}});
					break;
				case 'friendsOf':
					match = Matches.findOne(matchId, {fields: {friendsOf: 1}});
					break;
				case 'waitingList':
					match = Matches.findOne(matchId, {fields: {waitingList: 1}});
					break;
				case 'admin':
					match = Matches.findOne(matchId, {fields: {admin: 1}});
					break;
			}
			instance.match.set(match);
		}
	});

	// Subscribe to the given match in case the auto-popullate widget is used
	instance.autorun(function() {
		if (Session.equals('resetSelect2_PREPO', true) && !Session.equals('matchId_PREPO', null)) {
			instance.subsReady.set(false);
			var subs = instance.subscribe("match", Session.get('matchId_PREPO'));

			if (subs.ready()) {
				instance.subsReady.set(true); // this will trigger and autorun inside onRendered
			}
		}
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.select2.onRendered(function() {

	// Current template instance
	var instance = this;

	// Get NOT reactive data context
	var group = Template.currentData().group;
	var team = Template.currentData().team;
	var matchId = Template.currentData().matchId; // could be undefined
	var elemDOM = Template.currentData().elemDOM;
	var $select2 = instance.$("#" + elemDOM);

	// Initialize select2
	//instance.$("#" + elemDOM).select2({
	$select2.select2({
		language: 'en',
		minimumInputLength: 1,
		templateResult: resultTemplate,
		templateSelection: group !== 'friendsOf' ? selectionTemplate : friendOfSelectionTemplate,
	   // Fetch the results with a Meteor method instead of ajax
		ajax: {
			delay: 500,
			data: function (params) {
				return {q: params.term};
			},
			transport: function (params, success, failure) {
		      Meteor.call('getUsersMatchingRegExp', params.data.q, function(err, res) {
		         if (err) {
		            failure(err);
		            return;
		         }
	          	success(res);
		      });
			},
			processResults: function(data) {
				// VARIABLES
				var filteredData = [];
				var processedData = [];
				var option = {};

				// REMOVE PREVIOUSLY SELECTED VALUES
				filteredData = _.filter(data, function(obj) {return _.indexOf(instance.selection.get(), obj._id) === -1});

				// PROCESS DATA IN THE SELECT2 FORMAT
				_.each(filteredData, function(user) {
					option = {
						id: user._id,
						text: user.profile.avatar + '|' + user.profile.name
					}
					processedData.push(option);
				});

				return {results: processedData};
			}
		}
	}).on("change", function(e) {
		//console.log('ON CHANGE');
		var ids = $(this).select2('val');
		var textArr = $(this).select2('data');
		instance.selection.set(ids);
		saveIntoSessionVars(elemDOM, ids, textArr)
	});

	// Pre-populate select2
	if (!Session.equals(elemDOM + '_SELECT2', null)) {
		fillPlayersFromSessionVars(elemDOM, $select2);
	} else {
		instance.autorun(function() {
			if (group === 'admin') {
				fillAdminFromDatabase(instance.match.get(), elemDOM, $select2);	// in case match is undefined function does nothing
			} else {
				fillPlayersFromDatabase(instance.match.get(), elemDOM, $select2, group, team);	// in case match is undefined function does nothing
			}
		});
	}

	// In case the user hits the cancel edit match button, reset values from DB
	instance.autorun(function() {
		if (Session.equals(elemDOM + '_RESET_SELECT2', true)) {
			fillPlayersFromDatabase(instance.match.get(), elemDOM, $select2, group, team);	// in case match is undefined function does nothing
			Session.set(elemDOM + '_RESET_SELECT2', false);
		}
	});

	// In case the user choose one match in the pre-popullate widget
	instance.autorun(function() {
		if (Session.equals('resetSelect2_PREPO', true) && instance.subsReady.get() === true) {
			if (group === 'admin') {
				fillAdminFromPrepo(Session.get('matchId_PREPO'), elemDOM, $select2);	// in case matchId is undefined function does nothing
			} else {
				fillPlayersFromPrepo(Session.get('matchId_PREPO'), elemDOM, $select2, group, team);	// in case matchId is undefined function does nothing
			}
			instance.subsReady.set(false);
			//Session.set('resetSelect2_PREPO', false);
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.select2.onDestroyed(function() {

	// Destroy select2 element
	var elem = document.getElementsByClassName("select2-dropdown")[0];
	if (!_.isUndefined(elem)) {
		elem.parentNode.removeChild(elem);
	}
	//console.log('TEMPLATE DESTROYED!!!!!!!!!!!');

});
//----------------------------------------------------------------------
// HELPERS:
Template.select2.helpers({

	'classIsSet': function() {
		return !_.isUndefined(this.class);
	},

	'hintIsSet': function() {
		return !_.isUndefined(this.hint);
	}

});

//======================================================================
// AUXILIARY FUNCTIONS:
//======================================================================
// Must return String or JQuery object based on Select2 4.0.0 docs:
// https://select2.github.io/options.html
function resultTemplate(player) {

	//console.log('USING RESULT-TEMPLATE');
	if (!player.id) return "No contact selected";

	var user = player.text.split('|');
	var avatar = user[0];
	var name = user[1];
	var template = "<div class='media' style='height: 80px;'>"+
				"<div class='media-left'>"+
					"<img style='margin-top: 5px; width: 80px;' class='media-object' data-src='holder.js/64x64' src='"+avatar+"'/>"+
				"</div>"+
				"<div class='media-body' style='font-size: 15px;'>"+
					"<strong>"+name+"</strong>"+
				"</div>"+
		   "</div>";
	return $(template);
};
//----------------------------------------------------------------------
var saveIntoSessionVars = function(elemDOM, ids, textArr) {

	//console.log('SAVE INTO SESSION VARS');
	var data = [];
	// Filter values
	var uniqIds = _.uniq(ids);
	var uniqText = _.uniq(_.pluck(textArr, 'text'));
	for (var i = 0; i < uniqIds.length; i++) {
		var selection = {id: uniqIds[i], text: uniqText[i]};
		data.push(selection);
	}
	Session.set(elemDOM + '_SELECT2', data);

};
//----------------------------------------------------------------------
function selectionTemplate(player) {

	//console.log('USING SELECTION-TEMPLATE');
	//console.log('player: ', player);
	if (!player.id) return;

	var user = player.text.split('|');
	var avatar = user[0];
	var name = user[1];
	return name;

};
//----------------------------------------------------------------------
function friendOfSelectionTemplate(player) {

	if (!player.id) return;

	var lang = TAPi18n.getLanguage();
	var friend = lang === 'es' ? 'amigo de ' : 'friend of ';
	var user = player.text.split('|');
	var avatar = user[0];
	var name = user[1];
	return friend + name;

};
//----------------------------------------------------------------------
var fillPlayersFromDatabase = function(match, elemDOM, select2, group, team) {

	if (!match) return;

	//console.log('FILL FROM DB');
	// Variables declaration
	var data = [];
	var options = {
		fields: {
			'profile.avatar': 1,
			'profile.name': 1
		}
	};

	// Clear select2 form
	select2.empty();

	// Parse data from DB
	_.each(filterByTeam(match[group], team), function(player) {
		var userId = player.userId;
		var user = Meteor.users.findOne(userId, options); // requires 'match' subs
		var text = user.profile.avatar + '|' + user.profile.name;
		var option = $("<option selected></option>").val(userId).text(text);
		select2.append(option); // append the option and update Select2
	});
	select2.trigger('change');
}
//----------------------------------------------------------------------
var fillAdminFromPrepo = function(matchId, elemDOM, select2) {

	if (!matchId) return;

	//console.log('ADMIN PREPO, matchId: ', matchId);
	select2.empty();
	var options = {
		fields: {
			"profile.name": 1,
			"profile.avatar": 1
		}
	};

	var match = Matches.findOne(matchId); // requieres 'match' subs
	// Admin
	if (!_.isUndefined(match.admin) && match.admin.length > 0 && !_.isNull(match.admin[0])) {
		var admin = Meteor.users.findOne(match.admin[0], options);
		var option = $("<option selected></option>").val(admin._id).text('undefined|' + admin.profile.name);
		select2.append(option); // append the option to Select2
	}
	select2.trigger('change'); // update Select2 to display the new option(s)

}
//----------------------------------------------------------------------
var fillPlayersFromPrepo = function(matchId, elemDOM, select2, group, team) {

	if (!matchId) return;

	//console.log('PLAYERS PREPO, elemDOM: ', elemDOM);
	var options = {
		fields: {
			"profile.name": 1,
			"profile.avatar": 1
		}
	};
	var match = Matches.findOne(matchId); // requires 'match' subs
	select2.empty();

	if (!_.isUndefined(match.peopleInOnCreation)) {
		_.each(match.peopleInOnCreation, function(player) { // match.peopleInOnCreation[i] = {userId, team, createdAt}
			var user = Meteor.users.findOne(player.userId, options);
			var option = $("<option selected></option>").val(player.userId).text(user.profile.avatar + '|' + user.profile.name);
			select2.append(option); // append the option to Select2
		});
	}
	// Display data
	select2.trigger('change'); // update Select2

}
//----------------------------------------------------------------------
var fillPlayersFromSessionVars = function(elemDOM, select2) {

	if (Session.equals(elemDOM + '_SELECT2', null)) {
		throw new Meteor.Error('fillPlayersFromSessionVars session = null');
		return;
	}

	select2.empty();

	// Parse data from sessions
	_.each(Session.get(elemDOM + '_SELECT2'), function(selection) { // selection = {id, text}
		var option = $("<option selected></option>").val(selection.id).text(selection.text);
		select2.append(option); // append the option and update Select2
	});
	select2.trigger('change'); // append the option and update Select2

}
//----------------------------------------------------------------------
function fillAdminFromDatabase(match, elemDOM, select2) {

	//if (!match || _.isUndefined(match.admin) || _.isUndefined(match.admin[0]) || _.isNull(match.admin[0]) || match.admin[0] === '') return;
	if (!match || !match.admin || !match.admin[0]) return;
	//console.log("admin: ", match.admin[0]);

	//console.log('FILL ADMIN FROM DB');
	select2.empty();

	var options = {
		fields: {
			'profile.avatar': 1,
			'profile.name': 1
		}
	};

	// Parse data from DB
	var admin = Meteor.users.findOne({_id: match.admin[0]}, options); // requires 'match' subs
	var option = $("<option selected></option>").val(admin._id).text(admin.profile.avatar + '|' + admin.profile.name);
	select2.append(option); // append the option and update Select2
	select2.trigger('change'); // append the option and update Select2

}
