//======================================================================
// FIELD PHONE FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [fieldPhone]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATED:
Template.fieldPhoneFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.fieldPhone = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var fieldPhone = !_.isUndefined(Template.currentData().fieldPhone) ? Template.currentData().fieldPhone : ''; // reactive source, could be undefined
		var fieldPhone = Template.currentData().fieldPhone || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.fieldPhone.set(fieldPhone);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.fieldPhoneFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var fieldPhone = '';

	instance.autorun(function(){
		// Get data context
	   fieldPhone = instance.fieldPhone.get(); // reactive source, could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(fieldPhone)) {
	      instance.$('#field_phone').val(fieldPhone);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('fieldPhone_FORM_COMPONENT', null)) {
			instance.$('#field_phone').val(Session.get('fieldPhone_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.fieldPhoneFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('fieldPhone_FORM_COMPONENT', instance.$('#field_phone').val());

});


/*
//----------------------------------------------------------------------
// RENDERED:
Template.fieldPhoneFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

   // Get data context
   var fieldPhone = Template.currentData().fieldPhone; // could be undefined

   // Initialize input in case some value is provided
   if (!_.isUndefined(fieldPhone)) {
      instance.$('#field_phone').val(fieldPhone);
   }

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('fieldPhone_FORM_COMPONENT', null)) {
		instance.$('#field_phone').val(Session.get('fieldPhone_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.fieldPhoneFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('fieldPhone_FORM_COMPONENT', instance.$('#field_phone').val());

});
*/
