//======================================================================
// DATEPICKER:
//======================================================================
// CONTEXT: this = {lang, [date]}
// REQUIRED SUBS: ''
// EXTERNAL SESSION VARS: 'resetDATEPICKER', 'newDateDATEPICKER'
// SEE: Router.onBeforeAction
// SOURCE: http://bootstrap-datepicker.readthedocs.org/en/release/options.html
//----------------------------------------------------------------------
// ON CREATED:
Template.datepicker.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.date = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var date = !_.isUndefined(Template.currentData().date) ? Template.currentData().date : ''; // reactive source, could be undefined
		var date = Template.currentData().date || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.date.set(date);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.datepicker.onRendered(function() {

	// Current template instance
	var instance = this;

	// Get not reactive data context
	//var date = Template.currentData().date; // could be undefined!
	var lang = Template.currentData().lang;

	switch (lang) {
		case 'es':
			$.fn.datepicker.dates['es'] = {
				days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
				daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
				daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"],
				months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
				monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
				today: "Hoy",
				clear: "Claro"
			};
			break;
		case 'nl':
			$.fn.datepicker.dates['nl'] = {
				days: ["Zondag", "Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag", "Zondag"],
				daysShort: ["Zon", "Maa", "Din", "Woe", "Don", "Vri", "Zat", "Zon"],
				daysMin: ["Zo", "Ma", "Di", "Wo", "Do", "Vr", "Za", "Zo"],
				months: ["Januari", "Februari", "Maart", "April", "Mei", "Juni", "Juli", "August", "September", "Oktober", "November", "December"],
				monthsShort: ["Jan", "Feb", "Maa", "Apr", "Mei", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dec"],
				today: "Vandaag",
				clear: "Duidelijk"
			};
			break;
		case 'de':
			$.fn.datepicker.dates['de'] = {
				days: ["Sonntag", "Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
				daysShort: ["Son", "Mon", "Die", "Mit", "Don", "Fre", "Sam", "Son"],
				daysMin: ["So", "Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
				months: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
				monthsShort: ["Jan", "Feb", "Mär", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
				today: "Heute",
				clear: "Klar"
			};
			break;
		case 'it':
			$.fn.datepicker.dates['it'] = {
				days: ["Domenica", "Lunedi", "Martedì", "Mercoledì", "Giovedi", "Venerdì", "Sabato", "Domenica"],
				daysShort: ["Dom", "Lun", "Mar", "Mer", "Gio", "Ven", "Sab", "Dom"],
				daysMin: ["Do", "Lu", "Ma", "Me", "Gi", "Ve", "Sa", "Do"],
				months: ["Gennaio", "Febbraio", "Marzo", "Aprile", "Maggio", "Giugno", "Luglio", "Agosto", "Settembre", "Ottobre", "Novembre", "Dicembre"],
				monthsShort: ["Gen", "Feb", "Mar", "Apr", "Mag", "Giu", "Lug", "Ago", "Set", "Ott", "Nov", "Dic"],
				today: "Oggi",
				clear: "Chiaro"
			};
			break;
	}

	// Initialize datepicker
	instance.$('.datepicker').datepicker({
		language: lang,
		autoclose: true,
		format: 'DD dd/mm/yyyy',
		startDate: '0d',
		endDate: '+1m'
	});

	// Internal var(s)
	var date = '';

	instance.autorun(function(){
		// Get data context
	   date = instance.date.get(); // reactive source could be undefined

		// Set database value if data is set
		if (!_.isUndefined(date)) {
			instance.$('.datepicker').datepicker('setUTCDate', date);
		}
	});

	// Fill datepicker with pre-selected date in case the user selected a date and then changed language/view
	if (!Session.equals('selectedDATEPICKER', null)) {
		instance.$('.datepicker').datepicker('setUTCDate', Session.get('selectedDATEPICKER'));
	}

	// In case the user hits the cancel edit button, reset datepicker
	instance.autorun(function(){
		if (Session.equals('resetDATEPICKER', true) && !Session.equals('newDateDATEPICKER', null)) {
			instance.$('.datepicker').datepicker('setUTCDate', Session.get('newDateDATEPICKER'));
			Session.set('resetDATEPICKER', false);
			Session.set('newDateDATEPICKER', null);
		}
	});

	// Set onChange datepicker-event to keep selected date in case the user changes language/view
	instance.autorun(function(){
		instance.$('.datepicker').datepicker().on('changeDate', function(e){
        	Session.set('selectedDATEPICKER', instance.$('.datepicker').datepicker('getUTCDate'));
    	});
	});

});



/*
//----------------------------------------------------------------------
// RENDERED:
Template.datepicker.onRendered(function() {

	// Current template instance
	var instance = this;

	// Get not reactive data context
	var date = Template.currentData().date; // could be undefined!
	var lang = Template.currentData().lang;

	if (lang === 'es') {
		$.fn.datepicker.dates['es'] = {
			days: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo"],
			daysShort: ["Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb", "Dom"],
			daysMin: ["Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sá", "Do"],
			months: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
			monthsShort: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
			today: "Hoy",
			clear: "Claro"
		};
	}

	// Initialize datepicker
	instance.$('.datepicker').datepicker({
		language: lang,
		autoclose: true,
		format: 'DD dd/mm/yyyy',
		startDate: '0d',
		endDate: '+1m'
	});

	// Set database value if data is set
	if (!_.isUndefined(date)) {
		instance.$('.datepicker').datepicker('setUTCDate', date);
	}

	// Fill datepicker with pre-selected date in case the user selected a date and then changed language/view
	if (!Session.equals('selectedDATEPICKER', null)) {
		instance.$('.datepicker').datepicker('setUTCDate', Session.get('selectedDATEPICKER'));
	}

	// In case the user hits the cancel edit button, reset datepicker
	instance.autorun(function(){
		if (Session.equals('resetDATEPICKER', true) && !Session.equals('newDateDATEPICKER', null)) {
			instance.$('.datepicker').datepicker('setUTCDate', Session.get('newDateDATEPICKER'));
			Session.set('resetDATEPICKER', false);
			Session.set('newDateDATEPICKER', null);
		}
	});

	// Set onChange datepicker-event to keep selected date in case the user changes language/view
	instance.autorun(function(){
		instance.$('.datepicker').datepicker().on('changeDate', function(e){
        	Session.set('selectedDATEPICKER', instance.$('.datepicker').datepicker('getUTCDate'));
    	});
	});

});
*/
