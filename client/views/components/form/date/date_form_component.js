//======================================================================
// DATE FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [date]}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.dateFormComponent.helpers({

   'dateIsSet': function() {
      return !_.isUndefined(this.date);
   }

});
