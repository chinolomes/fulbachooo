//======================================================================
// COORDINATES FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// RENDERED:
/*Template.coordsFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('coords_FORM_COMPONENT', null)) {
      var coords = Session.get('coords_FORM_COMPONENT'); // [lat, lng]
		instance.$('#lat').val(coords[0]);
      instance.$('#lng').val(coords[1]);
	}

});*/
//----------------------------------------------------------------------
// DESTROYED:
Template.coordsFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
   var lat = parseFloat(instance.$('#lat').val(), 10);
   var lng = parseFloat(instance.$('#lng').val(), 10);
	if (_.isNumber(lat) && _.isNumber(lng) && !_.isNaN(lat) && !_.isNaN(lng)) {
		Session.set('coords_FORM_COMPONENT', [lat, lng]);
	}

});
