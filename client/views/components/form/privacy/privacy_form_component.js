//======================================================================
// PRIVACY FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, privacy}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// RENDERED:
Template.privacyFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Auto populate component in case the user preselects some
   // value and then changes language or view
	if (!Session.equals('privacy_FORM_COMPONENT', null)) {
		instance.$('#privacy').val(Session.get('privacy_FORM_COMPONENT'));
	}

});
//----------------------------------------------------------------------
// DESTROYED:
Template.privacyFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('privacy_FORM_COMPONENT', instance.$('#privacy').val());

});
