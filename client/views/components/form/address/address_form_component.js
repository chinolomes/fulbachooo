//======================================================================
// ADDRESS FORM COMPONENT TEMPLATE:
//======================================================================
// CONTEXT: this = {label, [address]}
// REQUIRED SUBS: ''
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// ON CREATION:
Template.addressFormComponent.onCreated(function() {

	// 0. Current template instance
	var instance = this;

	// 1. Initialize reactive var(s)
	instance.address = new ReactiveVar('');

	// 2. Get -reactive- data context
	instance.autorun(function(){
		//var address = !_.isUndefined(Template.currentData().address) ? Template.currentData().address : ''; // reactive source, could be undefined
		var address = Template.currentData().address || ''; // reactive source, could be undefined
		// 3. Set reactive var(s)
 		instance.address.set(address);
	});

});
//----------------------------------------------------------------------
// RENDERED:
Template.addressFormComponent.onRendered(function() {

	// Current template instance
	var instance = this;

	// Internal var(s)
	var address = '';

	instance.autorun(function() {
		// Get data context
	   address = instance.address.get(); // reactive source could be undefined

	   // Initialize input in case some value is provided
	   if (!_.isUndefined(address)) {
	      instance.$('#address').val(address);
	   }

		// Auto populate component in case the user preselects some
	   // value and then changes language or view
		if (!Session.equals('address_FORM_COMPONENT', null)) {
			instance.$('#address').val(Session.get('address_FORM_COMPONENT'));
		}
	});

});
//----------------------------------------------------------------------
// DESTROYED:
Template.addressFormComponent.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Save data in case the user preselects some values and then changes language/view
	Session.set('address_FORM_COMPONENT', instance.$('#address').val());

});
