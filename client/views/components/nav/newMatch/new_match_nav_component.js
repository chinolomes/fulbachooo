//======================================================================
// NAVIGATION BAR NEW ACTIVITY TEMPLATE:
//======================================================================
// CONTEXT: this = {width}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.navBarNewActivity.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.active = new ReactiveVar(false);

});
//----------------------------------------------------------------------
// HELPERS:
Template.navBarNewActivity.helpers({

	'active': function() {
		return Template.instance().active.get() ? 'active' : '';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.navBarNewActivity.events({

	'click a.navbar-item-wrapper': function(event, instance) {
		event.preventDefault();
		instance.active.set(true);
		Router.go('/activities/new_activity');
	}

});
