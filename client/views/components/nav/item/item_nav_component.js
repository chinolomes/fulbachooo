//======================================================================
// NAVIGATION BAR ITEM TEMPLATE:
//======================================================================
// CONTEXT: this = {section, text, icon, active, width}
//----------------------------------------------------------------------
// EVENTS:
Template.navBarItem.events({

	'click a.navbar-item-wrapper': function(event) {
		event.preventDefault();

      // Get data context
      var section = Template.currentData().section;

		location.hash = section;
		Session.set('curSubSection', section);
	}

});
