//======================================================================
// HEADER ITEM TEMPLATE:
//======================================================================
// CONTEXT: this = {section, text, icon, active, href, type}
//----------------------------------------------------------------------
// HELPERS:
Template.headerItem.helpers({

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'tablet': function() {
		return this.type && this.type === 'tablet';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	}

});
