
//======================================================================
// EDIT ACTIVITY PARTICIPANTS:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.editActivityParticipants.onCreated(function() {

	// Current template instance
	var instance = this;

	// Declare reactive vars
	instance.match = new ReactiveVar(null);
	instance.loading = new ReactiveVar('wait'); // loading indicator

	// Internal var(s)
	var options = {
		fields: {
			fieldSize: 1,
			peopleIn: 1,
			friendsOf: 1,
			waitingList: 1,
			createdBy: 1,
			admin: 1,
			sport: 1,
			status: 1
		}
	};

	// Get data context
	var matchId = Template.currentData().matchId;

	// Set reactive vars
	instance.autorun(function() {
		var match = Matches.findOne({_id: matchId}, options);
		instance.match.set(match);
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.editActivityParticipants.helpers({ // reactive env

	'wlTeamAHidden': function() {
		var match = Template.instance().match.get();
		return match && getPeopleMissing(match, 'A') > 0 ? 'hidden' : '';
	},

	'wlTeamBHidden': function() {
		var match = Template.instance().match.get();
		return match && getPeopleMissing(match, 'B') > 0 ? 'hidden' : '';
	},

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.editActivityParticipants.events({

	'submit form': function(event, instance) {
		event.preventDefault();

	   ///////////////
	   // VARIABLES //
	   ///////////////

		var context = 'Edit Activity Participants';
		var matchId = this.matchId; // data context
		var match = instance.match.get(); // reactive vars
		var btn = instance.$('#saveEdit'); // button
		var newPeopleInTeamA = []; // ids array
		var newFriendsOfTeamA = []; // ids array
		var newWaitingListTeamA = []; // ids array
		var newPeopleInTeamB = []; // ids array
		var newFriendsOfTeamB = []; // ids array
		var newWaitingListTeamB = []; // ids array
		var prevPeopleInTeamA = []; // ids array
		var prevPeopleInTeamB = []; // ids array
		var prevFriendsOfTeamA = []; // ids array
		var prevFriendsOfTeamB = []; // ids array
		var prevWaitingListTeamA = []; // ids array
		var prevWaitingListTeamB = []; // ids array
		var addedPeopleInTeamA = []; // ids array
		var addedPeopleInTeamB = []; // ids array
		var addedFriendsOfTeamA = []; // ids array
		var addedFriendsOfTeamB = []; // ids array
		var addedWaitingListTeamA = []; // ids array
		var addedWaitingListTeamB = []; // ids array
		var removedPeopleInTeamA = []; // ids array
   	var removedPeopleInTeamB = []; // ids array
		var removedFriendsOfTeamA = []; // ids array
   	var removedFriendsOfTeamB = []; // ids array
   	var removedWaitingListTeamA = []; // ids array
   	var removedWaitingListTeamB = []; // ids array
		var changesTeamA = 0;
		var changesTeamB = 0;

   	////////////
   	// CHECKS //
   	////////////

      if (!curUserIsOwnerOrAdmin(match)) {
         throwError(context, 'user is not owner nor admin');
         return;
      } else if (_.isUndefined(match.status)) {
         throwError(context, 'status is not defined');
			processMessage({status: 'error', text: 'Unexpected_Error'});
			return;
   	} else if (match.status !== 'active') {
			processMessage({status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled', title: 'Operation_Unsuccessful'});
			// TODO: reset values stored in DB
			return;
   	}

	   ///////////////////
	   // SET VARIABLES //
	   ///////////////////

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		// Get selected values
		newPeopleInTeamA = instance.$("#people_in_team_A").select2("val") || []; // ids array
		newFriendsOfTeamA = instance.$("#friends_of_team_A").select2("val") || []; // ids array
		newWaitingListTeamA = instance.$("#waiting_list_team_A").select2("val") || []; // ids array
		newPeopleInTeamB = instance.$("#people_in_team_B").select2("val") || []; // ids array
		newFriendsOfTeamB = instance.$("#friends_of_team_B").select2("val") || []; // ids array
		newWaitingListTeamB = instance.$("#waiting_list_team_B").select2("val") || []; // ids array

		/*console.log('newPeopleInTeamA :', newPeopleInTeamA);
		console.log('newFriendsOfTeamA :', newFriendsOfTeamA);
		console.log('newWaitingListTeamA :', newWaitingListTeamA);
		console.log('newPeopleInTeamB :', newPeopleInTeamB);
		console.log('newFriendsOfTeamB :', newFriendsOfTeamB);
		console.log('newWaitingListTeamB :', newWaitingListTeamB);*/

		// Previous participants
		prevPeopleInTeamA = getUserIds(filterByTeam(match.peopleIn, 'A'));
		prevPeopleInTeamB = getUserIds(filterByTeam(match.peopleIn, 'B'));
		prevFriendsOfTeamA = getUserIds(filterByTeam(match.friendsOf, 'A'));
		prevFriendsOfTeamB = getUserIds(filterByTeam(match.friendsOf, 'B'));
		prevWaitingListTeamA = getUserIds(filterByTeam(match.waitingList, 'A'));
		prevWaitingListTeamB = getUserIds(filterByTeam(match.waitingList, 'B'));

		// Added participants
		addedPeopleInTeamA = _.difference(newPeopleInTeamA, prevPeopleInTeamA);
		addedPeopleInTeamB = _.difference(newPeopleInTeamB, prevPeopleInTeamB);
		addedFriendsOfTeamA = _.difference(newFriendsOfTeamA, prevFriendsOfTeamA);
		addedFriendsOfTeamB = _.difference(newFriendsOfTeamB, prevFriendsOfTeamB);
		addedWaitingListTeamA = _.difference(newWaitingListTeamA, prevWaitingListTeamA);
		addedWaitingListTeamB = _.difference(newWaitingListTeamB, prevWaitingListTeamB);

		// Removed participants
		removedPeopleInTeamA = _.difference(prevPeopleInTeamA, newPeopleInTeamA);
   	removedPeopleInTeamB = _.difference(prevPeopleInTeamB, newPeopleInTeamB);
		removedFriendsOfTeamA = _.difference(prevFriendsOfTeamA, newFriendsOfTeamA);
   	removedFriendsOfTeamB = _.difference(prevFriendsOfTeamB, newFriendsOfTeamB);
   	removedWaitingListTeamA = _.difference(prevWaitingListTeamA, newWaitingListTeamA);
   	removedWaitingListTeamB = _.difference(prevWaitingListTeamB, newWaitingListTeamB);

		// Calculate the number of changes made
		changesTeamA = addedPeopleInTeamA.length + addedFriendsOfTeamA.length + addedWaitingListTeamA.length + removedPeopleInTeamA.length + removedFriendsOfTeamA.length + removedWaitingListTeamA.length;
		changesTeamB = addedPeopleInTeamB.length + addedFriendsOfTeamB.length + addedWaitingListTeamB.length + removedPeopleInTeamB.length + removedFriendsOfTeamB.length + removedWaitingListTeamB.length;
		/*console.log('changesTeamA: ', changesTeamA);
		console.log('changesTeamB: ', changesTeamB);*/

	   ///////////
	   // LOGIC //
	   ///////////

		// Is there any changes with respect to the current values?
   	if (changesTeamA + changesTeamB === 0) {
			processMessage({status: 'warning', text: 'No_Changes_Were_Made'});
			btn.prop("disabled", false);
			instance.loading.set('wait');
   		return;
   	}

		Meteor.call('manageParticipants', matchId, newPeopleInTeamA, newFriendsOfTeamA, newPeopleInTeamB, newFriendsOfTeamB, newWaitingListTeamA, newWaitingListTeamB, function(err, msg) {
			if (err) {
				console.log(err);
				processMessage({status: 'error', text: 'Unexpected_Error'});
				btn.prop("disabled", false);
				instance.loading.set('error');
			} else {
				processMessage(msg);
				bootbox.hideAll();
				if (msg.status === 'error') {
					instance.loading.set('error');
				} else if (msg.status === 'success') {
					instance.loading.set('success');
				} else {
					instance.loading.set('wait');
				}
				btn.prop("disabled", false);
			}
		});
	},

	'click #cancelEdit': function(event, instance) {
		event.preventDefault();

      // Get button
      var btn = instance.$('#cancelEdit');

      // Disable button
		btn.prop("disabled", true);

		// Set session vars to trigger select2 to reset to DB value
		Session.set('people_in_team_A_RESET_SELECT2', true);
		Session.set('friends_of_team_A_RESET_SELECT2', true);
		Session.set('waiting_list_team_A_RESET_SELECT2', true);
		Session.set('people_in_team_B_RESET_SELECT2', true);
		Session.set('friends_of_team_B_RESET_SELECT2', true);
		Session.set('waiting_list_team_B_RESET_SELECT2', true);

		// Display message
		processMessage({status: 'warning', text: 'No_Changes_Were_Made'});
		btn.prop("disabled", false);
	}

});
