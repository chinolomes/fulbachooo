//======================================================================
// EDIT ACTIVITY TEMPLATE:
//======================================================================
// CONTEXT: this = {match}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.editActivity.onCreated(function() {

	// See router onBeforeAction: every time the user comes to edit_activity from a different route, reset session vars
	if(Session.equals('resetAll_FORM_COMPONENT', true)){
      Session.set('people_in_team_A_SELECT2', null);
      Session.set('people_in_team_B_SELECT2', null);
      Session.set('friends_of_team_A_SELECT2', null);
      Session.set('friends_of_team_B_SELECT2', null);
      Session.set('waiting_list_team_A_SELECT2', null);
      Session.set('waiting_list_team_B_SELECT2', null);

      // Match special actions vars
      Session.set('activity_admin_SELECT2', null);

      // Match info vars
      Session.set('address_FORM_COMPONENT', null);
      Session.set('time_FORM_COMPONENT', null);
      Session.set('duration_FORM_COMPONENT', null);
      Session.set('cost_FORM_COMPONENT', null);
      Session.set('title_FORM_COMPONENT', null);
      Session.set('description_FORM_COMPONENT', null);
      Session.set('fieldType_FORM_COMPONENT', null);
      Session.set('fieldPhone_FORM_COMPONENT', null);
      Session.set('fieldWebsite_FORM_COMPONENT', null);
      Session.set('repeat_FORM_COMPONENT', null);
      Session.set('coords_FORM_COMPONENT', null);
      Session.set('selectedDATEPICKER', null);

		Session.set('resetAll_FORM_COMPONENT', false);
	}

});
//----------------------------------------------------------------------
// HELPERS:
Template.editActivity.helpers({

   'viewerIsOwnerOrAdmin': function() {
      return curUserIsOwnerOrAdmin(this.match);
   }

});
