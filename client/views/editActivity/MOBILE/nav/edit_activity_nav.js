//======================================================================
// EDIT ACTIVITY NAVIGATION BAR TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIERED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.editActivityNav.onCreated(function() {

   // 1. Initialize reactive variables
	//Session.set('curSubSection', 'data');
	var instance = this;

	// Get -reactive- hash
	instance.autorun(function() {
		var hash = Iron.Location.get().hash;
		if (hash) {
			switch(hash) {
				case '#participants':
					Session.set('curSubSection', 'participants');
					break;
				case '#actions':
					Session.set('curSubSection', 'actions');
					break;
				case '#map':
					Session.set('curSubSection', 'map');
					break;
				default:
					Session.set('curSubSection', 'data');
					break;
			}
		} else {
			Session.set('curSubSection', 'data');
		}
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.editActivityNav.helpers({

	'data': function() {
		return Session.equals('curSubSection', 'data') ? 'active' : '';
	},

	'participants': function() {
		return Session.equals('curSubSection', 'participants') ? 'active' : '';
	},

	'actions': function() {
		return Session.equals('curSubSection', 'actions') ? 'active' : '';
	},

	'map': function() {
		return Session.equals('curSubSection', 'map') ? 'active' : '';
	}

});
