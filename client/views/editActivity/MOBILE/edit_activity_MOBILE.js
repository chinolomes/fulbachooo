//======================================================================
// EDIT ACTIVITY MOBILE TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: ''
// SEE: /client/views/editActivity/MOBILE/nav
//----------------------------------------------------------------------
// HELPERS:
Template.editActivityMOBILE.helpers({

	'data': function() {
		return Session.equals('curSubSection', 'data');
	},

	'participants': function() {
		return Session.equals('curSubSection', 'participants');
	},

	'actions': function() {
		return Session.equals('curSubSection', 'actions');
	},

	'map': function() {
		return Session.equals('curSubSection', 'map');
	}

});
