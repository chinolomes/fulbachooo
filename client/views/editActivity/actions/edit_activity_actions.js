//======================================================================
// EDIT ACTIVITY ACTIONS TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// RENDERED:
Template.editActivityActions.onCreated(function() {

	// Current template instance
	var instance = this;

	// Declare reactive vars
	instance.match = new ReactiveVar(null); // cursor
	instance.loading = new ReactiveVar('wait'); // loading indicator

	// Internal var(s)
	var options = {
		fields: {
	      createdBy: 1,
			admin: 1,
			repeat: 1,
			status: 1
		}
	};

	// Get data context
	var matchId = Template.currentData().matchId;

	// Set reactive vars
	instance.autorun(function() {
		var match = Matches.findOne(matchId, options);
		instance.match.set(match);
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.editActivityActions.helpers({

	'viewerIsOwner': function() {
		var match = Template.instance().match.get();
		return match.createdBy && match.createdBy === Meteor.userId();
	},

	'match': function() {
		return Template.instance().match.get();
	},

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.editActivityActions.events({

	'submit form': function(event, instance) {
		event.preventDefault();

		// Get data context
		var matchId = this.matchId;

		// Get reactive vars
		var match = instance.match.get();

      if (match.createdBy && match.createdBy !== Meteor.userId()) {
         throw new Meteor.Error('editActivityData submit form: user is not the owner');
         return;
      } else if (_.isUndefined(match.status)) {
         throw new Meteor.Error('Edit Match Actions: status is not defined');
         var msg = {status: 'error', text: 'Unexpected_Error'};
			processMessage(msg);
			return;
   	} else if (match.status !== 'active') {
         var msg = {status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled', title: 'Operation_Unsuccessful'}; // msg
			processMessage(msg);
			// TODO: reset DB values
			return;
   	}

      // Get button
      var btn = instance.$('#saveEdit');

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		// Get selected values
      var admin = instance.$("#activity_admin").select2("val") || [''];
		var repeat = instance.$('input[name=repeat]:checked').val() === "checked";

      // Check
      if (_.isArray(admin) && admin.length > 1) {
      	var msg = {status: 'error', text: 'Only_One_Admin'};
      	processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('error');
         return;
      } else if (_.isArray(admin) && admin.length === 1 && admin[0] === match.createdBy) {
      	var msg = {status: 'error', text: 'Admin_Cant_Be_Yourself'};
      	processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('error');
         return;
      }

      // Is there any changes with respect to current values?
		var curAdmin = !_.isUndefined(match.admin) && !_.isNull(match.admin[0]) ? match.admin : [''];
		//console.log('admin[0]: ', admin[0]);
		//console.log('curAdmin[0]: ', curAdmin[0]);
		var sameAdmin = admin[0] === curAdmin[0];
		//console.log('sameAdmin: ', sameAdmin);
		var sameRepeat = match.repeat === repeat;

		/*console.log("sameAdmin: ", sameAdmin);
		console.log("sameRepeat: ", sameRepeat);
		console.log("_.isUndefined(match.admin): ", _.isUndefined(match.admin));
		console.log("admin[0]: ", admin[0]);
		console.log("match.admin[0]: ", match.admin[0]);*/

      if (sameAdmin && sameRepeat) {
			var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
			processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('wait');
			return;
      }

		Meteor.call('editActivityActions', matchId, admin, repeat, function(err, msg) {
			if (err) {
				console.log(err);
				btn.prop("disabled", false);
				instance.loading.set('error');
			} else {
				processMessage(msg);
				bootbox.hideAll();
				btn.prop("disabled", false);
				instance.loading.set('success');
			}
		});
	}

});
