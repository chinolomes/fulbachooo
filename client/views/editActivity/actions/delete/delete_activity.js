//======================================================================
// DELETE ACTIVITY BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.deleteActivity.onCreated(function() {

	// Current template instance
	var instance = this;

	// Declare reactive vars
	instance.match = new ReactiveVar(null);

	// Internal var(s)
	var options = {
		fields: {
			createdBy: 1
		}
	};

	// Get data context
	var matchId = Template.currentData().matchId;

	// Set reactive vars
	instance.autorun(function() {
		var match = Matches.findOne(matchId, options);
		instance.match.set(match);
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.deleteActivity.helpers({

	'viewerIsOwner': function() {
		var match = Template.instance().match.get();
		return match.createdBy && match.createdBy === Meteor.userId();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.deleteActivity.events({

   'click button': function(event, instance) {
      event.preventDefault();

		// Get data context
		var matchId = this.matchId;

		// Get reactive vars
		var match = instance.match.get();

      if (match.createdBy && match.createdBy !== Meteor.userId()) {
         throw new Meteor.Error('deleteActivity click button: user is not the owner');
         return;
      }

		// TODO disable btn (?)

		bootbox.dialog({
			size: 'small',
			message: TAPi18n.__('Sure_You_Want_To_Delete_The_Activity'),
			buttons: {
			   cancel: {
			      label: "No!",
			      className: "btn-success",
			      callback: function() {
						// do nothing
			      }
			   },
			   confirm: {
			      label: TAPi18n.__('Yes_Delete_It'),
			      className: "btn-danger",
			      callback: function() {
			      	Meteor.call('deleteActivity', matchId, function(err, msg) {
							if (!err) processMessage(msg);
						});
			   	}
			 	}
			}
		});
   }

});
