//======================================================================
// PUBLISH ACTIVITY BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.publishActivity.onCreated(function() {

	// Current template instance
	var instance = this;

	// Declare reactive vars
	instance.match = new ReactiveVar(null);

	// Get data context
	var matchId = Template.currentData().matchId;

	// Internal var(s)
	var options = {
		fields: {
			status: 1,
			privacy: 1,
			createdBy: 1,
			admin: 1
		}
	};

	// Set reactive vars
	instance.autorun(function() {
		var match = Matches.findOne(matchId, options);
		instance.match.set(match);
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.publishActivity.helpers({

	'matchIsActive': function() {
		var match = Template.instance().match.get();
		return match.status === 'active';
	},

	'matchIsPrivate': function() {
		var match = Template.instance().match.get();
		return match.privacy === 'private';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.publishActivity.events({

   'click button': function(event, instance) {
      event.preventDefault();

		// Get data context
		var matchId = this.matchId;

		// Get reactive vars
		var match = instance.match.get();

      if (!curUserIsOwnerOrAdmin(match)) {
         throw new Meteor.Error('publishMatch click button: user is not the owner');
         return;
      }

		// TODO disable btn (?)

		bootbox.dialog({
			size: 'small',
			message: TAPi18n.__('Sure_You_Want_To_Publish_The_Activity'),
			buttons: {
			   cancel: {
			      label: "No!",
			      className: "btn-success",
			      callback: function() {
						// do nothing
			      }
			   },
			   confirm: {
			      label: TAPi18n.__('Yes_Make_It_Public'),
			      className: "btn-danger",
			      callback: function() {
			      	Meteor.call('changeActivityPrivacy', matchId, function(err, msg) {
							if (!err) processMessage(msg);
						});
			   	}
			 	}
			}
		});
   }

});
