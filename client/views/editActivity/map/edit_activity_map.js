//======================================================================
// EDIT ACTIVITY MAP:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
// COMMENTS: The document where the map is rendered, has to have two elements with
// 'lat' and 'lng' ids respectively.
//----------------------------------------------------------------------
// ON CREATION:
Template.editActivityMap.onCreated(function() {

   // 0. Current template instance
   var instance = this;

	// Declare reactive vars
	instance.match = new ReactiveVar(null); // cursor
   instance.mapIsSet = new ReactiveVar(false);

   // Set session variables
	Session.set('resetZoomSliderCIRCLE', false);
	Session.set('mapZoomCIRCLE', '');

   // Internal var(s)
   var options = {
      fields: {
         'loc.coordinates': 1,
         sport: 1,
         createdBy: 1,
         admin: 1,
         status: 1
      }
   };

	// Get data context
	var matchId = Template.currentData().matchId;

	// Set reactive vars
	instance.autorun(function() {
		var match = Matches.findOne(matchId, options);
		instance.match.set(match);
	});

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.editActivityMap.onRendered(function() {

	// WE NEED TO BE SURE THAT LOGGED-OUT VARS ARE PROPERLY SET!!
   // Current template instance
   var instance = Template.instance();

	// Get data context
   var matchId = Template.currentData().matchId;

   // Get reactive vars
	var match = instance.match.get();

	// Initialize google map
   var elemId = 'googleMap'; // DOM element
	var coords = match.loc.coordinates;
   var mapCenterLat = !Session.equals('coords_FORM_COMPONENT', null) ? Session.get('coords_FORM_COMPONENT', null)[0] : coords[1];
   var mapCenterLng = !Session.equals('coords_FORM_COMPONENT', null) ? Session.get('coords_FORM_COMPONENT', null)[1] : coords[0];
   var zoom = 17;
   var map = setMap(elemId, mapCenterLat, mapCenterLng, zoom);
   map = setGPSButton(map);
   map = setSaveButton(map);
   instance.mapIsSet.set(true);

	// Set default marker
   var sport = match.sport;
	var draggable = true;
	var marker = createMarker(mapCenterLat, mapCenterLng, map, sport, draggable);

	// Fill form lat and lng values	with the default marker position
	setFormLatLng(marker.getPosition().lat(), marker.getPosition().lng());

	// Add marker drag and drop event listeners
	google.maps.event.addListener(marker, 'drag', function(event) {
		setFormLatLng(event.latLng.lat(), event.latLng.lng());
	});
	google.maps.event.addListener(marker, 'dragend', function(event) {
		setFormLatLng(event.latLng.lat(), event.latLng.lng());
	});

	// Add map-event listener to place the new marker
	google.maps.event.addListener(map, 'click', function(event) {
		marker.setPosition(event.latLng);
		setFormLatLng(event.latLng.lat(), event.latLng.lng());
	});

	// In case map center session variables changes because of GPS button is hit,
	// modify map center and marker position accordingly
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMapGPS', true)) {
			var newCenter = new google.maps.LatLng(Session.get('mapCenterLatGPS'), Session.get('mapCenterLngGPS'));
			map.setCenter(newCenter);
			marker.setPosition(newCenter);
			setFormLatLng(newCenter.lat(), newCenter.lng());
			Session.set('resetMapGPS', false);
		}
	});

	// In case SAVE button was hit, save new position and modify map center and marker position accordingly
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMapSAVE', true)) {

         // Check
         if (!curUserIsOwnerOrAdmin(instance.match.get())) {
            throw new Meteor.Error('edit Activity map: user is not the owner');
            Session.set('resetMapSAVE', false);
            return;
         } else if (_.isUndefined(instance.match.get().status)) {
            throw new Meteor.Error('edit Activity map: status is not defined');
            var msg = {status: 'error', text: 'Unexpected_Error'};
   			processMessage(msg);
            Session.set('resetMapSAVE', false);
   			return;
      	} else if (instance.match.get().status !== 'active') {
            var msg = {status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled', title: 'Operation_Unsuccessful'}; // msg
   			processMessage(msg);
   			// TODO: reset DB values
            Session.set('resetMapSAVE', false);
   			return;
      	}

         var newLat = parseFloat(instance.$('#lat').val(), 10);
         var newLng = parseFloat(instance.$('#lng').val(), 10);

         // Look for changes
         var coords = instance.match.get().loc.coordinates; // reactive source
         var prevLat = coords[1];
         var prevLng = coords[0];
         var sameLat = newLat === prevLat;
         var sameLng = newLng === prevLng;

         if (sameLat && sameLng) {
         	var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
            processMessage(msg);
         } else {
   			Meteor.call('editActivityMap', matchId, newLat, newLng, function(err, msg) {
   				if (!err) processMessage(msg);
   			});
         }

         var newCenter = new google.maps.LatLng(newLat, newLng);
         map.setCenter(newCenter);
         marker.setPosition(newCenter);
         setFormLatLng(newCenter.lat(), newCenter.lng());
      	Session.set('resetMapSAVE', false);
		}
	});

	// In case the map coords are modified by other admin,
   // reset the marker location and map center
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true) {
         var coords = instance.match.get().loc.coordinates; // reactive source
         var newLat = coords[1];
         var newLng = coords[0];
			var newCenter = new google.maps.LatLng(newLat, newLng);
			map.setCenter(newCenter);
			marker.setPosition(newCenter);
		}
	});

});
