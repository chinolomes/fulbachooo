//======================================================================
// EDIT ACTIVITY DATA TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: 'match'
// SEE: Router.onBeforeAction
//----------------------------------------------------------------------
// RENDERED:
Template.editActivityData.onCreated(function() {

	// Current template instance
	var instance = this;

	// Declare reactive vars
	instance.match = new ReactiveVar(null); // cursor
	instance.loading = new ReactiveVar('wait'); // loading indicator

	// Initialize session vars
	Session.set('resetDATEPICKER', false); // triggers autorun function at /client/views/formComponents/date/date_picker.js
	Session.set('newDateDATEPICKER', null);

	// Internal var(s)
	var options = {
		fields: {
			address: 1,
			date: 1,
	 		time: 1,
			duration: 1,
	      cost: 1,
	      title: 1,
	      fieldType:1,
	      fieldPhone: 1,
	      fieldWebsite: 1,
	      createdBy: 1,
			description: 1,
			admin: 1,
			status: 1
		}
	};

	// Get data context
	var matchId = Template.currentData().matchId;

	// Set reactive vars
	instance.autorun(function() {
		var match = Matches.findOne(matchId, options); // requires 'match' subs
		instance.match.set(match);
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.editActivityData.helpers({

	'match': function() {
		return Template.instance().match.get();
	},

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.editActivityData.events({

	'submit form': function(event, instance) {
		event.preventDefault();

		// Get data context
		var matchId = this.matchId;

		// Get reactive vars
		var match = instance.match.get();

		////////////
		// CHECKS //
		////////////

      if (!curUserIsOwnerOrAdmin(match)) {
         throw new Meteor.Error('editActivityData click button: user is not the owner');
         return;
      } else if (_.isUndefined(match.status)) {
         throw new Meteor.Error('Edit Match Data: status is not defined');
         var msg = {status: 'error', text: 'Unexpected_Error'};
			processMessage(msg);
			return;
   	} else if (match.status !== 'active') {
         var msg = {status: 'error', text: 'The_Activity_Is_Finished_Slash_Canceled', title: 'Operation_Unsuccessful'}; // msg
			processMessage(msg);
			// TODO: reset DB values
			return;
   	}

      // Get button
      var btn = instance.$('#saveEdit');

		///////////////////
      // SET VARIABLES //
		///////////////////

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		// Get selected values
		var date = instance.$('.datepicker').datepicker('getUTCDate');
		var time = instance.$('#time option:selected').val();
		var duration = instance.$('#duration').val().trim();
		var cost = instance.$('#cost').val().trim();
		var address = instance.$('#address').val().trim();
		var fieldType = instance.$('#field_type').val().trim();
		var fieldPhone = instance.$('#field_phone').val().trim();
		var fieldWebsite = instance.$('#field_website').val().trim();
		var title = instance.$('#title').val().trim();
		var description = instance.$('#description').val().trim();
		//var repeat = instance.$('input[name=repeat]:checked').val() === "checked";

		// Mandatory fields
		var sameDate = match.date.toString() === date.toString();
		var sameTime = match.time === time;
		var sameCost = match.cost === cost;
		var sameAddress = match.address === address;
		var sameTitle = match.title === title;
		// Elective fields
		var sameDuration = (_.isUndefined(match.duration) && duration === '') || match.duration === duration;
		var sameFieldType = (_.isUndefined(match.fieldType) && fieldType === '') || match.fieldType === fieldType;
		var sameFieldPhone = (_.isUndefined(match.fieldPhone) && fieldPhone === '') || match.fieldPhone === fieldPhone;
		var sameFieldWebsite = (_.isUndefined(match.fieldWebsite) && fieldWebsite === '') || match.fieldWebsite === fieldWebsite;
		var sameDescription = (_.isUndefined(match.description) && description === '') || match.description === description;

		//////////////
      // CHANGES? //
		//////////////

		//console.log('sameDate: ' + sameDate + ', sameTime: ' + sameTime + ', sameDuration:' + sameDuration + ', sameCost: ' + sameCost + ', sameAddress: ' + sameAddress + ', sameTitle: ' + sameTitle + ', sameFieldType: ' + sameFieldType + ', sameFieldPhone: ' + sameFieldPhone + ', sameFieldWebsite: ' + sameFieldWebsite + ', sameDescription: ' + sameDescription);
      if (sameDate && sameTime && sameDuration && sameCost && sameAddress && sameTitle && sameFieldType && sameFieldPhone && sameFieldWebsite && sameDescription) {
			var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
			processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('wait');
			return;
      }

		////////////
		// CHECKS //
		////////////

		// Mandatory fields values
		if (date === null || date === '' || time === null || time === '' || cost === null || cost === '' || address === null || address === '' || title === null || title === '') {
			//console.log('sport: ' + match.sport + ', title: ' + match.title + ', lat: ' + match.lat + ', lng: ' + match.lng + ', label: ' + match.label + ', address: ' + match.address + ', date: ' + match.date + ', time: ' + match.time + ', cost: ' + match.cost + ', fieldSize: ' + match.fieldSize);
			var msg = {status: 'error', text: 'Fill_All_Mandatory_Fields'};
			processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('error');
			return;

		} else if (!sameTitle && title.length > 200) {
			var msg = {status: 'error', text: 'The_Title_Exceeds_200_Chars'};
			processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('error');
			return;

 		} else if (!sameFieldWebsite && fieldWebsite.length > 0 && fieldWebsite.substr(0,7) !== 'http://' && fieldWebsite.substr(0,8) !== 'https://') {
			var msg = {status: 'error', text: 'Incorrect_Website_Explanation'};
			processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('error');
			return;

		} else if (!sameDescription && description.length > 5000) {
			var msg = {status: 'error', text: 'The_Description_Exceeds_5000_Chars'};
			processMessage(msg);
			btn.prop("disabled", false);
			instance.loading.set('error');
			return;
 		}

		/////////////////
		// METHOD CALL //
		/////////////////

		Meteor.call('editActivityData', matchId, date, time, duration, cost, address, fieldType, fieldPhone, fieldWebsite, title, description, function(err, msg) {
			if (err) {
				console.log(err);
				btn.prop("disabled", false);
				instance.loading.set('error');
			} else {
				processMessage(msg);
				bootbox.hideAll();
				btn.prop("disabled", false);
				instance.loading.set('success');
			}
		})
	},

	'click #cancelEdit': function(event, instance) {
		event.preventDefault();

		// Get reactive vars
		var match = instance.match.get();

      if (!curUserIsOwnerOrAdmin(match)) {
         throw new Meteor.Error('editActivityData click #cancelEdit button: user is not the owner/admin');
         return;
      }

      // Variables declaration
      var btn = instance.$('#cancelEdit');

      // Disable button
		btn.prop("disabled", true);

		// Reset original values
		instance.$('#time').val(match.time);
		instance.$('#cost').val(match.cost);
		instance.$('#address').val(match.address);
		instance.$('#field_size').val(match.fieldSize);
		instance.$('#field_type').val(match.fieldType);
		instance.$('#field_phone').val(match.fieldPhone);
		instance.$('#field_website').val(match.fieldWebsite);
		instance.$('#title').val(match.title);
		instance.$('#duration').val(match.duration);
		instance.$('#description').val(match.description);

		// Reset datepicker
		Session.set('resetDATEPICKER', true); // triggers autorun function at /client/views/formComponents/date/date_picker.js
		Session.set('newDateDATEPICKER', match.date);

		// Display message
		var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
		processMessage(msg);
		btn.prop("disabled", false);
	}

});
