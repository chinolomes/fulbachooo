//======================================================================
// NOTIFICATIONS TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.notifications.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.notifications = new ReactiveVar([]);

   // 2. Set session vars
   Session.set('curNotif', null); // required by notifItem

   // 3. Internal vars
   var notifOptions = {
      sort: {
         createdAt: -1
      },
      limit: 30
   };
   var senderOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };

   // 4. Subscribe to current user notifications
   instance.autorun(function() {
      if (Meteor.userId()) {
         //console.log('SUBS CUR USER NOTIF!')
         var sub = instance.subscribe('curUserNotif');

         // 5. Set reactive variable(s)
         if (sub.ready()) {
            var notifications = Notifications.find({recipient: Meteor.userId()}, notifOptions).fetch();

            // Extend notification object by including sender avatar and name
            _.each(notifications, function(notification) {
               var sender = Meteor.users.findOne(notification.createdBy, senderOptions);
               _.extend(notification, {sender: sender});
            });
            instance.notifications.set(notifications);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.notifications.helpers({

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   },

	'notifications': function() {
		return Template.instance().notifications.get();
	}

});
