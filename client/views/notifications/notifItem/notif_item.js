//======================================================================
// NOTIFICATION ITEM:
//======================================================================
// CONTEXT: this = {notification}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.notifItem.helpers({

	'active': function() {
      return Session.equals('curNotif', this.notification._id) ? 'active' : '';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.notifItem.events({

	'mouseover .notifItem': function() {
		Session.set('curNotif', this.notification._id);
	},

	'mouseout .notifItem': function() {
		Session.set('curNotif', null);
	},

	'click .notifItem': function() {
		Router.go(this.notification.anchor);
	}

});
