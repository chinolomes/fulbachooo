//======================================================================
// USERS TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON RENDERED:
Template.users.onRendered(function() {

	// Display cookies message the first time the user visits the site
	if (_.isUndefined(Cookie.get('cookiesFULBACHO')) || Cookie.get('cookiesFULBACHO') !== 'set') {
		var title = TAPi18n.__('Cookies_On_Fulbacho');
		var content = TAPi18n.__('Cookies_Explanation');
		toastr.options = options2;
		toastr['success'](content, title);
		toastr.options = options1;
	}
	Cookie.set('cookiesFULBACHO', 'set', {months: 10});
	//Cookie.clear('cookiesFULBACHO');
});
