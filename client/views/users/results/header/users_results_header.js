//======================================================================
// USERS RESULTS HEADER TEMPLATE:
//======================================================================
// CONTEXT: this = {mapBounds}
// REQUIRED SUBS: 'totalUsersCounter' + template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.usersResultsHeader.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.totalPlayers = new ReactiveVar(0);
   instance.mapBounds = new ReactiveVar(null); // {bottomLeft: [lng, lat], topRight: [lng, lat]}

   // 2. Internal var(s)
	var options = {
		fields: {
			"geo": 1
		}
	};

   // 2. Get -reactive- data context and set reactive var(s)
   instance.autorun(function() {
      //var circle = instance.circle.get(); // reactive source
		var mapBounds = Template.currentData().mapBounds; // reactive source
		if (!_.isUndefined(mapBounds.bottomLeft) && !_.isUndefined(mapBounds.topRight)) {
			instance.mapBounds.set(mapBounds);
		}
	});

   // 3. Subscribe to users coordinates within map bounds
   instance.autorun(function() {
      var mapBounds = instance.mapBounds.get();
      if (mapBounds !== null) {
         var sub = instance.subscribe('playersCoords', mapBounds); // reactive source

         if (sub.ready()) {
            var players = Meteor.users.find({}, options).fetch();
            if (players) {
            	var count = countPlayersInBounds(players, mapBounds);
            	instance.totalPlayers.set(count);
            }
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.usersResultsHeader.helpers({

	'numberOfPlayers': function() {
		return Template.instance().totalPlayers.get();
	},

	'plural': function() {
		return Template.instance().totalPlayers.get() !== 1;
	},

	'totalNumberOfPlayers': function() {
		var totalUsers = CollectionsCounter.findOne({collectionName: 'users'}); // requires 'totalUsersCounter' sub
		return totalUsers && totalUsers.counter ? totalUsers.counter : '?';
	}

});
