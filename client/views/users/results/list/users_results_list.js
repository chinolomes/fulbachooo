//======================================================================
// USERS RESULTS LIST TEMPLATE:
//======================================================================
// CONTEXT: this = {mapBounds}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
/*
TODO:
SOURCE: http://guide.meteor.com/ui-ux.html
UX patterns for displaying new data

An interesting UX challenge in a realtime system like Meteor involves how to
bring new information (like changing data in a list) to the user’s attention. As
Dominic points out, it’s not always a good idea to simply update the contents of
a list as quickly as possible, as it’s easy to miss changes or get confused
about what’s happened.

One solution to this problem is to animate list changes (which we’ll look at in
the animation section), but this isn’t always the best approach. For instance,
if a user is reading a list of comments, they may not want to see any changes
until they are done with the current comment thread.

An option in this case is to call out that there are changes to the data the
user is looking at without actually making UI updates. In a system like Meteor
which is reactive by default, it isn’t necessarily easy to stop such changes
from happening!

However, it is possible to do this thanks to our split between smart and
reusable components. The reusable component simply renders what it’s given, so
we use our smart component to control that information. We can use a local
collection to store the rendered data, and then push data into it when the user
requests an update:
*/
// TODO: to avoid blinking results (show results --> loading... --> show results),
// try subscribing only when the map is set
//----------------------------------------------------------------------
// ON CREATION:
Template.usersResultsList.onCreated(function() {

   // 0. Current template instance
   var instance = this;

	// Initialize session variables
	Session.set('curPlayer', null); // required to display and highlight the list of players

   // 1. Initialize reactive variables
	instance.playersArray = new ReactiveVar([]);
   instance.mapBounds = new ReactiveVar(null); // {bottomLeft: [lng, lat], topRight: [lng, lat]}
   instance.loaded = new ReactiveVar(0);
   instance.limit = new ReactiveVar(LIMIT); // see /client/lib/constants.js
	instance.containerWidth = new ReactiveVar(1000);

   // Internal var(s)
   var query = {
      createdAt: {
         $exists: true
      }
   };
	var options = {
      fields: {
         "services.twitter.profile_image_url_https": 1,
         "services.facebook.id": 1,
         "services.google.picture": 1,
         "createdAt": 1,
         "profile": 1,
         "geo": 1
	   },
      sort: {
         createdAt: -1
      }
   };

   // 2. Get -reactive- data context and set reactive var(s)
   instance.autorun(function() {
      //var circle = instance.circle.get(); // reactive source
		var mapBounds = Template.currentData().mapBounds; // reactive source
		if (!_.isUndefined(mapBounds.bottomLeft) && !_.isUndefined(mapBounds.topRight)) {
			instance.mapBounds.set(mapBounds);
			instance.limit.set(LIMIT); // see /client/lib/constants.js
			instance.loaded.set(0);
		}
	});

   // 3. Subscribe to players (profile and services) in map bounds
   instance.autorun(function() {
		var mapBounds = instance.mapBounds.get(); // reactive source
      var limit = instance.limit.get(); // reactive source

		if (!_.isNull(mapBounds) && !_.isUndefined(mapBounds.bottomLeft) && !_.isUndefined(mapBounds.topRight)) {
         var sub = instance.subscribe('playersInBounds', mapBounds, limit); // returns players profile and createdAt
         // if subscription is ready, set limit to newLimit
         if (sub.ready()) {
            instance.loaded.set(limit);
         }
      }
   });

   // 3. Reactive function
   instance.players = function() {
      var mapBounds = instance.mapBounds.get();
		var playersArray = [];

      if (!_.isNull(mapBounds)) {
			// Query only the records where the createdAt field is set
			var players = Meteor.users.find(query, options).fetch();
			if (players) {
				// Filter out players by location and limit
            // We use this because mini-mongo does not support $geoWithin query
				var result = calculatePlayersInBounds(players, mapBounds, instance.loaded.get());

            // Extend player object by adding 'border' property
            if (result.selected.length > 0) {
               var lastIndex = result.selected.length - 1; // last feasible index
               for (var i = 0; i < lastIndex; i++) {
                  playersArray.push({player: result.selected[i], border: true});
               }
               playersArray.push({player: result.selected[lastIndex], border: false});
            }
			}
			instance.playersArray.set(playersArray);
			return playersArray;
      }
   }

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.usersResultsList.onRendered(function() {

	// Current template instance
   var instance = this;

	// Calculate container width
   instance.containerWidth.set($(window).width());
   $(window).resize(function(e) {
      var width = instance.$('.resultsContainer').width();
      instance.containerWidth.set(width);
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.usersResultsList.helpers({

	'playersArray': function() {
		return Template.instance().players();
	},

   // are there more players to show?
   'hasMorePlayers': function () {
      return Template.instance().playersArray.get().length >= Template.instance().limit.get() /*&& Template.instance().playersArray.get().length < Template.instance().totalPlayers.get()*/;
   },

	'cardType': function() {
		return Template.instance().containerWidth.get() > 420 ? 'desktop' : 'mobile';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.usersResultsList.events({

   'click .load-more': function (event, instance) {
      event.preventDefault();

      // get current value for limit, i.e. how many players are currently displayed
      var limit = instance.limit.get();

      // increase limit by 5 and update it
      limit += LOAD_MORE; // see /client/lib/constants
      instance.limit.set(limit);
   }

});
