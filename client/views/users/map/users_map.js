//======================================================================
// USERS MAP TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.usersMap.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Set session variables
   Session.set('showCircleGOOGLE_MAPS', false);
	/*Session.set('resetZoomSliderCIRCLE', false);
	Session.set('mapZoomCIRCLE', '');*/

   // 2. Initialize reactive variables
   instance.mapIsSet = new ReactiveVar(false);
   //instance.circleIsSet = new ReactiveVar(false);
   instance.bounds = new ReactiveVar(null); // {bottomLeft: [coords], topRight: [coords]}
   instance.resetMarkers = new ReactiveVar(false);

   // 3. Subscribe to users coordinates
   instance.autorun(function() {
      if (instance.bounds.get() !== null) {
         var sub = instance.subscribe('playersCoords', instance.bounds.get()); // reactive source
         instance.resetMarkers.set(false);

         if (sub.ready()) {
            instance.resetMarkers.set(true);
         }
      }
   });

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.usersMap.onRendered(function() {

	// Current template instance
   var instance = this;

	// Initialize google map
   var elemId = 'googleMapPlayers'; // DOM element
   var mapCenterLat = Session.get('circleCenterLatMATCHES');
   var mapCenterLng = Session.get('circleCenterLngMATCHES');
   var zoom = Session.get('mapZoomMATCHES');
   var map = setMap(elemId, mapCenterLat, mapCenterLng, zoom);
   map = setGPSButton(map);
   if (!ResponsiveHelpers.isXs()) { map = setSaveRegionButton(map); }
   instance.mapIsSet.set(true);

	// Attach listener to the map idle event to make it execute the markers subscription
   // SOURCE: https://developers.google.com/maps/articles/toomanymarkers
   var step1 = _.debounce(function(bounds) {
       var bottomLeft = [bounds.getSouthWest().lng(), bounds.getSouthWest().lat()];
       var topRight = [bounds.getNorthEast().lng(), bounds.getNorthEast().lat()];
       var newBounds = {
          bottomLeft: bottomLeft,
          topRight: topRight
       };
       instance.bounds.set(newBounds);
       saveBoundsInSession(newBounds);
       var zoom = map.getZoom();
       Session.set('mapZoomMATCHES', zoom);
       // Save circle session variables so that every time the user pans and then change section, the map location is preserved
       var newCenter = getMapCenterBounds(bottomLeft, topRight)
       Session.set('circleCenterLatMATCHES', newCenter.lat);
       Session.set('circleCenterLngMATCHES', newCenter.lng);
   }, 300);
   google.maps.event.addDomListener(map, 'idle', function () {
     var bounds = map.getBounds();
     step1(bounds);
   });

	// Set initial player-markers and keep track of future changes
   // SOURCE: https://googlemaps.github.io/js-marker-clusterer/docs/reference.html
	var markers = [];
   var options = {
      fields: {
         'geo.loc.coordinates': 1
      }
   };
   //MarkerClusterer.IMAGE_PATH = "https://google-maps-utility-library-v3.googlecode.com/svn/trunk/markerclustererplus/images/m"; // required to avoid mixed content warning
    var clusterStyles = [
       {
         textColor: 'black',
         url: '/images/m1.png',
         height: 52,
         width: 53
       },
      {
         textColor: 'black',
         url: '/images/m2.png',
         height: 55,
         width: 56
       },
      {
         textColor: 'black',
         url: '/images/m3.png',
         height: 65,
         width: 66
       }
     ];
    var mcOptions = {styles: clusterStyles};
   var markerClusterer = new MarkerClusterer(map, markers, mcOptions);
	instance.autorun(function() {
      if (instance.mapIsSet.get() === true && instance.resetMarkers.get() === true) {
         var players = Meteor.users.find({}, options).fetch(); // reactive source, get all users within window bounds
         markerClusterer.clearMarkers(); // TODO: optimize this operation. Only add/remove new and modified markers
         markers = generatePlayersMarkers(players);
         markerClusterer.addMarkers(markers);
		}
	});
   // TODO: see http://meteorcapture.com/how-to-create-a-reactive-google-map/
   // to optimize adding, removing and changing markers

   // Display info window in case the user is logged in and window bounds contains the user
   var contentString = TAPi18n.__('You_Are_Here');
   var infowindow = new google.maps.InfoWindow({
      content: contentString,
      position: new google.maps.LatLng(0.0, 0.0)
   });
   instance.autorun(function() {
      if (Meteor.userId() && instance.bounds.get() !== null) {
         var user = Meteor.users.findOne(Meteor.userId(), {fields: {"geo.loc.coordinates": 1}}); // reactive source
         if (user && user.geo && user.geo.loc && user.geo.loc.coordinates) {
            var userLat = parseFloat(user.geo.loc.coordinates[1], 10);
            var userLng = parseFloat(user.geo.loc.coordinates[0], 10);
            var winBounds = instance.bounds.get();
            var bottomLeft = winBounds.bottomLeft;
            var topRight = winBounds.topRight;
            if (inBounds(bottomLeft, topRight, userLat, userLng)) {
               infowindow.setPosition(new google.maps.LatLng(userLat, userLng));
               infowindow.open(map);
            }
         }
      }
   });

	// In case map center session variables changes (GPS button), modify map center and circle
	instance.autorun(function() {
      if (instance.mapIsSet.get() === true /*&& instance.circleIsSet.get() === true*/ && Session.equals('resetMapGPS', true)) {
         var newCenter = new google.maps.LatLng(parseFloat(Session.get('mapCenterLatGPS'), 10), parseFloat(Session.get('mapCenterLngGPS'), 10));
			map.setCenter(newCenter);
         var newZoom = 12;
         map.setZoom(newZoom);
         Session.set('mapZoomMATCHES', newZoom);
			Session.set('resetMapGPS', false);
		}
	});

	// In case the map was loaded before user.geo is ready, reset map and circle user.geo values
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true /*&& instance.circleIsSet.get() === true*/ && Session.equals('resetMapMATCHES', true)) {
			var newCenter = new google.maps.LatLng(Session.get('circleCenterLatMATCHES'), Session.get('circleCenterLngMATCHES'));
			map.setCenter(newCenter);
			map.setZoom(Session.get('mapZoomMATCHES'));
 			Session.set('resetMapMATCHES', false);
		}
	});

	// In case the save button is hit, display region
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('showCircleGOOGLE_MAPS', true)) {

			var newCenter = new google.maps.LatLng(Session.get('circleCenterLatMATCHES'), Session.get('circleCenterLngMATCHES'));
			map.setCenter(newCenter);
			map.setZoom(Session.get('mapZoomMATCHES'));

      	// Set initial circle
         var draggable = false;
         var circleRadius = parseInt(Session.get('circleRadiusMATCHES'), 10);
         var circle = createCircle(map, circleRadius, draggable);
         circle.setCenter(newCenter);
         // Remove circle after 5 secs.
         setTimeout(function(){ circle.setMap(null); }, 5000);
	      Session.set('showCircleGOOGLE_MAPS', false);
		}
	});

});
