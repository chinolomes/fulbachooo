//======================================================================
// EDIT PROFILE FORM:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.editProfileForm.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.profile = new ReactiveVar(null);
   instance.loading = new ReactiveVar('wait'); // loading indicator

   // 2. Internal vars
   var options = {
      fields: {
         'profile.name': 1,
         'profile.gender': 1,
         'profile.sports': 1,
         'profile.dominantLeg': 1,
         'profile.dominantHand': 1,
         'profile.selfDescription': 1
      }
   };

   // 3. Subscribe to current user profile
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub = instance.subscribe('curUserProfile');

         // 4. Set reactive variable
         if (sub.ready()) {
            var curUser = Meteor.users.findOne(Meteor.userId(), options);
            var profile = curUser && curUser.profile ? curUser.profile : null;
            instance.profile.set(profile);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.editProfileForm.helpers({

	'profile': function() {
		return Template.instance().profile.get();
	},

   'selectedSports': function() {
   	return Template.instance().profile.get().sports;
   },

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.editProfileForm.events({

	'submit form': function(event, instance) {
		event.preventDefault();

      // Get reactive var
      var profile = instance.profile.get();

      // Check
      if (!Meteor.userId()) {
         throw new Meteor.Error('editProfileForm submit form: user is not logged in');
         return;
      }

      // Get button
      var btn = instance.$('button');

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		// Get selected values
		var name = instance.$('#username').val().trim();
		var gender = instance.$('input[name=gender]:checked').val();
		var leg = instance.$('input[name=dominant_leg]:checked').val();
		var hand = instance.$('input[name=dominant_hand]:checked').val();
		var sports = instance.$('input[name=sports]:checked').map(function() {return this.value;}).get();
		var text = instance.$('#self_description').val().trim();

      // Is there any changes with respect to current values?
		var sameName = profile.name === name;
		var sameGender = profile.gender === gender;
		var sameHand = profile.dominantHand === hand;
		var sameLeg = profile.dominantLeg === leg;
		var sameSports = profile.sports.toString() === sports.toString();
		var sameSelfDescription = (_.isUndefined(profile.selfDescription) && text === '') || profile.selfDescription === text;

      if (sameName && sameGender && sameHand && sameLeg && sameSports && sameSelfDescription) {
			var msg = {status: 'warning', text: 'No_Changes_Were_Made'};
			processMessage(msg);
		   btn.prop("disabled", false);
         instance.loading.set('wait');
			return;
      }

		// Check name
		if (!name || name.length === 0) {
			var msg = {status: 'error', text: 'Enter_Your_Name'};
			processMessage(msg);
		   btn.prop("disabled", false);
         instance.loading.set('error');
			return;
		}

      if (_.isUndefined(gender) || gender === null) {
         gender = '';
      }
      if (_.isUndefined(leg) || leg === null) {
         leg = '';
      }
      if (_.isUndefined(hand) || hand === null) {
         hand = '';
      }

		Meteor.call('saveProfile', sports, name, gender, leg, hand, text, function(err, msg) {
			if (err) {
				console.log(err);
				btn.prop("disabled", false);
            instance.loading.set('error');
			} else {
				processMessage(msg);
				bootbox.hideAll();
		      btn.prop("disabled", false);
            instance.loading.set('success');
			}
		});
	}

});

/*
//======================================================================
// EDIT PROFILE FORM SPORT ITEM:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.editProfileFormSportItem.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.sports = new ReactiveVar(null);

   // 3. Subscribe to current user profile
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub = instance.subscribe('curUserProfile');

         // 4. Set reactive variable
         if (sub.ready()) {
            var curUser = Meteor.users.findOne({_id: Meteor.userId()}, {fields: {'profile.sports': 1}});
            var profile = curUser && curUser.profile ? curUser.profile : null;
            instance.sports.set(profile.sports);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.editProfileFormSportItem.helpers({

	'sports': function() {
		return Template.instance().sports.get();
	}

});*/
