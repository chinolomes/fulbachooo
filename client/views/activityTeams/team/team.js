//======================================================================
// TEAM TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId, team}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.team.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.players = new ReactiveVar([]); // [{userId, addedAt, team, [name]}, {...}, ...]
   instance.dummies = new ReactiveVar([]); // [{matchId, team}, {...}, ...]

	// 2. Get data context
   var matchId = Template.currentData().matchId;
	var team = Template.currentData().team;

   // 3. Internal vars
   var matchOptions = {
      fields: {
         peopleIn: 1,
         friendsOf: 1,
         fieldSize: 1
      }
   };
   var userOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };

   // 4. Set reactive vars
   instance.autorun(function() {
      var match = Matches.findOne(matchId, matchOptions); // reactive source
      var peopleIn = filterByTeam(match.peopleIn, team); // peopleIn[i] = {userId, addedAt, team}
      var friendsOf = filterByTeam(match.friendsOf, team); // friendsOf[i] = {userId, addedAt, team, name}
      var players = _.union(peopleIn, friendsOf);

		// Extend player object by including user avatar, matchId and type
		_.each(players, function(player) {
         var type = player.name ? 'friend' : 'user'; // friendOf : registered user
         var user = Meteor.users.findOne(player.userId, userOptions);
         _.extend(player, {matchId: matchId, user: user, type: type});
		});
      instance.players.set(_.sortBy(players, 'addedAt'));

      // Set dummy players
      var nDummies = match.fieldSize - players.length;
	   var dummies = [];
		for (var i = 0; i < nDummies; i++) {
			dummies.push({matchId: matchId, team: team});
		}
      instance.dummies.set(dummies);
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.team.helpers({

	'players': function() {
      return Template.instance().players.get();
	},

	'dummies': function() {
      return Template.instance().dummies.get();
	}

});
