//======================================================================
// SIGN MY FRIEND IN BUTTON:
//======================================================================
// CONTEXT: {matchId, team}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.signMyFriendIn.onCreated(function() {

   // 1. Initialize
   var instance = this; // current template instance

   // 2. Reactive vars declaration
   instance.loading = new ReactiveVar('wait'); // loading indicator

});
//----------------------------------------------------------------------
// HELPERS:
Template.signMyFriendIn.helpers({

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.signMyFriendIn.events({

   'submit form': function(event, instance) {
   	event.preventDefault();

   	if (!Meteor.user()) {
   		throw new Meteor.Error('Match Sign Me Friend In Button: user is not logged in');
   		return;
   	}

      // Variables declaration
      var btn = instance.$('button');
   	var name = instance.$('input').val().trim();

      // Disable button
      btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

   	if (name.length === 0 ) {
   		var msg = {status: 'error', text: 'Indicate_Your_Friends_Name'};
   		processMessage(msg);
         btn.prop("disabled", false);
			instance.loading.set('error');
   		return;
   	}

   	Meteor.call('addFriendOfCurUserToParticipantsList', this.matchId, name, this.team, function(err, msg) {
   		if (err) {
   			console.log(err);
            processMessage({status: 'error', text: 'Unexpected_Error'});
				btn.prop("disabled", false);
				instance.loading.set('error');
   		} else {
   			bootbox.hideAll();
   			processMessage(msg);
            btn.prop("disabled", false);
            instance.loading.set('success');
   		}
   	});
   }

});
