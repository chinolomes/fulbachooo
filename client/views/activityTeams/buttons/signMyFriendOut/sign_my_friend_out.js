//======================================================================
// SIGN MY FRIEND OUT BUTTON:
//======================================================================
// CONTEXT: {matchId}
// required subs: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.signMyFriendOut.onCreated(function() {

   // 1. Initialize
   var instance = this; // current template instance

   // 2. Reactive vars declaration
   instance.loading = new ReactiveVar('wait'); // loading indicator

});
//----------------------------------------------------------------------
// HELPERS:
Template.signMyFriendOut.helpers({

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.signMyFriendOut.events({

	'click button': function(event, instance) {
		event.preventDefault();

		if (!Meteor.user()) {
			throw new Meteor.Error('signMyFriendOut: You are not logged in!');
			return;
		}

      // Variables declaration
      var btn = instance.$('button');

      // Disable button
      btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		Meteor.call('removeFriendOfCurUserFromParticipantsList', this.matchId, function(err, msg) {
			if (err) {
				console.log(err);
            processMessage({status: 'error', text: 'Unexpected_Error'});
				btn.prop("disabled", false);
				instance.loading.set('error');
			} else {
				bootbox.hideAll();
				processMessage(msg);
				btn.prop("disabled", false);
				instance.loading.set('success');
			}
		});
	}

});
