//======================================================================
// SIGN ME IN BUTTON:
//======================================================================
// CONTEXT: {matchId, team}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.signMeIn.onCreated(function() {

   // 1. Initialize
   var instance = this; // current template instance

   // 2. Reactive vars declaration
   instance.loading = new ReactiveVar('wait'); // loading indicator

});
//----------------------------------------------------------------------
// HELPERS:
Template.signMeIn.helpers({

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.signMeIn.events({

	'click button': function(event, instance) {
		event.preventDefault();

		if (!Meteor.user()) {
			throw new Meteor.Error('signMeInButton: You are not logged in!');
			return;
		}

      // Variables declaration
      var btn = instance.$('button');

      // Disable button
      btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		Meteor.call('addCurUserToParticipantsList', this.matchId, this.team, function(err, msg) {
			if (err) {
				console.log(err);
            processMessage({status: 'error', text: 'Unexpected_Error'});
				btn.prop("disabled", false);
				instance.loading.set('error');
			} else {
				bootbox.hideAll();
				processMessage(msg);
				btn.prop("disabled", false);
				instance.loading.set('success');
			}
		});
	}

});
