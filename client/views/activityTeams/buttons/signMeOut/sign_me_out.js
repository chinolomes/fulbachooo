//======================================================================
// SIGN ME OUT BUTTON:
//======================================================================
// CONTEXT: {matchId}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.signMeOut.onCreated(function() {

   // 1. Initialize
   var instance = this; // current template instance

   // 2. Reactive vars declaration
   instance.loading = new ReactiveVar('wait'); // loading indicator

});
//----------------------------------------------------------------------
// HELPERS:
Template.signMeOut.helpers({

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.signMeOut.events({

	'click button': function(event, instance) {
		event.preventDefault();

		if (!Meteor.user()) {
			throw new Meteor.Error('signMeOutButton: You are not logged in!');
			return;
		}

      // Variables declaration
      var btn = instance.$('button');

      // Disable button
      btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		Meteor.call('removeCurUserFromParticipantsList', this.matchId, function(err, msg) {
			if (err) {
				console.log(err);
            processMessage({status: 'error', text: 'Unexpected_Error'});
				btn.prop("disabled", false);
				instance.loading.set('error');
			} else {
				bootbox.hideAll();
				processMessage(msg);
				btn.prop("disabled", false);
				instance.loading.set('success');
			}
		});
	}

});
