//======================================================================
// DUMMY PLAYER TEMPLATE:
//======================================================================
// CONTEXT: this = {data}, where data = {matchId, team}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// EVENTS:
Template.dummyPlayer.events({

   'click .dummyPlayer': function(event) {
      event.preventDefault();

      // Internal var(s)
      var matchOptions = {
         fields: {
            peopleIn: 1,
            friendsOf: 1,
            waitingList: 1,
            status: 1
         }
      };

      // Get data context
      var matchId = this.data.matchId;
      var team = this.data.team;

      // Get user and match
      var curUser = Meteor.userId();
      var match = Matches.findOne(matchId, matchOptions);

      // Internal vars
      var curUserInPlayersList = _.indexOf(getUserIds(match.peopleIn), curUser) !== -1;
      var curUserInWaitingList = _.indexOf(getUserIds(match.waitingList), curUser) !== -1;
      var friendInPlayersList = _.indexOf(getUserIds(match.friendsOf), curUser) !== -1;

      // Catch the 'user not logged in' situation
      if (!curUser) {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert">' + TAPi18n.__('Log_In_To_Join_Activity') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      // If the viewer taps on a dummy player but the match is not active...
      if (match.status !== 'active') {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert">' + TAPi18n.__('The_Activity_Is_Finished_Slash_Canceled') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      /* Situations when a dummyPlayer is clicked:
         |friend/user|IN players list|IN waiting list|OUT     |
         |IN         |no more friends|no more friends|signMeIn|
         |OUT        |SignMyFriendIn |SignMyFriendIn |signMeIn|
      */

      // Catch the 'no more friends' situation
      if ((curUserInPlayersList || curUserInWaitingList) && friendInPlayersList) {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert"><strong>' + TAPi18n.__('Sorry') + '</strong><br>' + TAPi18n.__('You_Can_Only_Add_One_Friend') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      // Catch the 'signMeIn' situation
      if (!curUserInPlayersList && !curUserInWaitingList) {
         bootbox.dialog({
            title: "",
            message: "<div id='dialogNode'></div>",
            onEscape: function() {}
         });
         Blaze.renderWithData(Template.signMeIn, {matchId: matchId, team: team}, $("#dialogNode")[0]);
         return;
      }

      // Catch the 'signMyFriendIn' situation
      if ((curUserInPlayersList || curUserInWaitingList) && !friendInPlayersList) {
         bootbox.dialog({
            title: "",
            message: "<div id='dialogNode'></div>",
            onEscape: function() {}
         });
         Blaze.renderWithData(Template.signMyFriendIn, {matchId: matchId, team: team}, $("#dialogNode")[0]);
         return;
      }
   }

});
