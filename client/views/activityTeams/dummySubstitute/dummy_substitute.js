//======================================================================
// DUMMY SUBSTITUTE TEMPLATE:
//======================================================================
// CONTEXT: this = {data}, where data = {matchId, team}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// EVENTS:
Template.dummySubstitute.events({

   'click .dummySubstitute': function(event) {
      event.preventDefault();

      // Internal var(s)
      var matchOptions = {
         fields: {
            peopleIn: 1,
            friendsOf: 1,
            waitingList: 1,
            status: 1
         }
      };

      // Get data context
      var matchId = this.data.matchId;
      var team = this.data.team;

      // Get user and match
      var curUser = Meteor.userId();
      var match = Matches.findOne(matchId, matchOptions);

      // Internal vars
      var curUserInPlayersList = _.indexOf(getUserIds(match.peopleIn), curUser) !== -1;
      var curUserInWaitingList = _.indexOf(getUserIds(match.waitingList), curUser) !== -1;
      var curUserInWaitingListOfThisTeam = _.indexOf(getUserIds(filterByTeam(match.waitingList, team)), curUser) !== -1;
      var friendInPlayersList = _.indexOf(getUserIds(match.friendsOf), curUser) !== -1;

      // Catch the 'user not logged in' situation
      if (!curUser) {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert">' + TAPi18n.__('Log_In_To_Join_Activity') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      // Catch the 'match not active' situation
      if (match.status !== 'active') {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert">' + TAPi18n.__('The_Activity_Is_Finished_Slash_Canceled') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      /* Situations when a dummySubstitute is clicked:
         |friend/user|IN players list |IN waiting list of this team|IN waiting list of other team|OUT                |
         |IN         |no friends in wl|no friends in wl            |signMeInWaitingList          |signMeInWaitingList|
         |OUT        |no friends in wl|no friends in wl            |signMeInWaitingList          |signMeInWaitingList|

      */

      // Catch the 'no friends in wl' situation
      if (curUserInPlayersList || curUserInWaitingListOfThisTeam) {
         bootbox.dialog({
            title: '',
            message: '<br><div class="alert alert-danger" role="alert"><strong>' + TAPi18n.__('Sorry') + '</strong><br>' + TAPi18n.__('Cant_Add_Friends_To_The_Waiting_List') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      // Catch the 'signMeInWaitingList' situation
      if ((curUserInWaitingList && !curUserInWaitingListOfThisTeam) ||
          (!curUserInPlayersList && !curUserInWaitingList)) {
         bootbox.dialog({
            title: "",
            message: "<div id='dialogNode'></div>",
            onEscape: function() {}
         });
         Blaze.renderWithData(Template.signMeInWaitingList, {matchId: matchId, team: team}, $("#dialogNode")[0]);
         return;
      }
   }

});
