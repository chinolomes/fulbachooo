//======================================================================
// EXTENDED AVATAR TEMPLATE:
//======================================================================
// CONTEXT: this = {data}, where data = {userId, addedAt, team, matchId, user}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// EVENTS:
Template.substituteAvatar.events({

   'click .substituteAvatar': function(event) {
      event.preventDefault();

      // Internal var(s)
      var matchOptions = {
         fields: {
            waitingList: 1,
            status: 1
         }
      };

      // Get data context
      var team = this.data.team;
      var avatarUser = this.data.user;
      var matchId = this.data.matchId;

      // Get current user and match
      var curUserId = Meteor.userId();
      var match = Matches.findOne(matchId, matchOptions);

      // Internal vars
      var curUserInWaitingList = _.indexOf(getUserIds(match.waitingList), curUserId) !== -1;

      // If the viewer (either logged in or not) taps on a player different than himself,
      // redirect him to the player's profile
      if (curUserId !== avatarUser._id) {
         Router.go('/profile/' + avatarUser._id);
         return;
      }

      // Catch the 'match not active' situation
      if (match.status !== 'active') {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert">' + TAPi18n.__('The_Activity_Is_Finished_Slash_Canceled') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      /* Only one situation:
         viewer taps on his own avatar --> show signMeOutWaitingList
      */

      // Catch the 'signMeOutWaitingList' situation
      if (curUserInWaitingList) {
         bootbox.dialog({
            title: "",
            message: "<div id='dialogNode'></div>",
            onEscape: function() {}
         });
         Blaze.renderWithData(Template.signMeOutWaitingList, {matchId: matchId, team: team}, $("#dialogNode")[0]);
         return;
      }
   }

});
