//======================================================================
// WAITING LIST TEMPLATE:
//======================================================================
// CONTEXT: this = {matchId, team}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// ON CREATION:
Template.waitingList.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.substitutes = new ReactiveVar([]); // [{userId, addedAt, team, [name], matchId, user}, {...}, ...]
   instance.showWaitingList = new ReactiveVar(false); // boolean

	// 2. Get context data
	var matchId = Template.currentData().matchId;
	var team = Template.currentData().team;

   // 3. Internal vars
   var matchOptions = {
      fields: {
         peopleIn: 1,
         friendsOf: 1,
         waitingList: 1,
         fieldSize: 1
      }
   };
   var userOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };

   // 4. Set reactive vars
   instance.autorun(function() {
      var match = Matches.findOne(matchId, matchOptions); // reactive source
      var substitutes = filterByTeam(match.waitingList, team); // waitingList[i] = {userId, addedAt, team}

      // Extend substitute object by including user avatar and matchId
		_.each(substitutes, function(substitute) {
         var user = Meteor.users.findOne(substitute.userId, userOptions);
			_.extend(substitute, {matchId: matchId, user: user});
		});
      instance.substitutes.set(_.sortBy(substitutes, 'addedAt'));

      var showWL = getPeopleMissing(match, team) === 0; // requires peopleIn, firendsOf and fieldSize
      instance.showWaitingList.set(showWL);
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.waitingList.helpers({

	'teamIsFull': function() {
      return Template.instance().showWaitingList.get();
	},

	'substitutes': function() {
		return Template.instance().substitutes.get();
	},

	'dummy': function() {
      // Get data context
      var matchId = this.matchId;
      var team = this.team;

      // Set dummy substitute
      var dummy = {matchId: matchId, team: team};
		return dummy;
	}

});
