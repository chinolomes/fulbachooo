//======================================================================
// USER AVATAR TEMPLATE:
//======================================================================
// CONTEXT: this = {data}, where data = {userId, addedAt, team, [name], matchId, user, type}
// REQUIRED SUBS: 'match'
//----------------------------------------------------------------------
// HELPERS:
Template.userAvatar.helpers({

   'isFriend': function() {
      return this.data.type === 'friend';
   },

   'friendNameIsSet': function() {
      return this.data.name && this.data.name !== 'Friend_Of';
   }

});
//----------------------------------------------------------------------
Template.userAvatar.events({

   'click .playerAvatar': function(event) {
      event.preventDefault();

      // Internal var(s)
      var matchOptions = {
         fields: {
            peopleIn: 1,
            friendsOf: 1,
            waitingList: 1,
            status: 1
         }
      };

      // Get data context
      var type = this.data.type;
      var avatarUser = this.data.user;
      var matchId = this.data.matchId;

      // Get current user and match
      var curUserId = Meteor.userId();
      var match = Matches.findOne(matchId, matchOptions);

      // Internal vars
      var curUserInPlayersList = _.indexOf(getUserIds(match.peopleIn), curUserId) !== -1;
      var curUserInWaitingList = _.indexOf(getUserIds(match.waitingList), curUserId) !== -1;
      var friendInPlayersList = _.indexOf(getUserIds(match.friendsOf), curUserId) !== -1;

      // If the viewer (either logged in or not) taps on a player different than himself,
      // redirect him to the player's profile
      if (curUserId !== avatarUser._id) {
         Router.go('/profile/' + avatarUser._id);
         return;
      }

      // If the viewer taps on his own avatar but the match is not active...
      if (match.status !== 'active') {
         bootbox.dialog({
            title: "",
            message: '<br><div class="alert alert-danger" role="alert">' + TAPi18n.__('The_Activity_Is_Finished_Slash_Canceled') + '</div>',
            onEscape: function() {}
         });
         return;
      }

      /* Remaining situations:
         viewer taps on his own avatar (type === 'user')--> show signMeOut
         viewer taps on his friend's avatar (type === 'user') --> show signMyFriendOut
      */

      // Catch the 'signMeOut' situation
      if (curUserInPlayersList && type === 'user') {
         bootbox.dialog({
            title: "",
            message: "<div id='dialogNode'></div>",
            onEscape: function() {}
         });
         Blaze.renderWithData(Template.signMeOut, {matchId: matchId}, $("#dialogNode")[0]);
         return;
      }

      // Catch the 'signMyFriendOut' situation
      if (friendInPlayersList && type === 'friend') {
         bootbox.dialog({
            title: "",
            message: "<div id='dialogNode'></div>",
            onEscape: function() {}
         });
         Blaze.renderWithData(Template.signMyFriendOut, {matchId: matchId}, $("#dialogNode")[0]);
         return;
      }

   }

});
