//======================================================================
// HEADER LANGUAGE BUTTON
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.headerLangButton.helpers({

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'tablet': function() {
		return this.type && this.type === 'tablet';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.headerLangButton.events({

	'click .swapLang': function(event, instance) {
		var lang = event.target.id;
		setLocale(lang);
		saveLocale(lang);
	}

});
