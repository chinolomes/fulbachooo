//======================================================================
// HEADER PROFILE BUTTON:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.headerProfileButton.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variable(s)
	instance.curUser = new ReactiveVar(null);

   // Internal var(s)
   var options = {
      fields: {
         "profile.name": 1,
         "services.twitter.profile_image_url_https": 1,
         "services.facebook.id": 1,
         "services.google.picture": 1
      }
   };

   // 2. Subscribe to current user avatar (profile + services)
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub = instance.subscribe('curUserAvatar');

         // 3. Set reactive variable
         if (sub.ready()) {
            var curUser = Meteor.users.findOne(Meteor.userId(), options);
            instance.curUser.set(curUser);
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.headerProfileButton.helpers({

	'curUserId': function() {
      return Meteor.userId();
	},

	'curUser': function() {
      return Template.instance().curUser.get();
	},

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'tablet': function() {
		return this.type && this.type === 'tablet';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	}

});
