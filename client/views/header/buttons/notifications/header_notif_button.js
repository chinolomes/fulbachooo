//======================================================================
// HEADER NOTIFICATIONS BUTTON
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.headerNotifButton.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.unreadNotif = new ReactiveVar(0);
   instance.displayDropdown = new ReactiveVar(false);

   // 2. Subscribe to number of unread notifications
   instance.autorun(function() {
      if (Meteor.userId()) {
         var sub = instance.subscribe('curUserUnreadNotif');

         // 3. Set reactive variable(s)
         if (sub.ready()) {
            var unreadNotif = UnreadNotifications.findOne({recipientId: Meteor.userId()});
            if (!_.isUndefined(unreadNotif) && !_.isUndefined(unreadNotif.counter)) {
               instance.unreadNotif.set(unreadNotif.counter);
            }
         }
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.headerNotifButton.helpers({

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'tablet': function() {
		return this.type && this.type === 'tablet';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	},

	'positive': function() {
		return Template.instance().unreadNotif.get() > 0;
	},

	'unreadNotif': function() {
      var unreadNotif = Template.instance().unreadNotif.get();
      return unreadNotif <= NOTIF_LIMIT ? unreadNotif : NOTIF_LIMIT + "+";
	},

   'displayDropdown': function() {
      return Template.instance().displayDropdown.get();
   }

});
//----------------------------------------------------------------------
// EVENTS:
Template.headerNotifButton.events({

   'click a#link': function(event, instance) {
      event.preventDefault();
      if (instance.unreadNotif.get() > 0) {
         Meteor.call('setNotifToRead');
      }
      Router.go('/notifications');
	},

	'click a#dropdown': function(event, instance) {
      event.preventDefault();
      if (instance.unreadNotif.get() > 0) {
         Meteor.call('setNotifToRead');
      }
      // Set reactive vars
      instance.displayDropdown.set(true);
	}

});
