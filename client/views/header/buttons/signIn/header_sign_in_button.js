//======================================================================
// HEADER SIGN IN BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {type, [align]}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATED:
Template.headerSignInButton.onCreated(function() {

	// Current template instance
	var instance = this;

	// Initialize reactive vars
	instance.loading = new ReactiveVar(false);

});
//----------------------------------------------------------------------
// HELPERS:
Template.headerSignInButton.helpers({

	'loading': function() {
		return Template.instance().loading.get();
	},

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'tablet': function() {
		return this.type && this.type === 'tablet';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	},

	'services': function() {
		var self = this;

		  // First look for OAuth services.
		  var services = Package['accounts-oauth'] ? Accounts.oauth.serviceNames() : [];

		  // Be equally kind to all login services. This also preserves
		  // backwards-compatibility. (But maybe order should be
		  // configurable?)
		  services.sort();

		  // Add password, if it's there; it must come last.
		  //if (hasPasswordService())
		  //  services.push('password');

		  return _.map(services, function(name) {
		    return {name: name};
		  });
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.headerSignInButton.events({

	'click .login-button': function(event, instance) {
		// don't prevent default!
		instance.loading.set(true);
	}

});
