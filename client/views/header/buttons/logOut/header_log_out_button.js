//======================================================================
// HEADER LOG OUT BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// EVENTS:
Template.headerLogOutButton.helpers({

	'active': function() {
		return Session.equals('curSubSection', 'logout') ? 'active' : '';
	},

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'tablet': function() {
      return this.type && this.type === 'tablet';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   }

});
//----------------------------------------------------------------------
// EVENTS:
Template.headerLogOutButton.events({

	'mouseover .moreItem': function() {
		Session.set('curSubSection', 'logout');
	},

	'mouseout .moreItem': function() {
		Session.set('curSubSection', null);
	},

   'click .logout': function(event) {
		event.preventDefault();
	   Meteor.logout();
   }

});
