//======================================================================
// HEADER MORE BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {type}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// EVENTS:
Template.headerMoreButton.helpers({

   'mobile': function() {
      return this.type && this.type === 'mobile';
   },

   'desktop': function() {
      return this.type && this.type === 'desktop';
   },

   'tablet': function() {
      return this.type && this.type === 'tablet';
   }

});
//----------------------------------------------------------------------
// EVENTS:
Template.headerMoreButton.events({

   'click .showSettings': function(event) {
      event.preventDefault();

      if (!Meteor.userId()) {
         throw new Meteor.Error('headerSettingsButton click .showSettings: user is not logged in');
         return;
      }

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

   	Blaze.renderWithData(Template.emailNotifDESKTOP, {}, $("#dialogNode")[0]);
   }

});
