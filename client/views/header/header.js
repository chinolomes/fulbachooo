//======================================================================
// HEADER TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.header.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Initialize reactive variables
	instance.width = new ReactiveVar(1000);

});
//----------------------------------------------------------------------
// RENDERED:
Template.header.onRendered(function() {

	// Current template instance
   var instance = this;

	// Calculate container width
   instance.width.set($(window).width());
   $(window).resize(function(e) {
      instance.width.set($(window).width());
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.header.helpers({

   'width': function() {
      return Template.instance().width.get();
   },

   'MOBILE': function() {
      return Template.instance().width.get() < 768;
   },

   'TABLET': function() {
      return Template.instance().width.get() >= 768 && Template.instance().width.get() < 1040;
   },

   'DESKTOP': function() {
      return Template.instance().width.get() >= 1040;
   }

});
