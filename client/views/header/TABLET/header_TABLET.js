//======================================================================
// HEADER TABLET TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.headerTABLET.helpers({

	'activities': function() {
		return Router.current().route.getName() === 'activities' ? 'active' : '';
	},

	'users': function() {
		return Router.current().route.getName() === 'users' ? 'active' : '';
	},

   'curUserId': function() {
      return Meteor.userId();
   }

});
