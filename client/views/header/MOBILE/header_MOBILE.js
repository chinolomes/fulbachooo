//======================================================================
// HEADER MOBILE TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.headerMOBILE.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.curRouteName = new ReactiveVar('');
   instance.prevRouteName = new ReactiveVar('');
   instance.prevRoute = new ReactiveVar('');

   // 2. Initialize session vars
   Session.set('curSection', 'main');

   instance.autorun(function() {
      var curRoute = Router.current().route.getName();
      var params = Router.current().params._id || ''; // could be undefined
      switch(curRoute) {
         case 'newActivity':
            instance.curRouteName.set('Create_Activity');
            instance.prevRouteName.set('Activities');
            instance.prevRoute.set('/activities');
            break;
         case 'editActivity':
            instance.curRouteName.set('Admin_Activity_Short');
            instance.prevRouteName.set('Activity');
            instance.prevRoute.set('/activities/activity/' + params);
            break;
         case 'editProfileMOBILE':
            instance.curRouteName.set('Edit_Profile');
            instance.prevRouteName.set('Profile');
            instance.prevRoute.set('/profile/' + params);
            break;
         case 'profile':
            instance.curRouteName.set('Profile');
            instance.prevRouteName.set('Users');
            instance.prevRoute.set('/users');
            break;
         case 'notifications':
            instance.curRouteName.set('Notifications_Short');
            instance.prevRouteName.set('Activities');
            instance.prevRoute.set('/activities');
            break;
         case 'activity':
            instance.curRouteName.set('Activity');
            instance.prevRouteName.set('Activities');
            instance.prevRoute.set('/activities');
            break;
         case 'emailNotifMOBILE':
            instance.curRouteName.set('Email_Notifications_Short');
            instance.prevRouteName.set('More');
            instance.prevRoute.set('/more');
            break;
         case 'languages':
            instance.curRouteName.set('Language');
            instance.prevRouteName.set('More');
            instance.prevRoute.set('/more');
            break;
         case 'FAQ':
            instance.curRouteName.set('FAQs');
            instance.prevRouteName.set('More');
            instance.prevRoute.set('/more');
            break;
         case 'whoWeAre':
            instance.curRouteName.set('Who_Are_We');
            instance.prevRouteName.set('More');
            instance.prevRoute.set('/more');
            break;
         case 'termsAndConditions':
            instance.curRouteName.set('Terms');
            instance.prevRouteName.set('More');
            instance.prevRoute.set('/more');
            break;
        case 'privacyPolicy':
           instance.curRouteName.set('Privacy Policy');
           instance.prevRouteName.set('More');
           instance.prevRoute.set('/more');
           break;
         case 'more':
            instance.curRouteName.set('More');
            instance.prevRouteName.set('Activities');
            instance.prevRoute.set('/activities');
            break;
         case 'contactUs':
            instance.curRouteName.set('Contact_Us');
            instance.prevRouteName.set('More');
            instance.prevRoute.set('/more');
            break;
         case 'verifyEmail':
            instance.curRouteName.set('Verify_Email');
            instance.prevRouteName.set('Activities');
            instance.prevRoute.set('/activities');
            break;
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.headerMOBILE.helpers({

   'main': function() {
      var curRoute = Router.current().route.getName();
      return curRoute === 'activities' || curRoute === 'users';
   },

   'curRouteName': function() {
      return TAPi18n.__(Template.instance().curRouteName.get());
   },

   'prevRouteName': function() {
      return TAPi18n.__(Template.instance().prevRouteName.get());
   },

   'prevRoute': function() {
      return Template.instance().prevRoute.get();
   },

	'activities': function() {
		return Router.current().route.getName() === 'activities' ? 'active' : '';
	},

	'users': function() {
		return Router.current().route.getName() === 'users' ? 'active' : '';
	},

   'curUserId': function() {
      return Meteor.userId();
   }

});


//======================================================================
// HEADER MOBILE TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
/*Template.headerMOBILE.onCreated(function() {

   // 0. Current template instance
   //var instance = this;

   // 1. Initialize reactive variables
   //instance.displayDropdown = new ReactiveVar(false);

   // 2. Initialize session vars
   Session.set('curSection', 'main');

});
//----------------------------------------------------------------------
// HELPERS:
Template.headerMOBILE.helpers({

   'main': function() {
      var curRoute = Router.current().route.getName();
      return curRoute === 'activities' || curRoute === 'users';
   },

   'curRouteName': function() {
      var curRoute = Router.current().route.getName();
      return TAPi18n.__('HEADER_' + curRoute);
   },

   'prevRouteName': function() {
      var curRoute = Router.current().route.getName();
      //return TAPi18n.__('HEADER_' + curRoute + '_prev');
      var prevRouteName = '';
      switch(curRoute) {
         case 'newActivity':
            prevRouteName = 'Activities';
            break;
         case 'editActivity':
            prevRouteName = 'activity';
            break;
         case 'editProfileMOBILE':
            prevRouteName = 'Profile';
            break;
         case 'profile':
            prevRouteName = 'Users';
            break;
         case 'notifications':
            prevRouteName = 'Activities';
            break;
         case 'activity':
            prevRouteName = 'Activities';
            break;
         case 'emailNotifMOBILE':
            prevRouteName = 'More';
            break;
         case 'languages':
            prevRouteName = 'More';
            break;
         case 'FAQ':
            prevRouteName = 'More';
            break;
         case 'whoWeAre':
            prevRouteName = 'More';
            break;
         case 'termsAndConditions':
            prevRouteName = 'More';
            break;
         case 'more':
            prevRouteName = 'Activities';
            break;
         case 'contactUs':
            prevRouteName = 'More';
            break;
         case 'verifyEmail':
            prevRouteName = 'Activities';
            break;
      }
      return TAPi18n.__(prevRouteName);
   },

   'prevRoute': function() {
      //return Session.equals('curSection', 'profile');
      var curRoute = Router.current().route.getName();
      var params = Router.current().params._id;
      var prevRoute = '';
      switch(curRoute) {
         case 'newActivity':
            prevRoute = '/activities';
            break;
         case 'editActivity':
            prevRoute = '/activities/activity/' + params;
            break;
         case 'editProfileMOBILE':
            prevRoute = '/profile/' + params;
            break;
         case 'profile':
            prevRoute = '/users';
            break;
         case 'notifications':
            prevRoute = '/activities';
            break;
         case 'activity':
            prevRoute = '/activities';
            break;
         case 'emailNotifMOBILE':
            prevRoute = '/more';
            break;
         case 'languages':
            prevRoute = '/more';
            break;
         case 'FAQ':
            prevRoute = '/more';
            break;
         case 'whoWeAre':
            prevRoute = '/more';
            break;
         case 'termsAndConditions':
            prevRoute = '/more';
            break;
         case 'more':
            prevRoute = '/activities';
            break;
         case 'contactUs':
            prevRoute = '/more';
            break;
         case 'verifyEmail':
            prevRoute = '/activities';
            break;
      }
      return prevRoute;
   },

	'activities': function() {
		return Router.current().route.getName() === 'activities' ? 'active' : '';
	},

	'users': function() {
		return Router.current().route.getName() === 'users' ? 'active' : '';
	},

   'curUserId': function() {
      return Meteor.userId();
   },

	//'services': function() {
	//	var self = this;

		  // First look for OAuth services.
	//	  var services = Package['accounts-oauth'] ? Accounts.oauth.serviceNames() : [];

		  // Be equally kind to all login services. This also preserves
		  // backwards-compatibility. (But maybe order should be
		  // configurable?)
	//	  services.sort();

		  // Add password, if it's there; it must come last.
		  //if (hasPasswordService())
		  //  services.push('password');

	//	  return _.map(services, function(name) {
	//	    return {name: name};
	//	  });
	//}

});*/
