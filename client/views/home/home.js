//======================================================================
// HOME HEADER:
//======================================================================
// ON CREATED:
Template.home.onCreated(function() {

   // Current template instance
   var instance = this;

   instance.autorun(function() {
      if (ResponsiveHelpers.isXs()) {
         Router.go('activities');
      }
   });

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.home.onRendered(function() {

   // Current template instance
   var instance = this;

   instance.autorun(function() {
      if (!ResponsiveHelpers.isXs()) {
         instance.$('#video-container').vide('/videos/video_low');
      } else {
         Router.go('activities');
      }
   });

   var elem = $('img');
   var animation = 'rubberBand';
   //elem.removeClass('animated ' + animation);
   Meteor.setTimeout(function(){
      elem.addClass('animated ' + animation);
   }, 1500);
   Meteor.setTimeout(function() {
      elem.removeClass('animated ' + animation);
   }, 2500);

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.home.events({

   'mouseover img': function() {
      //console.log('fire animation');
      var elem = $('img');
      var animation = 'rubberBand';
      //elem.removeClass('animated ' + animation);
      elem.addClass('animated ' + animation);

      //wait for animation to finish before removing classes
      Meteor.setTimeout(function() {
         elem.removeClass('animated ' + animation);
      }, 2000);
   }

});
