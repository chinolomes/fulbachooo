//======================================================================
// PERSONAL INFO:
//======================================================================
// CONTEXT: this = {userId}
// REQUIRED SUBS: 'userProfile'
// TODO: calculate age from this.birthday
//----------------------------------------------------------------------
// ON CREATION:
/*Template.profileData.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.user = new ReactiveVar(null);

	// 2. Get data context
	var userId = Template.currentData().userId;

   // 3. Subscribe to senders profile and services
   instance.autorun(function() {
      var fields = {fields: {profile: 1, createdAt: 1}};
      var user = Meteor.users.findOne(userId, fields);
      instance.user.set(user);
   });

});*/
//----------------------------------------------------------------------
// HELPERS:
Template.profileData.helpers({

	'user': function() {
		//return Template.instance().user.get();
		var options = {
			fields: {
				profile: 1,
				createdAt: 1
			}
		};
		return Meteor.users.findOne(this.userId, options);
	}

});
