//======================================================================
// BUTTONS:
//======================================================================
// CONTEXT: this = {userId}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.profileButtons.helpers({

	'viewerIsOwner': function() {
		return this.userId && this.userId === Meteor.userId();
	}

});
//----------------------------------------------------------------------
// AUX FUNCTIONS:
function handleDelete() {
	bootbox.dialog({
		size: 'small',
		message: 'Sure that you want to delete your account?',
		buttons: {
		   cancel: {
		      label: "No!",
		      className: "btn-success",
		      callback: function() {
					// do nothing
		      }
		   },
		   confirm: {
		      label: 'Yes, delete it!',
		      className: "btn-danger",
		      callback: function() {
            Meteor.call('deleteCurUserAccount', function(err) {
              if (err) {
                console.log(err);
              }
            });
		   	  }
		 	}
		}
	});
}
//----------------------------------------------------------------------
function handleDownload() {
  Meteor.call('downloadCurUSerData', function(err, res) {
    console.log(err, res);
		if (err) {
			console.log('err', err);
		} else {
      var json = JSON.stringify(res, null, 2);
      var blob = new Blob([json]);
	    var a = window.document.createElement('a');
      a.href = window.URL.createObjectURL(blob, {type: "application/json"});
      a.download = Meteor.user().profile.name.replace(' ', '_') + '_fulbacho' + '.json';
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }
	});
}
//----------------------------------------------------------------------
// EVENTS:
Template.profileButtons.events({

	'click #editProfileMOBILE': function(event) {
		event.preventDefault();

      if (this.userId && this.userId !== Meteor.userId()) {
         throw new Meteor.Error('profileButtons click #editProfileMOBILE: user is not logged in');
         return;
      }

		Router.go('/profile/edit/' + this.userId);
	},

   'click #editProfileDESKTOP': function(event) {
      event.preventDefault();

      if (this.userId && this.userId !== Meteor.userId()) {
         throw new Meteor.Error('profileButtons click #editProfileDESKTOP: user is not logged in');
         return;
      }

      bootbox.dialog({
         title: "",
         message: "<div id='dialogNode'></div>",
         onEscape: function() {}
      });

      Blaze.renderWithData(Template.editProfileDESKTOP, {}, $("#dialogNode")[0]);
   },

	'click #deleteAccountMOBILE': function(event) {
		event.preventDefault();

    if (this.userId && this.userId !== Meteor.userId()) {
      throw new Meteor.Error('profileButtons click deleteAccount: user is not logged in');
      return;
    }

    handleDelete();
	},

	'click #deleteAccountDESKTOP': function(event) {
		event.preventDefault();

    if (this.userId && this.userId !== Meteor.userId()) {
      throw new Meteor.Error('profileButtons click deleteAccount: user is not logged in');
      return;
    }

    handleDelete();
	},

	'click #downloadPersonalDataMOBILE': function(event) {
		event.preventDefault();

    if (this.userId && this.userId !== Meteor.userId()) {
      throw new Meteor.Error('profileButtons click downloadData: user is not logged in');
      return;
    }

    handleDownload();
	},

	'click #downloadPersonalDataDESKTOP': function(event) {
		event.preventDefault();

    if (this.userId && this.userId !== Meteor.userId()) {
      throw new Meteor.Error('profileButtons click downloadData: user is not logged in');
      return;
    }

    handleDownload();
	},
});
