//======================================================================
// PROFILE TABS TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.profileTabs.onCreated(function() {

	// 1. Initialize reactive variables
	var instance = this;

	// Get -reactive- hash
	instance.autorun(function() {
		var hash = Iron.Location.get().hash; // reactive source
		if (hash) {
			switch(hash) {
				case '#upcoming':
					Session.set('curSubSection', 'upcoming');
					break;
				case '#past':
					Session.set('curSubSection', 'past');
					break;
				default:
					Session.set('curSubSection', 'upcoming');
					break;
			}
		} else {
			Session.set('curSubSection', 'upcoming');
		}
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.profileTabs.helpers({

	upcoming: function() {
		return Session.equals('curSubSection', 'upcoming');
	},

	past: function() {
		return Session.equals('curSubSection', 'past');
	}

	/*played: function() {
		return Session.equals('curSubSection', 'played');
	},

	organized: function() {
 		return Session.equals('curSubSection', 'organized');
	},

	waiting: function() {
		return Session.equals('curSubSection', 'waiting');
	}*/

});
//----------------------------------------------------------------------
// EVENTS:
Template.profileTabs.events({

	'click .upcoming': function(event) {
		event.preventDefault();
		window.location.hash = 'upcoming';
		Session.set('curSubSection', 'upcoming');
	},

	'click .past': function(event) {
		event.preventDefault();
		window.location.hash = 'past';
		Session.set('curSubSection', 'past');
	}

	/*'click #played': function(event) {
		event.preventDefault();
		Session.set('curSubSection', 'played');
	},

	'click #organized': function(event) {
		event.preventDefault();
		Session.set('curSubSection', 'organized');
	},

	'click #waiting': function(event) {
		event.preventDefault();
		Session.set('curSubSection', 'waiting');
	}*/

});
