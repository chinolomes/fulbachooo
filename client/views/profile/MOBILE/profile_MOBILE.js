//======================================================================
// PROFILE MOBILE:
//======================================================================
// CONTEXT: this = {userId}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.profileMOBILE.helpers({

	'profile': function() {
		return Session.equals('curSubSection', 'profile');
	}

});
