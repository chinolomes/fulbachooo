//======================================================================
// PROFILE NAVIGATION BAR BUTTONS:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.profileNav.onCreated(function() {

	// 1. Initialize reactive variables
	var instance = this;

	// Get -reactive- hash
	instance.autorun(function() {
		var hash = Iron.Location.get().hash;
		if (hash) {
			switch(hash) {
				case '#upcoming':
					Session.set('curSubSection', 'upcoming');
					break;
				case '#past':
					Session.set('curSubSection', 'past');
					break;
				default:
					Session.set('curSubSection', 'profile');
					break;
			}
		} else {
			Session.set('curSubSection', 'profile');
		}
	});

});
//----------------------------------------------------------------------
// HELPERS:
Template.profileNav.helpers({

	'profile': function() {
		return Session.equals('curSubSection', 'profile') ? 'active' : '';
	},

	'upcoming': function() {
		return Session.equals('curSubSection', 'upcoming') ? 'active' : '';
	},

	'past': function() {
		return Session.equals('curSubSection', 'past') ? 'active' : '';
	}

	/*'played': function() {
		return Session.equals('curSubSection', 'played') ? 'active' : '';
	},

	'organized': function() {
		return Session.equals('curSubSection', 'organized') ? 'active' : '';
	},

	'waiting': function() {
		return Session.equals('curSubSection', 'waiting') ? 'active' : '';
	}*/

});
