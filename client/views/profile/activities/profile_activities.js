//======================================================================
// PROFILE ACTIVITIES:
//======================================================================
// CONTEXT: this = {userId}
// REQUIRED SUBS: userProfile + userServices + template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.profileActivities.onCreated(function() {

   // Current template
   var instance = this;

   // 2. Context data
   var ownerId = Template.currentData().userId;

	// Initialize session variables
	Session.set('curMatch', null);

   // Initialize reactive vars
   instance.matchesArray = new ReactiveVar([]);
   instance.containerWidth = new ReactiveVar(1000);
   instance.loaded = new ReactiveVar(0);
   instance.limit = new ReactiveVar(LIMIT); // see /client/lib/constants

	// Initialize internal vars
   var userOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };
   // Activity fields when the viewer is owner
   var fieldsPrivPubActs = {
      "createdAt": 0,
   	"loc.type": 0,
   	"fieldPhone": 0,
   	"fieldWebsite": 0,
   	"privacyOnCreation": 0,
   	"peopleInOnCreation": 0
   };
   // Activity fields when the viewer is NOT owner
   var fieldsOnlyPubActs = _.extend({"followers": 0}, fieldsPrivPubActs);

   // 3. Subscribe to user activities
   instance.autorun(function() {
      var limit = instance.limit.get();
      if (!Session.equals('curSubSection', null)) {
         var sub = instance.subscribe('userMatches', ownerId, Session.get('curSubSection'), limit);

         // if subscription is ready, set limit to newLimit
         if (sub.ready()) {
            instance.loaded.set(limit);
         }
      }
   });

   // 3. Reactive function
   instance.matches = function() {
      var matches = [];
      var matchesArray = [];
      var limit = instance.loaded.get();
      var owner = Meteor.users.findOne(ownerId, {fields: {activities: 1}});
      var activities = owner.activities;
      // 4 viewer-category combinations:
      //         |upcoming|past|
      //owner    |   x    |  x |
      //not-owner|   x    |  x |

      var actUpcPub = activities.upcoming.public;
      var actUpcPri = activities.upcoming.private;
      var actPasPub = activities.past.public;
      var actPasPri = activities.past.private;

      // Get activities ids
      var actIdsOwnerUpcoming = _.union(_.flatten(_.values(actUpcPub)), _.flatten(_.values(actUpcPri))); // unique ids
      var actIdsOwnerPast = _.union(_.flatten(_.values(actPasPub)), _.flatten(_.values(actPasPri))); // unique ids
      var actIdsNotOwnerUpcoming = _.union(actUpcPub.created, actUpcPub.enrolled, actUpcPub.waiting, actUpcPub.friendIsEnrolled);
      var actIdsNotOwnerPast = _.union(actPasPub.created, actPasPub.enrolled, actPasPub.waiting, actPasPub.friendIsEnrolled);

   	// If the viewer is the profile's owner, publish all data
      if (ownerId && ownerId === Meteor.userId()) {
   		switch(Session.get('curSubSection')) {
   			case 'upcoming':
               matches = Matches.find({_id: {$in: actIdsOwnerUpcoming}}, {sort: {date: 1, time: 1}, limit: limit, fields: fieldsPrivPubActs}).fetch();
   				break;
   			case 'past':
   				matches = Matches.find({_id: {$in: actIdsOwnerPast}}, {sort: {date: -1, time: -1}, limit: limit, fields: fieldsPrivPubActs}).fetch();
   				break;
   		}
   	} else { // Else, only display the public games
   		switch(Session.get('curSubSection')) {
   			case 'upcoming':
   				matches = Matches.find({_id: {$in: actIdsNotOwnerUpcoming}}, {sort: {date: 1, time: 1}, limit: limit, fields: fieldsOnlyPubActs}).fetch();
   				break;
   			case 'past':
   				matches = Matches.find({_id: {$in: actIdsNotOwnerPast}}, {sort: {date: -1, time: -1}, limit: limit, fields: fieldsOnlyPubActs}).fetch();
   				break;
   		}
   	}
      // Extend match object by adding 'border' property
      if (matches.length > 0) {
         var lastIndex = matches.length - 1; // last feasible index
         for (var i = 0; i < lastIndex; i++) {
            matchesArray.push({match: matches[i], border: true});
         }
         matchesArray.push({match: matches[lastIndex], border: false});
      }
      Template.instance().matchesArray.set(matchesArray);
      return matchesArray;
   }

});
//----------------------------------------------------------------------
// RENDERED:
Template.profileActivities.onRendered(function() {

   var instance = this;

   instance.containerWidth.set($(window).width());
   $(window).resize(function(e) {
      var width = $('.matchesContainer').width();
      instance.containerWidth.set(width);
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.profileActivities.helpers({

	'matchesArray': function() {
      return Template.instance().matches();
	},

   // are there more matches to show?
   'hasMoreMatches': function () {
      return Template.instance().matchesArray.get().length >= Template.instance().limit.get();
   },

   'cardType': function() {
      return Template.instance().containerWidth.get() > 570 ? 'desktop' : 'mobile';
   },

   'sectionName': function() {
      var sectionName = 'Upcoming_Activities';
      switch(Session.get('curSubSection')) {
         case 'upcoming':
            sectionName = 'Upcoming_Activities';
         break;
         case 'past':
            sectionName = 'Past_Activities';
         break;
      }
      return sectionName;
   }

});
//----------------------------------------------------------------------
// EVENTS:
Template.profileActivities.events({

   'click .load-more': function (event, instance) {
      event.preventDefault();

      // get current value for limit, i.e. how many players are currently displayed
      var limit = instance.limit.get();

      // increase limit by 5 and update it
      limit += LOAD_MORE; // see /client/lib/constants
      instance.limit.set(limit);
   }

});


/*
//======================================================================
// PROFILE ACTIVITIES:
//======================================================================
// CONTEXT: this = {userId}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.profileActivities.onCreated(function() {

   // Current template
   var instance = this;

   // 2. Context data
   var ownerId = Template.currentData().userId;

	// Initialize session variables
	Session.set('curMatch', null);

   // Initialize reactive vars
   instance.matchesArray = new ReactiveVar([]);
   instance.containerWidth = new ReactiveVar(1000);
   instance.loaded = new ReactiveVar(0);
   instance.limit = new ReactiveVar(LIMIT); // see /client/lib/constants

   // Owner queries and options
   var fieldsOwner = {
      "createdAt": 0,
   	"loc.type": 0,
   	"fieldPhone": 0,
   	"fieldWebsite": 0,
   	"privacyOnCreation": 0,
   	"peopleInOnCreation": 0
   };
   // TODO: instead of looking through all the activities, use info stored in the user profile
   var orOwner = [
      {'peopleIn.userId': {$in: [ownerId]}},
      {'friendsOf.userId': {$in: [ownerId]}},
      {createdBy: ownerId},
      {'waitingList.userId': {$in: [ownerId]}},
      {followers: {$in: [ownerId]}},
      {admin: {$in: [ownerId]}}
   ];
   var queryUpcomingOwner = {
      status: {
         $ne: 'finished'
      },
      $or: orOwner
   };
   var queryPastOwner = {
      status: 'finished',
      $or: orOwner
   };

   // Not owner queries and options
   var fields = {
      "createdAt": 0,
   	"loc.type": 0,
   	"fieldPhone": 0,
   	"fieldWebsite": 0,
   	"privacyOnCreation": 0,
   	"peopleInOnCreation": 0,
      "followers": 0
   };
   var or = [
      {'peopleIn.userId': {$in: [ownerId]}},
      {'friendsOf.userId': {$in: [ownerId]}},
      {createdBy: ownerId},
      {'waitingList.userId': {$in: [ownerId]}}
   ];
   var queryUpcoming = {
      privacy: 'public',
      status: {
         $ne: 'finished'
      },
      $or: or
   };
   var queryPast = {
      privacy: 'public',
      status: 'finished',
      $or: or
   };

   // 3. Subscribe to current user services
   instance.autorun(function() {
      var limit = instance.limit.get();
      if (!Session.equals('curSubSection', null)) {
         var sub = instance.subscribe('userMatches', ownerId, Session.get('curSubSection'), limit);

         // if subscription is ready, set limit to newLimit
         if (sub.ready()) {
            instance.loaded.set(limit);
         }
      }
   });

   // 3. Reactive function
   instance.matches = function() {
      var matches = [];
      var matchesArray = [];
      var limit = instance.loaded.get();

   	// If the viewer is the profile's owner, publish all data
      if (ownerId && ownerId === Meteor.userId()) {
   		switch(Session.get('curSubSection')) {
   			case 'upcoming':
               matches = Matches.find(queryUpcomingOwner, {sort: {date: 1}, limit: limit, fields: fieldsOwner}).fetch();
   				break;
   			case 'past':
   				matches = Matches.find(queryPastOwner, {sort: {date: -1}, limit: limit, fields: fieldsOwner}).fetch();
   				break;
   		}
   	} else { // Else, only display the public games
   		switch(Session.get('curSubSection')) {
   			case 'upcoming':
   				matches = Matches.find(queryUpcoming, {sort: {date: 1}, limit: limit, fields: fields}).fetch();
   				break;
   			case 'past':
   				matches = Matches.find(queryPast, {sort: {date: -1}, limit: limit, fields: fields}).fetch();
   				break;
   		}
   	}
      // Extend match object by adding 'border' property
      if (matches.length > 0) {
         var lastIndex = matches.length - 1; // last feasible index
         for (var i = 0; i < lastIndex; i++) {
            matchesArray.push({match: matches[i], border: true});
         }
         matchesArray.push({match: matches[lastIndex], border: false});
      }
      Template.instance().matchesArray.set(matchesArray);
      return matchesArray;
   }

});
//----------------------------------------------------------------------
// RENDERED:
Template.profileActivities.onRendered(function() {

   var instance = this;

   instance.containerWidth.set($(window).width());
   $(window).resize(function(e) {
      var width = $('.matchesContainer').width();
      instance.containerWidth.set(width);
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.profileActivities.helpers({

	'matchesArray': function() {
      return Template.instance().matches();
	},

   // are there more matches to show?
   'hasMoreMatches': function () {
      return Template.instance().matchesArray.get().length >= Template.instance().limit.get();
   },

   'cardType': function() {
      return Template.instance().containerWidth.get() > 570 ? 'desktop' : 'mobile';
   },

   'sectionName': function() {
      var sectionName = 'Upcoming_Activities';
      switch(Session.get('curSubSection')) {
         case 'upcoming':
            sectionName = 'Upcoming_Activities';
         break;
         case 'past':
            sectionName = 'Past_Activities';
         break;
      }
      return sectionName;
   }

});
//----------------------------------------------------------------------
// EVENTS:
Template.profileActivities.events({

   'click .load-more': function (event, instance) {
      event.preventDefault();

      // get current value for limit, i.e. how many players are currently displayed
      var limit = instance.limit.get();

      // increase limit by 5 and update it
      limit += LOAD_MORE; // see /client/lib/constants
      instance.limit.set(limit);
   }

});
*/
