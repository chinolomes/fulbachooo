//======================================================================
// VERIFY EMAIL TEMPLATE:
//======================================================================
// CONTEXT: this = {userId, token}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.verifyEmail.onCreated(function() {

   // Current template instance
   var instance = this;

   // Initialize the reactive variables
   instance.status = new ReactiveVar('verifying');

   // Get data context
   var data = Template.currentData();
   var userId = data && data.userId && data.userId !== '' ? data.userId : '';
   var token = data && data.token && data.token !== '' ? data.token : '';

   if (userId.length > 0 || token.length > 0) {
      Meteor.call('customVerifyEmail', userId, token, function (err, res) {
         if (err) {
            console.log(err);
            instance.status.set('error');
         } else {
            if (res.status === 'success') {
               instance.status.set('success');
            } else if (res.status === 'failed') {
               instance.status.set('failed');
            } else {
               instance.status.set('error');
            }
         }
      });
   } else {
      instance.status.set('error');
   }

});
//----------------------------------------------------------------------
// HELPERS:
Template.verifyEmail.helpers({

   'verifying': function() {
      return Template.instance().status.get() === 'verifying';
   },

   'success': function() {
      return Template.instance().status.get() === 'success';
   },

   'failed': function() {
      return Template.instance().status.get() === 'failed';
   },

   'error': function() {
      return Template.instance().status.get() === 'error';
   }

});
