//======================================================================
// NEW ACTIVITY PRE POPULATE TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.newActivityPrePopulate.onCreated(function() {

	// Current template instance
	var instance = this;

	// Initialize session variables
	Session.set('resetMapPREPO', false); // triggers autorun function at newMatchMap
	Session.set('resetMarkerNEWMATCH', false); // triggers autorun function at newMatchMap
	Session.set('resetSelect2_PREPO', false);

	// 1. Initialize reactive variables
   instance.prevMatches = new ReactiveVar([]);

	// Internal var(s)
	var options = {
		fields: {
			sport: 1,
			date: 1,
			time: 1,
			admin: 1
		},
		sort: {
			date: -1
		},
		limit: 15
	};

   // 2. Set reactive vars
   instance.autorun(function() {
      if (Meteor.userId()) {
			//instance.prevMatches.set([]);
			var subs = instance.subscribe('previousMatches');

			if (subs.ready()) {
				var prevMatches = Matches.find({createdBy: Meteor.userId()}, options).fetch();
				//console.log('prevMatches.length: ', prevMatches.length);
				instance.prevMatches.set(prevMatches);
			}
      }
   });

	/*
   // 2. Set reactive vars
   instance.autorun(function() {
      if (Meteor.userId()) {
         var prevMatches = Matches.find(query, options).fetch();
         instance.prevMatches.set(prevMatches);
      }
   });
	*/

});
//----------------------------------------------------------------------
// HELPERS:
Template.newActivityPrePopulate.helpers({

	'thereArePrevMatches': function() {
		return Template.instance().prevMatches.get().length > 0;
	},

	'matches': function() {
		return Template.instance().prevMatches.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.newActivityPrePopulate.events({

	'change #pre_populate': function(event, instance) {
		event.preventDefault();

		// Get id of the selected match
		var matchId = instance.$('#pre_populate').val();
		if (matchId === '') return;

		// Query selected match
		var prevMatches = instance.prevMatches.get();
		var match = Matches.findOne(matchId);

		if (!match) {
			throw new Meteor.Error('no-match-found-on-prepopulate');
			return;
		}

		// Get match location and reset map and marker
		var lat = match.loc.coordinates[1];
		var lng = match.loc.coordinates[0];
		Session.set('mapCenterLatPREPO', lat);
		Session.set('mapCenterLngPREPO', lng);
		Session.set('resetMapPREPO', true); // triggers autorun functions at new_match_map.js

		// Get match sport and reset marker icon
		Session.set('markerTypeNEWMATCH', match.sport);
		Session.set('resetMarkerNEWMATCH', true); // triggers autorun functions at new_match_map.js

		// Get matchId and pre-populate admin and participants
		Session.set('matchId_PREPO', match._id);
		Session.set('resetSelect2_PREPO', false);
		Session.set('resetSelect2_PREPO', true); // triggers autorun functions at new_match_map.js

		// TODO: introduce session var in order to insert the following logic within each formComponent template
		// Fulfil the remaining fields
		$('#sport').val(match.sport);
		$('#privacy').val(match.privacyOnCreation);
		$('#address').val(match.address);
		// do nothing with datepicker
		$('#time').val(match.time);
		$('#cost').val(match.cost);
		$('#address').val(match.address);
		$('#title').val(match.title);
		// Participants
		/*if (isTeamSport(match.sport)) {
			$('#field_size').val(match.fieldSize);
		} else {*/
			$('#max_participants').val(match.fieldSize);
		//}
		// Elective fields
		var duration = match.duration || '';
		$('#duration').val(duration);
		var fieldWebsite = match.fieldWebsite || '';
		$('#field_website').val(fieldWebsite);
		var fieldPhone = match.fieldPhone || '';
		$('#field_phone').val(fieldPhone);
		var fieldType = match.fieldType || '';
		$('#field_type').val(fieldType);
		var description = match.description || '';
		$('#description').val(description);
		var repeat = match.repeat || false;
		$('#repeat').prop('checked', repeat);
/*
		var duration = !_.isUndefined(match.duration) ? match.duration : '';
		$('#duration').val(duration);
		var fieldWebsite = !_.isUndefined(match.fieldWebsite) ? match.fieldWebsite : '';
		$('#field_website').val(fieldWebsite);
		var fieldPhone = !_.isUndefined(match.fieldPhone) ? match.fieldPhone : '';
		$('#field_phone').val(fieldPhone);
		var fieldType = !_.isUndefined(match.fieldType) ? match.fieldType : '';
		$('#field_type').val(fieldType);
		var description = !_.isUndefined(match.description) ? match.description : '';
		$('#description').val(description);
		var repeat = !_.isUndefined(match.repeat) ? match.repeat : false;
		$('#repeat').prop('checked', repeat);
*/
	}

});
