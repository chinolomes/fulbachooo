//----------------------------------------------------------------------
// ON CREATION:
Template.newActivity.onCreated(function() {

	// See router onBeforeAction: every time the user comes to new_activity from a different route, reset session vars
	if(Session.equals('resetAll_FORM_COMPONENT', true)){
		Session.set('sport_FORM_COMPONENT', null);
		Session.set('privacy_FORM_COMPONENT', null);
		Session.set('address_FORM_COMPONENT', null);
		Session.set('time_FORM_COMPONENT', null);
		Session.set('duration_FORM_COMPONENT', null);
		Session.set('cost_FORM_COMPONENT', null);
		Session.set('fieldSize_FORM_COMPONENT', null);
		Session.set('maxParticipants_FORM_COMPONENT', null);
		Session.set('title_FORM_COMPONENT', null);
		Session.set('description_FORM_COMPONENT', null);
		Session.set('fieldType_FORM_COMPONENT', null);
		Session.set('fieldPhone_FORM_COMPONENT', null);
		Session.set('fieldWebsite_FORM_COMPONENT', null);
		Session.set('repeat_FORM_COMPONENT', null);
		Session.set('coords_FORM_COMPONENT', null);
		Session.set('people_in_team_A_SELECT2', null);
		Session.set('people_in_team_B_SELECT2', null);
		Session.set('people_in_no_team_sport_SELECT2', null);
		Session.set('activity_admin_SELECT2', null);
		Session.set('selectedDATEPICKER', null);

		Session.set('resetAll_FORM_COMPONENT', false);
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.newActivity.events({

	'focus textarea': function(event, instance) {
		event.preventDefault();
		if (!Meteor.userId()) {
			var msg = {status: 'error', text: TAPi18n.__('Log_In_To_Create_Activity_You_Will_Lose_Your_Data')};
			processMessage(msg);
		}
	},

	'focus input': _.throttle(function(event, instance) {
		event.preventDefault();
		if (!Meteor.userId() && event.type !== 'focus') {
			var msg = {status: 'error', text: TAPi18n.__('Log_In_To_Create_Activity_You_Will_Lose_Your_Data')};
			processMessage(msg);
		}
	}, 5000),

	'change select': function(event, instance) {
		event.preventDefault();
		if (!Meteor.userId()) {
			var msg = {status: 'error', text: TAPi18n.__('Log_In_To_Create_Activity_You_Will_Lose_Your_Data')};
			processMessage(msg);
		}
	}

});
