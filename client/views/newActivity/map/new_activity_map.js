//======================================================================
// NEW ACTIVITY MAP:
//======================================================================
// REQUIRED: The document where the map is rendered, has to have two elements with
// 'lat' and 'lng' ids respectively.
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.newActivityMap.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Set session variables
	Session.set('resetZoomSliderCIRCLE', false);
	Session.set('mapZoomCIRCLE', '');

   // 3. Initialize reactive variables
   instance.mapIsSet = new ReactiveVar(false);

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.newActivityMap.onRendered(function() {

   // Current template instance
   var instance = this;

	// Initialize google map
   var elemId = 'googleMapMarker'; // DOM element
   /*var mapCenterLat = Session.get('circleCenterLatMATCHES');
   var mapCenterLng = Session.get('circleCenterLngMATCHES');*/
   var mapCenterLat = !Session.equals('coords_FORM_COMPONENT', null) ? Session.get('coords_FORM_COMPONENT', null)[0] : Session.get('circleCenterLatMATCHES');
   var mapCenterLng = !Session.equals('coords_FORM_COMPONENT', null) ? Session.get('coords_FORM_COMPONENT', null)[1] : Session.get('circleCenterLngMATCHES');
   var zoom = 12;
   var map = setMap(elemId, mapCenterLat, mapCenterLng, zoom);
   map = setGPSButton(map);
   instance.mapIsSet.set(true);

	// Set default marker
   var sport = Session.get('sport_FORM_COMPONENT') || DEFAULT_SPORT;
   var draggable = true;
	var marker = createMarker(mapCenterLat, mapCenterLng, map, sport, draggable);

	// Fill form lat and lng values	with the default marker position
	setFormLatLng(marker.getPosition().lat(), marker.getPosition().lng());

	// Add marker drag and drop event listeners
	google.maps.event.addListener(marker, 'drag', function(event) {
		setFormLatLng(event.latLng.lat(), event.latLng.lng());
	});
	google.maps.event.addListener(marker, 'dragend', function(event) {
		setFormLatLng(event.latLng.lat(), event.latLng.lng());
	});

	// Add map-event listener to place the new marker
	google.maps.event.addListener(map, 'click', function(event) {
		marker.setPosition(event.latLng);
		setFormLatLng(event.latLng.lat(), event.latLng.lng());
	});

	// In case map center session variables changes because of GPS button was hit,
	// modify map center and marker position accordingly
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMapGPS', true)) {
			var newCenter = new google.maps.LatLng(Session.get('mapCenterLatGPS'), Session.get('mapCenterLngGPS'));
			map.setCenter(newCenter);
			marker.setPosition(newCenter);
			setFormLatLng(newCenter.lat(), newCenter.lng());
			Session.set('resetMapGPS', false);
		}
	});

	// In case map center session variables changes because of (new match) PRE-POPULATE button was hit,
	// modify map center and marker position accordingly
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMapPREPO', true)) {
			var newCenter = new google.maps.LatLng(Session.get('mapCenterLatPREPO'), Session.get('mapCenterLngPREPO'));
			map.setCenter(newCenter);
			marker.setPosition(newCenter);
			setFormLatLng(newCenter.lat(), newCenter.lng());
			Session.set('resetMapPREPO', false);
		}
	});

	// In case the map or circle changes, reset map and marker position
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMapMATCHES', true)) {
			var newCenter = new google.maps.LatLng(Session.get('circleCenterLatMATCHES'), Session.get('circleCenterLngMATCHES'));
			map.setCenter(newCenter);
			marker.setPosition(newCenter);
			setFormLatLng(newCenter.lat(), newCenter.lng());
			Session.set('resetMapMATCHES', false);
		}
	});

	// In case the user changes sport, change the marker type accordingly
	instance.autorun(function() {
		if (instance.mapIsSet.get() === true && Session.equals('resetMarkerNEWMATCH', true)) {
			marker.setIcon(getMarkerIcon(Session.get('markerTypeNEWMATCH'), 'red'));
			Session.equals('resetMarkerNEWMATCH', false);
		}
	});

});
