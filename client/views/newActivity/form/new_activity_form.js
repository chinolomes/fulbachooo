//======================================================================
// NEW ACTIVITY FORM:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.newActivityForm.onCreated(function() {

   // 1. Current template instance
   var instance = this; // current template instance

   // 2. Initialiaze reactive vars
   instance.loading = new ReactiveVar('wait'); // loading indicator
   /*instance.teamSport = new ReactiveVar(true); // boolean

   // 3. Set reactive var(s)
   instance.autorun(function() {
      var sport = Session.get('markerTypeNEWMATCH'); // reactive source
      instance.teamSport.set(isTeamSport(sport)); // boolean
   });*/

});
//----------------------------------------------------------------------
// HELPERS:
Template.newActivityForm.helpers({

	'loadingStatus': function() {
      return Template.instance().loading.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
/*
TODO:
SOURCE: http://guide.meteor.com/ui-ux.html
Template.appBody.events({
  'click .js-new-list'() {
    const listId = Lists.methods.insert.call((err) => {
      if (err) {
        // At this point, we have already redirected to the new list page, but
        // for some reason the list didn't get created. This should almost never
        // happen, but it's good to handle it anyway.
        FlowRouter.go('home');
        alert('Could not create list.');
      }
    });

    FlowRouter.go('listsShow', { _id: listId }); <-- don't wait for the server response
  }
});
*/
Template.newActivityForm.events({

	'submit form': function(event, instance) {
		event.preventDefault();

      // Variables declaration
      var btn = instance.$('#createMatchButton');

      // Disable button
		btn.prop("disabled", true);

      // Show loading indicator
      instance.loading.set('loading');

		// Check if the user is logged in
		if (!Meteor.user()) {
			//throw new Meteor.Error('You must log in if you want to create a new match');
         toastr.error(TAPi18n.__('Log_In_To_Create_Activity'));
			btn.prop("disabled", false);
         instance.loading.set('error');
			return;
		}

      // Get peopleIn and fieldSize
      var participants = instance.$("#people_in_no_team_sport").select2("val") || [];
      var peopleInTeamA = [];
      var peopleInTeamB = [];
      for (var i = 0; i < participants.length; i++) {
         if (i % 2 === 0) {
            peopleInTeamB.push(participants[i]);
         } else {
            peopleInTeamA.push(participants[i]);
         }
      }
      var fieldSize = parseInt(event.target.max_participants.value.trim(), 10);

      /*console.log('peopleInTeamA: ', peopleInTeamA);
      console.log('peopleInTeamB: ', peopleInTeamB);
      //console.log("selct2 type:", _.isArray(instance.$("#activity_admin").select2("val")));
      console.log("admin array:", instance.$("#activity_admin").select2("val") || []);
      //console.log("admin array.length:", instance.$("#activity_admin").select2("val").length);
      //console.log("admin array[0]:", instance.$("#activity_admin").select2("val")[0]);
      //return;*/

		// Create match object
		var match = new MatchClass(
                     instance.$("#activity_admin").select2("val") || [],          // activity admin (array of ids)
							instance.$('#sport').val(),                                  // sport
							instance.$('.datepicker').datepicker('getUTCDate'),          // date
							event.target.time.value.trim(), 	                            // time
                     event.target.duration.value.trim(), 	                      // duration
							event.target.cost.value.trim(), 	                            // cost
							event.target.address.value.trim(), 	                         // address
							parseFloat($('#lat').val().trim(), 10),                      // lat
							parseFloat($('#lng').val().trim(), 10),                      // lng
							peopleInTeamA, //instance.$("#people_in_team_A").select2("val"),		 // array of ids
							peopleInTeamB, //instance.$("#people_in_team_B").select2("val"),     // array of ids
							[], 							                                     // friendsOf, array
							[], 							                                     // waitingList, array of objects {userId, addedAt}
							fieldSize, //parseInt(event.target.field_size.value.trim(), 10), // fieldSize
							event.target.field_type.value.trim(),                        // fieldType
							event.target.field_phone.value.trim(),                       // fieldPhone
							event.target.field_website.value.trim(),                     // fieldWebsite
							event.target.title.value.trim(),                             // title
							event.target.privacy.value.trim(),	                         // privacy
                     event.target.description.value.trim(),                       // description
                     instance.$('input[name=repeat]:checked').val() === "checked" // repeat
						);

      //match.display();
      //return;

		// Check mandatory fields
		var msg = match.check();
		if (msg.status === 'error') {
			processMessage(msg);
			btn.prop("disabled", false);
         instance.loading.set('error');
			return;
		}

		//var captcha = $('#g-recaptcha-response').val()
		Meteor.call('insertNewActivity', /*captcha,*/ match, function(err, msg) {
				if (err) {
					console.log(err);
					btn.prop("disabled", false);
         		instance.loading.set('error');
					return;
				} else {
					processMessage(msg);
               btn.prop("disabled", false);
               instance.loading.set(msg.status);
					if (msg.status === 'error') {
						return;
					} else if (msg.status === 'success' && !_.isUndefined(msg.response)) {
						Router.go('/activities/activity/' + msg.response);
					}
				}
			}
		); // end Meteor.call
	}

});
