//======================================================================
// ACTIVITIES RESULTS HEADER TEMPLATE:
//======================================================================
// CONTEXT: this = {mapBounds}
// REQUIRED SUBS: 'totalUpcomingActivitiesCounter' + template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.activitiesResultsHeader.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Initialize reactive variables
	instance.totalMatches = new ReactiveVar(0);
   instance.mapBounds = new ReactiveVar(null); // {bottomLeft: [lng, lat], topRight: [lng, lat]}

   // 2. Internal var(s)
   var selector = {
      privacy: 'public',
      status: {
         $ne: 'finished'
      }
   };
	var options = {
		fields: {
			"loc.coordinates": 1
		}
	};

   // 2. Get -reactive- data context and set reactive var(s)
   instance.autorun(function() {
      //var circle = instance.circle.get(); // reactive source
		var mapBounds = Template.currentData().mapBounds; // reactive source
		if (!_.isUndefined(mapBounds.bottomLeft) && !_.isUndefined(mapBounds.topRight)) {
			instance.mapBounds.set(mapBounds);
		}
	});

   // 3. Set total activities in bounds reactive var
   instance.autorun(function() {
		var mapBounds = instance.mapBounds.get();
		if (mapBounds !== null) {
         var sub = instance.subscribe('matchesSportCoords', mapBounds); // reactive source

         if (sub.ready()) {
   			var matches = Matches.find(selector, options).fetch();
   			if (matches) {
   				var count = countMatchesInBounds(matches, mapBounds);
   				instance.totalMatches.set(count);
   			}
         }
		}
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.activitiesResultsHeader.helpers({

	'numberOfMatches': function() {
		return Template.instance().totalMatches.get();
	},

	'plural': function() {
		return Template.instance().totalMatches.get() !== 1 ? true : false;
	},

	'totalNumberOfMatches': function() {		
      var totalUpcomingActivities = CollectionsCounter.findOne({collectionName: 'upcomingActivities'}); // requires 'totalUsersCounter' sub
      return totalUpcomingActivities && totalUpcomingActivities.counter ? totalUpcomingActivities.counter : '?';
	}

});
