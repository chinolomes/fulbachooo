//======================================================================
// POSTS:
//======================================================================
// CONTEXT: {matchId}
// REQUIRED SUBS: template-level
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.posts.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.posts = new ReactiveVar([]); // [post, ...]

	// 2. Get data context
	var matchId = Template.currentData().matchId;

   // 3. Internal vars
   var postOptions = {
      sort: {
         createdAt: -1
      }
   }
   var senderOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };

   // 4. Subscribe to match posts
   instance.autorun(function() {
      //console.log('SUBSCRIBE TO MATCH POSTS');
      var sub = instance.subscribe('matchPosts', matchId); // reactive source also returns user avatar

      // 5. Set reactive variable(s)
      if (sub.ready()) {
         var posts = Posts.find({postedOn: matchId}, postOptions).fetch();

         // Extend post object by including sender avatar
         _.each(posts, function(post) {
            var sender = Meteor.users.findOne(post.createdBy, senderOptions);
            _.extend(post, {sender: sender});
         });
         instance.posts.set(posts);
      }
   });

});
//-----------------------------------------------------------------------
// HELPERS:
Template.posts.helpers({

	'posts': function() {
      return Template.instance().posts.get();
	}

});
