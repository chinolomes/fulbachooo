//======================================================================
// FORM:
//======================================================================
// CONTEXT: this = {matchId}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// EVENTS
Template.postsForm.events({

	'submit form': function(event, instance) {
		event.preventDefault();

		// Variables declaration
		var btn = instance.$('button');
		var textarea = instance.$('textarea');
		var text = textarea.val().trim();

		// Get data context
		var matchId = Template.currentData().matchId;

		// Disable send post button
		btn.prop('disabled', true);

		// Check if the user is logged in
		if (!Meteor.user()) {
			throw new Meteor.Error('Posts Form Template: user not logged in.');
			return;
		}

		// Check post content
		if (text.length === 0) {
			btn.prop('disabled', false);
			toastr.error(TAPi18n.__('Enter_Your_Message'));
			return;
		}
		if (text.length > 2000) {
			btn.prop('disabled', false);
			toastr.error(TAPi18n.__('The_Message_Exceeds_2000_Chars'));
			return;
		}

		Meteor.call('insertPost', text, matchId, function(err) {
				if (err) {
					console.log(err);
				} else {
					textarea.val('');
					btn.prop('disabled', false);
				}
			}
		);
	}
});
