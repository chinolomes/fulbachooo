//======================================================================
// POST ITEM:
//======================================================================
// CONTEXT: {post}
// REQUIRES SUBS: ''
//----------------------------------------------------------------------
// ON CREATION:
Template.postItem.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
	instance.editPost = new ReactiveVar(false); // boolean

});
//----------------------------------------------------------------------
// HELPERS:
Template.postItem.helpers({

	'viewerIsAuthor': function() {
		return this.post.createdBy === Meteor.userId();
	},

	'editPost': function() {
		return Template.instance().editPost.get();
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.postItem.events({

	'click .post-edit': function() {
		// Check
		if (this.post.createdBy && this.post.createdBy !== Meteor.userId()) {
			throw new Meteor.Error('non-authorised', 'You are not logged in or you are not the author of the post.');
         return;
		}

      Template.instance().editPost.set(true);
	},

	'click .post-edit-cancel': function() {
		// Check
		if (this.post.createdBy && this.post.createdBy !== Meteor.userId()) {
			throw new Meteor.Error('non-authorised', 'You are not logged in or you are not the author of the post.');
         return;
		}

      Template.instance().editPost.set(false);
	},

	'click .post-remove': function() {
		// Check
		if (this.post.createdBy && this.post.createdBy !== Meteor.userId()) {
			throw new Meteor.Error('non-authorised', 'You are not logged in or you are not the author of the post.');
         return;
		}

		var postId = this.post._id;

		Meteor.call('removePost', postId, function(err) {
			if (err) {
				console.log(err);
			}
		});
	},

	'click .post-edit-save': function(event, instance) {
		event.preventDefault();

		// Variables declaration
		var textarea = instance.$('textarea');
		var text = textarea.val().trim();

		// Get data context
		var postId = this.post._id;

		// Check if the user is logged in
		if (this.post.createdBy && this.post.createdBy !== Meteor.userId()) {
			throw new Meteor.Error('Posts Form Template: user is not the owner.');
			return;
		}

		// Check post content
		if (text.length === 0) {
			toastr.error(TAPi18n.__('Enter_Your_Message'));
			return;
		}
		if (text.length > 2000) {
			toastr.error(TAPi18n.__('The_Message_Exceeds_2000_Chars'));
			return;
		}

		Meteor.call('editPost', postId, text, function(err) {
				if (err) {
					console.log(err);
				} else {
               instance.editPost.set(false);
            }
			}
		);
	}
});
