//======================================================================
// SUB SECTION TITLE:
//======================================================================
// CONTEXT: {text, [number], [class]}
//-----------------------------------------------------------------------
// HELPERS:
Template.sectionTitle.helpers({

	'classIsSet': function() {
		return !_.isUndefined(this.class);
	}

});
//-----------------------------------------------------------------------
// HELPERS:
Template.subSectionTitle.helpers({

	'numberIsSet': function() {
		return !_.isUndefined(this.number);
	},

	'classIsSet': function() {
		return !_.isUndefined(this.class);
	}

});
