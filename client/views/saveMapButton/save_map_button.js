
//======================================================================
// SAVE MAP BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
// TODO: re-write this component so that the user can olny hit the save region btn
// once the map is idle. Otherwise, the location is re-set to the previous location :(
// https://developers.google.com/maps/documentation/javascript/controls
//----------------------------------------------------------------------
// ON CREATION:
Template.saveMapButton.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize session var(s)
   Session.set('saveRegionBTN', false);

   // 2. Initialize reactive var(s)
	instance.regionChanged = new ReactiveVar(false); // compares the new region with the region stored at user.geo

   // 3. Internal var(s)
   var options = {
      fields: {
         "geo.loc.coordinates": 1,
         "geo.radius": 1,
         "geo.zoom": 1
      }
   };

   // TODO: there is no need to check if the position changes all the time.
   // Create a function containing that logic and only check for changes once the
   // save region btn has been hit
	// 3. Subscribe to current user geo
	instance.autorun(function() {
      if (Meteor.userId()) {

         var sub = instance.subscribe('curUserGeo'); // reactive source
         var bottomLeft = Session.get('bottomLeftMapBounds'); // reactive source
         var topRight = Session.get('topRightMapBounds'); // reactive source
         //console.log('save map btn autorun');
         //console.log('bottomLeft: ', bottomLeft);
         //console.log('topRight: ', topRight);

         // Check if the region has changed and set reactive var(s) accordingly
         if (sub.ready() && !_.isUndefined(bottomLeft) && !_.isUndefined(topRight)) {
            //instance.regionChanged.set(false); // re-initialize reactive var
            var user = Meteor.users.findOne({_id: Meteor.userId()}, options);

            if (userGeoOk(user)) { // rename geoDataIsReady()
               // Get user region and map zoom
               var lat = user.geo.loc.coordinates[1];
               var lng = user.geo.loc.coordinates[0];
               var rad = user.geo.radius;
               var zoom = user.geo.zoom;
               //console.log('lat: ', lat);
               //console.log('lng: ', lng);

               // Get current map bounds center
               var boundsCenter = getMapCenterBounds(bottomLeft, topRight); // center = {lat, lng}.

               // Check for region changes and set reactive var
               var changed = Math.abs(boundsCenter.lat - lat) > 0.01 || Math.abs(boundsCenter.lng - lng) > 0.01 || Session.get('mapZoomMATCHES') !== zoom;
               instance.regionChanged.set(changed);
               //console.log("regionChanged: ", instance.regionChanged.get());

            }
         }
      }
	});

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.saveMapButton.onRendered(function() {

   // Current template instance
   var instance = this;

   // Every time the save region button is hit, save the new region into
   // user.geo and display circle on map
   instance.autorun(function() {
      if (Session.equals('saveRegionBTN', true)) {

         if (instance.regionChanged.get() === false) {
            //console.log('no changes has been made in the region');
            Session.set('showCircleGOOGLE_MAPS', true);
            var msg = {status: 'success', text: 'Region_Saved_Successfully_Explanation', title: 'Changes_Were_Made_Successfully'};
            processMessage(msg);
            Session.set('saveRegionBTN', false);
            return;
         }

         // Get map bounds
         var bottomLeft = Session.get('bottomLeftMapBounds');
         var topRight = Session.get('topRightMapBounds');
         if (_.isUndefined(bottomLeft) || _.isUndefined(topRight)) {
            throw new Meteor.Error('Save map btn: bttomLeft, topRight undef');
            return;
         }

         // Calculate circle center
         var circleCenterLat = bottomLeft[1] + (topRight[1] - bottomLeft[1]) / 2;
         var circleCenterLng = bottomLeft[0] + (topRight[0] - bottomLeft[0]) / 2;
         //console.log("circleCenterLat: ", circleCenterLat);
         //console.log("circleCenterLng: ", circleCenterLng);


         ////////////
         // CHECKS //
         ////////////
         // TODO: the following logic has to be inside a function
         if (circleCenterLat < -85) {
            circleCenterLat = -85;
         } else if (circleCenterLat > 85) {
            circleCenterLat = 85;
         }
         if (circleCenterLng < -180) {
         	circleCenterLng = -180;
         } else if (circleCenterLng > 180) {
         	circleCenterLng = 180;
         }

         // Calculate circle radius to fit within bounds (max 20 km)
         //    ---------- tR
         //    |        |
         //    |        | d2
         //    |        |
         // bL ---------- bR
         //        d1
         var bL = new google.maps.LatLng(bottomLeft[1], bottomLeft[0]);
         var bR = new google.maps.LatLng(topRight[1], bottomLeft[0]);
         var tR = new google.maps.LatLng(topRight[1], topRight[0]);
         var d1 = Math.abs(google.maps.geometry.spherical.computeDistanceBetween(bR, bL) / 1000); // in km!
         var d2 = Math.abs(google.maps.geometry.spherical.computeDistanceBetween(tR, bR) / 1000); // in km!
         var r1 = Math.floor(d1/2);
         var r2 = Math.floor(d2/2);
         var rad = Math.min(r1, r2, 20); // 20km max
         rad = rad === 0 ? 1 : rad; // at least rad = 1 km.

         // Adjust zoom if necesary (keep it within 10 and 14)
         var zoom = Math.max(Session.get('mapZoomMATCHES'), 10);
         zoom = zoom > 14 ? 14 : zoom;

         Meteor.call('saveNearbyRegion', circleCenterLat, circleCenterLng, rad, zoom, function(err, msg) {
            if (err) {
               console.log(err);
            } else {
               processMessage(msg);
               //console.log("save map button return callback");
               // TODO: replace the following code with a subscription in google maps (?)
               // see client/autrun/listen_to_cur_user_location.js
               Session.set('circleCenterLatMATCHES', circleCenterLat);
               Session.set('circleCenterLngMATCHES', circleCenterLng);
               Session.set('mapZoomMATCHES', zoom);
               Session.set('circleRadiusMATCHES', rad);
               Session.set('showCircleGOOGLE_MAPS', true); // triggers autorun function at users/activities_map.js
               //console.log("AFTER CALLBACK: ", [circleCenterLng, circleCenterLat]);

            }
         });
         Session.set('saveRegionBTN', false); // don't wait for the callback!
      }
   });
	//}
});
//----------------------------------------------------------------------
// EVENTS:
Template.saveMapButton.events({

	'click a.navbar-item-wrapper': function() {
		if (Meteor.userId()) {
         Session.set('saveRegionBTN', true); // triggers autorun function at save_map_button.js
      } else {
         var msg = {status: 'warning', text: 'Login_To_Use_This_Feature'};
         processMessage(msg);
      }
   }

});
//----------------------------------------------------------------------
// AUXILIARY FUNCTIONS
var userGeoOk = function(user) {

   //return !_.isNull(user) && !_.isUndefined(user) && !_.isUndefined(user.geo) && !_.isUndefined(user.geo.radius) && !_.isUndefined(user.geo.zoom) && !_.isUndefined(user.geo.loc) && !_.isUndefined(user.geo.loc.coordinates);
   return user && user.geo && user.geo.radius && user.geo.zoom && user.geo.loc && user.geo.loc.coordinates;
}



/*
//======================================================================
// SAVE MAP BUTTON TEMPLATE:
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
Template.saveMapButton.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize session var(s)
   Session.set('saveRegionBTN', false);


   // comment from here...
   // 2. Initialize reactive var(s)
	instance.regionChanged = new ReactiveVar(false); // compares the new region with the region stored at user.geo

   // 3. Internal var(s)
   var options = {
      fields: {
         "geo.loc.coordinates": 1,
         "geo.radius": 1,
         "geo.zoom": 1
      }
   };

	// 3. Subscribe to current user geo
	instance.autorun(function() {
      if (Meteor.userId()) {

         var sub = instance.subscribe('curUserGeo'); // reactive source
         var bottomLeft = Session.get('bottomLeftMapBounds'); // reactive source
         var topRight = Session.get('topRightMapBounds'); // reactive source
         console.log('save map btn autorun');
         console.log('bottomLeft: ', bottomLeft);
         console.log('topRight: ', topRight);

         // Check if the region has changed and set reactive var(s) accordingly
         if (sub.ready() && !_.isUndefined(bottomLeft) && !_.isUndefined(topRight)) {
            //instance.regionChanged.set(false); // re-initialize reactive var
            var user = Meteor.users.findOne({_id: Meteor.userId()}, options);

            if (userGeoOk(user)) { // rename geoDataIsReady()
               // Get user region and map zoom
               var lat = user.geo.loc.coordinates[1];
               var lng = user.geo.loc.coordinates[0];
               var rad = user.geo.radius;
               var zoom = user.geo.zoom;
               //console.log('lat: ', lat);
               //console.log('lng: ', lng);

               // Get current map bounds center
               var boundsCenter = getMapCenterBounds(bottomLeft, topRight); // center = {lat, lng}.

               // Check for region changes and set reactive var
               var changed = Math.abs(boundsCenter.lat - lat) > 0.01 || Math.abs(boundsCenter.lng - lng) > 0.01 || Session.get('mapZoomMATCHES') !== zoom;
               instance.regionChanged.set(changed);
               console.log("regionChanged: ", instance.regionChanged.get());

            }
         }
      }
	});
   // ... comment to here

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.saveMapButton.onRendered(function() {

   // Current template instance
   var instance = this;

   var div = $('#save-region-btn'); // svae region btn container
   console.log("div: ", div);

   //div.hide();
   div.css("display", "none");

   // Every time the save region button is hit, save the new region into
   // user.geo and display circle on map
   instance.autorun(function() {
      if (Session.equals('saveRegionBTN', true)) {
         div.hide();
         if (!Meteor.userId()) {
            var msg = {status: 'warning', text: 'Login_To_Use_This_Feature'};
            processMessage(msg);
         } //else if (instance.regionChanged.get() === false) {
            //console.log('no changes has been made in the region');
            //Session.set('showCircleGOOGLE_MAPS', true);
            //var msg = {status: 'success', text: 'Region_Saved_Successfully_Explanation', title: 'Changes_Were_Made_Successfully'};
            //processMessage(msg);
            //Session.set('saveRegionBTN', false);
            //return;
         //}

         // Get map bounds
         var bottomLeft = Session.get('bottomLeftMapBounds');
         var topRight = Session.get('topRightMapBounds');
         if (!bottomLeft || !topRight) {
            throw new Meteor.Error('Save map btn: bttomLeft or topRight undef');
            return;
         }

         // Calculate circle center
         var circleCenterLat = bottomLeft[1] + (topRight[1] - bottomLeft[1]) / 2;
         var circleCenterLng = bottomLeft[0] + (topRight[0] - bottomLeft[0]) / 2;
         console.log("circleCenterLat: ", circleCenterLat);
         console.log("circleCenterLng: ", circleCenterLng);


         ////////////
         // CHECKS //
         ////////////
         // TODO: the following logic has to be inside a function
         if (circleCenterLat < -85) {
            circleCenterLat = -85;
         } else if (circleCenterLat > 85) {
            circleCenterLat = 85;
         }
         if (circleCenterLng < -180) {
         	circleCenterLng = -180;
         } else if (circleCenterLng > 180) {
         	circleCenterLng = 180;
         }

         // Calculate circle radius to fit within bounds (max 20 km)
         //    ---------- tR
         //    |        |
         //    |        | d2
         //    |        |
         // bL ---------- bR
         //        d1
         var bL = new google.maps.LatLng(bottomLeft[1], bottomLeft[0]);
         var bR = new google.maps.LatLng(topRight[1], bottomLeft[0]);
         var tR = new google.maps.LatLng(topRight[1], topRight[0]);
         var d1 = Math.abs(google.maps.geometry.spherical.computeDistanceBetween(bR, bL) / 1000); // in km!
         var d2 = Math.abs(google.maps.geometry.spherical.computeDistanceBetween(tR, bR) / 1000); // in km!
         var r1 = Math.floor(d1/2);
         var r2 = Math.floor(d2/2);
         var rad = Math.min(r1, r2, 20); // 20km max
         rad = rad === 0 ? 1 : rad; // at least rad = 1 km.

         // Adjust zoom if necesary (keep it within 10 and 14)
         var zoom = Math.max(Session.get('mapZoomMATCHES'), 10);
         zoom = zoom > 14 ? 14 : zoom;

         Meteor.call('saveNearbyRegion', circleCenterLat, circleCenterLng, rad, zoom, function(err, msg) {
            if (err) {
               console.log(err);
            } else {
               processMessage(msg);
               console.log("save map button return callback");
               // TODO: replace the following code with a subscription in google maps (?)
               //Session.set('circleCenterLatMATCHES', circleCenterLat);
               //Session.set('circleCenterLngMATCHES', circleCenterLng);
               Session.set('mapZoomMATCHES', zoom);
               Session.set('circleRadiusMATCHES', rad);
               Session.set('showCircleGOOGLE_MAPS', true); // triggers autorun function at users/activities_map.js
               console.log("AFTER CALLBACK: ", [circleCenterLng, circleCenterLat]);

            }
         });
         Session.set('saveRegionBTN', false); // don't wait for the callback!
      }
   });
	//}
});
//----------------------------------------------------------------------
// EVENTS:
Template.saveMapButton.events({

	'click a.navbar-item-wrapper': function() {
		if (Meteor.userId()) {
         Session.set('saveRegionBTN', true); // triggers autorun function at save_map_button.js
      } else {
         var msg = {status: 'warning', text: 'Login_To_Use_This_Feature'};
         processMessage(msg);
      }
   }

});
//----------------------------------------------------------------------
// AUXILIARY FUNCTIONS
var userGeoOk = function(user) {

   //return !_.isNull(user) && !_.isUndefined(user) && !_.isUndefined(user.geo) && !_.isUndefined(user.geo.radius) && !_.isUndefined(user.geo.zoom) && !_.isUndefined(user.geo.loc) && !_.isUndefined(user.geo.loc.coordinates);
   return user && user.geo && user.geo.radius && user.geo.zoom && user.geo.loc && user.geo.loc.coordinates;
}
*/
