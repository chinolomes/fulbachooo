//======================================================================
// LOADING TEMPLATE:
//======================================================================
// CONTEXT: this = {status}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.loading.helpers({

   'status': function(value) {
      return this.status === value;
   }

});


/*
Template.loading.helpers({

   'loading': function() {
      return this.status === 'loading';
   },

   'success': function() {
      return this.status === 'success';
   },

   'error': function() {
      return this.status === 'error';
   },

   'wait': function() {
      return this.status === 'wait';
   }

});
*/
