//======================================================================
// LANG CARD:
//======================================================================
// CONTEXT: this = {langName, lang, flag}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.langCard.helpers({

	'active': function() {
		return TAPi18n.getLanguage() === this.lang ? 'active' : '';
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.langCard.events({

   'click .anchorWrap': function() {
		var lang = this.lang;
		setLocale(lang);
		saveLocale(lang);
   }

});
