//======================================================================
// LANGUAGES
//======================================================================
// CONTEXT: this = {}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
// ON CREATION:
/*Template.languages.onCreated(function() {

	// Current template instance
	var instance = this;

	// Initialize reactive vars
	instance.savedLang = new ReactiveVar(null); // 'en', 'es', ...

	// Subscribe to user language
	instance.autorun(function() {
		if (Meteor.userId()) {
			//var sub = instance.subscribe('curUserLang');
			var sub = instance.subscribe('curUserProfile'); // includes user language

			// Set reactive vars
			if (sub.ready()) {
				var curUser = Meteor.users.findOne(Meteor.userId(), {fields: {'profile.language': 1}});
				var lang = curUser && curUser.profile && curUser.profile.language ? curUser.profile.language : 'en';
				instance.savedLang.set(lang);
			}
		}
	});

});*/
//----------------------------------------------------------------------
// EVENTS:
/*Template.languages.events({

	'click .swapLang': function(event, instance) {
		var lang = event.target.id;
		saveLanguage(lang);
	}

});*/
//----------------------------------------------------------------------
// ON DESTROYED:
/*Template.languages.onDestroyed(function() {

	// Current template instance
	var instance = this;

	// Get last chosen lang
	var newLang = TAPi18n.getLanguage();

	// Get reactive vars
	var savedLang = instance.savedLang.get();

	if (!Meteor.userId()) {
		Cookie.set('langFULBACHO', newLang, {months: 1});
	} else {
   	if (newLang !== savedLang) {
			//console.log('SAVE LANG');
   		Meteor.call('changeUserLanguage', newLang, function(err) {
   			if (err) {
   				console.log(err);
   			}
   		});
   	}
	}

});*/
