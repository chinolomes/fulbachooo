//======================================================================
// ACTIVITY CARD DATA TEMPLATE:
//======================================================================
// CONTEXT: this = {match}.
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.activityCardData.helpers({

	'playersCounter': function() {
		var match = this.match;
		var totalPlayers = 2 * match.fieldSize;
		//var enrolledPlayers = totalPlayers - getPeopleMissing(this.match);
		var enrolledPlayers = match.peopleIn.length + match.friendsOf.length + match.waitingList.length;
		return enrolledPlayers + ' / ' + totalPlayers;
	},

	/*'posts': function() {
		return Posts.find({postedOn: this.match._id}).count() > 0;
	},

	'numberOfPosts': function() {
		return Posts.find({postedOn: this.match._id}).count();
	},*/

	'fieldTypeIsSet': function() {
		return !_.isUndefined(this.match.fieldType) && this.match.fieldType !== '';
	},

	'isPublic': function() {
		return this.match.privacy === 'public';
	}

});
