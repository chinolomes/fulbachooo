//======================================================================
// CALENDAR:
//======================================================================
// CONTEXT: this = {type, color, date, time, class}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.calendar.helpers({

	'flat': function() {
		return this.type === 'flat';
	},

	'square': function() {
		return this.type === 'square';
	},

	'colorIsSet': function() {
		return !_.isUndefined(this.color) && this.color === 'green' || 'red';
	},

	'classIsSet': function() {
		return !_.isUndefined(this.class);
	}

});
