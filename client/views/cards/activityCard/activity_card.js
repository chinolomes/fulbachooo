//======================================================================
// ACTIVITY CARD:
//======================================================================
// REQUIRED: Set Session.setDefault('curMatch', null) somewhere in your app
// + set Session.set('curMatch', null) at the parent template.
// CONTEXT: this = {match, type, [border]}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.activityCard.helpers({

	'matchStatus': function() {
		return this.match && this.match.status && this.match.status === 'active' ? 'green' : 'red';
	},

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	},

	'active': function() {
		return Session.equals('curMatch', this.match._id) ? 'active' : '';
	},

	'withBorder': function() {
		return this.border && this.border === true;
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.activityCard.events({

	'mouseover .matchCard': function() {
		Session.set('curMatch', this.match._id);
	},

	'mouseout .matchCard': function() {
		Session.set('curMatch', null);
	}

});
