//======================================================================
// PARTICIPANTS:
//======================================================================
// CONTEXT: {matchId}
// REQUIRED SUBS: 'match' (actually 'matchesInBounds')
//----------------------------------------------------------------------
// ON CREATION:
Template.participants.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.players = new ReactiveVar([]); // [userObj, userObj, ...]

   // 2. Internal vars
   var matchOptions = {
      fields: {
         peopleIn: 1,
         friendsOf: 1,
         waitingList: 1
      }
   };
   var playerOptions = {
      fields: {
      	"services.twitter.profile_image_url_https": 1,
      	"services.facebook.id": 1,
      	"services.google.picture": 1,
         "profile.name": 1
      }
   };

   // 3. Get -reactive- data context
   instance.autorun(function() {
      var matchId = Template.currentData().matchId;

      // 4. Set reactive vars
      var match = Matches.findOne(matchId, matchOptions); // reactive source
      var players = [];
      if (!_.isUndefined(match)) {
         // Get enrolled players ids
         var peopleIn = _.pluck(match.peopleIn, "userId"); // match.peopleIn[i] = {userId, addedAt, team}
         var friendsOf = _.pluck(match.friendsOf, "userId"); // match.friendsOf[i] = {userId, addedAt, team, name}
         var waitingList = _.uniq(_.pluck(match.waitingList, "userId")); // match.waitingList[i] = {userId, addedAt, team}. In case a player is enrolled in both wl keep only one record
         var playersIdsRep = peopleIn.concat(friendsOf).concat(waitingList); // allow repeated ids

         // Query player object from each playerId
         _.each(playersIdsRep, function(playerId) {
            var player = Meteor.users.findOne(playerId, playerOptions);
            players.push(player);
         });
      }
      instance.players.set(players);
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.participants.helpers({

	'players': function() {
      return Template.instance().players.get();
	}

});
