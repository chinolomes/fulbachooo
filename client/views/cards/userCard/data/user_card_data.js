//======================================================================
// USER CARD DATA TEMPLATE:
//======================================================================
// CONTEXT: this = {player}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.userCardData.helpers({

	'dominantLegIsSet': function() {
		return this.player && this.player.profile && this.player.profile.dominantLeg && this.player.profile.dominantLeg !== '' && this.player.profile.dominantLeg !== null;
	},

	'dominantHandIsSet': function() {
		return this.player && this.player.profile && this.player.profile.dominantHand && this.player.profile.dominantHand !== '' && this.player.profile.dominantHand !== null;
	},

	'birthdayIsSet': function() {
		return this.player && this.player.profile && this.player.profile.birthday && this.player.profile.birthday !== '' && this.player.profile.birthday !== null;
	},

	'genderIsSet': function() {
		return this.player && this.player.profile && this.player.profile.gender && this.player.profile.gender !== '' && this.player.profile.gender !== 'other' && this.player.profile.gender !== null;
	},

	'selfDescriptionIsSet': function() {
		return this.player && this.player.profile && this.player.profile.selfDescription && this.player.profile.selfDescription !== '' && this.player.profile.selfDescription !== null;
	}

});
