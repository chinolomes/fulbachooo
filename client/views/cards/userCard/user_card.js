//======================================================================
// USER CARD:
//======================================================================
// REQUIRED: Set Session.setDefault('curPlayer', '') somewhere in your app.
// Set Session.set('curPlayer', '') at the parent template.
// CONTEXT: this = {player, type, [border]}
// REQUIRED SUBS: ''
//----------------------------------------------------------------------
// HELPERS:
Template.userCard.helpers({

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	},

	'active': function() {
		return Session.equals('curPlayer', this.player._id) ? 'active' : '';
	},

	'withBorder': function() {
		return this.border && this.border === true;
	}

});
//----------------------------------------------------------------------
// EVENTS:
Template.userCard.events({

	'mouseover .playerCard': function() {
		Session.set('curPlayer', this.player._id);
	},

	'mouseout .playerCard': function() {
		Session.set('curPlayer', null);
	}

});
