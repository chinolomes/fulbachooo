//======================================================================
// ADVERTISEMENT CARD:
//======================================================================
// CONTEXT: this = {location, type}
// REQUIRED SUBS: template-level
//----------------------------------------------------------------------
var handler = null;
// ON CREATION:
Template.adCard.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Initialize reactive variables
	instance.adsArray = new ReactiveVar([]);
   instance.curAd = new ReactiveVar({});

   // 3. Get -reactive- data context and set reactive var(s)
   instance.autorun(function() {
      var adsArray = [];
      var loc = { // reactive source
         lat: Session.get('circleCenterLatMATCHES'),
         lng: Session.get('circleCenterLngMATCHES')
      };
		if (!!loc.lat && !!loc.lng) {
			var subs = instance.subscribe('ads', loc);

         if (subs.ready()) {
            var ads = Ads.find({}).fetch();
            if (ads) {
            	// Filter out ads by location
               // We use this because mini-mongo does not support $geoWithin query
            	var result = calculateAdsInCircle(ads, loc);
               adsArray = result.selected;
               //console.log('result.total: ', result.total);
               if (result.total > 0) {
                  instance.curAd.set(adsArray[0]);
               }
         	}
            instance.adsArray.set(adsArray);
         }
		}
	});

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.adCard.onRendered(function() {

   // 1. Current template instance
   var instance = this;

   instance.autorun(function() {
      // Clear previous Meteor.setInterval if any
      if (!!handler) {
         //console.log('clear handler');
         Meteor.clearInterval(handler);
      }
      var i = 1; // start with the second ad, if any
      var adsArray = instance.adsArray.get();
      //console.log('adsArray: ', adsArray);
      var limit = adsArray.length;

      //console.log('limit: ', limit);
      if (limit === 1) {
         instance.curAd.set(adsArray[0]);
         //console.log('companyName: ', instance.curAd.get().companyName);

      } else if (limit > 1) {
         // Save handler to kill function on change
         handler = Meteor.setInterval(function() {
            instance.curAd.set(adsArray[i]);
            //console.log('companyName: ', instance.curAd.get().companyName);
            i++;
            if (i === limit) {i = 0}
            //console.log('i: ', i);
         }, 5000);
         //console.log('handler: ', handler);
      }
   });

});
//----------------------------------------------------------------------
// ON DESTROYED:
Template.adCard.onDestroyed(function() {

   // 1. Current template instance
   var instance = this;

   if (!!handler) {
      //console.log('clear handler');
      Meteor.clearInterval(handler);
   }

});
//----------------------------------------------------------------------
// HELPERS:
Template.adCard.helpers({

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	},

   'adsNotEmpty': function() {
      //console.log('adsNotEmpty: ', Template.instance().adsArray.get().length > 0);
   	return Template.instance().adsArray.get().length > 0;
   },

   'mobileImg': function() {
      //console.log('curAd.companyName: ', Template.instance().curAd.get().companyName);
   	return Template.instance().curAd.get().mobileImg;
   },

   'desktopImg': function() {
      //console.log('curAd.companyName: ', Template.instance().curAd.get().companyName);
   	return Template.instance().curAd.get().desktopImg;
   },

   'adAnchor': function() {
   	return Template.instance().curAd.get().anchor;
   },

   'external': function() {
      var anchor = Template.instance().curAd.get().anchor;
   	return anchor.substr(0,7) === 'http://' || anchor.substr(0,8) === 'https://';
   }

});


/*
Template.adCard.onCreated(function() {

   // 1. Current template instance
   var instance = this;

   // 2. Initialize reactive variables
	instance.adsArray = new ReactiveVar([]);
   instance.curAd = new ReactiveVar({});

   // 3. Get -reactive- data context and set reactive var(s)
   instance.autorun(function() {
      var loc = { // reactive source
         lat: Session.get('circleCenterLatMATCHES'),
         lng: Session.get('circleCenterLngMATCHES')
      };
		if (!!loc.lat && !!loc.lng) {
			var subs = instance.subscribe('ads', loc);

         if (subs.ready()) {
            var ads = Ads.find({}).fetch();
               instance.adsArray.set(ads);
         }
		}
	});

});
//----------------------------------------------------------------------
// ON RENDERED:
Template.adCard.onRendered(function() {

   // 1. Current template instance
   var instance = this;

   instance.autorun(function() {
      var i = 0;
      var adsArray = instance.adsArray.get();
      var limit = adsArray.length;

      console.log('limit: ', limit);
      if (limit > 0) {
         setInterval(function() {
            console.log('set curAd!');
            instance.curAd.set(adsArray[i]);
            console.log('companyName: ', instance.curAd.get().companyName);
            i++;
            if (i === limit) {i = 0}
            console.log('i: ', i);
         }, 5000);
      }
   });

});
//----------------------------------------------------------------------
// HELPERS:
Template.adCard.helpers({

	'mobile': function() {
		return this.type && this.type === 'mobile';
	},

	'desktop': function() {
		return this.type && this.type === 'desktop';
	},

   'adsNotEmpty': function() {
      console.log('adsNotEmpty: ', Template.instance().adsArray.get().length > 0);
   	return Template.instance().adsArray.get().length > 0;
   },

   'mobileImg': function() {
      console.log('curAd.companyName: ', Template.instance().curAd.get().companyName);
   	return Template.instance().curAd.get().mobileImg;
   },

   'desktopImg': function() {
      console.log('curAd.companyName: ', Template.instance().curAd.get().companyName);
   	return Template.instance().curAd.get().desktopImg;
   },

   'adAnchor': function() {
   	return Template.instance().curAd.get().anchor;
   }

});
*/
