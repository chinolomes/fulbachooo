//======================================================================
// RADIUS SLIDER:
//======================================================================
// CONTEXT: this = {initialValue}
// REQUIRED SUBS: ''
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// ON CREATION:
Template.radiusSlider.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.sliderValue = new ReactiveVar(0);

	// 2. Initialize session variables
	Session.set('circleRadiusRADBTN', 0);
	Session.set('resetCircleRADBTN', false);

   // 3. Update reactive variables in case the data context changes
   instance.autorun(function() {
      var initialValue = Template.currentData().initialValue;
      //console.log('SLIDER, INITIAL VALUE: ', initialValue);
      Template.instance().sliderValue.set(initialValue);
   });

});
//----------------------------------------------------------------------
// RENDERED:
Template.radiusSlider.onRendered(function() {

   // Current template instance
   var instance =  this;

	// On radius slider change, reset template instance
	instance.$("#radius_slider").noUiSlider({
		start: [instance.sliderValue.get()],
		step: 1,
		behaviour: 'tap-drag',
		range: {
			'min': [0],
			'max': [20]
		},
		format: wNumb({
			decimals: 0
		})
	}).on('change', function (ev, val) {
      instance.sliderValue.set(val);
		Session.set('circleRadiusRADBTN', val);
		Session.set('resetCircleRADBTN', true); // triggers autorun function in google maps
	});

	// On template instance change, reset slider value
	instance.autorun(function() {
		instance.$('#radius_slider').val(instance.sliderValue.get()); // fires off on change event listener
	});

});
//---------------------------------------------------------------------
Template.radiusSlider.helpers({

	'sliderValue': function () {
		return Template.instance().sliderValue.get();
	}

});
