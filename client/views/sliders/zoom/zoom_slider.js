//======================================================================
// ZOOM SLIDER:
//======================================================================
// CONTEXT: this = {initialValue}
// REQUIRED SUBS: ''
// SOURCE: https://www.discovermeteor.com/blog/template-level-subscriptions/
//----------------------------------------------------------------------
// CREATED:
Template.zoomSlider.onCreated(function() {

   // 0. Current template instance
   var instance = this;

   // 1. Initialize reactive variables
   instance.sliderValue = new ReactiveVar(0);

	// 2. Initialize session variables
	Session.set('mapZoomZOOMBTN', 0);
	Session.set('resetMapZOOMBTN', false);

   // 3. Update reactive variables in case the data context changes
   instance.autorun(function() {
      var initialValue = Template.currentData().initialValue;
      Template.instance().sliderValue.set(initialValue);
   });

});
//----------------------------------------------------------------------
// RENDERED:
Template.zoomSlider.onRendered(function() {

   // Current template instance
   var instance =  this;

	// On radius slider change, reset template instance
	instance.$("#zoom_slider").noUiSlider({
		start: [instance.sliderValue.get()],
		step: 1,
		behaviour: 'tap-drag',
		range: {
			'min': [0],
			'max': [21]
		},
		format: wNumb({
			decimals: 0
		})
	}).on('change', function (ev, val) {
      instance.sliderValue.set(val);
		Session.set('mapZoomZOOMBTN', val);
		Session.set('resetMapZOOMBTN', true); // triggers autorun function in google_maps.js
	});

	// On template instance change, reset slider value
	instance.autorun(function() {
		instance.$('#zoom_slider').val(instance.sliderValue.get());
	});

	// In case the user uilizes google-map-embed-zoom
	instance.autorun(function() {
		if (Session.equals('resetZoomSliderCIRCLE', true)) {
			instance.sliderValue.set(Session.get('mapZoomCIRCLE'));
         Session.equals('resetZoomSliderCIRCLE', false);
		}
	});

});
//---------------------------------------------------------------------
Template.zoomSlider.helpers({

	'sliderValue': function () {
		return Template.instance().sliderValue.get();
	}

});
