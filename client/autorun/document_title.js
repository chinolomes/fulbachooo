//======================================================================
// RESET DOCUMENT TITLE WHEN NEW NOTIFICATIONS ARRIVE:
//======================================================================
Tracker.autorun(function() {

   ///////////////
   // VARIABLES //
   ///////////////

   var curUserId = Meteor.userId();
   var sub = null;
   var unreadNotif = null;

   ///////////
   // LOGIC //
   ///////////

	if (curUserId) {
      sub = Meteor.subscribe('curUserUnreadNotif'); // reactive source

		if (sub.ready()) {
      	unreadNotif = UnreadNotifications.findOne({recipientId: curUserId});

      	// Set document title
			if (unreadNotif && unreadNotif.counter && unreadNotif.counter > 0) {
      		document.title = unreadNotif.counter <= NOTIF_LIMIT ? '(' + unreadNotif.counter + ') Fulbacho' : '(' + NOTIF_LIMIT + '+) Fulbacho';
      	} else {
            document.title = 'Fulbacho';
         }
      }
	}

});
