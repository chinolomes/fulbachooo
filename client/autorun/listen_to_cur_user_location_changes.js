// TODO: try to merge this logic with save_map_button.js callback
// Maybe we can replace with a subscription in google maps (?)
//======================================================================
// LISTEN TO USER LOCATION CHANGES AND RESET SESSION VARS ACCORDINGLY:
//======================================================================
Tracker.autorun(function() {

   ///////////////
   // VARIABLES //
   ///////////////

   var curUserId = Meteor.userId();
   var sub = null;
   var user = null;
   var userLocation = [];
   var lat = 0.0;
   var lng = 0.0;

   ///////////
   // LOGIC //
   ///////////

   if (curUserId) {
      sub = Meteor.subscribe('curUserGeo'); // reactive source

      // Update google map session vars in case user location changes
      if (sub.ready()) {
         user = Meteor.users.findOne({_id: curUserId}, {fields: {"geo.loc.coordinates": 1}});

         if (user && user.geo && user.geo.loc && user.geo.loc.coordinates) {
            userLocation = user.geo.loc.coordinates;
            lat = userLocation[1];
            lng = userLocation[0];
            Session.set('circleCenterLatMATCHES', lat);
            Session.set('circleCenterLngMATCHES', lng);
            Session.set('resetMapMATCHES', true);
            //console.log('autorun userLocation: ' + userLocation);
         }
      }
   }

});
