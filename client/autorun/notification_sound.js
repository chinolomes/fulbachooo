//======================================================================
// PLAY SOUND EVERY NEW NOTIFICATION:
//======================================================================
Tracker.autorun(function() {

   ///////////////
   // VARIABLES //
   ///////////////

   var curUserId = Meteor.userId();
   var sub = null;
   var unreadNotif = null;
   var counter = 0;
   var audio = null;

   ///////////
   // LOGIC //
   ///////////

   if (curUserId) {
      sub = Meteor.subscribe('curUserUnreadNotif'); // reactive source

      // Play sound in case the number of unseen notifications increases
      if (sub.ready() && !Session.equals('prevNumberOfNotif', null)) {
         unreadNotif = UnreadNotifications.findOne({recipientId: curUserId});

         if (!_.isUndefined(unreadNotif) && !_.isUndefined(unreadNotif.counter)) {
            counter = unreadNotif.counter;

            if (counter > Session.get('prevNumberOfNotif')) {
               audio = new Audio("/notif_sound.mp3"); // buffers automatically when created
               audio.play();
               console.log('play sound');
            }
            Session.set('prevNumberOfNotif', counter);
            //console.log('autorun prevNotif: '+ Session.get('prevNumberOfNotif'));
         }
      }
   }

});
